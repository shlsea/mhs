package com.skuo.mhs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.skuo.mhs.support.CustomRepositoryFactoryBean;

/**
 * @ClassName: Application
 * @Description: Spring Boot项目启动类
 * @Company: 四川西谷智慧科技有限公司
 * @author: 余德山
 * @date: 2017年9月1日 下午5:33:43
 * @version: V1.0.0
 */
@SpringBootApplication
@ServletComponentScan
@EnableJpaRepositories(repositoryFactoryBeanClass = CustomRepositoryFactoryBean.class)
public class Application {

	/**
	 * @Description:main方法入口
	 * @param args
	 * @date 2017年9月1日下午5:34:26
	 */
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}
