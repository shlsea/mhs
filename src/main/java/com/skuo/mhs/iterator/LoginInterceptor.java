package com.skuo.mhs.iterator;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.skuo.mhs.system.pojo.SysLoginLog;
import com.skuo.mhs.system.pojo.SysManager;
import com.skuo.mhs.system.service.SysLoginLogRepository;
import com.skuo.mhs.utils.Constant;
import com.skuo.mhs.utils.StringUtils;

/**
 * 
 * 
 * @ClassName: LoginInterceptor
 * @Description: 登录拦截器
 * @Company: 四川西谷智慧科技有限公司
 * @author: 余德山
 * @date: 2017年9月1日 下午1:33:21
 * @version: V1.0.0
 */
@Configuration
public class LoginInterceptor extends HandlerInterceptorAdapter {

	@Autowired
	private SysLoginLogRepository sysLoginLogRepository;

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object obj, Exception e)
			throws Exception {

	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object obj, ModelAndView model)
			throws Exception {
		SysManager manager = (SysManager) request.getSession().getAttribute(Constant.ADMINUSER);
		SysLoginLog log = (SysLoginLog) request.getSession().getAttribute("LoginLog");
		if (manager != null) {
			log.setState(1L);
			sysLoginLogRepository.save(log);
		}
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object obj) throws Exception {

		String account = request.getParameter("username");
		String IP = StringUtils.getRemoteAddr(request);
		String browser = StringUtils.getBrowserName(request);
		SysLoginLog log = new SysLoginLog();
		log.setAccount(account);
		log.setAddTime(new Date());
		log.setBrowser(browser);
		log.setState(0L);
		log.setIp(IP);
		try {
			// 解决service为null无法注入问题
			if (sysLoginLogRepository == null) {
				BeanFactory factory = WebApplicationContextUtils
						.getRequiredWebApplicationContext(request.getServletContext());
				sysLoginLogRepository = (SysLoginLogRepository) factory.getBean("sysLoginLogRepository");
			}
			log = sysLoginLogRepository.saveAndFlush(log);
			request.getSession().setAttribute("LoginLog", log);
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return true;
	}

}
