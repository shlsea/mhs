package com.skuo.mhs.iterator;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

/**
 * 
 * 
 * @ClassName: FrontInterceptor
 * @Description: 前端访问拦截器
 * @Company: 四川西谷智慧科技有限公司
 * @author: 余德山
 * @date: 2017年9月1日 下午1:33:09
 * @version: V1.0.0
 */
public class FrontInterceptor implements HandlerInterceptor {

	@Override
	public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {
		System.out.println("FrontInterceptor请求结束之后");

	}

	@Override
	public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, ModelAndView arg3)
			throws Exception {
		System.out.println(">>>FrontInterceptor>>>>>>>请求处理之后进行调用，但是在视图被渲染之前（Controller方法调用之后）");

	}

	@Override
	public boolean preHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2) throws Exception {
		// System.out.println(arg0.getRequestURI());
		String parames = "";
		Map<String, String[]> map = arg0.getParameterMap();
		Set<?> keSet = map.entrySet();
		for (Iterator<?> itr = keSet.iterator(); itr.hasNext();) {
			Map.Entry<String, Integer> me = extracted(itr);
			Object ok = me.getKey();
			Object ov = me.getValue();
			String[] value = new String[1];
			if (ov instanceof String[]) {
				value = (String[]) ov;
			} else {
				value[0] = ov.toString();
			}

			for (int k = 0; k < value.length; k++) {
				parames = parames + ok + "=" + value[k] + "&";
			}
		}
		// System.out.println("传递参数" + parames);
		return true;
	}

	@SuppressWarnings("unchecked")
	private Entry<String, Integer> extracted(Iterator<?> itr) {
		return (Map.Entry<String, Integer>) itr.next();
	}

}
