package com.skuo.mhs.iterator;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.skuo.mhs.system.pojo.SysErrorLog;
import com.skuo.mhs.system.pojo.SysManager;
import com.skuo.mhs.system.pojo.SysMenu;
import com.skuo.mhs.system.pojo.SysOpLog;
import com.skuo.mhs.system.pojo.SysOperate;
import com.skuo.mhs.system.service.SysErrorLogRepository;
import com.skuo.mhs.system.service.SysOpLogRepository;
import com.skuo.mhs.utils.Constant;
import com.skuo.mhs.utils.RequestUtils;

/**
 * 
 * 
 * @ClassName: AdminOpInterceptor
 * @Description: 后台操作拦截器
 * @Company: 四川西谷智慧科技有限公司
 * @author: 余德山
 * @date: 2017年9月1日 下午1:32:51
 * @version: V1.0.0
 */
public class AdminOpInterceptor implements HandlerInterceptor {

	@Autowired
	private SysOpLogRepository sysOpLogRepository;
	@Autowired
	private SysErrorLogRepository sysErrorLogRepository;

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {

		String uri = RequestUtils.getURI(request);

		String message = null;
		if (ex != null) {
			message = ex.getMessage();
		}
		SysOperate operate = (SysOperate) request.getSession().getAttribute(Constant.OP_IN_REQUEST);
		SysManager account = (SysManager) request.getSession().getAttribute(Constant.ADMINUSER);
		SysMenu menu = (SysMenu) request.getSession().getAttribute(Constant.OP_IN_MENU);
		if (account != null) {
			try {
				String param = RequestUtils.getQueryString(request);
				if (operate != null && ex == null) {
					// 操作日志
					if (uri.contains("opartion.action")) {
						SysOpLog log = new SysOpLog();
						log.setMenuName(operate.getMenu().getName());
						log.setOpName(operate.getName());
						log.setAddTime(new Date());
						log.setAccount(account.getAccount());
						log.setParames(param);
						log.setUrl(uri);
						if (sysOpLogRepository == null) {
							BeanFactory factory = WebApplicationContextUtils
									.getRequiredWebApplicationContext(request.getServletContext());
							sysOpLogRepository = (SysOpLogRepository) factory.getBean("sysOpLogRepository");
						}
						sysOpLogRepository.save(log);
					}
				} else if (menu != null && ex == null) {
					if (uri.contains("opartion.action")) {
						SysOpLog log = new SysOpLog();
						log.setMenuName(menu.getName());
						log.setOpName("列表");
						log.setAddTime(new Date());
						log.setAccount(account.getAccount());
						log.setParames(param);
						log.setUrl(uri);
						// 解决service为null无法注入问题
						if (sysOpLogRepository == null) {
							BeanFactory factory = WebApplicationContextUtils
									.getRequiredWebApplicationContext(request.getServletContext());
							sysOpLogRepository = (SysOpLogRepository) factory.getBean("sysOpLogRepository");
						}
						sysOpLogRepository.save(log);
					}
				} else if (ex != null) {
					SysErrorLog log = new SysErrorLog();
					log.setAccount(account.getAccount());
					log.setAddTime(new Date());
					if (operate != null) {
						log.setMenuName(operate.getMenu().getName());
						log.setOpName(operate.getName());
					} else {
						log.setMenuName(menu.getName());
						log.setOpName("列表");
					}
					log.setParames(param);
					log.setUrl(uri);
					log.setContent(message);
					// 解决service为null无法注入问题
					if (sysErrorLogRepository == null) {
						BeanFactory factory = WebApplicationContextUtils
								.getRequiredWebApplicationContext(request.getServletContext());
						sysErrorLogRepository = (SysErrorLogRepository) factory.getBean("sysErrorLogRepository");
					}
					sysErrorLogRepository.save(log);
				}
			} catch (Exception e) {
				System.out.println("此处异常");
			}
		}

	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView model)
			throws Exception {

	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object obj) throws Exception {
		SysManager manager = (SysManager) request.getSession().getAttribute(Constant.ADMINUSER);
		if (manager != null) {
			return true;
		} else {
			response.sendRedirect("/lostLogin");
			return false;
		}

	}

}
