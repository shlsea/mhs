package com.skuo.mhs.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.bind.RelaxedPropertyResolver;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class EnvironmentUtils 
{

	
	private static Environment env;
	
	public static Environment getEnv() {
		return env;
	}
	
	@Autowired
	public void setEnv(Environment env) {
		EnvironmentUtils.env = env;
	}

	private static RelaxedPropertyResolver relaxedPropertyResolver;
	
	public static String getConfigValue(String key){
		if (StringUtils.isNotEmpty(key)) {
			initRelaxedPropertyResolver();
			return relaxedPropertyResolver.getProperty(key);
		}
		return null;
	}
	public static String getConfigValue(String key,String defaultValue){
		if (StringUtils.isNotEmpty(key)) {
			initRelaxedPropertyResolver();
			return relaxedPropertyResolver.getProperty(key,defaultValue);
		}
		return defaultValue;
	}
	public static <T> T getConfigValue(String key,Class<T> targetType){
		if (StringUtils.isNotEmpty(key)) {
			initRelaxedPropertyResolver();
			return relaxedPropertyResolver.getProperty(key, targetType);
		}
		return null;
	}
	public static <T> T getConfigValue(String key,Class<T> targetType,T defaultValue){
		if (StringUtils.isNotEmpty(key)) {
			initRelaxedPropertyResolver();
			return relaxedPropertyResolver.getProperty(key, targetType,defaultValue);
		}
		return defaultValue;
	}
	public static void initRelaxedPropertyResolver() {
		if (relaxedPropertyResolver == null) {
			relaxedPropertyResolver= new RelaxedPropertyResolver(env);			
		}
		
	}
}
