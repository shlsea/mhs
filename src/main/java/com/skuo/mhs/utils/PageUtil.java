package com.skuo.mhs.utils;

import java.util.HashMap;
import java.util.Map;

import org.apache.poi.ss.formula.functions.T;
import org.springframework.data.domain.Page;

public class PageUtil {
	
	public static Map<String,Object> toBootStrapList(Page<T> page){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("rows", page.getContent());
		//System.out.println(page.getNumber()+":"+page.getNumber()+":"+page.getTotalElements()+":"+page.getTotalPages());
		map.put("total", page.getTotalElements());
		return map;
		
	}

}
