package com.skuo.mhs.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;


@SuppressWarnings("unused")
public class UploadUtil {
	

	private UploadUtil() {

	}

	
	private static SimpleDateFormat fullSdf = new SimpleDateFormat("yyyyMMddHHmmss");
	private static SimpleDateFormat folder = new SimpleDateFormat("yyyy" + "/" + "MM" + "/" + "dd");

	/**
	 * 返回yyyy File.separator MM File.separator dd格式的字符串
	 * 
	 * @return
	 */
	public static String getFolder() {
		return folder.format(new Date());
	}

	/**
	 * 文件上传
	 * 
	 * @param srcName
	 *            原文件名
	 * @param dirName
	 *            目录名
	 * @param input
	 *            要保存的输入流
	 * @return 返回要保存到数据库中的路径
	 */
	public static String writeFile(String srcName, String dirName, InputStream input){
		// 取出上传的目录，此目录是tomcat的server.xml中配置的虚拟目录
		
		String dir = UploadUtil.getFolder();
		String suffix=srcName.substring(srcName.lastIndexOf(".")+1).toLowerCase();
		String fileType=FileMap.getType(suffix);
		if(fileType==null ||fileType.equals("")){
			fileType="file";
		}
		
		srcName = srcName.substring(srcName.indexOf("."));
		String path="/upload/"+fileType+"/"+dir+ "/" + new Date().getTime() + srcName;
		String filename = dirName+path;
		// 得到将要保存到数据中的路径
		
		File file = new File(filename);
		if (!file.getParentFile().exists()) {
			file.getParentFile().mkdirs();
		}
		FileOutputStream fos;
		try {
			fos = new FileOutputStream(file);
			byte[] readBuff = new byte[1024 * 30];
			int count = -1;
			while ((count = input.read(readBuff, 0, readBuff.length)) != -1) {
				fos.write(readBuff, 0, count);
			}
			fos.flush();
			fos.close();
			input.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// 一次30kb
		
		
		return path;
	}
}
