package com.skuo.mhs.utils;

import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("serial")
public class FileMap {
	public static final Map<String, String> mapping;
	// 获取配置请求
	public static final String PICTRRE = "image";
	public static final String VIDEO = "video";
	public static final String ZIP = "zip";
	public static final String FILE = "file";
	public static final String AUDIO = "audio";

	static {
		mapping = new HashMap<String, String>() {
			{
				// 图片
				put("jpg", FileMap.PICTRRE);
				put("png", FileMap.PICTRRE);
				put("gif", FileMap.PICTRRE);
				put("tif", FileMap.PICTRRE);
				put("jpeg", FileMap.PICTRRE);
				put("tiff", FileMap.PICTRRE);
				put("raw", FileMap.PICTRRE);
				put("bmp", FileMap.PICTRRE);
				// 压缩文件
				put("zip", FileMap.ZIP);
				put("rar", FileMap.ZIP);
				// 文件格式
				put("doc", FileMap.FILE);
				put("docx", FileMap.FILE);
				put("txt", FileMap.FILE);
				put("pdf", FileMap.FILE);
				put("wps", FileMap.FILE);
				put("docm", FileMap.FILE);
				put("dotx", FileMap.FILE);
				put("xlsx", FileMap.FILE);
				put("xlsm", FileMap.FILE);
				put("xltx", FileMap.FILE);
				put("xltm", FileMap.FILE);
				put("xlsb", FileMap.FILE);
				put("ppt", FileMap.FILE);
				put("pptm", FileMap.FILE);
				put("ppsx", FileMap.FILE);
				put("potx", FileMap.FILE);
				put("potm", FileMap.FILE);
				put("ppam", FileMap.FILE);
				put("xls", FileMap.FILE);

				// 视频
				put("mp4", FileMap.VIDEO);
				put("rmvb", FileMap.VIDEO);
				put("mkv", FileMap.VIDEO);
				put("avi", FileMap.VIDEO);
				put("3gp", FileMap.VIDEO);
				put("qlv", FileMap.VIDEO);
				put("flv", FileMap.VIDEO);

				// 音频
				put("mp3", FileMap.AUDIO);
				put("wav", FileMap.AUDIO);
				put("wam", FileMap.AUDIO);
				put("asf", FileMap.AUDIO);

			}
		};
	}
	
	/***
	 * 通过key值获取文档类型
	 * @param key
	 * @return
	 */
	public static String getType ( String key ) {
		return FileMap.mapping.get( key );
	}
}
