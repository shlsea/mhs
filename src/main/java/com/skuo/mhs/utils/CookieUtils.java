package com.skuo.mhs.utils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

/**
 * 
 * @ClassName: CookieUtils
 * @Description: Cookie管理工具
 * @Company: 四川西谷智慧科技有限公司
 * @author: 余德山
 * @date: 2017年9月1日 下午5:23:54
 * @version: V1.0.0
 */
public class CookieUtils {

	/**
	 * 
	 *
	 * @Description: 通过键值获取cookie
	 * @param request
	 * @param name
	 * @return
	 * @date 2017年9月1日下午5:24:51
	 *
	 */
	public static Cookie getCookie(HttpServletRequest request, String name) {

		Cookie[] cookies = request.getCookies();
		if ((cookies != null) && (cookies.length > 0)) {
			for (Cookie c : cookies) {
				if (c.getName().equals(name)) {
					return c;
				}
			}
		}
		return null;
	}

	/**
	 * 
	 *
	 * @Description:添加cookie
	 * @param request
	 * @param response
	 * @param name
	 *            键值
	 * @param value
	 *            存放值
	 * @param expiry
	 *            时效
	 * @return
	 * @date 2017年9月1日下午5:25:30
	 *
	 */
	public static Cookie addCookie(HttpServletRequest request, HttpServletResponse response, String name, String value,
			Integer expiry) {
		Cookie cookie = new Cookie(name, value);
		if (expiry != null) {
			cookie.setMaxAge(expiry.intValue());
		}
		String ctx = request.getContextPath();
		cookie.setPath(StringUtils.isBlank(ctx) ? "/" : ctx);
		response.addCookie(cookie);
		return cookie;
	}

	/**
	 * 
	 *
	 * @Description:删除cookie
	 * @param request
	 * @param response
	 * @param name
	 *            键值
	 * @date 2017年9月1日下午5:26:23
	 *
	 */
	public static void cancleCookie(HttpServletRequest request, HttpServletResponse response, String name) {
		Cookie cookie = new Cookie(name, null);
		cookie.setMaxAge(0);
		String ctx = request.getContextPath();
		cookie.setPath(StringUtils.isBlank(ctx) ? "/" : ctx);
		response.addCookie(cookie);
	}
}