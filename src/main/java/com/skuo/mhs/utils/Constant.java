package com.skuo.mhs.utils;

/**
 * 
* 
* @ClassName: Constant 
* @Description: 常量管理工具
* @Company: 四川西谷智慧科技有限公司
* @author: 余德山 
* @date: 2017年9月1日 下午3:30:19 
* @version: V1.0.0
 */
public class Constant {

	/***
	 * 验证码
	 */
	public static final String CAPTCHA = "catchCode";
	/***
	 * 后台登录账户
	 */
	public static final String ADMINUSER = "ADMINUSER";
	/***
	 * 操作
	 */
	public static final String OP_IN_REQUEST = "OPERATE";
	/***
	 * 操作菜单
	 */
	public static final String OP_IN_MENU = "MENU";
	/***
	 * 上次访问列表地址
	 */
	public static final String LAST_URL = "LAST_URL";
	/***
	 * 账号所属地区
	 */
	public static final String REGION = "REGION";
	/***
	 * 当前站点
	 */
	public static final String SITE = "SITE";

	/**
	 * 上次访问操作路径
	 */
	public static final String LAST_OP_URL = "LAST_OP_URL";

	/**
	 * 成功状态
	 */
	public static final String STATUS_SCUESS = "1";
	
	/**
	 * 失败状态
	 */
	public static final String STATUS_ERROR = "0";
	
	public static final String CHANNEL="0000000000000000";
}
