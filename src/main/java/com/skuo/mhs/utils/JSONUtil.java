package com.skuo.mhs.utils;

import java.io.IOException;
import java.lang.reflect.Field;

import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * 实体类和JSON对象之间相互转化（依赖包jackson-all-1.7.6.jar、jsoup-1.5.2.jar）
 * 
 * @author wck
 *
 */
public class JSONUtil {
	/**
	 * 将json转化为实体POJO
	 * 
	 * @param jsonStr
	 * @param obj
	 * @return
	 */
	public static <T> Object JSONToObj(String jsonStr, Class<T> obj) {
		T t = null;
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			t = objectMapper.readValue(jsonStr, obj);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return t;
	}

	/**
	 * 将实体POJO转化为JSON
	 * 
	 * @param obj
	 * @return
	 * @throws JSONException
	 * @throws IOException
	 */
	public static <T> JSONObject objectToJson(T obj) throws JSONException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		// Convert object to JSON string
		String jsonStr = "";
		try {
			jsonStr = mapper.writeValueAsString(obj);
		} catch (IOException e) {
			throw e;
		}
		return new JSONObject(jsonStr);
	}

	public static String ObjToJson(Object obj) throws Exception {
		Field[] f = obj.getClass().getDeclaredFields();
		StringBuffer jsons = new StringBuffer("{\"json\":\"\"");
		for (Field field : f) {
			field.setAccessible(true);
			if (null != field.get(obj)) {
				String ss = field.get(obj).toString();
				jsons.append(",\"" + field.getName() + "\":\"" + ss + "\"");
			} else {
				jsons.append(",\"" + field.getName() + "\":\"\"");
			}
		}
		jsons.append("}");
		return jsons.toString();
	}
	
	/**
	 * 将实体POJO转化为JSON 排除不需要转换的
	 * @param obj
	 * @param params
	 * @return
	 * @throws Exception
	 */
	public static String ObjToJsonExclude(Object obj, String[] params) throws Exception {
		Field[] f = obj.getClass().getDeclaredFields();
		StringBuffer jsons = new StringBuffer("{\"json\":\"\"");
		for (Field field : f) {
			field.setAccessible(true);
			if (!exclude(field.getName(), params)) {
				continue;
			}
			if (null != field.get(obj)) {
				String ss = field.get(obj).toString();
				jsons.append(",\"" + field.getName() + "\":\"" + ss + "\"");
			} else {
				jsons.append(",\"" + field.getName() + "\":\"\"");
			}
		}
		jsons.append("}");
		return jsons.toString();
	}
	
	public static boolean exclude(String name, String[] params) {
		boolean flag = true;
		if (params != null && params.length > 0) {
			for (String p : params) {
				if (p.equals(name)) {
					flag = false;
				}
			}
		}
		return flag;
	}

}