package com.skuo.mhs.utils;


import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.StringTokenizer;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class HttpUtils
{
public static Hashtable parseQueryString(String s)
  {
    String[] valArray = (String[])null;
    if (s == null) {
      throw new IllegalArgumentException("queryString must not null");
    }
    Hashtable ht = new Hashtable();
    StringBuffer sb = new StringBuffer();
    StringTokenizer st = new StringTokenizer(s, "&");
    while (st.hasMoreTokens())
    {
      String pair = st.nextToken();
      if (pair.trim().length() != 0)
      {
        int pos = pair.indexOf('=');
        if (pos == -1) {
          throw new IllegalArgumentException("cannot parse queryString:" + 
            s);
        }
        String key = parseName(pair.substring(0, pos), sb);
        String val = parseName(pair.substring(pos + 1, pair.length()), sb);
        if (ht.containsKey(key))
        {
          String[] oldVals = (String[])ht.get(key);
          valArray = new String[oldVals.length + 1];
          for (int i = 0; i < oldVals.length; i++) {
            valArray[i] = oldVals[i];
          }
          valArray[oldVals.length] = val;
        }
        else
        {
          valArray = new String[1];
          valArray[0] = val;
        }
        ht.put(key, valArray);
      }
    }
    return fixValueArray2SingleStringObject(ht);
  }
  

private static Hashtable fixValueArray2SingleStringObject(Hashtable ht)
  {
    Hashtable result = new Hashtable();
    for (Iterator it = ht.entrySet().iterator(); it.hasNext();)
    {
      Map.Entry entry = (Map.Entry)it.next();
      String[] valueArray = (String[])entry.getValue();
      if (valueArray == null) {
        result.put(entry.getKey(), valueArray);
      } else {
        result.put(entry.getKey(), 
          valueArray.length == 1 ? valueArray[0] : valueArray);
      }
    }
    return result;
  }
  
  private static String parseName(String s, StringBuffer sb)
  {
    sb.setLength(0);
    for (int i = 0; i < s.length(); i++)
    {
      char c = s.charAt(i);
      switch (c)
      {
      case '+': 
        sb.append(' ');
        break;
      case '%': 
        try
        {
          sb.append((char)Integer.parseInt(
            s.substring(i + 1, i + 3), 16));
          i += 2;
        }
        catch (NumberFormatException e)
        {
          throw new IllegalArgumentException();
        }
        catch (StringIndexOutOfBoundsException e)
        {
          String rest = s.substring(i);
          sb.append(rest);
          if (rest.length() != 2) {
            continue;
          }
        }
        i++;
        

        break;
      default: 
        sb.append(c);
      }
    }
    return sb.toString();
  }
  
  public static byte[] sendRequestByForm(String url, String params, String method, String contentType)
    throws Exception
  {
    URL url1 = new URL(url);
    HttpURLConnection conn = (HttpURLConnection)url1.openConnection();
    conn.setRequestMethod(method);
    

    conn.setDoOutput(true);
    conn.setRequestProperty("Content-type", contentType);
    byte[] bypes = params.toString().getBytes("UTF-8");
    conn.getOutputStream().write(bypes);
    InputStream inStream = conn.getInputStream();
    ByteArrayOutputStream outStream = new ByteArrayOutputStream();
    byte[] buffer = new byte[1024];
    int len = 0;
    while ((len = inStream.read(buffer)) != -1) {
      outStream.write(buffer, 0, len);
    }
    byte[] data = outStream.toByteArray();
    outStream.close();
    inStream.close();
    return data;
  }
  
  public static byte[] sendRequestByForm(String url, InputStream params, String method, String contentType)
    throws Exception
  {
    URL url1 = new URL(url);
    HttpURLConnection conn = (HttpURLConnection)url1.openConnection();
    conn.setRequestMethod(method);
    

    conn.setDoOutput(true);
    conn.setRequestProperty("Content-type", contentType);
    byte[] buffer = new byte[1024];
    int len = 0;
    OutputStream out = conn.getOutputStream();
    while ((len = params.read(buffer)) != -1) {
      out.write(buffer, 0, len);
    }
    out.close();
    InputStream inStream = conn.getInputStream();
    ByteArrayOutputStream outStream = new ByteArrayOutputStream();
    while ((len = inStream.read(buffer)) != -1) {
      outStream.write(buffer, 0, len);
    }
    byte[] data = outStream.toByteArray();
    outStream.close();
    inStream.close();
    return data;
  }
}
