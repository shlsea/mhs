package com.skuo.mhs.utils;

import java.io.BufferedReader;  
import java.io.InputStreamReader;  
import java.util.List;  
  
  
/**
 *  
* 
* @ClassName: CmdExecuter 
* @Description: 命令执行器,封装对操作系统命令行发送指令相关操作
* @Company: 四川西谷智慧科技有限公司
* @author: 余德山 
* @date: 2017年9月1日 下午3:27:12 
* @version: V1.0.0
 */
public class CmdExecuter {  
      
    /** 
     * 执行指令 
     * @param cmd 执行指令 
     * @param getter 指令返回处理接口，若为null则不处理输出 
     */  
    static public void exec( List<String> cmd, IStringGetter getter ){  
        try {  
            ProcessBuilder builder = new ProcessBuilder();    
            builder.command(cmd);  
            builder.redirectErrorStream(true);  
            Process proc = builder.start();  
            BufferedReader stdout = new BufferedReader(  
                    new InputStreamReader(proc.getInputStream()));  
            String line;  
            while ((line = stdout.readLine()) != null) {  
                if( getter != null )  
                    getter.dealString(line);  
            }  
            proc.waitFor();     
            stdout.close();  
        } catch (Exception e) {  
            e.printStackTrace();  
        }  
    }  
} 