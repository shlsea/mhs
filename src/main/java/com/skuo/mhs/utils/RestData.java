package com.skuo.mhs.utils;

/**
 * Rest返回格式处理
* @项目名称 hntj 
* @作者 余德山
* @包名称 com.daqsoft.hntj.utils
* @功能描述 TODO
* @日期 2017年3月16日
* @版本 V1.0.0
 */
public class RestData<T> {

	public RestData() {
	}

	public RestData(T data,String uri) {
		this.data = data;
		this.isSuccess = true;
		this.uri=uri;
	}
	
	public RestData(T data,long elapsedMilliseconds,String uri) {
		this.data = data;
		this.isSuccess = true;
		this.elapsedMilliseconds=elapsedMilliseconds;
		this.uri=uri;
	}

	public RestData(String errorCode, String errorDesc,String uri) {
		this.errorCode = errorCode;
		this.errorDesc = errorDesc;
		this.isSuccess = false;
		this.uri=uri;
	}

	public RestData(String errorCode, String errorDesc, long elapsedMilliseconds,String uri) {
		this.errorCode = errorCode;
		this.errorDesc = errorDesc;
		this.isSuccess = false;
		this.elapsedMilliseconds = elapsedMilliseconds;
		this.uri=uri;
	}

	public RestData(T data,String errorCode, String errorDesc, long elapsedMilliseconds,String uri) {
		this.errorCode = errorCode;
		this.errorDesc = errorDesc;
		this.isSuccess = true;
		this.elapsedMilliseconds = elapsedMilliseconds;
		this.uri=uri;
		this.data=data;
	}
	/**
	 * 是否处理成功
	 */
	private boolean isSuccess;

	/**
	 * 返回的数据
	 */
	private T data;

	/**
	 * 错误代码
	 */
	private String errorCode;

	/**
	 * 错误描述
	 */
	private String errorDesc;
	
	
	/**
	 * 服务器访问地址
	 */
	private String uri;

	/**
	 * 处理耗时(毫秒)
	 */
	private long elapsedMilliseconds;

	public boolean isSuccess() {
		return isSuccess;
	}

	public void setIsSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorDesc() {
		return errorDesc;
	}

	public void setErrorDesc(String errorDesc) {
		this.errorDesc = errorDesc;
	}

	public long getElapsedMilliseconds() {
		return elapsedMilliseconds;
	}

	public void setElapsedMilliseconds(long elapsedMilliseconds) {
		this.elapsedMilliseconds = elapsedMilliseconds;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	
}
