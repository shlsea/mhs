package com.skuo.mhs.utils;


import java.util.ArrayList;
import java.util.List;


/**
 * 
* 
* @ClassName: FFmpegUtil 
* @Description: FFmpeg多媒体解析工具
* @Company: 四川西谷智慧科技有限公司
* @author: 余德山 
* @date: 2017年9月1日 下午5:29:39 
* @version: V1.0.0
 */
public class FFmpegUtil implements IStringGetter {
	
	private long runtime = 0;
	private String ffmpegUri;// ffmpeg地址
	private String originFileUri; // 视频源文件地址
	private boolean isSupported;

	private enum FFMpegUtilStatus {
		Empty, CheckingFile, GettingRuntime
	};

	private FFMpegUtilStatus status = FFMpegUtilStatus.Empty;

	/**
	 * 构造函数
	 * 
	 * @param ffmpegUri
	 *            ffmpeg的全路径 如e:/ffmpeg/ffmpeg.exe 或 /usr/local/bin/ffmpeg
	 * @param originFileUri
	 *            所操作视频文件的全路径 如e:/upload/temp/test.wmv
	 */
	public FFmpegUtil(String ffmpegUri, String originFileUri) {
		this.ffmpegUri = ffmpegUri;
		this.originFileUri = originFileUri;
	}

	/**
	 * 获取视频时长
	 * 
	 * @return
	 */
	public long getRuntime() {
		runtime = 0;
		status = FFMpegUtilStatus.GettingRuntime;
		cmd.clear();
		cmd.add(ffmpegUri);
		cmd.add("-i");
		cmd.add(originFileUri);
		CmdExecuter.exec(cmd, this);
		return runtime;
	}
	
	public void getChange(String path) {
		status = FFMpegUtilStatus.GettingRuntime;
		cmd.clear();
		cmd.add(ffmpegUri);
		cmd.add("-i");
		cmd.add(originFileUri);
		cmd.add(path);
		CmdExecuter.exec(cmd, this);
		
	}
	
	public String StringTime(long time){
		return formatVideoTimeLength(time);
	}

	/**
	 * 检测文件是否是支持的格式 将检测视频文件本身，而不是扩展名
	 * 
	 * @return
	 */
	public boolean isSupported() {
		isSupported = false;
		status = FFMpegUtilStatus.CheckingFile;
		cmd.clear();
		cmd.add(ffmpegUri);
		cmd.add("-i");
		cmd.add(originFileUri);
		cmd.add("Duration: (.*?), start: (.*?), bitrate: (\\d*) kb\\/s");
		CmdExecuter.exec(cmd, this);
		return isSupported;
	}

	

	/**
	 * 生成视频截图
	 * 
	 * @param imageSavePath
	 *            截图文件保存全路径
	 * @param screenSize
	 *            截图大小 如640x480
	 */
	public void makeScreenCut(String imageSavePath, String screenSize) {
		cmd.clear();
		cmd.add(ffmpegUri);
		cmd.add("-i");
		cmd.add(originFileUri);
		cmd.add("-y");
		cmd.add("-f");
		cmd.add("image2");
		cmd.add("-ss");
		cmd.add("2");
		cmd.add("-t");
		cmd.add("0.001");
		cmd.add("-s");
		cmd.add(screenSize);
		cmd.add(imageSavePath);
		CmdExecuter.exec(cmd, this);
	}

	private List<String> cmd = new ArrayList<String>();


	public void dealString(String str) {

		switch (status) {
		case Empty:
			break;
		case CheckingFile: {
			if (-1 != str.indexOf("Metadata:"))
				this.isSupported = true;
			break;
		}
		case GettingRuntime: {
			String strs="";
			
			if(str.contains("Duration")){
				strs=str.substring(str.indexOf(":")+1,str.indexOf(","));
				if(strs!=null){
					runtime = Integer.parseInt(strs.substring(1,3))*60*60+Integer.parseInt(strs.substring(4,6))*60+Integer.parseInt(strs.substring(7,9));
				}
			}

			break;
		}
		}
	}

//	/**
//	 * 获取视频总时间
//	 * @param viedo_path
//	 *            视频路径
//	 * @param ffmpeg_path
//	 *            ffmpeg路径
//	 * @return 文件时长秒数
//	 */
//	public static String getVideoTime(String video_path, String ffmpeg_path) {
//		List<String> commands = new ArrayList<String>();
//		commands.add(ffmpeg_path);
//		commands.add("-i");
//		commands.add(video_path);
//		try {
//			ProcessBuilder builder = new ProcessBuilder();
//			
//			builder.command(commands);
//			
//			builder.redirectErrorStream(true); 
//			System.out.println(builder.toString());
//			final Process p = builder.start();
//
//			// 从输入流中读取视频信息
//			BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
//			StringBuffer sb = new StringBuffer();
//			String line = "";
//			while ((line = br.readLine()) != null) {
//				sb.append(line);
//			}
//			br.close();
//			// 从视频信息中解析时长
//			String regexDuration = "Duration: (.*?), start: (.*?), bitrate: (\\d*) kb\\/s";
//			Pattern pattern = Pattern.compile(regexDuration);
//			Matcher m = pattern.matcher(sb.toString());
//			if (m.find()) {
//				int time = getTimelen(m.group(1));
//				return formatVideoTimeLength(time);
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//		return null;
//	}

	// 格式:"00:00:10.68"
	@SuppressWarnings("unused")
	private static int getTimelen(String timelen) {
		int min = 0;
		String strs[] = timelen.split(":");
		if (strs[0].compareTo("0") > 0) {
			min += Integer.valueOf(strs[0]) * 60 * 60;// 秒
		}
		if (strs[1].compareTo("0") > 0) {
			min += Integer.valueOf(strs[1]) * 60;
		}
		if (strs[2].compareTo("0") > 0) {
			min += Math.round(Float.valueOf(strs[2]));
		}
		return min;
	}

	/***
	 * 转换时间格式字符串类型
	 * @param time 时长秒数
	 * @return 字符串时长
	 */
	public String formatVideoTimeLength(long time) {
		long s = time % 60;
		long m = (time / 60) % 60;
		long h = time / 3600;
		if (h > 0) {
			return h + "小时" + m + "分" + s + "秒";
		} else if (h < 1 && m > 0) {
			return m + "分" + s + "秒";
		} else {
			return s + "秒";
		}

	}

	
}
