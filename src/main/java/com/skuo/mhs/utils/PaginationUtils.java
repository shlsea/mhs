package com.skuo.mhs.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @ClassName: PaginationUtils 
 * @Description: 分页工具类
 * @Company: 四川西谷智慧科技有限公司
 * @author: songhailing 
 * @date: 2017年9月21日 下午7:55:20 
 * @version: V1.0.0
 */
public class PaginationUtils {

	/**
	 * 
	 * @Description:分页的方法
	 * @param originalList
	 * @param offset
	 * @param limit
	 * @return 
	 * @date 2017年9月21日下午7:55:29
	 */
	@SuppressWarnings("rawtypes")
	public static List<?> pagination(List<?> originalList, int offset, int limit) {
		// 如果不为空进行处理
		if (originalList != null && !originalList.isEmpty()) {
			// 获得总数据量
			int total = originalList.size();
//			如果分页参数大于当前数据总量，那么直接返回空的集合
			if(offset>total){
				originalList=new ArrayList();
			}else{
				// 获得总分页
				int totalPage = total % limit == 0 ? total / limit : (total / limit + 1);
				// 获得当前页
				int currentPage = offset / limit + 1;
				// 如果当前是最后一页，或者当前的位移加上偏移量的值大于总数据量，那么都采用总数据量结尾
				if (currentPage == totalPage||offset + limit>total) {
					originalList = originalList.subList(offset, total);
					// 如果不相等，取限定的数值(限定长度的值)
				} else {
					originalList = originalList.subList(offset, offset + limit);
				}
			}
		}
		return originalList;
	}
}
