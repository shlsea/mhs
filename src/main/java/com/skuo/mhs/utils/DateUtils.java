package com.skuo.mhs.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 
 * 
 * @ClassName: DateUtils
 * @Description: 时间格式化工具
 * @Company: 四川西谷智慧科技有限公司
 * @author: 余德山
 * @date: 2017年9月1日 下午5:27:08
 * @version: V1.0.0
 */
public class DateUtils {

	/**
	 * 
	 *
	 * @Description:根据传入要转类型转换传入时间为字符串
	 * @param date
	 *            时间
	 * @param format
	 *            格式化规范
	 * @return
	 * @date 2017年9月1日下午5:27:25
	 *
	 */
	public static String format(Date date, String format) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(format);
		if (date == null) {
			return "";
		}
		return dateFormat.format(date);
	}
}
