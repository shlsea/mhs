package com.skuo.mhs.utils;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

/**
 * 
 * 
 * @ClassName: Configure
 * @Description: 配置文件读取工具类
 * @Company: 四川西谷智慧科技有限公司
 * @author: 余德山
 * @date: 2017年9月1日 下午3:27:37
 * @version: V1.0.0
 */
public class Configure {
	private static Logger logger = Logger.getLogger(Configure.class);
	private static Properties config = null;

	public Configure() {
		config = new Properties();
	}

	/**
	 * 
	 *
	 * 构造带文件路径的配置
	 * 
	 * @param filePath
	 *            配置文件路径
	 * @date 2017年9月1日下午3:28:07
	 *
	 */
	public Configure(String filePath) {
		config = new Properties();
		try {
			ClassLoader CL = this.getClass().getClassLoader();
			InputStream in;
			if (CL != null) {
				in = CL.getResourceAsStream(filePath);
			} else {
				in = ClassLoader.getSystemResourceAsStream(filePath);
			}
			config.load(in);
			in.close();
		} catch (FileNotFoundException e) {
			logger.error("服务器配置文件没有找到");
		} catch (Exception e) {
			logger.error("服务器配置信息读取错误");
		}
	}

	/**
	 * 
	 *
	 * @Description:获取配置文件内属性值
	 * @param key
	 *            配置文件内容key值
	 * @return
	 * @date 2017年9月1日下午3:28:49
	 *
	 */
	public String getValue(String key) {
		if (config.containsKey(key)) {
			String value = config.getProperty(key);
			return value;
		} else {
			return "";
		}
	}

	/**
	 * 
	 *
	 * @Description:获取配置属性值，并转Int类型
	 * @param key
	 *            配置文件属性key值
	 * @return
	 * @date 2017年9月1日下午3:29:22
	 *
	 */
	public int getValueInt(String key) {
		String value = getValue(key);
		int valueInt = 0;
		try {
			valueInt = Integer.parseInt(value);
		} catch (NumberFormatException e) {
			e.printStackTrace();
			return valueInt;
		}
		return valueInt;
	}
}
