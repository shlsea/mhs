package com.skuo.mhs.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * String类型树形结构
* @项目名称 行业管理公共服务 
* @作者 余德山
* @包名称 com.daqsoft.hzCloudHYGL.custom.utils.utils
* @功能描述 TODO
* @日期 2017年1月19日
* @版本 V1.0.0
 */
public class TreeNode {
	
	private String id;
	
	private String name;
	
	private String pid;
	
	private List<TreeNode> nodes=new ArrayList<TreeNode>();

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public List<TreeNode> getNodes() {
		return nodes;
	}

	public void setNodes(List<TreeNode> nodes) {
		this.nodes = nodes;
	}
	
	
}
