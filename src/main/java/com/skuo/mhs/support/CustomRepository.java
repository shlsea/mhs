package com.skuo.mhs.support;

import java.io.Serializable;
import java.util.Map;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;

import com.skuo.mhs.form.PageForm;

/**
 * 自定义接口 继承JpaRepository,让我们具备了JpaRepository所提供的方法;<br>
 * 继承JpaSpecificationExecutor,让我们具备了Specification所提供分页能力。
 * 
 * @ClassName: CustomRepository
 * @Description: 自定义接口 继承JpaRepository,让我们具备了JpaRepository所提供的方法;继承JpaSpecificationExecutor,让我们具备了Specification所提供分页能力。
 * @Company: 四川西谷智慧科技有限公司
 * @author: 余德山
 * @date: 2017年9月1日 下午1:36:34
 * @version: V1.0.0
 */
@NoRepositoryBean
public interface CustomRepository<T, ID extends Serializable>
		extends JpaRepository<T, ID>, JpaSpecificationExecutor<T> {

	/**
	 * 查询分页
	 * 
	 * @param example
	 *            实体
	 * @param pageForm
	 *            分页参数
	 * @return
	 */
	Map<String, Object> findByAuto(T example, PageForm pageForm);

	/**
	 * 多排序查询
	 * 
	 * @param example
	 * @param pageForm
	 * @return
	 */
	Map<String, Object> findByOrder(T example, PageForm pageForm, Sort sort);

}
