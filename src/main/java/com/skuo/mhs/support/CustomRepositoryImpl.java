package com.skuo.mhs.support;

import static com.skuo.mhs.specs.CustomSpecs.byAuto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;

import com.skuo.mhs.form.PageForm;

/**
 * 公共读取列表实现
 * 
 * @ClassName: CustomRepositoryImpl
 * @Description: 公共读取列表实现
 * @Company: 四川西谷智慧科技有限公司
 * @author: 余德山
 * @date: 2017年9月1日 下午1:37:37
 * @version: V1.0.0
 */
public class CustomRepositoryImpl<T, ID extends Serializable> extends SimpleJpaRepository<T, ID>
		implements CustomRepository<T, ID> {

	private final EntityManager entityManager;

	/**
	 * 
	 *
	 * 实例化CustomRepositoryImpl
	 * 
	 * @param domainClass
	 *            实体类
	 * @param entityManager
	 *            实体管理器
	 * @date 2017年8月31日上午11:06:21
	 *
	 */
	public CustomRepositoryImpl(Class<T> domainClass, EntityManager entityManager) {
		super(domainClass, entityManager);
		this.entityManager = entityManager;
	}

	@Override
	public Map<String, Object> findByAuto(T example, PageForm pageForm) {

		// 排序设置
		Sort sort = null;
		if (pageForm.getSort() == null) {
			sort = new Sort(Direction.ASC, "id");

		} else {
			if (pageForm.getOrder().toUpperCase().equals("ASC")) {
				sort = new Sort(Direction.ASC, pageForm.getSort());
			} else {
				sort = new Sort(Direction.DESC, pageForm.getSort());
			}
		}

		// 计算从第几页开始读取
		int pageNo = pageForm.getOffset() / pageForm.getLimit();

		Pageable pageable = new PageRequest(pageNo, pageForm.getLimit(), sort);
		Page<T> page = findAll(byAuto(entityManager, example), pageable);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("rows", page.getContent());
		map.put("total", page.getTotalElements());
		return map;
	}

	@Override
	public Map<String, Object> findByOrder(T example, PageForm pageForm, Sort sort) {

		int pageNo = pageForm.getOffset() / pageForm.getLimit();

		Pageable pageable = new PageRequest(pageNo, pageForm.getLimit(), sort);
		Page<T> page = findAll(byAuto(entityManager, example), pageable);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("rows", page.getContent());
		map.put("total", page.getTotalElements());
		return map;
	}
}
