package com.skuo.mhs.form;

/**
 * 
 * 
 * @ClassName: LoginForm
 * @Description: 登录输入表单
 * @Company: 四川西谷智慧科技有限公司
 * @author: 余德山
 * @date: 2017年9月1日 下午1:32:06
 * @version: V1.0.0
 */
public class LoginForm {

	/**
	 * 登录账号
	 */
	public String username;

	/**
	 * 登录密码
	 */
	public String password;

	/**
	 * 登录验证码
	 */
	public String captcha;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCaptcha() {
		return captcha;
	}

	public void setCaptcha(String captcha) {
		this.captcha = captcha;
	}

}
