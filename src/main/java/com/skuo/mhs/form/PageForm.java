package com.skuo.mhs.form;

import java.io.Serializable;

/**
 * 
 * 
 * @ClassName: PageForm
 * @Description: 分页工具表单
 * @Company: 四川西谷智慧科技有限公司
 * @author: 余德山
 * @date: 2017年9月1日 下午1:32:29
 * @version: V1.0.0
 */
public class PageForm implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 每页显示条数
	 */
	private int limit;

	/**
	 * 开始页数
	 */
	private int offset;

	/**
	 * 排序方式
	 */
	private String order;

	/**
	 * 排序字段
	 */
	private String sort;

	/**
	 * 跨域
	 */
	private String jsoncallback;

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String getJsoncallback() {
		return jsoncallback;
	}

	public void setJsoncallback(String jsoncallback) {
		this.jsoncallback = jsoncallback;
	}

}
