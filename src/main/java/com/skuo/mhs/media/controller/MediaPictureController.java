package com.skuo.mhs.media.controller;

import net.sf.json.JSONObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.skuo.mhs.form.PageForm;
import com.skuo.mhs.media.pojo.MediaPicture;
import com.skuo.mhs.media.service.MediaPictureRepository;
import com.skuo.mhs.system.pojo.SysDict;
import com.skuo.mhs.system.pojo.SysManager;
import com.skuo.mhs.system.pojo.SysRegion;
import com.skuo.mhs.system.service.SysDictRepository;
import com.skuo.mhs.system.service.SysRegionRepository;
import com.skuo.mhs.utils.Constant;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName: MediaPictureController
 * @Description:系统图片逻辑控制器
 * @Company: 四川西谷智慧科技有限公司
 * @author: 钟操
 * @date: 2017/9/2 17:25:16
 * @version: V1.0.0
 */
@Controller
@RequestMapping(value = "/media-pic")
@SuppressWarnings("unchecked")
public class MediaPictureController {

	@Autowired
	private MediaPictureRepository mediaPictureRepository;
	@Autowired
	private SysRegionRepository sysRegionRepository;
	@Autowired
	private SysDictRepository sysDictRepository;

	/**
	 * @Description:跳转图片列表页面
	 * @return
	 * @date 2017年9月2日下午4:57:47
	 */
	@RequestMapping("/page.action")
	public String page() {
		return "media/pic_list";
	}

	/**
	 * @Description:读取图片列表
	 * @param pageForm
	 * @param dto
	 * @return
	 * @date 2017年9月2日下午4:57:47
	 */
	@RequestMapping(value = "/list.action", method = RequestMethod.POST)
	@ResponseBody
	public Object list(PageForm pageForm, MediaPicture dto) {
		Map<String, Object> dataMap = mediaPictureRepository.findByAuto(dto, pageForm);
		Map<String, Object> map = new HashMap<String, Object>();
		List<MediaPicture> list = (List<MediaPicture>) dataMap.get("rows");
		List<Map<String, Object>> _list = new ArrayList<Map<String, Object>>();
		SimpleDateFormat yf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		for (MediaPicture data : list) {
			Map<String, Object> _map = new HashMap<String, Object>();
			_map.put("id", data.getId());
			_map.put("title", data.getTitle());
			_map.put("imgPath", data.getImgPath());
			_map.put("createTime", yf.format(data.getCreateTime()));
			_map.put("state", data.getState());
			_map.put("orderList", data.getOrderList());
			_list.add(_map);
		}
		map.put("rows", _list);
		map.put("total", dataMap.get("total"));
		return map;
	}

	/**
	 * @Description:编辑图片
	 * @param id
	 *            图片Id
	 * @param request
	 * @param model
	 * @return
	 * @date 2017年9月2日下午4:57:47
	 */
	@RequestMapping("/edit.action")
	public String edit(Long id, HttpServletRequest request, Model model) {
		SysManager manager = (SysManager) request.getSession().getAttribute(Constant.ADMINUSER);
		SysRegion region = sysRegionRepository.findOne(manager.getRegion());
		model.addAttribute("region", region);
		MediaPicture dto = new MediaPicture();
		if (id != null) {
			dto = mediaPictureRepository.findOne(id);
		}
		List<SysDict> list = sysDictRepository.findByParent("PicType");
		List<SysDict> resourceType = sysDictRepository.findByParent("PicResourceType"); // 读取数据字典图库资源类型
		model.addAttribute("dict", list);
		model.addAttribute("dto", dto);
		model.addAttribute("resourceType", resourceType);
		return "media/pic_input";
	}

	/**
	 * @Description:保存图片
	 * @param dto
	 *            图片实体
	 * @param model
	 * @return
	 * @date 2017年9月2日下午4:57:47
	 */
	@RequestMapping(value = "/save.action", method = RequestMethod.POST)
	public String save(@ModelAttribute MediaPicture dto, Model model) {
		if (dto.getState() == null || dto.getState().equals("")) {
			dto.setState(0L);
		}
		if (dto.getOrderList() == null || dto.getOrderList().equals("")) {
			dto.setOrderList(9999L);
		}
		if (dto.getId() != null) {
			MediaPicture data = mediaPictureRepository.findOne(dto.getId());
			data.setUpdateTime(new Date());
			BeanUtils.copyProperties(dto, data, new String[] { "id", "createTime", "checkNum", "followNum" });
			mediaPictureRepository.save(data);
		} else {
			dto.setCreateTime(new Date());
			mediaPictureRepository.save(dto);
		}

		return "forward:/success.action";
	}

	/**
	 * @Description:删除图片
	 * @param ids
	 * @return
	 * @date 2017年9月2日下午4:57:47
	 */
	@RequestMapping("/remove.action")
	@ResponseBody
	public Object delete(Long[] ids) {
		String status = Constant.STATUS_SCUESS;
		String msg;
		try {
			for (Long id : ids) {
				mediaPictureRepository.delete(id);
			}
			msg = "删除成功！";
		} catch (Exception e) {
			status = Constant.STATUS_ERROR;
			msg = "删除失败！";
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("state", status);
		map.put("msg", msg);
		JSONObject json = JSONObject.fromObject(map);
		return json;
	}

}
