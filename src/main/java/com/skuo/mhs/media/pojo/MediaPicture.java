package com.skuo.mhs.media.pojo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

/**
 * @ClassName: MediaPicture
 * @Description: 图片实体
 * @Company: 四川西谷智慧科技有限公司
 * @author: 钟操
 * @date: 2017/9/2 18:04:30
 * @version: V1.0.0
 */
@Entity
public class MediaPicture {
	
	/**
	 *主键 
	 */
	@Id
	@GeneratedValue
	private Long id;
	
	/**
	 * 图片名称
	 */
	private String title;
	
	/**
	 * 图片类型
	 */
	private String type;
	

	/**
	 * 图片地址
	 */
	private String imgPath;
	

	/**
	 * 排序
	 */
	private Long orderList;
	
	/**
	 * 是否置顶
	 */
	private Long isTop;
	
	/**
	 * 状态
	 */
	private Long state;
	
	/**
	 * 创建时间
	 */
	private Date createTime;
	
	/**
	 * 更新时间
	 */
	private Date updateTime;
	
	/**
	 * 点击数
	 */
	private Long checkNum;
	
	/**
	 * 关注数
	 */
	private Long followNum;

	/**
	 * 所属资源类型
	 */
	public String resourceType;

	/**
	 * 资源Id
	 */
	public String resourceId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}


	public String getImgPath() {
		return imgPath;
	}

	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}

	public Long getOrderList() {
		return orderList;
	}

	public void setOrderList(Long orderList) {
		this.orderList = orderList;
	}

	public Long getIsTop() {
		return isTop;
	}

	public void setIsTop(Long isTop) {
		this.isTop = isTop;
	}

	public Long getState() {
		return state;
	}

	public void setState(Long state) {
		this.state = state;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Long getCheckNum() {
		return checkNum;
	}

	public void setCheckNum(Long checkNum) {
		this.checkNum = checkNum;
	}

	public Long getFollowNum() {
		return followNum;
	}

	public void setFollowNum(Long followNum) {
		this.followNum = followNum;
	}

	public String getResourceType() {
		return resourceType;
	}

	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}

	public String getResourceId() {
		return resourceId;
	}

	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}
}
