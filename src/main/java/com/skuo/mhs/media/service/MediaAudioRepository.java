package com.skuo.mhs.media.service;


import org.springframework.stereotype.Service;

import com.skuo.mhs.media.pojo.MediaAudio;
import com.skuo.mhs.support.CustomRepository;

/**
 * 
* 
* @ClassName: MediaAudioRepository 
* @Description: 音频实现DAO
* @Company: 四川西谷智慧科技有限公司
* @author: 余德山 
* @date: 2017年9月4日 上午11:40:06 
* @version: V1.0.0
 */
@Service
public interface MediaAudioRepository extends CustomRepository<MediaAudio, Long> {


}
