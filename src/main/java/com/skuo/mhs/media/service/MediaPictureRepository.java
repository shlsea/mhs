package com.skuo.mhs.media.service;

import org.springframework.stereotype.Service;

import com.skuo.mhs.media.pojo.MediaPicture;
import com.skuo.mhs.support.CustomRepository;

/**
 * 
 * 
 * @ClassName: MediaPictureRepository
 * @Description: 图片实现DAO
 * @Company: 四川西谷智慧科技有限公司
 * @author: 余德山
 * @date: 2017年9月4日 上午11:40:44
 * @version: V1.0.0
 */
@Service
public interface MediaPictureRepository extends CustomRepository<MediaPicture, Long> {

}
