package com.skuo.mhs.front.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class FrontWebController {

	@RequestMapping(value = "/api")
	public String index(){
		return "front/index";
		
	}
	
	@RequestMapping(value = "/api_list")
	public String apiList(){
		return "front/api_list";
	}
	
	@RequestMapping(value = "/api_type")
	public String apiType(){
		return "front/api_type";
	}
	
	@RequestMapping(value = "/api_details")
	public String apiDetails(){
		return "front/api_details";
	}
	
	@RequestMapping(value = "/api_debug")
	public String apiDeBug(){
		return "front/api_debug";
	}
	
	@RequestMapping(value = "/channel_list")
	public String channel(){
		return "front/channel_list";
	}
	
	@RequestMapping(value = "/news_details")
	public String newsDetails(){
		return "front/news_details";
	}
	
	@RequestMapping(value = "/news")
	public String news(){
		return "front/news";
	}
	
	
	
	
}
