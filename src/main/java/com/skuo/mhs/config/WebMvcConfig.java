package com.skuo.mhs.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.skuo.mhs.iterator.AdminOpInterceptor;
import com.skuo.mhs.iterator.FrontInterceptor;
import com.skuo.mhs.iterator.LoginInterceptor;

/**
 * 
 * 
 * @ClassName: WebMvcConfig
 * @Description: 页面访问注册拦截器
 * @Company: 四川西谷智慧科技有限公司
 * @author: 余德山
 * @date: 2017年9月1日 下午1:31:28
 * @version: V1.0.0
 */
@Configuration
public class WebMvcConfig extends WebMvcConfigurerAdapter {

	/**
	 * 注册拦截地址方法实现
	 */
	@Override
	public void addInterceptors(InterceptorRegistry registry) {

		// 多个拦截器组成一个拦截器链
		// addPathPatterns 用于添加拦截规则
		// excludePathPatterns 用户排除拦截
		/**
		 * 拦截后缀名为action的请求<br>
		 * 主要用于后台
		 */
		registry.addInterceptor(new AdminOpInterceptor()).addPathPatterns("/**.action", "/**/**.action");

		/**
		 * 拦截后缀名为do的请求<br>
		 * 主要用于前台
		 */
		registry.addInterceptor(new FrontInterceptor()).addPathPatterns("/**.do");

		/**
		 * 拦截loginForm请求<br>
		 * 用于后台登录请求
		 */
		registry.addInterceptor(new LoginInterceptor()).addPathPatterns("/checkLogin");
		super.addInterceptors(registry);
	}

}
