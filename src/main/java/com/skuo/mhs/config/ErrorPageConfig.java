package com.skuo.mhs.config;

import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.web.servlet.ErrorPage;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;

/**
 * 
 * @ClassName: ErrorPageConfig
 * @Description:错误页面拦截器配置
 * @Company:四川西谷智慧科技有限公司
 * @author 余德山
 * @date 2017年8月31日 下午3:53:47
 * @version V1.0.0
 */
@Configuration
public class ErrorPageConfig {

	/**
	 * 
	 *
	 * @Description:注入自定义拦截器
	 * @return
	 * @date 2017年9月1日下午1:30:31
	 *
	 */
	@Bean
	public EmbeddedServletContainerCustomizer embeddedServletContainerCustomizer() {
		return new MyCustomizer();
	}

	/**
	 * 
	 * 
	 * @ClassName: MyCustomizer
	 * @Description: 实现EmbeddedServletContainerCustomizer接口
	 * @Company: 四川西谷智慧科技有限公司
	 * @author: 余德山
	 * @date: 2017年9月1日 下午1:30:49
	 * @version: V1.0.0
	 */
	private static class MyCustomizer implements EmbeddedServletContainerCustomizer {

		/**
		 * 实现接口EmbeddedServletContainerCustomizer类 添加拦截错误页面路径
		 */
		@Override
		public void customize(ConfigurableEmbeddedServletContainer container) {
			container.addErrorPages(new ErrorPage(HttpStatus.FORBIDDEN, "/error/403"));
			container.addErrorPages(new ErrorPage(HttpStatus.BAD_GATEWAY, "/error/500"));
			container.addErrorPages(new ErrorPage(HttpStatus.BAD_REQUEST, "/error/400"));
			container.addErrorPages(new ErrorPage(HttpStatus.INTERNAL_SERVER_ERROR, "/error/500"));
			container.addErrorPages(new ErrorPage(HttpStatus.NOT_FOUND, "/error/404"));
		}

	}
}
