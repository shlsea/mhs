package com.skuo.mhs.trip.pojo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

import org.hibernate.annotations.Type;

import com.skuo.mhs.system.pojo.SysMenu;

/**
 * CmsTrip 资讯网行程管理
 * 
 * @author yudeshan
 *
 */
@Entity
public class CmsTrip implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -2402956701373738237L;

	/***
	 * 主键
	 */
	@Id
	@GeneratedValue
	private Long id;

	/***
	 * 行程名称
	 */
	private String name;

	/***
	 * 出发地区
	 */
	private String startAddress;

	/**
	 * 目的地
	 */
	private String endAddress;

	/***
	 * 行程人数
	 */
	private Long people;

	/***
	 * 行程天数
	 */
	private Long days;

	/***
	 * 住宿花费
	 */
	private Double hotelCost;

	/***
	 * 人均花费
	 */
	private Double avgMoney;

	/***
	 * 出发时间
	 */
	private String startDate;

	/***
	 * 新增时间
	 */
	private Date addDate;

	/***
	 * 修改时间
	 */
	private Date updateDate;

	/**
	 * 行程简介
	 */
	@Type(type = "text")
	private String content;

	/**
	 * 会员id--用于行程定制
	 */
	private Long memberId;

	/***
	 * 行程描述
	 */
	@Type(type = "text")
	private String remark;

	/***
	 * 排序
	 */
	private Long orderList;

	/***
	 * 状态<br/>
	 * 1、启用<br/>
	 * 0、禁用
	 */
	private Long state;

	/**
	 * 推荐 1、推荐 0、不推荐
	 */
	private Long recommend;

	private Long type;

	@OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.MERGE, CascadeType.PERSIST,
			CascadeType.REMOVE }, mappedBy = "trip")
	@OrderBy("day")
	private List<CmsTripDay> tripDays;

	/**
	 * 查看数
	 */
	private Long viewNum;

	/**
	 * 关注数
	 */
	private Long followNum;
	/**
	 * 所属栏目
	 */
	@ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinColumn(name = "channel", nullable = true, insertable = true, updatable = true)
	private SysMenu menu;

	/**
	 * 行程总广告图
	 */
	private String imgPath;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(length = 4000)
	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Long getOrderList() {
		return orderList;
	}

	public void setOrderList(Long orderList) {
		this.orderList = orderList;
	}

	public Long getState() {
		return state;
	}

	public void setState(Long state) {
		this.state = state;
	}

	public Long getPeople() {
		return people;
	}

	public void setPeople(Long people) {
		this.people = people;
	}

	public Long getDays() {
		return days;
	}

	public void setDays(Long days) {
		this.days = days;
	}

	public Double getHotelCost() {
		return hotelCost;
	}

	public void setHotelCost(Double hotelCost) {
		this.hotelCost = hotelCost;
	}

	public Double getAvgMoney() {
		return avgMoney;
	}

	public void setAvgMoney(Double avgMoney) {
		this.avgMoney = avgMoney;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public Date getAddDate() {
		return addDate;
	}

	public void setAddDate(Date addDate) {
		this.addDate = addDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Long getType() {
		return type;
	}

	public void setType(Long type) {
		this.type = type;
	}

	public String getStartAddress() {
		return startAddress;
	}

	public void setStartAddress(String startAddress) {
		this.startAddress = startAddress;
	}

	public String getEndAddress() {
		return endAddress;
	}

	public void setEndAddress(String endAddress) {
		this.endAddress = endAddress;
	}

	public List<CmsTripDay> getTripDays() {
		return tripDays;
	}

	public void setTripDays(List<CmsTripDay> tripDays) {
		this.tripDays = tripDays;
	}

	public Long getMemberId() {
		return memberId;
	}

	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}

	public Long getRecommend() {
		return recommend;
	}

	public void setRecommend(Long recommend) {
		this.recommend = recommend;
	}

	public Long getViewNum() {
		return viewNum;
	}

	public void setViewNum(Long viewNum) {
		this.viewNum = viewNum;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Long getFollowNum() {
		return followNum;
	}

	public void setFollowNum(Long followNum) {
		this.followNum = followNum;
	}

	public SysMenu getMenu() {
		return menu;
	}

	public void setMenu(SysMenu menu) {
		this.menu = menu;
	}

	public String getImgPath() {
		return imgPath;
	}

	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}

}
