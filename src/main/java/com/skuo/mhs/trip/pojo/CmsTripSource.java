package com.skuo.mhs.trip.pojo;


import javax.persistence.*;

import org.hibernate.annotations.Type;

import java.io.Serializable;
import java.util.List;

/**
 * CmsTripSource 资讯网行程管理每日行程资源
 * @author yudeshan
 *
 */
@Entity
public class CmsTripSource implements Serializable {


	/**
	 *
	 */
	private static final long serialVersionUID = -2402956701373738237L;

	/***
	 * 主键
	 */
	@Id
	@GeneratedValue
	private Long id;

	/***
	 * 资源ID
	 */
	private String dataId;

	/***
	 * 添加方式
	 * 0 数据库
	 * 1 自定义
	 */
	private Long way;

	/**
	 * 资源类型<br>
	 * 1:景区<br>
	 * 2:酒店住宿<br>
	 * 3:餐饮场所<br>
	 * 4:交通
	 */
	private Long type;

	/***
	 * 资源名称（当资源不存在时使用）
	 */
	private String name;

	/***
	 * 描述
	 */
	private String remark;

	/***
	 * 地区名称
	 */
	private String regionName;

	/***
	 * 图片地址
	 */
	private String imgPath;

	/***
	 * 地址或者建议游玩
	 */
	private String tag;

	/***
	 * 排序
	 */
	private Long orderList;

	/**
	 * 所属第几天行程行程
	 */
	@ManyToOne(fetch=FetchType.LAZY,cascade={CascadeType.REFRESH})
	@JoinColumn(name = "day", nullable = true, insertable = true, updatable = true)
	private CmsTripDay day;

	/***
	 * 关联交通
	 */
	@OrderBy("orderList")
	@OneToMany(fetch = FetchType.LAZY,cascade = {CascadeType.REFRESH,CascadeType.REMOVE},mappedBy = "source")
	private List<CmsTripTraffic> traffic;

	/**
	 * 内容
	 */
	@Type(type = "text")
	private String content;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(length = 4000)
	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Long getOrderList() {
		return orderList;
	}

	public void setOrderList(Long orderList) {
		this.orderList = orderList;
	}




	public String getDataId() {
		return dataId;
	}

	public void setDataId(String dataId) {
		this.dataId = dataId;
	}

	public Long getWay() {
		return way;
	}

	public void setWay(Long way) {
		this.way = way;
	}

	public Long getType() {
		return type;
	}

	public void setType(Long type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public CmsTripDay getDay() {
		return day;
	}

	public void setDay(CmsTripDay day) {
		this.day = day;
	}

	public List<CmsTripTraffic> getTraffic() {
		return traffic;
	}

	public void setTraffic(List<CmsTripTraffic> traffic) {
		this.traffic = traffic;
	}

	public String getRegionName() {
		return regionName;
	}

	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}

	public String getImgPath() {
		return imgPath;
	}

	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}



}
