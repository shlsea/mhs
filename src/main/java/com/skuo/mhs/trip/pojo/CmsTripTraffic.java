package com.skuo.mhs.trip.pojo;


import javax.persistence.*;
import java.io.Serializable;


/**
 * CmsTripTraffic 资讯网行程管理每日行程交通
 * @author yudeshan
 *
 */
@Entity
public class CmsTripTraffic implements Serializable {


	/**
	 *
	 */
	private static final long serialVersionUID = -2402956701373738237L;

	/***
	 * 主键
	 */
	@Id
	@GeneratedValue
	private Long id;


	/***
	 * 添加方式
	 * 1:汽车<br>
	 * 2:火车<br>
	 * 3:飞机<br>
	 */
	private Long way;


	/***
	 * 排序
	 */
	private Long orderList;



	/***
	 * 出发城市
	 */
	private String fromArea;

	/***
	 *到达城市
	 */
	private String toArea;

	/***
	 * 班/车次
	 */
	private String trafficSequence;

	/***
	 * 出发时间 小时
	 */
	private Long sdateh;

	/***
	 * 出发时间 分钟
	 */
	private Long sdatem;

	/***
	 *到达时间 小时
	 */
	private Long edateh;

	/***
	 *到达时间 分钟
	 */
	private Long edatem;

	/***
	 *0表示当日到达 1表示次日到达...
	 */
	private Long edays;

	@OneToOne(cascade=CascadeType.REMOVE)
	@JoinColumn(name="source")
	private CmsTripSource source;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public Long getOrderList() {
		return orderList;
	}

	public void setOrderList(Long orderList) {
		this.orderList = orderList;
	}




	public Long getWay() {
		return way;
	}

	public void setWay(Long way) {
		this.way = way;
	}

	public String getFromArea() {
		return fromArea;
	}

	public void setFromArea(String fromArea) {
		this.fromArea = fromArea;
	}

	public String getToArea() {
		return toArea;
	}

	public void setToArea(String toArea) {
		this.toArea = toArea;
	}

	public String getTrafficSequence() {
		return trafficSequence;
	}

	public void setTrafficSequence(String trafficSequence) {
		this.trafficSequence = trafficSequence;
	}

	public Long getSdateh() {
		return sdateh;
	}

	public void setSdateh(Long sdateh) {
		this.sdateh = sdateh;
	}

	public Long getSdatem() {
		return sdatem;
	}

	public void setSdatem(Long sdatem) {
		this.sdatem = sdatem;
	}

	public Long getEdateh() {
		return edateh;
	}

	public void setEdateh(Long edateh) {
		this.edateh = edateh;
	}

	public Long getEdatem() {
		return edatem;
	}

	public void setEdatem(Long edatem) {
		this.edatem = edatem;
	}

	public Long getEdays() {
		return edays;
	}

	public void setEdays(Long edays) {
		this.edays = edays;
	}

	public CmsTripSource getSource() {
		return source;
	}

	public void setSource(CmsTripSource source) {
		this.source = source;
	}

}
