package com.skuo.mhs.trip.pojo;


import javax.persistence.*;
import java.io.Serializable;
import java.util.List;


/**
 * CmsTripDay 资讯网行程管理每日行程
 * @author yudeshan
 *
 */
@Entity
public class CmsTripDay implements Serializable {


	/**
	 *
	 */
	private static final long serialVersionUID = -2402956701373738237L;

	/***
	 * 主键
	 */
	@Id
	@GeneratedValue
	private Long id;

	/***
	 * 目的地
	 */
	private String destination;


	/***
	 * 第几天
	 */
	private Long day;

	/***
	 * 描述
	 */
	private String remark;

	/***
	 * 排序
	 */
	private Long orderList;

	/**
	 * 所属行程
	 */
	@ManyToOne(fetch=FetchType.LAZY,cascade={CascadeType.REFRESH})
	@JoinColumn(name = "trip", nullable = true, insertable = true, updatable = true)
	private CmsTrip trip;


	@OrderBy("orderList")
	@OneToMany(fetch = FetchType.LAZY,cascade = {CascadeType.REFRESH,CascadeType.REMOVE, CascadeType.PERSIST},mappedBy = "day")
	private List<CmsTripSource> source;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(length = 4000)
	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Long getOrderList() {
		return orderList;
	}

	public void setOrderList(Long orderList) {
		this.orderList = orderList;
	}




	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public Long getDay() {
		return day;
	}

	public void setDay(Long day) {
		this.day = day;
	}

	public CmsTrip getTrip() {
		return trip;
	}

	public void setTrip(CmsTrip trip) {
		this.trip = trip;
	}

	public List<CmsTripSource> getSource() {
		return source;
	}

	public void setSource(List<CmsTripSource> source) {
		this.source = source;
	}


}
