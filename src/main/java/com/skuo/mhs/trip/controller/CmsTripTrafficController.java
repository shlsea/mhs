package com.skuo.mhs.trip.controller;

import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.skuo.mhs.trip.pojo.CmsTripDay;
import com.skuo.mhs.trip.pojo.CmsTripSource;
import com.skuo.mhs.trip.pojo.CmsTripTraffic;
import com.skuo.mhs.trip.service.CmsTripDayRepository;
import com.skuo.mhs.trip.service.CmsTripSourceRepository;
import com.skuo.mhs.trip.service.CmsTripTrafficRepository;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @项目名称 资讯网
 * @作者 Administrator
 * @包名称 com.skou.zxw.trip.controller
 * @功能描述 行程天数资源中交通信息控制器
 * @日期 2017年08月12日
 * @版本 V1.0.0
 */
@Controller
@RequestMapping(value = "/trip_traffic")
public class CmsTripTrafficController {

    @Autowired
    private CmsTripSourceRepository cmsTripSourceRepository;
    @Autowired
    private CmsTripDayRepository cmsTripDayRepository;
    @Autowired
    private CmsTripTrafficRepository cmsTripTrafficRepository;

    /**
     * 通过每日行程资源id保存
     * @param tripSourceId  每日行程资源id
     * @param traffic       前台传来的交通信息对象
     * @param response
     * @return
     * @throws IOException
     */
    @RequestMapping("save")
    @ResponseBody
    public Object saveByTripSourceId(Long dayId, String remark, CmsTripTraffic traffic, HttpServletResponse response) throws IOException {
    	CmsTripDay day = cmsTripDayRepository.findOne(dayId);
    	String state="1";
    	CmsTripSource source=new CmsTripSource();
    	source.setDay(day);
    	source.setType(4l);
    	source.setWay(1l);
    	source.setName(traffic.getFromArea()+"—"+traffic.getToArea());
    	source.setRemark(remark);
    	try{
	    	cmsTripSourceRepository.save(source);
	    	traffic.setSource(source);
	        cmsTripTrafficRepository.save(traffic);
    	}catch(Exception e){
    		state="0";
    	}
        response.setContentType("charset=UTF-8;text/json");
        response.setCharacterEncoding("UTF-8");
        JSONObject obj = new JSONObject();
        obj.put("id", source.getId());
        obj.put("state", state);
        obj.put("name", traffic.getFromArea()+"—"+traffic.getToArea());
        obj.put("remark", source.getRemark());
        obj.put("type", source.getType());
        response.getWriter().write(obj.toString());
        return null;
    }
}
