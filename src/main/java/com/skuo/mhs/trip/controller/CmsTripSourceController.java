package com.skuo.mhs.trip.controller;

import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.skuo.mhs.trip.pojo.CmsTripDay;
import com.skuo.mhs.trip.pojo.CmsTripSource;
import com.skuo.mhs.trip.service.CmsTripDayRepository;
import com.skuo.mhs.trip.service.CmsTripSourceRepository;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @项目名称 资讯网
 * @作者 Administrator
 * @包名称 com.skou.zxw.trip.controller
 * @功能描述 行程天数中资源控制器
 * @日期 2017年08月12日
 * @版本 V1.0.0
 */
@Controller
@RequestMapping(value = "/trip_source")
public class CmsTripSourceController {

	@Autowired
	private CmsTripDayRepository cmsTripDayRepository;
	@Autowired
	private CmsTripSourceRepository cmsTripSourceRepository;

	/**
	 * 通过行程天数id保存资源
	 *
	 * @param   dayId  行程天数id
	 * @param   type  前台传来资源类型
	 * @param  dataId  前台传来资源id
	 * @param   name  前台传来资源名称
	 * @param  remark   前台传来资源描述
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/save")
	@ResponseBody
	public Object saveByTripDayId(CmsTripSource tripSource, HttpServletRequest request, HttpServletResponse response)
			throws IOException {

		CmsTripDay tripDay = cmsTripDayRepository.findOne(tripSource.getDay().getId());
		if(tripSource.getImgPath().trim().length()==0){
			tripSource.setImgPath(null);
		}
		tripSource.setDay(tripDay);

		cmsTripSourceRepository.save(tripSource);
		response.setContentType("charset=UTF-8;text/json");
		response.setCharacterEncoding("UTF-8");
		JSONObject obj = new JSONObject();
		obj.put("id", tripSource.getId());
		response.getWriter().write(obj.toString());
		return null;
	}

	/**
	 * 添加备注时调用
	 * @param id  每日行程资源id
	 * @param remark  前台传过来的备注
	 * @param response
	 * @return
	 */
	@RequestMapping("/update")
	@ResponseBody
	public Object updateByTripSourceId(Long id, String remark, HttpServletResponse response) throws IOException {
		CmsTripSource tripSource = cmsTripSourceRepository.findOne(id);
		tripSource.setRemark(remark);
		String state = "1";
		try {
			cmsTripSourceRepository.save(tripSource);
		} catch (Exception e) {
			state = "0";
		}
		response.setContentType("charset=UTF-8;text/json");
		response.setCharacterEncoding("UTF-8");
		JSONObject obj = new JSONObject();
		obj.put("state", state);
		response.getWriter().write(obj.toString());
		return null;
	}

	/**
	 * 删除每日行程资源
	 * @param id  每日行程资源id
	 * @return
	 * @throws IOException
	 */
	@RequestMapping("/delete")
	@ResponseBody
	public Object deleteByTripSourceId(Long id, HttpServletResponse response) throws IOException {
		CmsTripSource tripSource = cmsTripSourceRepository.findOne(id);
		String state = "1";
		if (tripSource != null) {
			try {
				cmsTripSourceRepository.delete(tripSource);
			} catch (Exception e) {
				state = "0";
			}
		}
		response.setContentType("charset=UTF-8;text/json");
		response.setCharacterEncoding("UTF-8");
		JSONObject obj = new JSONObject();
		obj.put("state", state);
		response.getWriter().write(obj.toString());
		return null;
	}

	/**
	 * 保持行程天数
	 * @param tripSource
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@RequestMapping("/saveSource")
    @ResponseBody
    public Object saveSource(CmsTripSource tripSource, HttpServletRequest request, HttpServletResponse response) throws IOException {

        CmsTripDay tripDay = cmsTripDayRepository.findOne(tripSource.getDay().getId());
        tripSource.setDay(tripDay);
        tripSource.setWay(1l);
        String state="1";
	    try{
	    	cmsTripSourceRepository.save(tripSource);
	    }catch(Exception e){
	    	state="0";
	    }
        response.setContentType("charset=UTF-8;text/json");
        response.setCharacterEncoding("UTF-8");
        JSONObject obj = new JSONObject();
        obj.put("id", tripSource.getId());
        obj.put("state", state);
        obj.put("name", tripSource.getName());
        obj.put("remark", tripSource.getRemark());
        obj.put("type", tripSource.getType());
        response.getWriter().write(obj.toString());
        return null;
    }
}
