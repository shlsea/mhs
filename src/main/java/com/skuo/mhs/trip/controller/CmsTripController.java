package com.skuo.mhs.trip.controller;


import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.skuo.mhs.form.PageForm;
import com.skuo.mhs.system.pojo.SysManager;
import com.skuo.mhs.system.pojo.SysMenu;
import com.skuo.mhs.system.pojo.SysRegion;
import com.skuo.mhs.system.service.SysRegionRepository;
import com.skuo.mhs.trip.pojo.CmsTrip;
import com.skuo.mhs.trip.pojo.CmsTripDay;
import com.skuo.mhs.trip.service.CmsTripDayRepository;
import com.skuo.mhs.trip.service.CmsTripRepository;
import com.skuo.mhs.utils.Constant;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名称 资讯网
 * @作者 余德山
 * @包名称 com.skou.zxw.trip.controller
 * @功能描述 TODO
 * @日期 2017年08月12日
 * @版本 V1.0.0
 */
@Controller
@RequestMapping(value = "/trip")
public class CmsTripController {

    @Autowired
    private CmsTripRepository cmsTripRepository;
    @Autowired
    private SysRegionRepository sysRegionRepository;
    @Autowired
    private CmsTripDayRepository cmsTripDayRepository;


    /**
     * 跳转行程管理页面
     * @param request
     * @param response
     * @param model
     * @return
     */
    @RequestMapping("/page.action")
    public String main(HttpServletRequest request, HttpServletResponse response, Model model) {
        SysMenu menu=(SysMenu) request.getSession().getAttribute(Constant.OP_IN_MENU);
        model.addAttribute("menu", menu);
        return "trip/trip_list";
    }

    /**
     * 读取行程数据列表
     * @param pageForm
     * @param trip
     * @param request
     * @return
     */
    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/list.action", method = RequestMethod.POST)
    @ResponseBody
    public Object list(PageForm pageForm, CmsTrip trip, HttpServletRequest request) {
        Map<String, Object> mapLinks = cmsTripRepository.findByAuto(trip, pageForm);
        List<CmsTrip> list = (List<CmsTrip>) mapLinks.get("rows");
        Map<String, Object> map = new HashMap<String, Object>();
        List<Map<String, Object>> _list = new ArrayList<Map<String, Object>>();
        for (CmsTrip cmsTrip : list) {
            Map<String, Object> _map = new HashMap<String, Object>();
            _map.put("id", cmsTrip.getId());
            _map.put("name", cmsTrip.getName()); // 行程名称
            _map.put("days", cmsTrip.getDays()); // 行程天数
            _map.put("avgMoney", cmsTrip.getAvgMoney()); // 行程人均花费
            _map.put("state", cmsTrip.getState()); // 状态
            _list.add(_map);
        }
        map.put("rows", _list);
        map.put("total", mapLinks.get("total"));
        return map;
    }

    /**
     * 新增/编辑行程
     * @param id
     * @param request
     * @param response
     * @param model
     * @return
     */
    @RequestMapping("/edit.action")
    public String edit(Long id, HttpServletRequest request, HttpServletResponse response, Model model) {
        CmsTrip trip = new CmsTrip(); //行程管理
        SysManager manager = (SysManager) request.getSession().getAttribute(Constant.ADMINUSER);
        SysMenu menu=(SysMenu) request.getSession().getAttribute(Constant.OP_IN_MENU);
        model.addAttribute("menu", menu);
        if (id != null) {
            trip = cmsTripRepository.findOne(id);
            model.addAttribute("dto", trip);

            return "trip/trip_detailed";
        } else {
            SysRegion area = sysRegionRepository.findOne(manager.getRegion());
            model.addAttribute("region", area);
            model.addAttribute("area",area.getOrderNo());
            return "trip/trip_input";

        }
    }


    /**
     * 保存行程
     * @param trip
     * @param model
     * @param response
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/trip_save.action", method = RequestMethod.POST)
    public String trip_save(@ModelAttribute CmsTrip trip, Model model, HttpServletResponse response, HttpServletRequest request) throws Exception {
        String startRegion=trip.getStartAddress()!=null?trip.getStartAddress():null;
        String endRegion=trip.getEndAddress()!=null?trip.getEndAddress():null;
        String startAddress="";
        String endAddress="";
        if(startRegion!=null){
        	startAddress=sysRegionRepository.findOne(startRegion).getShortName();
        }
        if(endRegion!=null){
        	endAddress=sysRegionRepository.findOne(endRegion).getShortName();
        }
        trip.setName(startAddress + "—" + endAddress+ trip.getDays() + "日游");
        CmsTrip dto = cmsTripRepository.save(trip);
        for (Long i = 0L; i < dto.getDays(); i++) {
            CmsTripDay tripDay = new CmsTripDay(); // 每日行程
            tripDay.setTrip(dto);
            tripDay.setDay(i + 1); // 行程第几天
            cmsTripDayRepository.save(tripDay);
        }
        return "redirect:/trip/edit.action?id=" + dto.getId();
    }

    /**
     * 顺序新增一天日程
     * @param id
     * @param day
     * @param response
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/day_save.action", method = RequestMethod.POST)
    @ResponseBody
    public String day_save(Long id, Long day, HttpServletResponse response, HttpServletRequest request) throws Exception {
        CmsTrip trip = cmsTripRepository.findOne(id);

        CmsTripDay tripDay = new CmsTripDay(); // 每日行程
        tripDay.setTrip(trip);
        tripDay.setDay(day); // 行程第几天
        cmsTripDayRepository.save(tripDay);
        trip.setDays(trip.getDays() + 1); // 每加一个每日行程，行程天数加一
        cmsTripRepository.save(trip);
        response.setContentType("charset=UTF-8;text/json");
        response.setCharacterEncoding("UTF-8");
        JSONObject obj = new JSONObject();
        obj.put("id", tripDay.getId());
        obj.put("day", tripDay.getDay());
        response.getWriter().write(obj.toString());

        return null;
    }

    /**
     * 插入一天日程
     * @param id
     * @param response
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/insert_save.action", method = RequestMethod.POST)
    @ResponseBody
    public String insert_save(Long id, HttpServletResponse response, HttpServletRequest request) throws Exception {
        CmsTripDay tripDay = cmsTripDayRepository.findOne(id);
        CmsTrip trip = cmsTripRepository.findOne(tripDay.getTrip().getId());
        Long day = tripDay.getDay();
        for (CmsTripDay data : trip.getTripDays()) {
            if (data.getDay() >= day) {
                data.setDay(data.getDay() + 1);
                cmsTripDayRepository.save(data);
            }
        }
        CmsTripDay dto = new CmsTripDay(); // 每日行程
        dto.setTrip(trip);
        dto.setDay(day); // 行程第几天
        cmsTripDayRepository.save(dto);
        response.setContentType("charset=UTF-8;text/json");
        response.setCharacterEncoding("UTF-8");
        JSONObject obj = new JSONObject();
        obj.put("id", dto.getId());
        obj.put("day", dto.getDay());
        response.getWriter().write(obj.toString());

        return null;
    }


    /**
     * 完善行程信息后的保存
     * @param trip
     * @param response
     * @param request
     * @return
     * @throws IOException
     */
    @RequestMapping(value = "/save_detail", method = RequestMethod.POST)
    @ResponseBody
    private String save_detail(CmsTrip trip, HttpServletResponse response, HttpServletRequest request) throws IOException {
        CmsTrip data = cmsTripRepository.findOne(trip.getId());
        String state = "1";
        data.setName(trip.getName());
        data.setPeople(trip.getPeople());
        data.setAvgMoney(trip.getAvgMoney());
        data.setHotelCost(trip.getHotelCost());
        data.setRemark(trip.getRemark());
        data.setDays(trip.getDays());
        data.setState(trip.getState() != null ? 1 : 0L);
        data.setRecommend(trip.getRecommend() != null ? 1 : 0L);
        data.setContent(trip.getContent());
        data.setImgPath(trip.getImgPath());
        try {
            cmsTripRepository.save(data);
        } catch (Exception e) {
            state = "0";
        }
        response.setContentType("charset=UTF-8;text/json");
        response.setCharacterEncoding("UTF-8");
        JSONObject obj = new JSONObject();
        obj.put("state", state);
        response.getWriter().write(obj.toString());
        return null;
    }

    /**
     * 根据id删除行程
     * @param ids
     * @param request
     * @param response
     * @return
     * @throws IOException
     */
    @RequestMapping("/remove.action")
    @ResponseBody
    public Object delete(Long[] ids, HttpServletRequest request, HttpServletResponse response) throws IOException {
        String state = "0";
        String msg;
        try {
            for (Long id : ids) {
                // 先删除关联每日行程
                List<CmsTripDay> tripDays = cmsTripDayRepository.findByTripId(id);
                for (CmsTripDay tripDay : tripDays) {
                    cmsTripDayRepository.delete(tripDay.getId());
                }
                cmsTripRepository.delete(id);
            }
            state = "1";
            msg = "删除成功！";
        } catch (Exception e) {
            e.getStackTrace();
            msg = "删除失败！";
        }
        response.setContentType("charset=UTF-8;text/json");
        response.setCharacterEncoding("UTF-8");
        JSONObject obj = new JSONObject();
        obj.put("state", state);
        obj.put("msg", msg);
        response.getWriter().write(obj.toString());
        return null;
    }


}
