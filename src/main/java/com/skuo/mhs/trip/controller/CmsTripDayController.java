package com.skuo.mhs.trip.controller;


import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.skuo.mhs.trip.pojo.CmsTrip;
import com.skuo.mhs.trip.pojo.CmsTripDay;
import com.skuo.mhs.trip.service.CmsTripDayRepository;
import com.skuo.mhs.trip.service.CmsTripRepository;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @项目名称 资讯网
 * @作者 Administrator
 * @包名称 com.skou.zxw.trip.controller
 * @功能描述 行程天数控制器
 * @日期 2017年08月12日
 * @版本 V1.0.0
 */
@Controller
@RequestMapping(value = "/trip_day")
public class CmsTripDayController {

    @Autowired
    private CmsTripRepository cmsTripRepository;
    @Autowired
    private CmsTripDayRepository cmsTripDayRepository;

    /**
     * 通过行程id保存
     * @param text
     * @param id
     * @param response
     * @return
     * @throws IOException
     */
    @RequestMapping("/save")
    @ResponseBody
    public Object saveByTripId(String text, Long id, HttpServletResponse response) throws IOException {
    	CmsTripDay tripDay = cmsTripDayRepository.findOne(id);
        tripDay.setRemark(text);
        cmsTripDayRepository.save(tripDay);
        response.setContentType("charset=UTF-8;text/json");
        response.setCharacterEncoding("UTF-8");
        JSONObject obj = new JSONObject();
        obj.put("id", tripDay.getId());
        response.getWriter().write(obj.toString());
        return null;
    }

    /**
     * 添加备注时调用
     * @param id          每日行程id
     * @param remark      前台传过来的备注
     * @param destination 前台传过来的目的地
     * @param response
     * @return
     */
    @RequestMapping("/update")
    @ResponseBody
    public Object updateByTripDayId(Long id, String remark, String destination, HttpServletResponse response) throws IOException {
        String state="1";
        CmsTripDay tripDay = cmsTripDayRepository.findOne(id);
        try{
            tripDay.setRemark(remark);
            cmsTripDayRepository.save(tripDay);
        }catch(Exception e){
        	state="0";
        }

        response.setContentType("charset=UTF-8;text/json");
        response.setCharacterEncoding("UTF-8");
        JSONObject obj = new JSONObject();
        obj.put("state", state);
        response.getWriter().write(obj.toString());
        return null;
    }

    /**
     * 删除每日行程
     * @param id 每日行程id
     * @return
     * @throws IOException
     */
    @RequestMapping("/delete")
    @ResponseBody
    public Object deleteByTripDayId(Long id, HttpServletResponse response) throws IOException {
    	 CmsTripDay tripDay = cmsTripDayRepository.findOne(id);
    	 Long trip_id= tripDay.getTrip().getId();
    	 CmsTrip trip=cmsTripRepository.findOne(trip_id);
    	 String state="1";
    	 try{
    		 Long day=tripDay.getDay();
    		 cmsTripDayRepository.delete(tripDay);
    		 for(CmsTripDay data:trip.getTripDays()){
    			 if(data.getDay()>day){
    				 data.setDay(data.getDay()-1);
    				 cmsTripDayRepository.save(data);
    			 }
    		 }
    	 }catch(Exception e){
    		 state="0";
    	 }
        response.setContentType("charset=UTF-8;text/json");
        response.setCharacterEncoding("UTF-8");
        JSONObject obj = new JSONObject();
        obj.put("state", state);
        response.getWriter().write(obj.toString());
        return null;
    }
}
