package com.skuo.mhs.trip.service;

import org.springframework.stereotype.Service;

import com.skuo.mhs.support.CustomRepository;
import com.skuo.mhs.trip.pojo.CmsTripTraffic;

/***
 *
* @项目名称 资讯网
* @作者 余德山
* @包名称 com.skou.zxw.trip.service
* @功能描述 TODO
* @日期 2017年08月022日
* @版本 V1.0.0
 */
@Service
public interface CmsTripTrafficRepository extends CustomRepository<CmsTripTraffic, Long> {

}
