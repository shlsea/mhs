package com.skuo.mhs.trip.service;


import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import com.skuo.mhs.support.CustomRepository;
import com.skuo.mhs.trip.pojo.CmsTripSource;

import java.util.List;

/***
 *
* @项目名称 资讯网
* @作者 余德山
* @包名称 com.skou.zxw.trip.service
* @功能描述 TODO
* @日期 2017年08月02日
* @版本 V1.0.0
 */
@Service
public interface CmsTripSourceRepository extends CustomRepository<CmsTripSource, Long> {

    /**
     * 根据id查询每日行程
     * @param dayId
     * @return
     */
    @Query("select u from CmsTripSource u where u.day.id=?1 order by u.orderList")
    List<CmsTripSource> findByDayId(Long dayId);
}
