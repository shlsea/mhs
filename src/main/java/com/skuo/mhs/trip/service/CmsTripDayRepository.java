package com.skuo.mhs.trip.service;


import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import com.skuo.mhs.support.CustomRepository;
import com.skuo.mhs.trip.pojo.CmsTripDay;

import java.util.List;

/***
 *
* @项目名称 资讯网
* @作者 余德山
* @包名称 com.skou.zxw.trip.service
* @功能描述 TODO
* @日期 2017年08月02日
* @版本 V1.0.0
 */
@Service
public interface CmsTripDayRepository extends CustomRepository<CmsTripDay, Long> {

    /**
     * 查询id查询行程
     * @param tripId
     * @return
     */
    @Modifying
    @Query("select u from CmsTripDay u where u.trip.id=?1 order by u.orderList")
    List<CmsTripDay> findByTripId(Long tripId);
}
