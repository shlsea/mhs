package com.skuo.mhs.trip.service;


import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import com.skuo.mhs.support.CustomRepository;
import com.skuo.mhs.system.pojo.SysMenu;
import com.skuo.mhs.trip.pojo.CmsTrip;

import java.util.List;

/***
 *
* @项目名称 资讯网
* @作者 余德山
* @包名称 com.skou.zxw.trip.service
* @功能描述 TODO
* @日期 2017年08月02日
* @版本 V1.0.0
 */
@Service
public interface CmsTripRepository extends CustomRepository<CmsTrip, Long> {

    /**
     * 根据行程id查询行程详情
     * @param id
     * @return
     */
    @Query("select t from CmsTrip t where t.id = ?1 and t.state = 1")
    CmsTrip findChannelByid(Long id);

    /**
     *  根据状态及栏目id 查询
     * @param state
     * @param id
     * @return
     */
    List<CmsTrip> findByStateAndMenuId(Long state, Long id);

    /**
     * 根据数据ID，状态及所属栏目查询大于当前的数据
     * @param id
     * @param state
     * @param data
     * @param sort
     * @return
     */
    List<CmsTrip> findByIdGreaterThanAndStateAndMenu(Long id, Long state, SysMenu data, Sort sort);

    /**
     * 根据数据ID，状态及所属栏目查询小于当前的数据
     * @param id
     * @param state
     * @param data
     * @param sort
     * @return
     */
    List<CmsTrip> findByIdLessThanAndStateAndMenu(Long id, Long state, SysMenu data, Sort sort);

	List<CmsTrip> findByState(Long l);

}
