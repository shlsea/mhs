package com.skuo.mhs.system.controller;

import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.skuo.mhs.form.PageForm;
import com.skuo.mhs.system.pojo.SysLoginLog;
import com.skuo.mhs.system.service.SysLoginLogRepository;
import com.skuo.mhs.utils.Constant;

import net.sf.json.JSONObject;

/**
 * 
 * 
 * @ClassName: SysLoginLogController
 * @Description: 登录日志管理逻辑控制器
 * @Company: 四川西谷智慧科技有限公司
 * @author: 余德山
 * @date: 2017年9月1日 下午1:38:59
 * @version: V1.0.0
 */
@Controller
@RequestMapping(value = "/sys_login_log")
public class SysLoginLogController {

	@Autowired
	private SysLoginLogRepository sysLoginLogRepository;

	/**
	 * 
	 *
	 * @Description:跳转登录日志管理页面
	 * @return
	 * @date 2017年9月1日下午1:56:08
	 *
	 */
	@RequestMapping("/page.action")
	public String page() {
		return "system/login_log_list";
	}

	/**
	 * 
	 *
	 * @Description:读取登录日志列表
	 * @param pageForm
	 *            分页表单
	 * @param dto
	 *            查询字段封装为实体属性
	 * @return
	 * @date 2017年9月1日下午1:56:20
	 *
	 */
	@RequestMapping(value = "/list.action", method = RequestMethod.POST)
	@ResponseBody
	public Object list(PageForm pageForm, SysLoginLog dto) {
		Map<String, Object> map = sysLoginLogRepository.findByAuto(dto, pageForm);
		return map;
	}

	/**
	 * 
	 *
	 * @Description:根据id删除登录日志
	 * @param ids
	 *            登录日志id数组
	 * @return
	 * @date 2017年9月1日下午1:56:59
	 *
	 */
	@RequestMapping("/remove.action")
	@ResponseBody
	public Object delete(Long[] ids) {
		String status = Constant.STATUS_SCUESS;
		String msg;
		try {
			for (Long id : ids) {
				sysLoginLogRepository.delete(id);
			}

			msg = "删除成功！";
		} catch (Exception e) {
			status = Constant.STATUS_ERROR;
			msg = "删除失败！";
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("state", status);
		map.put("msg", msg);
		JSONObject json = JSONObject.fromObject(map);
		return json;
	}
}
