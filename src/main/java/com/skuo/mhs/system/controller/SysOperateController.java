package com.skuo.mhs.system.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.skuo.mhs.form.PageForm;
import com.skuo.mhs.system.pojo.SysMenu;
import com.skuo.mhs.system.pojo.SysOperate;
import com.skuo.mhs.system.service.SysMenuRepository;
import com.skuo.mhs.system.service.SysOperateRepository;
import com.skuo.mhs.utils.Constant;

import net.sf.json.JSONObject;

/**
 * 
 * 
 * @ClassName: SysOperateController
 * @Description: 菜单操作管理逻辑控制器
 * @Company: 四川西谷智慧科技有限公司
 * @author: 余德山
 * @date: 2017年9月1日 下午1:39:43
 * @version: V1.0.0
 */
@SuppressWarnings("unchecked")
@Controller
@RequestMapping(value = "/sys_operate")
public class SysOperateController {

	@Autowired
	private SysMenuRepository sysMenuRepository;
	@Autowired
	private SysOperateRepository sysOperateRepository;

	/**
	 * 
	 *
	 * @Description:跳转操作管理页面
	 * @return
	 * @date 2017年9月1日下午2:03:16
	 *
	 */
	@RequestMapping("/page.action")
	public String page() {
		return "system/operate_list";
	}

	/**
	 * 
	 *
	 * @Description:读取菜单操作列表
	 * @param pageForm
	 *            分页表单
	 * @param dto
	 *            查询条件
	 * @return
	 * @date 2017年9月1日下午2:03:39
	 *
	 */
	@RequestMapping(value = "/list.action", method = RequestMethod.POST)
	@ResponseBody
	public Object list(PageForm pageForm, SysOperate dto) {
		Long _id = dto.getMenu().getId();
		if (_id != null && _id != 0 && !_id.equals("")) {
			SysMenu menu = sysMenuRepository.findOne(_id);
			dto.setMenu(menu);
		} else {
			dto.setMenu(null);
		}
		Map<String, Object> mapOperate = sysOperateRepository.findByAuto(dto, pageForm);
		Map<String, Object> map = new HashMap<String, Object>();
		List<SysOperate> list = (List<SysOperate>) mapOperate.get("rows");
		List<SysOperate> _list = new ArrayList<SysOperate>();
		for (SysOperate _op : list) {
			SysMenu data = _op.getMenu();
			if (data != null) {
				_op.setMenu(data);
				if (data.getParent() != null) {
					data.setParent(null);
				}
				if (data.getSysMenus().size() > 0) {
					data.setSysMenus(null);
				}
			}
			_list.add(_op);
		}
		map.put("rows", _list);
		map.put("total", mapOperate.get("total"));
		return map;
	}

	/**
	 * 
	 *
	 * @Description:编辑菜单操作
	 * @param id
	 *            操作id
	 * @param request
	 * @param model
	 * @return
	 * @date 2017年9月1日下午2:04:20
	 *
	 */
	@RequestMapping("/edit.action")
	public String edit(Long id, HttpServletRequest request, Model model) {
		SysOperate menu = new SysOperate();
		if (id != null) {
			menu = sysOperateRepository.findOne(id);
		}
		model.addAttribute("dto", menu);
		return "system/operate_input";
	}

	/**
	 * 
	 *
	 * @Description:编辑操作
	 * @param dto
	 *            操作实体
	 * @param model
	 * @param request
	 * @return
	 * @date 2017年9月1日下午2:04:50
	 *
	 */
	@RequestMapping(value = "/save.action", method = RequestMethod.POST)
	public String save(@ModelAttribute SysOperate dto, Model model, HttpServletRequest request) {

		if (dto.getIsShow() == null || dto.getIsShow().equals("")) {
			dto.setIsShow(0L);
		}
		if (dto.getState() == null || "".equals(dto.getState())) {
			dto.setState(0L);
		}
		sysOperateRepository.save(dto);
		return "forward:/success.action";
	}

	/**
	 * 
	 *
	 * @Description:根据id删除操作
	 * @param ids
	 *            操作id数组
	 * @return
	 * @date 2017年9月1日下午2:05:53
	 *
	 */
	@RequestMapping("/remove.action")
	@ResponseBody
	public Object delete(Long[] ids) {
		String status = Constant.STATUS_SCUESS;
		String msg;
		try {
			for (Long i : ids) {
				sysOperateRepository.delete(i);
			}
			msg = "删除成功！";
		} catch (Exception e) {
			status = Constant.STATUS_ERROR;
			msg = "删除失败！";
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("state", status);
		map.put("msg", msg);
		JSONObject json = JSONObject.fromObject(map);
		return json;
	}

	/**
	 * 
	 *
	 * @Description:查询出所有的操作
	 * @return
	 * @date 2017年9月1日下午2:06:20
	 *
	 */
	@RequestMapping(value = "/findAll.action", method = RequestMethod.POST)
	@ResponseBody
	public Object findAll() {
		List<SysOperate> operates = sysOperateRepository.findAll();
		return operates;
	}

}
