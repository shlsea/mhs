package com.skuo.mhs.system.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.skuo.mhs.form.PageForm;
import com.skuo.mhs.system.pojo.SysMenu;
import com.skuo.mhs.system.pojo.SysOperate;
import com.skuo.mhs.system.pojo.SysRole;
import com.skuo.mhs.system.service.SysMenuRepository;
import com.skuo.mhs.system.service.SysOperateRepository;
import com.skuo.mhs.system.service.SysRoleRepository;
import com.skuo.mhs.utils.Constant;

import net.sf.json.JSONObject;

/**
 * 
 * 
 * @ClassName: SysRoleController
 * @Description: 系统角色管理逻辑控制器
 * @Company: 四川西谷智慧科技有限公司
 * @author: 余德山
 * @date: 2017年9月1日 下午1:40:26
 * @version: V1.0.0
 */
@Controller
@RequestMapping(value = "/sys_role")
@SuppressWarnings("unchecked")
public class SysRoleController {

	@Autowired
	private SysRoleRepository sysRoleRepository;
	@Autowired
	private SysMenuRepository sysMenuRepository;
	@Autowired
	private SysOperateRepository sysOperateRepository;

	/**
	 * 
	 *
	 * @Description:跳转系统角色管理页面
	 * @return
	 * @date 2017年9月1日下午2:13:11
	 *
	 */
	@RequestMapping("/page.action")
	public String page() {
		return "system/role_list";
	}

	/**
	 * 
	 *
	 * @Description:读取系统角色列表
	 * @param pageForm
	 *            分页表单
	 * @param dto
	 *            查询条件
	 * @return
	 * @date 2017年9月1日下午2:13:24
	 *
	 */
	@RequestMapping(value = "/list.action", method = RequestMethod.POST)
	@ResponseBody
	public Object list(PageForm pageForm, SysRole dto) {
		Map<String, Object> mapRole = sysRoleRepository.findByAuto(dto, pageForm);
		Map<String, Object> map = new HashMap<String, Object>();
		List<SysRole> list = (List<SysRole>) mapRole.get("rows");
		List<Map<String, Object>> _list = new ArrayList<Map<String, Object>>();
		for (SysRole data : list) {
			Map<String, Object> _map = new HashMap<String, Object>();
			_map.put("id", data.getId());
			_map.put("name", data.getName());
			_map.put("code", data.getCode());
			_map.put("state", data.getState());

			_list.add(_map);
		}
		map.put("rows", _list);
		map.put("total", mapRole.get("total"));
		return map;
	}

	/**
	 * 
	 *
	 * @Description:新增、编辑角色
	 * @param id
	 *            角色id
	 * @param request
	 * @param model
	 * @return
	 * @date 2017年9月1日下午2:14:16
	 *
	 */
	@RequestMapping("/edit.action")
	public String edit(Long id, HttpServletRequest request, Model model) {
		SysRole role = new SysRole();
		if (id != null) {
			role = sysRoleRepository.getOne(id);
			if (role != null) {
				String menus = role.getMenusId();
				String ops = role.getOperatesId();

				model.addAttribute("menus", menus);
				model.addAttribute("ops", ops);
			}
		} else {
			model.addAttribute("menus", ",");
			model.addAttribute("ops", ",");
		}
		model.addAttribute("dto", role);
		return "system/role_input";
	}

	/**
	 * 
	 *
	 * @Description:查询出所有的角色
	 * @return
	 * @date 2017年9月1日下午2:14:38
	 *
	 */
	@RequestMapping(value = "/findAll.action", method = RequestMethod.POST)
	@ResponseBody
	public Object findAll() {
		List<SysRole> list = new ArrayList<SysRole>();
		return list;
	}

	/**
	 * 
	 *
	 * @Description:保存角色
	 * @param dto
	 * @param model
	 * @return
	 * @date 2017年9月1日下午2:15:02
	 *
	 */
	@RequestMapping(value = "/save.action", method = RequestMethod.POST)
	public String save(@ModelAttribute SysRole dto, Model model) {
		model.addAttribute("sysRole", dto);
		sysRoleRepository.save(dto);
		return "forward:/success.action";
	}

	/**
	 * 
	 *
	 * @Description:根据id删除角色
	 * @param ids
	 *            角色id数组
	 * @return
	 * @date 2017年9月1日下午2:15:14
	 *
	 */
	@RequestMapping("/remove.action")
	@ResponseBody
	public Object delete(Long[] ids) {
		String status = Constant.STATUS_SCUESS;
		String msg;
		try {
			for (Long id : ids) {
				sysRoleRepository.delete(id);
			}
			msg = "删除成功！";
		} catch (Exception e) {
			status = Constant.STATUS_ERROR;
			msg = "删除失败！";
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("state", status);
		map.put("msg", msg);
		JSONObject json = JSONObject.fromObject(map);
		return json;
	}

	/**
	 * 
	 *
	 * @Description:获取权限树形结构
	 * @param request
	 * @return
	 * @date 2017年9月1日下午2:15:38
	 *
	 */
	@RequestMapping(value = "/getTree.action", method = RequestMethod.POST)
	@ResponseBody
	public Object getTree(HttpServletRequest request) {
		List<Map<String, Object>> treeList = new ArrayList<Map<String, Object>>();
		List<SysMenu> list = sysMenuRepository.findMenus();
		for (SysMenu menu : list) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("id", menu.getId());
			map.put("pId", menu.getParent() != null ? menu.getParent().getId() : null);
			map.put("name", menu.getName());
			map.put("open", true);
			List<Map<String, Object>> opList = new ArrayList<Map<String, Object>>();
			List<SysOperate> operateList = sysOperateRepository.findByMenuId(menu.getId());

			for (SysOperate operate : operateList) {
				Map<String, Object> _map = new HashMap<String, Object>();
				_map.put("id", operate.getId());
				_map.put("code", operate.getCode());
				_map.put("name", operate.getName());
				opList.add(_map);
			}
			map.put("op", opList);
			treeList.add(map);
		}

		JSONObject obj = new JSONObject();
		obj.put("treeNodes", treeList);
		return obj.toString();
	}

}
