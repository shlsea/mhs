package com.skuo.mhs.system.controller;

import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.skuo.mhs.form.PageForm;
import com.skuo.mhs.system.pojo.SysErrorLog;
import com.skuo.mhs.system.service.SysErrorLogRepository;
import com.skuo.mhs.utils.Constant;

import net.sf.json.JSONObject;

/**
 * 
 * 
 * @ClassName: SysErrorLogController
 * @Description: 错误日志逻辑控制器
 * @Company: 四川西谷智慧科技有限公司
 * @author: 余德山
 * @date: 2017年9月1日 下午1:38:31
 * @version: V1.0.0
 */
@Controller
@RequestMapping(value = "/sys_error_log")
public class SysErrorLogController {

	@Autowired
	private SysErrorLogRepository sysErrorLogRepository;

	/**
	 * 
	 *
	 * @Description:跳转错误日志管理页面
	 * @return
	 * @date 2017年9月1日下午1:54:30
	 *
	 */
	@RequestMapping("/page.action")
	public String page() {
		return "system/error_log_list";
	}

	/**
	 * 
	 *
	 * @Description:读取错误日志列表
	 * @param pageForm
	 *            分页表单
	 * @param dto
	 *            查询条件封装成实体
	 * @return
	 * @date 2017年9月1日下午1:54:40
	 *
	 */
	@RequestMapping(value = "/list.action", method = RequestMethod.POST)
	@ResponseBody
	public Object list(PageForm pageForm, SysErrorLog dto) {
		Map<String, Object> map = sysErrorLogRepository.findByAuto(dto, pageForm);
		return map;
	}

	/**
	 * 
	 *
	 * @Description:根据id删除错误日志
	 * @param ids
	 *            错误日志id数组
	 * @return
	 * @date 2017年9月1日下午1:55:27
	 *
	 */
	@RequestMapping("/remove.action")
	@ResponseBody
	public Object delete(Long[] ids) {
		String status = Constant.STATUS_SCUESS;
		String msg;
		try {
			for (Long id : ids) {
				sysErrorLogRepository.delete(id);
			}
			msg = "删除成功！";
		} catch (Exception e) {
			msg = "删除失败！";
			status = Constant.STATUS_ERROR;
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("state", status);
		map.put("msg", msg);
		JSONObject json = JSONObject.fromObject(map);
		return json;
	}
}
