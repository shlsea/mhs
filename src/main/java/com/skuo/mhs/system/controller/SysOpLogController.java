package com.skuo.mhs.system.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.skuo.mhs.form.PageForm;
import com.skuo.mhs.system.pojo.SysOpLog;
import com.skuo.mhs.system.service.SysOpLogRepository;
import com.skuo.mhs.utils.Constant;

import net.sf.json.JSONObject;

/**
 * 
 * 
 * @ClassName: SysOpLogController
 * @Description: 系统操作日志管理逻辑控制器
 * @Company: 四川西谷智慧科技有限公司
 * @author: 余德山
 * @date: 2017年9月1日 下午1:39:55
 * @version: V1.0.0
 */
@Controller
@RequestMapping(value = "/sys_op_log")
public class SysOpLogController {

	@Autowired
	private SysOpLogRepository sysOpLogRepository;

	/**
	 * 
	 *
	 * @Description:跳转操作日志管理页面
	 * @return
	 * @date 2017年9月1日下午2:06:38
	 *
	 */
	@RequestMapping("/page.action")
	public String page() {
		return "system/op_log_list";
	}

	/**
	 * 
	 *
	 * @Description:读取操作日志列表
	 * @param pageForm
	 *            分页表单
	 * @param dto
	 *            查询条件
	 * @return
	 * @date 2017年9月1日下午2:06:47
	 *
	 */
	@RequestMapping(value = "/list.action", method = RequestMethod.POST)
	@ResponseBody
	public Object list(PageForm pageForm, SysOpLog dto) {
		Map<String, Object> map = sysOpLogRepository.findByAuto(dto, pageForm);
		return map;
	}

	/**
	 * 
	*
	* @Description:根据id删除操作日志
	* @param ids 操作日志id数组
	* @return 
	* @date 2017年9月1日下午2:07:38
	*
	 */
	@RequestMapping("/remove.action")
	@ResponseBody
	public Object delete(Long[] ids) {
		String status = Constant.STATUS_SCUESS;
		String msg;
		try {
			for (Long id : ids) {
				sysOpLogRepository.delete(id);
			}
			msg = "删除成功！";
		} catch (Exception e) {
			status = Constant.STATUS_ERROR;
			msg = "删除失败！";
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("state", status);
		map.put("msg", msg);
		JSONObject json = JSONObject.fromObject(map);
		return json;
	}
}
