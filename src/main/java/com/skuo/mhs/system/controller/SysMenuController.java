package com.skuo.mhs.system.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.skuo.mhs.form.PageForm;
import com.skuo.mhs.system.ipml.SysMenuServiceImpl;
import com.skuo.mhs.system.pojo.SysManager;
import com.skuo.mhs.system.pojo.SysMenu;
import com.skuo.mhs.system.pojo.SysRole;
import com.skuo.mhs.system.service.SysManagerRepository;
import com.skuo.mhs.system.service.SysMenuRepository;
import com.skuo.mhs.utils.Constant;

import net.sf.json.JSONObject;

/**
 * 
 * 
 * @ClassName: SysMenuController
 * @Description: 系统菜单管理逻辑控制器
 * @Company: 四川西谷智慧科技有限公司
 * @author: 余德山
 * @date: 2017年9月1日 下午1:39:29
 * @version: V1.0.0
 */
@SuppressWarnings("unchecked")
@Controller
@RequestMapping(value = "/sys_menu")
public class SysMenuController {

	@Autowired
	private SysManagerRepository sysManagerRepository;
	@Autowired
	private SysMenuRepository sysMenuRepository;
	@Autowired
	private SysMenuServiceImpl sysMenuServiceImpl;

	/**
	 * 
	 *
	 * @Description:跳转菜单列表页面
	 * @return
	 * @date 2017年9月1日下午2:00:15
	 *
	 */
	@RequestMapping("/page.action")
	public String page() {
		return "system/menu_list";
	}

	/**
	 * 
	 *
	 * @Description:获取菜单列表数据
	 * @param pageForm
	 *            分页表单
	 * @param dto
	 *            查询条件
	 * @return
	 * @date 2017年9月1日下午2:00:25
	 *
	 */
	@RequestMapping(value = "/list.action", method = RequestMethod.POST)
	@ResponseBody
	public Object list(PageForm pageForm, SysMenu dto) {
		Long _id = dto.getParent().getId();
		if (_id != null && _id != 0 && !_id.equals("")) {
			SysMenu parent = sysMenuRepository.findOne(_id);
			dto.setParent(parent);
		} else {
			dto.setParent(null);
		}
		Map<String, Object> mapMenu = sysMenuRepository.findByAuto(dto, pageForm);
		Map<String, Object> map = new HashMap<String, Object>();
		List<SysMenu> list = (List<SysMenu>) mapMenu.get("rows");
		List<Map<String, Object>> _list = new ArrayList<Map<String, Object>>();
		for (SysMenu data : list) {
			Map<String, Object> _map = new HashMap<String, Object>();
			_map.put("id", data.getId());
			_map.put("name", data.getName());
			_map.put("state", data.getState());
			_map.put("menuLevel", data.getMenuLevel());
			_map.put("orderList", data.getOrderList());
			_map.put("parent.name", data.getParent() != null ? data.getParent().getName() : null);
			_map.put("uri", data.getUri());
			_list.add(_map);
		}
		map.put("rows", _list);
		map.put("total", mapMenu.get("total"));
		return map;
	}

	/**
	 * 
	 *
	 * @Description:编辑菜单
	 * @param id
	 *            菜单id
	 * @param request
	 * @param model
	 * @return
	 * @date 2017年9月1日下午2:01:00
	 *
	 */
	@RequestMapping("/edit.action")
	public String edit(Long id, HttpServletRequest request, Model model) {
		SysMenu dto = new SysMenu();
		if (id != null) {
			dto = sysMenuRepository.findOne(id);
		}
		model.addAttribute("dto", dto);
		return "system/menu_input";
	}

	/**
	 * 
	 *
	 * @Description:保存菜单
	 * @param dto
	 *            菜单实体
	 * @param model
	 * @param request
	 * @return
	 * @date 2017年9月1日下午2:01:20
	 *
	 */
	@RequestMapping(value = "/save.action", method = RequestMethod.POST)
	public String save(@ModelAttribute SysMenu dto, Model model, HttpServletRequest request) {
		model.addAttribute("sysMenu", dto);
		if (sysMenuServiceImpl == null) {
			BeanFactory factory = WebApplicationContextUtils
					.getRequiredWebApplicationContext(request.getServletContext());
			sysMenuServiceImpl = (SysMenuServiceImpl) factory.getBean("sysMenuServiceImpl");
		}
		if (dto.getId() != null) {
			SysMenu data = sysMenuRepository.findOne(dto.getId());
			BeanUtils.copyProperties(dto, data, new String[] { "id" });
			sysMenuServiceImpl.save(data);
		} else {
			sysMenuServiceImpl.save(dto);
		}
		return "forward:/success.action";
	}

	/**
	 * 
	 *
	 * @Description:根据id删除菜单
	 * @param ids
	 *            菜单id数组
	 * @return
	 * @date 2017年9月1日下午2:01:56
	 *
	 */
	@RequestMapping("/remove.action")
	@ResponseBody
	public Object delete(Long[] ids) {
		String status = Constant.STATUS_SCUESS;
		String msg;
		try {
			for (Long id : ids) {
				SysMenu data=sysMenuRepository.findOne(id);
				data.setState(-1L);
				sysMenuRepository.save(data);
			}
			msg = "删除成功！";
		} catch (Exception e) {
			status = Constant.STATUS_ERROR;
			msg = "删除失败！";
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("state", status);
		map.put("msg", msg);
		JSONObject json = JSONObject.fromObject(map);
		return json;
	}

	/**
	 * 
	 *
	 * @Description:根据父类菜单Id查询有权限的子菜单
	 * @param id
	 *            父类菜单id
	 * @param request
	 * @return
	 * @date 2017年9月1日下午2:02:30
	 *
	 */
	@RequestMapping(value = "/menu.action", method = RequestMethod.POST)
	@ResponseBody
	public Object menu(Long id, HttpServletRequest request) {
		SysManager manager = (SysManager) request.getSession().getAttribute(Constant.ADMINUSER);
		SysManager account = sysManagerRepository.findOne(manager.getId());
		SysMenu parent = sysMenuRepository.findOne(id);
		List<SysMenu> menus = sysMenuRepository.findMenusByParentAndStateOrderByOrderListAsc(parent,1L);
		if (account.getIsSuper() == null || account.getIsSuper() != 1) {

			// 1、当账号为非超级管理员时先获取账号角色
			SysRole role = account.getRole();

			// 2、获取当前角色所有权限菜单
			Set<SysMenu> menuList = role.getMenus();

			menus.clear();
			for (SysMenu menu : menuList) {
				if (menu.getParent() != null && parent != null && menu.getParent().getId() == parent.getId()) {
					menus.add(menu);
				}
			}

		}
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		for (SysMenu menu : menus) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("id", menu.getId());
			map.put("icon", menu.getIcon());
			map.put("uri", menu.getUri());
			map.put("name", menu.getName());

			list.add(map);
		}
		return list;
	}

	/**
	 * 
	 *
	 * @Description:查询出所有的菜单
	 * @return
	 * @date 2017年9月1日下午2:03:00
	 *
	 */
	@RequestMapping(value = "/findAll.action", method = RequestMethod.POST)
	@ResponseBody
	public Object findAll() {
		List<SysMenu> list = new ArrayList<SysMenu>();
		List<SysMenu> menus = sysMenuRepository.findMenus();
		for (SysMenu menu : menus) {
			menu.setSysMenus(null);
			menu.setParent(null);
			list.add(menu);
		}
		return list;
	}
}
