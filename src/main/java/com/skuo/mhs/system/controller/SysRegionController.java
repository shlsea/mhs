package com.skuo.mhs.system.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.skuo.mhs.form.PageForm;
import com.skuo.mhs.system.ipml.SysRegionServiceImpl;
import com.skuo.mhs.system.pojo.SysManager;
import com.skuo.mhs.system.pojo.SysRegion;
import com.skuo.mhs.system.service.SysRegionRepository;
import com.skuo.mhs.utils.Constant;

import net.sf.json.JSONObject;

/**
 * 
 * 
 * @ClassName: SysRegionController
 * @Description: 地区管理逻辑控制器
 * @Company: 四川西谷智慧科技有限公司
 * @author: 余德山
 * @date: 2017年9月1日 下午1:40:13
 * @version: V1.0.0
 */
@Controller
@RequestMapping(value = "/sys_region")
public class SysRegionController {

	@Autowired
	private SysRegionRepository sysRegionRepository;
	@Autowired
	private SysRegionServiceImpl sysRegionServiceImpl;

	/**
	 * 
	 *
	 * @Description:跳转地区列表页面
	 * @param request
	 * @param model
	 * @return
	 * @date 2017年9月1日下午2:08:27
	 *
	 */
	@RequestMapping("/page.action")
	public String page(HttpServletRequest request, Model model) {
		SysManager manager = (SysManager) request.getSession().getAttribute(Constant.ADMINUSER);
		SysRegion region = sysRegionRepository.findOne(manager.getRegion());
		model.addAttribute("region", region);
		return "system/region_list";
	}

	/**
	 * 
	 *
	 * @Description:获取地区列表
	 * @param pageForm
	 *            分页表单
	 * @param dto
	 *            查询条件
	 * @return
	 * @date 2017年9月1日下午2:10:12
	 *
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/list.action", method = RequestMethod.POST)
	@ResponseBody
	public Object list(PageForm pageForm, SysRegion dto) {
		SysRegion region = new SysRegion();
		if (!dto.getPregion().getId().equals("100000")) {
			if (dto.getPregion().getId().endsWith("0000")) {
				region.setRegion(dto.getPregion().getId().substring(0, 2));
			} else if (dto.getPregion().getId().endsWith("00")) {
				region.setRegion(dto.getPregion().getId().substring(0, 4));
			} else {
				region.setRegion(dto.getPregion().getId().substring(0, 6));
			}
		}
		region.setName(dto.getName());
		Map<String, Object> mapRegion = sysRegionRepository.findByAuto(region, pageForm);
		Map<String, Object> map = new HashMap<String, Object>();
		List<SysRegion> list = (List<SysRegion>) mapRegion.get("rows");
		List<Map<String, Object>> _list = new ArrayList<Map<String, Object>>();
		for (SysRegion data : list) {
			Map<String, Object> _map = new HashMap<String, Object>();
			_map.put("id", data.getId());
			_map.put("name", data.getName());
			_map.put("region", data.getRegion());
			_map.put("fullName", data.getFullName());
			_map.put("parent.name", data.getPregion() != null ? data.getPregion().getName() : null);
			_list.add(_map);
		}
		map.put("rows", _list);
		map.put("total", mapRegion.get("total"));
		return map;
	}

	/**
	 * 
	 *
	 * @Description:编辑地区
	 * @param id
	 *            地区id
	 * @param model
	 * @return
	 * @date 2017年9月1日下午2:10:46
	 *
	 */
	@RequestMapping("/edit.action")
	public String edit(String id, Model model) {
		SysRegion dto = new SysRegion();
		if (id != null) {
			dto = sysRegionRepository.findOne(id);
		}
		model.addAttribute("dto", dto);
		return "system/region_input";
	}

	/**
	 * 
	 *
	 * @Description:保存地区
	 * @param dto
	 *            地区实体
	 * @param model
	 * @param request
	 * @return
	 * @date 2017年9月1日下午2:11:05
	 *
	 */
	@RequestMapping(value = "/save.action", method = RequestMethod.POST)
	public String save(@ModelAttribute SysRegion dto, Model model, HttpServletRequest request) {
		model.addAttribute("sysMenu", dto);
		if (sysRegionServiceImpl == null) {
			BeanFactory factory = WebApplicationContextUtils
					.getRequiredWebApplicationContext(request.getServletContext());
			sysRegionServiceImpl = (SysRegionServiceImpl) factory.getBean("sysRegionServiceImpl");
		}
		if (dto.getId() != null) {
			SysRegion data = sysRegionRepository.findOne(dto.getId());
			BeanUtils.copyProperties(dto, data, new String[] { "id" });
			sysRegionServiceImpl.save(data);
		} else {
			sysRegionServiceImpl.save(dto);
		}
		return "forward:/success.action";
	}

	/**
	 * 
	 *
	 * @Description:删除地区
	 * @param ids
	 *            地区id数组
	 * @return
	 * @date 2017年9月1日下午2:11:29
	 *
	 */
	@RequestMapping("/remove.action")
	@ResponseBody
	public Object delete(String[] ids) {
		String status = Constant.STATUS_SCUESS;
		String msg;
		try {
			for (String id : ids) {
				sysRegionRepository.delete(id);
			}
			msg = "删除成功！";
		} catch (Exception e) {
			status = Constant.STATUS_ERROR;
			msg = "删除失败！";
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("state", status);
		map.put("msg", msg);
		JSONObject json = JSONObject.fromObject(map);
		return json;
	}

	/**
	 * 
	 *
	 * @Description:根据父类地区Id查询子地区
	 * @param pid
	 *            父类地区id
	 * @return
	 * @date 2017年9月1日下午2:11:54
	 *
	 */
	@RequestMapping(value = "/findByPid", method = RequestMethod.POST)
	@ResponseBody
	public Object findByPid(String pid) {
		List<SysRegion> list = new ArrayList<SysRegion>();
		List<SysRegion> regions = sysRegionRepository.findRegions(pid);
		for (SysRegion region : regions) {
			region.setPregion(null);
			region.setRegions(null);
			list.add(region);
		}
		return list;
	}

	/**
	 * 
	 *
	 * @Description:根据父级地区ID查询列表返回树形结构
	 * @param id
	 *            父类地区id
	 * @return
	 * @date 2017年9月1日下午2:12:24
	 *
	 */
	@RequestMapping(value = "/findAll")
	@ResponseBody
	public Object findAll(String id) {
		Map<String, Object> map = getJSon(id);
		return map;

	}

	/**
	 * 
	 *
	 * @Description:地区获取递归算法
	 * @param id
	 *            地区id
	 * @return
	 * @date 2017年9月1日下午2:12:49
	 *
	 */
	public Map<String, Object> getJSon(String id) {
		SysRegion region = sysRegionRepository.findOne(id);
		Map<String, Object> map = new HashMap<String, Object>();
		if (region != null && region.getRegions().size() > 0) {
			Map<String, Object> map1 = new HashMap<String, Object>();
			for (SysRegion area : region.getRegions()) {
				map1.put(area.getId(), area.getName());
				Map<String, Object> map2 = getJSon(area.getId());
				if (!map2.isEmpty()) {
					map.put(area.getId(), map2);
				}
			}
			map.put(region.getId(), map1);
		}
		return map;
	}

}
