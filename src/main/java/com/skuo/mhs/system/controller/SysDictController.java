package com.skuo.mhs.system.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.skuo.mhs.form.PageForm;
import com.skuo.mhs.system.pojo.SysDict;
import com.skuo.mhs.system.service.SysDictRepository;
import com.skuo.mhs.utils.Constant;

import net.sf.json.JSONObject;

/**
 * 
 * 
 * @ClassName: SysDictController
 * @Description: 数据字典逻辑控制器
 * @Company: 四川西谷智慧科技有限公司
 * @author: 余德山
 * @date: 2017年9月1日 上午10:43:41
 * @version: V1.0.0
 */
@Controller
@RequestMapping(value = "/sys_dict")
@SuppressWarnings("unchecked")
public class SysDictController {

	@Autowired
	private SysDictRepository sysDictRepository;

	/**
	 * 
	 *
	 * @Description:数据字典列表页面跳转
	 * @return
	 * @date 2017年9月1日下午1:50:45
	 *
	 */
	@RequestMapping("/page.action")
	public String page() {
		return "system/dict_list";
	}

	/**
	 * 
	 *
	 * @Description:获取数据字典列表数据
	 * 
	 * @param pageForm
	 *            分页工具表单
	 * @param dto
	 *            查询参数封装实体
	 * @return
	 * @date 2017年9月1日下午1:51:07
	 *
	 */
	@RequestMapping(value = "/list.action", method = RequestMethod.POST)
	@ResponseBody
	public Object list(PageForm pageForm, SysDict dto) {

		Map<String, Object> mapRegion = sysDictRepository.findByAuto(dto, pageForm);
		Map<String, Object> map = new HashMap<String, Object>();
		List<SysDict> list = (List<SysDict>) mapRegion.get("rows");
		List<Map<String, Object>> _list = new ArrayList<Map<String, Object>>();
		for (SysDict data : list) {
			Map<String, Object> _map = new HashMap<String, Object>();
			_map.put("skey", data.getSkey());
			_map.put("name", data.getName());
			_map.put("state", data.getState());
			_map.put("type", data.getType());
			_map.put("pkey.name", data.getPkey() != null ? data.getPkey().getName() : null);
			_map.put("id", data.getSkey());
			_list.add(_map);
		}
		map.put("rows", _list);
		map.put("total", mapRegion.get("total"));
		return map;
	}

	/**
	 * 
	 *
	 * @Description:查询所有数据字典
	 * @return
	 * @date 2017年9月1日下午1:51:53
	 *
	 */
	@RequestMapping(value = "/findAll.action", method = RequestMethod.POST)
	@ResponseBody
	public Object findAll() {
		List<SysDict> list = new ArrayList<SysDict>();
		List<SysDict> dicts = sysDictRepository.findMenus();
		for (SysDict dict : dicts) {
			dict.setPkey(null);
			list.add(dict);
		}
		return list;
	}

	/**
	 * 
	 *
	 * @Description:保存数据字典
	 * @param dto
	 * @param model
	 * @return
	 * @date 2017年9月1日下午1:52:04
	 *
	 */
	@RequestMapping(value = "/save.action", method = RequestMethod.POST)
	public String save(@ModelAttribute SysDict dto, Model model) {
		model.addAttribute("sysMenu", dto);
		if (dto.getState() == null || dto.getState().equals("")) {
			dto.setState(0L);
		}
		sysDictRepository.save(dto);
		return "forward:/success.action";
	}

	/**
	 * 
	 *
	 * @Description:根据id判断是是新增还是修改页面
	 * @param id
	 *            字典skey值
	 * @param model
	 * @return
	 * @date 2017年9月1日下午1:52:16
	 *
	 */
	@RequestMapping("/edit.action")
	public String edit(String id, Model model) {
		SysDict dto = new SysDict();
		if (id != null) {
			dto = sysDictRepository.getOne(id);
		}
		model.addAttribute("dto", dto);
		return "system/dict_input";
	}

	/**
	 * 
	 *
	 * @Description:根据skey删除数据字典
	 * @param ids
	 *            字典skey数组
	 * @return
	 * @date 2017年9月1日下午1:52:50
	 *
	 */
	@RequestMapping("/remove.action")
	@ResponseBody
	public Object delete(String[] ids) {
		String status = Constant.STATUS_SCUESS;
		String msg;
		try {
			for (String id : ids) {
				sysDictRepository.delete(id);
			}
			msg = "删除成功！";
		} catch (Exception e) {
			status = Constant.STATUS_ERROR;
			msg = "删除失败！";
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("state", status);
		map.put("msg", msg);
		JSONObject json = JSONObject.fromObject(map);
		return json;
	}

	/**
	 * 
	 *
	 * @Description:根据数据字典编码验证是否存在
	 * @param skey
	 *            数据字典编码
	 * @return
	 * @date 2017年9月1日下午1:53:21
	 *
	 */
	@RequestMapping("/checkSkey.action")
	@ResponseBody
	public Object checkSkey(String skey) {
		boolean result = true;
		List<SysDict> dicts = sysDictRepository.findAll();
		for (SysDict dict : dicts) {
			if (dict.getSkey().equals(skey)) {
				result = false;
				break;
			}
		}
		return result;
	}

	/**
	 * 
	 *
	 * @Description:根据父类skey值查询其下所有子集
	 * @param skey
	 *            父类字典skey
	 * @return
	 * @date 2017年9月1日下午1:54:00
	 *
	 */
	@RequestMapping(value = "/findByPkey.action", method = RequestMethod.POST)
	@ResponseBody
	public Object findByPkey(String skey) {
		List<SysDict> list = new ArrayList<SysDict>();
		SysDict parent = sysDictRepository.findOne(skey);
		if (parent != null) {
			List<SysDict> dicts = sysDictRepository.findByPkeyAndStateOrderBySkeyAsc(parent, 1l);
			for (SysDict dict : dicts) {
				dict.setPkey(null);
				list.add(dict);
			}
		}
		return list;
	}
}
