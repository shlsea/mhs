package com.skuo.mhs.system.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.skuo.mhs.form.LoginForm;
import com.skuo.mhs.system.pojo.SysManager;
import com.skuo.mhs.system.pojo.SysMenu;
import com.skuo.mhs.system.pojo.SysRole;
import com.skuo.mhs.system.service.SysManagerRepository;
import com.skuo.mhs.system.service.SysMenuRepository;
import com.skuo.mhs.utils.Constant;
import com.skuo.mhs.utils.Md5Tool;
import com.skuo.mhs.utils.ResponseJsonUtils;

/**
 * 
 * 
 * @ClassName: LoginController
 * @Description: 用户登录后操作以及注销跳转
 * @Company: 四川西谷智慧科技有限公司
 * @author: 余德山
 * @date: 2017年9月1日 上午10:43:04
 * @version: V1.0.0
 */
@Controller
public class LoginController {

	@Autowired
	private SysManagerRepository sysManagerRepository;
	@Autowired
	private SysMenuRepository sysMenuRepository;

	/**
	 * 
	 *
	 * @Description:登录验证
	 * @param form
	 *            登录表单
	 * @param request
	 * @param response
	 * @param model
	 * @date 2017年9月1日下午1:49:31
	 *
	 */
	@RequestMapping("/checkLogin")
	@ResponseBody
	public void login(LoginForm form, HttpServletRequest request, HttpServletResponse response, ModelMap model) {
		String account = form.getUsername();
		String password = form.getPassword();
		String captcha = form.getCaptcha().toUpperCase();
		int status = 1;
		String verifyCode = (String) request.getSession().getAttribute("verifyCode");
		if (verifyCode != null) {
			if (!verifyCode.equals(captcha)) {
				status = 0;// 验证码不正确
			} else {
				SysManager manager = sysManagerRepository.findByAccount(account);
				if (manager != null) {
					// System.out.println(Md5Tool.getMd5(password));
					if (Md5Tool.getMd5(password).equals(manager.getPassword())) {
						if (manager.getState() == -1) {
							status = 3;// 账号停用
						} else {
							status = 4;// 账号正确登录成功
							request.getSession().setAttribute(Constant.ADMINUSER, manager);
							request.getSession().setAttribute(Constant.REGION, manager.getRegion());
						}
					} else {
						status = 2;// 密码错误
					}
				} else {
					status = 1;// 账号不存在
				}
			}
		} else {
			status = -1;
		}
		String callback = request.getParameter("callback");
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("status", status);
		ResponseJsonUtils.jsonp(response, callback, map);

	}

	/**
	 * 
	 *
	 * @Description:登录成功后跳转主页面
	 * @param request
	 * @param model
	 * @return
	 * @date 2017年9月1日下午1:49:55
	 *
	 */
	@RequestMapping("/main.action")
	public String main(HttpServletRequest request, Model model) {
		SysManager manager = (SysManager) request.getSession().getAttribute(Constant.ADMINUSER);
		SysManager account = sysManagerRepository.findOne(manager.getId());
		List<SysMenu> menus = new ArrayList<SysMenu>();
		if (account.getIsSuper() != null && account.getIsSuper() == 1) {
			menus = sysMenuRepository.findMenusByMenuLevelAndStateOrderByOrderListAsc(1, 1);
		} else {
			SysRole role = account.getRole();
			Set<SysMenu> menuList = role.getMenus();
			for (SysMenu menu : menuList) {
				if (!menus.contains(menu) && menu.getState() == 1 && menu.getMenuLevel() == 1) {
					menus.add(menu);
				}
			}
		}
		model.addAttribute("USER", account);
		model.addAttribute("menus", menus);
		return "main";
	}

	/**
	 * 
	 *
	 * @Description:退出登录
	 * @param request
	 * @return
	 * @date 2017年9月1日下午1:50:09
	 *
	 */
	@RequestMapping(value = "/logout.action")
	public String logout(HttpServletRequest request) {
		HttpSession session = request.getSession();
		session.removeAttribute(Constant.ADMINUSER);
		return "redirect:/login";

	}

}
