package com.skuo.mhs.system.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.skuo.mhs.utils.Configure;
import com.skuo.mhs.utils.Constant;
import com.skuo.mhs.utils.FFmpegUtil;
import com.skuo.mhs.utils.FileMap;
import com.skuo.mhs.utils.UploadUtil;

import net.sf.json.JSONObject;

/**
 * 
 * 系统文件，视频，音频逻辑控制器
 * 
 * @ClassName: FileUploadController
 * @Description: 系统文件，视频，音频逻辑控制器
 * @Company: 四川西谷智慧科技有限公司
 * @author: 余德山
 * @date: 2017年9月1日 上午9:20:01
 * @version: V1.0.0
 */
@Controller
@RequestMapping(value = "/file")
public class FileUploadController {

	/**
	 * 
	 *
	 * @Description:上传文件
	 * @param file
	 *            上传文件
	 * @param request
	 * @return
	 * @date 2017年9月1日下午1:47:04
	 *
	 */
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	@ResponseBody
	public Object upload(@RequestParam(value = "file", required = false) MultipartFile[] file,
			HttpServletRequest request) {

		String savePath = request.getServletContext().getRealPath("/");
		String status = Constant.STATUS_SCUESS;
		String path = "";
		for (MultipartFile myfile : file) {
			if (!myfile.isEmpty()) {

				String suffix = myfile.getOriginalFilename()
						.substring(myfile.getOriginalFilename().lastIndexOf(".") + 1).toLowerCase();
				String fileType = FileMap.getType(suffix);
				if (fileType == null || fileType.equals("")) {
					fileType = "file";
				}
				try {
					path = UploadUtil.writeFile(myfile.getOriginalFilename(), savePath, myfile.getInputStream());
				} catch (IOException e) {
					status = Constant.STATUS_ERROR;
				}

			}
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("state", status);
		map.put("path", path);
		JSONObject json = JSONObject.fromObject(map);
		return json;

	}

	/**
	 * 
	 *
	 * @Description:媒体库(音视频)文件上传
	 * @param file
	 *            上传文件
	 * @param request
	 * @return
	 * @date 2017年9月1日下午1:47:31
	 *
	 */
	@RequestMapping(value = "/mediaUpload", method = RequestMethod.POST)
	@ResponseBody
	public Object mediaUpload(@RequestParam(value = "file", required = false) MultipartFile[] file,
			HttpServletRequest request) {
		Properties props = System.getProperties();
		String os = props.getProperty("os.name");
		String savePath = request.getServletContext().getRealPath("/");
		String path = "";
		String times = "";
		String flv = "";
		String status = Constant.STATUS_SCUESS;
		for (MultipartFile myfile : file) {
			if (!myfile.isEmpty()) {

				String suffix = myfile.getOriginalFilename()
						.substring(myfile.getOriginalFilename().lastIndexOf(".") + 1).toLowerCase();

				String fileType = FileMap.getType(suffix);
				if (fileType == null || fileType.equals("")) {
					fileType = "file";
				}

				try {
					path = UploadUtil.writeFile(myfile.getOriginalFilename(), savePath, myfile.getInputStream());
					String loadPath = savePath + path.substring(1);

					String cmdUrl = "";

					// 读取配置文件
					Configure config = new Configure("application.properties");

					// 读取ffmpeg地址
					String ffmpeg = config.getValue("ffmpeg.path");

					// 判断是否为Windows系统
					if (os.contains("Windows")) {
						cmdUrl = savePath + "tools/ffprobe.exe";
					} else {
						cmdUrl = ffmpeg;
					}
					FFmpegUtil futil = new FFmpegUtil(cmdUrl, loadPath);
					times = futil.StringTime(futil.getRuntime());

					if (fileType.equals("video")) {
						String flvPath = "", mp4Path = "";
						if (suffix.equals("flv")) {

							// flv转mp4格式视频
							flvPath = loadPath;
							mp4Path = loadPath.substring(0, loadPath.lastIndexOf(".")) + ".mp4";
							futil.getChange(mp4Path);
						} else {
							// mp4转flv格式视频
							mp4Path = loadPath;
							flvPath = loadPath.substring(0, loadPath.lastIndexOf(".")) + ".flv";
							futil.getChange(flvPath);
						}

						// mp4存放路径
						path = "/"+mp4Path.substring(savePath.length());

						// flv存放路径
						flv = "/"+flvPath.substring(savePath.length());
					}
				} catch (IOException e) {
					status = Constant.STATUS_ERROR;
				}
			}
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("state", status);
		map.put("path", path);
		map.put("flvPath", flv);
		map.put("times", times);
		JSONObject json = JSONObject.fromObject(map);
		return json;
	}

}
