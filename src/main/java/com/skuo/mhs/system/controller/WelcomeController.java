package com.skuo.mhs.system.controller;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.skuo.mhs.specs.BaseController;
import com.skuo.mhs.system.pojo.SysLoginLog;
import com.skuo.mhs.system.pojo.SysManager;
import com.skuo.mhs.system.pojo.SysMenu;
import com.skuo.mhs.system.pojo.SysOperate;
import com.skuo.mhs.system.pojo.SysRole;
import com.skuo.mhs.system.service.SysManagerRepository;
import com.skuo.mhs.system.service.SysMenuRepository;
import com.skuo.mhs.system.service.SysOperateRepository;
import com.skuo.mhs.utils.Constant;
import com.skuo.mhs.utils.RequestUtils;

/**
 * @ClassName: WelcomeController
 * @Description: 公共操作逻辑控制器
 * @Company: 四川西谷智慧科技有限公司
 * @author: 余德山
 * @date: 2017年9月1日 下午1:41:55
 * @version: V1.0.0
 */
@Controller
@SuppressWarnings("rawtypes")
public class WelcomeController extends BaseController {

	@Autowired
	private SysMenuRepository sysMenuRepository;
	@Autowired
	private SysOperateRepository sysOperateRepository;
	@Autowired
	private SysManagerRepository sysManagerRepository;

	/**
	 * 
	 *
	 * @Description:操作跳转
	 * @param pid
	 *            操作菜单ID
	 * @param op
	 *            操作编码
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @date 2017年9月1日下午1:42:27
	 *
	 */
	@RequestMapping("/opartion.action")
	public String opartion(Long pid, @RequestParam(defaultValue = "list") String op, HttpServletRequest request,
			HttpServletResponse response, Model model) {
		SysManager manager = (SysManager) request.getSession().getAttribute(Constant.ADMINUSER);
		model.addAttribute("region", manager.getRegion());
		String forward = "";
		request.getParameterNames();
		Enumeration enu = request.getParameterNames();
		String LAST_OP_URL = "";
		while (enu.hasMoreElements()) {
			String paraName = (String) enu.nextElement();
			LAST_OP_URL += paraName + "=" + request.getParameter(paraName) + "&";
		}
		LAST_OP_URL = request.getRequestURI() + "?" + LAST_OP_URL.substring(0, LAST_OP_URL.length() - 1);

		request.getSession().removeAttribute(Constant.OP_IN_REQUEST);
		SysManager account = sysManagerRepository.findOne(manager.getId());
		SysMenu menu = sysMenuRepository.findOne(pid);
		List<SysOperate> toolbar = sysOperateRepository.findToolBar(pid);
		List<SysOperate> toolbar1 = new ArrayList<SysOperate>();
		if (account.getIsSuper() == null || account.getIsSuper() != 1) {
			SysRole role = account.getRole();
			List<SysOperate> operates = new ArrayList<SysOperate>();

			Set<SysOperate> list = role.getOperates();
			for (SysOperate operate : list) {
				if (operate.getIsShow() == 1 && operate.getState() == 1) {
					operates.add(operate);
				}
			}

			for (SysOperate operate : toolbar) {
				for (SysOperate operate1 : operates)
					if (operate.equals(operate1)) {
						toolbar1.add(operate);
					}
			}
			toolbar = toolbar1;
		}
		String redirectURL = "";
		Map<String, Object> map2 = (Map<String, Object>) RequestUtils.getQueryParams(request);
		map2.remove("pid");
		map2.remove("op");
		redirectURL = map2.toString().substring(1, map2.toString().length() - 1).replaceAll(", ", "&");
		if ("list".equals(op) || op.contains("list")) {
			request.getSession().removeAttribute(Constant.OP_IN_MENU);
			redirectURL = request.getRequestURI() + "?" + "pid=" + pid + "&op=" + op;
			forward = menu.getUri();
			request.getSession().setAttribute(Constant.LAST_URL, redirectURL);
			request.getSession().setAttribute(Constant.OP_IN_MENU, menu);
		} else {
			request.getSession().removeAttribute(Constant.LAST_OP_URL);
			SysOperate oper = sysOperateRepository.findByMenuAndCode(menu, op);
			request.getSession().setAttribute(Constant.OP_IN_REQUEST, oper);
			request.getSession().setAttribute(Constant.LAST_OP_URL, LAST_OP_URL);
			forward = oper.getUrl();
		}
		model.addAttribute("ops", toolbar);
		if (forward == null || forward.equals("")) {
			return noright();
		}
		return "forward:" + forward;
	}

	/**
	 * 
	 *
	 * @Description:没有权限跳转
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @date 2017年9月1日下午1:45:09
	 *
	 */
	@RequestMapping(value = "/noright.action")
	public String noright(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
		model.addAllAttributes(RequestUtils.getRequestMap(request));
		return "noright";
	}

	/**
	 * 
	 *
	 * @Description:操作成功
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @date 2017年9月1日下午1:45:44
	 *
	 */
	@RequestMapping("/success.action")
	public String lostLogin(HttpServletRequest request, HttpServletResponse response, Model model) {
		String url = (String) request.getSession().getAttribute(Constant.LAST_URL);
		model.addAttribute("url", url);
		return "success";
	}

	/**
	 * 
	 *
	 * @Description:跳转上一次访问列表页面,返回使用
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @date 2017年9月1日下午1:45:59
	 *
	 */
	@RequestMapping("/lastVisited.action")
	public String lastVisited(HttpServletRequest request, HttpServletResponse response, Model model) {
		String url = (String) request.getSession().getAttribute(Constant.LAST_URL);
		return "forward:" + url;
	}

	/**
	 * 
	 *
	 * @Description:错误页面
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @date 2017年9月1日下午1:46:09
	 *
	 */
	@RequestMapping(value = "/error.action")
	public String error(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
		model.addAllAttributes(RequestUtils.getRequestMap(request));
		return "error";
	}

	/**
	 * 
	 *
	 * @Description:系统首页
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @date 2017年9月1日上午11:58:51
	 *
	 */
	@RequestMapping(value = "/home.action")
	public String home(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
		SysManager manager = (SysManager) request.getSession().getAttribute(Constant.ADMINUSER);
		String region = manager.getRegion();
		if (region == null || region.equals("") || region.equals("100000")) {
			region = "1";
		} else if (region.endsWith("0000")) {
			region = region.substring(0, 2);
		} else if (region.endsWith("00")) {
			region = region.substring(0, 4);
		}
		region = region + "%";
		SysLoginLog log = (SysLoginLog) request.getSession().getAttribute("LoginLog");
		model.addAttribute("log", log);
		return "home";
	}

}
