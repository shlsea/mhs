package com.skuo.mhs.system.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 
 * 
 * @ClassName: IndexController
 * @Description: 欢迎页面逻辑控制器
 * @Company: 四川西谷智慧科技有限公司
 * @author: 余德山
 * @date: 2017年9月1日 上午10:43:19
 * @version: V1.0.0
 */
@Controller
public class IndexController {

	/**
	 * 
	 *
	 * @Description:登录页面
	 * @return
	 * @date 2017年9月1日下午1:48:14
	 *
	 */
	@RequestMapping("/")
	public String index() {
		return "login";
	}

	/**
	 * 
	 *
	 * @Description:登录页面
	 * @return
	 * @date 2017年9月1日下午1:48:25
	 *
	 */
	@RequestMapping("/login")
	public String login() {
		return "login";
	}

	/**
	 * 
	 *
	 * @Description:操作帮助页面
	 * @return
	 * @date 2017年9月1日下午1:48:38
	 *
	 */
	@RequestMapping("/instructions")
	public String instructions() {
		return "instructions";
	}

	/**
	 * 
	 *
	 * @Description:Sisseion过期
	 * @return
	 * @date 2017年9月1日下午1:48:54
	 *
	 */
	@RequestMapping("/lostLogin")
	public String lostLogin() {
		return "lostLogin";
	}

	/**
	 * 
	 *
	 * @Description:欢迎页面
	 * @return
	 * @date 2017年9月1日下午1:49:04
	 *
	 */
	@RequestMapping("/index")
	public String indexPage() {
		return "index";
	}
}
