package com.skuo.mhs.system.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.baidu.ueditor.ActionEnter;

/**
 * 
 * 
 * @ClassName: UeditorController
 * @Description: 百度编辑器控制器
 * @Company: 四川西谷智慧科技有限公司
 * @author: 余德山
 * @date: 2017年9月1日 下午1:40:40
 * @version: V1.0.0
 */
@Controller
public class UeditorController {
	
	/**
	 * 
	 *
	 * @Description:百度编辑器控制
	 * @param request
	 * @param response
	 * @date 2017年9月1日下午1:40:53
	 *
	 */
	@RequestMapping(value = "/ueditor/config")
	public void config(HttpServletRequest request, HttpServletResponse response) {
		// response.setContentType("application/json");
		String rootPath = request.getSession().getServletContext().getRealPath("/");
		try {
			String exec = new ActionEnter(request, rootPath).exec();
			PrintWriter writer = response.getWriter();
			writer.write(exec);
			writer.flush();
			writer.close();
		} catch (IOException | JSONException e) {
			e.printStackTrace();
		}
	}
}
