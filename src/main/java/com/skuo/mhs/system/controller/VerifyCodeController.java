package com.skuo.mhs.system.controller;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.skuo.mhs.utils.VerifyCodeUtil;

/**
 * 
 * 
 * @ClassName: VerifyCodeController
 * @Description: 登录验证码控制器
 * @Company: 四川西谷智慧科技有限公司
 * @author: 余德山
 * @date: 2017年9月1日 下午1:41:14
 * @version: V1.0.0
 */
@Controller
public class VerifyCodeController {

	/**
	 * 
	 *
	 * @Description:获取验证码
	 * @param request
	 * @param response
	 * @date 2017年9月1日下午1:41:28
	 *
	 */
	@RequestMapping("/code")
	public void getCode(HttpServletRequest request, HttpServletResponse response) {
		response.setHeader("Pragma", "no-cache");
		response.setHeader("Cache-Control", "no-cache");
		response.setDateHeader("Expires", 0);
		String verifyCode = VerifyCodeUtil.generateTextCode(VerifyCodeUtil.TYPE_UPPER_ONLY, 4, null);
		request.getSession().setAttribute("verifyCode", verifyCode);

		response.setContentType("image");
		BufferedImage bufferedImage = VerifyCodeUtil.generateImageCode(verifyCode, 92, 30, 3, true, Color.WHITE,
				Color.BLACK, null);
		try {
			ImageIO.write(bufferedImage, "JPEG", response.getOutputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
