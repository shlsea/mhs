package com.skuo.mhs.system.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.skuo.mhs.form.PageForm;
import com.skuo.mhs.system.pojo.SysManager;
import com.skuo.mhs.system.pojo.SysRegion;
import com.skuo.mhs.system.pojo.SysRole;
import com.skuo.mhs.system.service.SysManagerRepository;
import com.skuo.mhs.system.service.SysRegionRepository;
import com.skuo.mhs.system.service.SysRoleRepository;
import com.skuo.mhs.utils.Constant;
import com.skuo.mhs.utils.Md5Tool;

import net.sf.json.JSONObject;

/**
 * 
 * 
 * @ClassName: SysManagerController
 * @Description: 系统用户管理逻辑控制器
 * @Company: 四川西谷智慧科技有限公司
 * @author: 余德山
 * @date: 2017年9月1日 下午1:39:14
 * @version: V1.0.0
 */
@Controller
@RequestMapping(value = "/sys_manager")
@SuppressWarnings("unchecked")
public class SysManagerController {

	@Autowired
	private SysManagerRepository sysManagerRepository;
	@Autowired
	private SysRoleRepository sysRoleRepository;
	@Autowired
	private SysRegionRepository sysRegionRepository;

	/**
	 * 
	 *
	 * @Description:跳转用户管理页面
	 * @param request
	 * @param model
	 * @return
	 * @date 2017年9月1日下午1:57:47
	 *
	 */
	@RequestMapping("/page.action")
	public String page(HttpServletRequest request, Model model) {
		SysManager manager = (SysManager) request.getSession().getAttribute(Constant.ADMINUSER);
		SysRegion region = sysRegionRepository.findOne(manager.getRegion());
		model.addAttribute("region", region);
		return "system/manager_list";
	}

	/**
	 * 
	 *
	 * @Description:读取用户列表
	 * @param pageForm
	 *            分页表单
	 * @param dto
	 *            查询跳转封装
	 * @return
	 * @date 2017年9月1日下午1:58:03
	 *
	 */
	@RequestMapping(value = "/list.action", method = RequestMethod.POST)
	@ResponseBody
	public Object list(PageForm pageForm, SysManager dto) {
		if (dto.getRegion() != null && !dto.getRegion().equals("")) {
			SysRegion region = sysRegionRepository.findOne(dto.getRegion());
			if (region != null) {
				dto.setRegion(region.getSubRegion(region));
			}
		}
		if (dto.getRole() != null && dto.getRole().getCode() != null && !dto.getRole().getCode().equals("")) {
			SysRole role = sysRoleRepository.findByCode(dto.getRole().getCode());
			if (role != null) {
				dto.setRole(role);
			} else {
				dto.setRole(null);
			}
		}

		Map<String, Object> dataMap = sysManagerRepository.findByAuto(dto, pageForm);
		Map<String, Object> map = new HashMap<String, Object>();
		List<SysManager> list = (List<SysManager>) dataMap.get("rows");
		List<Map<String, Object>> _list = new ArrayList<Map<String, Object>>();
		for (SysManager data : list) {
			Map<String, Object> _map = new HashMap<String, Object>();
			_map.put("id", data.getId());
			_map.put("name", data.getName());
			_map.put("state", data.getState());
			_map.put("account", data.getAccount());
			_map.put("phone", data.getPhone());
			_map.put("address", data.getAddress());
			_map.put("roles.code", data.getRole() != null && data.getRole().getCode() != null
					? sysRoleRepository.findOne(data.getRole().getId()).getName() : "");
			_map.put("region",
					data.getRegion() != null ? sysRegionRepository.findOne(data.getRegion()).getFullName() : null);

			_list.add(_map);
		}
		map.put("rows", _list);
		map.put("total", dataMap.get("total"));
		return map;
	}

	/**
	 * 
	 *
	 * @Description:保存用户信息
	 * @param dto
	 *            用户实体对象
	 * @param model
	 * @param request
	 * @return
	 * @date 2017年9月1日下午1:58:33
	 *
	 */
	@RequestMapping(value = "/save.action", method = RequestMethod.POST)
	public String save(@ModelAttribute SysManager dto, Model model, HttpServletRequest request) {
		String pwd = Md5Tool.getMd5(dto.getPassword());
		if (dto.getId() != null) {
			SysManager data = sysManagerRepository.findOne(dto.getId());
			if (!dto.getPassword().equals(data.getPassword())) {
				dto.setPassword(pwd);
			}
			BeanUtils.copyProperties(dto, data, new String[] { "id","account"});
			sysManagerRepository.save(data);

		} else {
			dto.setPassword(pwd);
			sysManagerRepository.save(dto);
		}
		return "forward:/success.action";
	}

	/**
	 * 
	 *
	 * @Description:新增/编辑用户账号信息
	 * @param id
	 *            用户ID
	 * @param request
	 * @param model
	 * @return
	 * @date 2017年9月1日下午1:58:59
	 *
	 */
	@RequestMapping("/edit.action")
	public String edit(Long id, HttpServletRequest request, Model model) {
		SysManager manager = (SysManager) request.getSession().getAttribute(Constant.ADMINUSER);
		SysRegion area = sysRegionRepository.findOne(manager.getRegion());
		model.addAttribute("region", area);
		SysManager dto = new SysManager();
		Long roleId = 0L;
		String areacode = "";
		if (id != null) {
			dto = sysManagerRepository.findOne(id);
			roleId = dto.getRole() != null ? dto.getRole().getId() : 0L;
			if (dto.getRegion() != null && !dto.getRegion().equals("")) {
				SysRegion region = sysRegionRepository.findOne(dto.getRegion());
				areacode = region != null ? region.getOrderNo() : "";
			}
		}
		List<SysRole> roleList = sysRoleRepository.findByStateOrderByCode(1L);
		model.addAttribute("dto", dto);
		model.addAttribute("roles", roleList);
		model.addAttribute("roleId", roleId);
		model.addAttribute("area", areacode);
		model.addAttribute("region", area);
		return "system/manager_input";
	}

	/**
	 * 
	 *
	 * @Description:根据id删除用户
	 * @param ids
	 *            用户id数组
	 * @return
	 * @date 2017年9月1日下午1:59:23
	 *
	 */
	@RequestMapping("/remove.action")
	@ResponseBody
	public Object delete(Long[] ids) {
		String status = Constant.STATUS_SCUESS;
		String msg;
		try {
			for (Long id : ids) {
				SysManager user = sysManagerRepository.findOne(id);
				user.setState(-1L);
				sysManagerRepository.save(user);
			}
			msg = "删除成功！";
		} catch (Exception e) {
			status = Constant.STATUS_ERROR;
			msg = "删除失败！";
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("state", status);
		map.put("msg", msg);
		JSONObject json = JSONObject.fromObject(map);
		return json;
	}

	/**
	 * 
	 *
	 * @Description:判断账号是否存在
	 * @param account
	 *            当前输入账号
	 * @return
	 * @date 2017年9月1日下午1:59:48
	 *
	 */
	@RequestMapping(value = "/findOne.action", method = RequestMethod.POST)
	@ResponseBody
	public Object findOne(String account) {
		boolean is = true;
		SysManager manager = sysManagerRepository.findByAccount(account);
		if (manager != null) {
			is = false;
		}
		return is;
	}

}
