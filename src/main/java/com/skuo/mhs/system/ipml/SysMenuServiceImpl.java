package com.skuo.mhs.system.ipml;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.skuo.mhs.system.pojo.SysMenu;
import com.skuo.mhs.system.service.SysMenuRepository;

/**
 * 
 * 
 * @ClassName: SysMenuServiceImpl
 * @Description: 系统菜单具体实现类
 * @Company: 四川西谷智慧科技有限公司
 * @author: 余德山
 * @date: 2017年9月1日 下午3:24:12
 * @version: V1.0.0
 */
@Service
public class SysMenuServiceImpl {

	@Autowired
	private SysMenuRepository sysMenuRepository;

	/**
	 * @Description:查询所有有效菜单
	 * @return
	 * @date 2017年9月1日下午3:24:37
	 *
	 */
	public List<SysMenu> findAll() {
		List<SysMenu> list = new ArrayList<SysMenu>();
		List<SysMenu> menus = sysMenuRepository.findMenusByMenuLevelAndStateOrderByOrderListAsc(1, 1);
		for (SysMenu menu : menus) {
			if (menu.getParent() != null) {
				list.add(menu);
			}
		}

		return list;
	}

	/**
	 * 
	 *
	 * @Description:递归保存菜单
	 * @param sysMenu
	 *            菜单实体
	 * @return
	 * @date 2017年9月1日下午3:25:00
	 *
	 */
	public SysMenu save(SysMenu sysMenu) {
		// 判断当前菜单等级，在父类菜单等级上加1
		Long level = sysMenu.getParent() != null ? sysMenu.getParent().getMenuLevel() + 1 : 0L;
		sysMenu.setMenuLevel(level);
		// 获取当前菜单排序，默认为1
		Long i = sysMenu.getOrderList() > 0 ? sysMenu.getOrderList() : 1;
		// 强制转换字符排序
		char c1 = (char) (i + 96);
		// 拼接字符串排序
		String ordno = (sysMenu.getParent() != null ? sysMenu.getParent().getOrdno() : "") + c1 + ",";
		sysMenu.setOrdno(ordno);
		sysMenu = sysMenuRepository.save(sysMenu);
		// 获取当前菜单子菜单
		List<SysMenu> menus = sysMenuRepository.findMenusByParentOrderByOrderListAsc(sysMenu);
		if (!menus.isEmpty()) {
			for (SysMenu menu : menus) {
				// 递归保存子菜单
				save(menu);
			}

		}
		return sysMenu;

	}

	/**
	 * 
	 *
	 * @Description:循环遍历当前栏目及子栏目
	 * @param dto
	 *            菜单
	 * @return
	 * @date 2017年9月1日下午3:25:24
	 *
	 */
	public Map<String, Object> findMenuByParent(SysMenu dto) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", dto.getId());
		map.put("name", dto.getName());
		if (dto.getSysMenus().size() > 0) {
			List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
			for (SysMenu data : dto.getSysMenus()) {
				Map<String, Object> dataMap = findMenuByParent(data);
				list.add(dataMap);
			}
			map.put("child", list);
		}
		return map;
	}

}