package com.skuo.mhs.system.ipml;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.skuo.mhs.system.pojo.SysRegion;
import com.skuo.mhs.system.service.SysRegionRepository;

/**
 * 
 * 
 * @ClassName: SysRegionServiceImpl
 * @Description: 系统地区管理具体实现类
 * @Company: 四川西谷智慧科技有限公司
 * @author: 余德山
 * @date: 2017年9月1日 下午3:25:50
 * @version: V1.0.0
 */
@Service
public class SysRegionServiceImpl {

	@Autowired
	private SysRegionRepository sysRegionRepository;

	/**
	 * @Description:递归保存地区
	 * @param region
	 *            地区
	 * @return
	 * @date 2017年9月1日下午3:26:13
	 *
	 */
	public SysRegion save(SysRegion region) {
		String orderNo = region.getPregion() != null ? region.getPregion().getOrderNo() + "," + region.getId()
				: region.getId();
		region.setOrderNo(orderNo);
		region = sysRegionRepository.save(region);
		List<SysRegion> datas = sysRegionRepository.findRegions(region.getId());
		if (!datas.isEmpty()) {
			for (SysRegion data : datas) {
				// 递归保存子分项
				save(data);
			}

		}
		return region;

	}

}