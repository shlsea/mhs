package com.skuo.mhs.system.pojo;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Where;

/**
 * 系统用户实体
* @项目名称 xzzxw
* @作者 余德山
* @包名称 com.skou.zxw.pojo
* @功能描述 系统用户属性设置
* @日期 2017年1月6日
* @版本 V1.0.0
 */
@Entity
@Where(clause = "state > -1")
public class SysManager {

	
	/**
	 * 主键
	 */
	@Id
	@GeneratedValue
	private Long id;
	
	/**
	 * 账号
	 */
	private String account;
	
	/**
	 * 用户姓名
	 */
	private String name;
	
	/**
	 *密码 
	 */
	private String password;
	
	/**
	 * 电话
	 */
	private String phone;
	
	/**
	 * 电子邮箱
	 */
	private String email;
	
	/**
	 * 身份证号码
	 */
	private String idCard;
	
	/**
	 * 详细地址
	 */
	private String address;
	
	/**
	 * 所属地区
	 */
	private String region;
	
	/**
	 * 所属角色
	 */
	@ManyToOne(fetch = FetchType.LAZY, cascade ={CascadeType.PERSIST,CascadeType.MERGE})
	@JoinColumn(name = "role", nullable = true, insertable = true, updatable = true)
	private SysRole role;
	
	
	
	/**
	 * 添加时间
	 */
	private Date addTime;
	
	/**
	 *修改时间 
	 */
	private Date updateTime;
	
	/**
	 * 状态
	 */
	private Long state;
	
	/**
	 * 是否超级管理员
	 */
	private Long isSuper;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}


	public Date getAddTime() {
		return addTime;
	}

	public void setAddTime(Date addTime) {
		this.addTime = addTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Long getState() {
		return state;
	}

	public void setState(Long state) {
		this.state = state;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public SysRole getRole() {
		return role;
	}

	public void setRole(SysRole role) {
		this.role = role;
	}

	public Long getIsSuper() {
		return isSuper;
	}

	public void setIsSuper(Long isSuper) {
		this.isSuper = isSuper;
	}

	
	
}
