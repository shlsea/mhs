package com.skuo.mhs.system.pojo;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OrderBy;

import org.springframework.transaction.annotation.Transactional;



/**
 * 系统用户角色实体
* @项目名称 xzzxw 
* @作者 余德山
* @包名称 com.skou.zxw.pojo
* @功能描述 系统用户角色属性设置
* @日期 2017年1月6日
* @版本 V1.0.0
 */
@Entity
public class SysRole {

	/**
	 * 主键
	 */
	@Id
	@GeneratedValue
	private Long id;
	
	/**
	 * 角色名称
	 */
	private String name;
	
	/**
	 *	角色代码 
	 */
	private String code;
	
	/**
	 * 是否有效
	 */
	private Long state;
	
	/**
	 * 角色操作
	 */
	@ManyToMany(cascade={CascadeType.REFRESH},fetch=FetchType.EAGER)
	private Set<SysOperate> operates;
	
	@ManyToMany(cascade={CascadeType.REFRESH},fetch=FetchType.EAGER)
	@OrderBy("ordno")
	private Set<SysMenu> menus;

	public SysRole(){
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Long getState() {
		return state;
	}

	public void setState(Long state) {
		this.state = state;
	}

	public Set<SysOperate> getOperates() {
		return operates;
	}

	public void setOperates(Set<SysOperate> operates) {
		this.operates = operates;
	}

	public Set<SysMenu> getMenus() {
		return menus;
	}

	public void setMenus(Set<SysMenu> menus) {
		this.menus = menus;
	}
	
	@Transactional
	public String getMenusId() {
		Set<SysMenu> list=this.getMenus();
		String str="";
		for(SysMenu menu:list){
			if(!str.contains(","+menu.getId()))
			str+=","+menu.getId();
		}
	
		return str;
	}

	@Transactional
	public String getOperatesId() {
		Set<SysOperate> list=this.getOperates();
		String str="";
		for(SysOperate op:list){
			if(!str.contains(","+op.getId()+"-"+op.getCode()+","))
			str+=","+op.getId()+"-"+op.getCode();
		}
		
		return str;
	}

}
