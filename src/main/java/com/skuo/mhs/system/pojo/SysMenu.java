package com.skuo.mhs.system.pojo;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 系统菜单实体
 * 
 * @项目名称 xzzxw
 * @作者 余德山
 * @包名称 com.skou.zxw.pojo
 * @功能描述 系统菜单属性设置
 * @日期 2017年1月6日
 * @版本 V1.0.0
 */
@Entity
@JsonIgnoreProperties(value={"hibernateLazyInitializer","handler","fieldHandler"}) 
public class SysMenu {

	/**
	 * 主键
	 */
	@Id
	@GeneratedValue
	private Long id;

	/**
	 * 菜单名称
	 */
	private String name;

	/**
	 * 菜单图标
	 */
	private String icon;

	/**
	 * 备注
	 */
	private String remark;

	/**
	 * 排序
	 */
	private Long orderList;

	/**
	 * 菜单等级
	 */
	private Long menuLevel;

	/**
	 * 子菜单
	 */
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST, mappedBy = "parent")
	@OrderBy("orderList")
	private Set<SysMenu> sysMenus = new HashSet<SysMenu>(0);

	/**
	 * 父类菜单
	 */
	@ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinColumn(name = "parent", nullable = true, insertable = true, updatable = true)
	private SysMenu parent;

	/**
	 * 页面地址
	 */
	private String uri;

	/**
	 * 状态
	 */
	private Long state;

	/**
	 * 字符排序
	 */
	private String ordno;

	public SysMenu() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Long getOrderList() {
		return orderList;
	}

	public void setOrderList(Long orderList) {
		this.orderList = orderList;
	}

	public Long getMenuLevel() {
		return menuLevel;
	}

	public void setMenuLevel(Long menuLevel) {
		this.menuLevel = menuLevel;
	}

	public Set<SysMenu> getSysMenus() {
		return sysMenus;
	}

	public void setSysMenus(Set<SysMenu> sysMenus) {
		this.sysMenus = sysMenus;
	}

	public SysMenu getParent() {
		return parent;
	}

	public void setParent(SysMenu parent) {
		this.parent = parent;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public Long getState() {
		return state;
	}

	public void setState(Long state) {
		this.state = state;
	}

	public String getOrdno() {
		return ordno;
	}

	public void setOrdno(String ordno) {
		this.ordno = ordno;
	}

	@Override
	public String toString() {
		return "SysMenu [id=" + id + ", name=" + name + ", state=" + state + ", uri=" + uri+ ", ordno=" + ordno
				+ ", menuLevel=" + menuLevel + ", orderList=" + orderList + ", remark=" + remark + ", icon="
				+ icon + "]";
	}
}
