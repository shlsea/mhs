package com.skuo.mhs.system.pojo;

import javax.persistence.*;

/**
 * 系统数据字典实体
* @项目名称 xzzxw  
* @作者 余德山
* @包名称 com.skou.zxw.pojo
* @功能描述 TODO
* @日期 2017年2月13日
* @版本 V1.0.0
 */
@Entity
public class SysDict {

	/***
	 * 数据字典编码<br>
	 * 主键
	 */
	@Id
	@Column(name = "skey", nullable = false, length = 20)
	private String skey;

	/***
	 * 字典编码名称
	 */
	private String name;

	/***
	 * 字典类型
	 */
	private String type;
	
	/***
	 * 状态
	 *
	 * 1、有效  0、无效
	 */
	private Long state;
	
	/***
	 * 备注
	 */
	private String remark;
	
	/***
	 * 排序
	 */
	private String orderNo;
	
	/***
	 * 父类字典
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pkey", nullable = true, insertable = true, updatable = true)
	@OrderBy("orderList")
	private SysDict pkey;

	/**
	 * 字典等级
	 */
	private Long level;

	public String getSkey() {
		return skey;
	}

	public void setSkey(String skey) {
		this.skey = skey;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Long getState() {
		return state;
	}

	public void setState(Long state) {
		this.state = state;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	
	public SysDict getPkey() {
		return pkey;
	}

	public void setPkey(SysDict pkey) {
		this.pkey = pkey;
	}

	public Long getLevel() {
		return level;
	}

	public void setLevel(Long level) {
		this.level = level;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	
	
}
