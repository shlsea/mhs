package com.skuo.mhs.system.pojo;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


/**
 * 系统操作实体
* @项目名称 xzzxw 
* @作者 余德山
* @包名称 com.skou.zxw.pojo
* @功能描述 系统操作实体属性
* @日期 2017年1月6日
* @版本 V1.0.0
 */
@Entity
public class SysOperate {

	/**
	 * 主键
	 */
	@Id
	@GeneratedValue
	private Long id;
	
	/**
	 * 操作名称
	 */
	private String name;
	
	/**
	 * 所属菜单
	 */
	@ManyToOne(fetch = FetchType.LAZY,cascade ={CascadeType.MERGE,CascadeType.PERSIST})
	@JoinColumn(name = "menu", nullable = true, insertable = true, updatable = true)
	private SysMenu menu;
	
	/**
	 * 操作地址
	 */
	private String url;
	
	/**
	 * 操作代码
	 */
	private String code;
	
	/**
	 * 工具栏图标样式
	 */
	private String icon;
	
	/**
	 * 是否显示工具栏
	 *
	 * 1：显示工具栏
	 */
	private Long isShow;
	
	/**
	 * 排序
	 */
	private Long orderList;
	
	/**
	 * 状态
	 *
	 * 1：状态有效
	 */
	private Long state;
	
	public SysOperate(){
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public SysMenu getMenu() {
		return menu;
	}

	public void setMenu(SysMenu menu) {
		this.menu = menu;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public Long getIsShow() {
		return isShow;
	}

	public void setIsShow(Long isShow) {
		this.isShow = isShow;
	}

	public Long getOrderList() {
		return orderList;
	}

	public void setOrderList(Long orderList) {
		this.orderList = orderList;
	}

	public Long getState() {
		return state;
	}

	public void setState(Long state) {
		this.state = state;
	}
	
	
}
