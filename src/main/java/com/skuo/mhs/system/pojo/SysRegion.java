package com.skuo.mhs.system.pojo;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

 /**
  * 系统地区实体
 * @项目名称 xzzxw  
 * @作者 余德山
 * @包名称 com.skou.zxw.pojo
 * @功能描述 地区实体属性
 * @日期 2017年1月6日
 * @版本 V1.0.0
  */
@Entity
public class SysRegion {

	/**
	 * 地区编码，主键
	 */
	@Id
	@Column(name = "id",  nullable = false, length = 6)
	private String id;
	
	/**
	 * 地区编码
	 */
	private String region;
	
	/**
	 * 地区名
	 */
    private String name;
    
    /**
     * 全名
     */
    private String fullName;
    
    /**
     * 简称
     */
    private String shortName;
    
    /**
     * 状态 1表示正常 
     */
    private Long status;
    
    /**
     * gps经度
     */
    private Double gps_x;
    
    /**
     * gps纬度
     */
    private Double gps_y;
    
    /**
     * google经度
     */
    private Double google_x;
    
    /**
     * google纬度
     */
    private Double google_y;
    
    /**
     * 百度经度
     */
    private Double baidu_x;
    
    /**
     * 百度纬度
     */
    private Double baidu_y;
    
    /**
     * 政府地址
     */
    private String address;
    
    /**
     * 等级
     */
    @Column(name = "region_level")
    private Long level;
	
	
	/**
	 * 父级地区
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "pregion", nullable = true, insertable = true, updatable = true)
	private SysRegion pregion;
	
	/**
	 * 子集地区
	 */
	@OneToMany(fetch = FetchType.LAZY,cascade = CascadeType.PERSIST,mappedBy = "pregion")
	@OrderBy("region")
	private Set<SysRegion> regions= new HashSet<SysRegion>(0);
	
	/**
	 * 排序（主要使用于级联中）
	 */
	private String orderNo;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
	
	
	public Set<SysRegion> getRegions() {
		return regions;
	}

	public void setRegions(Set<SysRegion> regions) {
		this.regions = regions;
	}

	
	
	
	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public Double getGps_x() {
		return gps_x;
	}

	public void setGps_x(Double gps_x) {
		this.gps_x = gps_x;
	}

	public Double getGps_y() {
		return gps_y;
	}

	public void setGps_y(Double gps_y) {
		this.gps_y = gps_y;
	}

	public Double getGoogle_x() {
		return google_x;
	}

	public void setGoogle_x(Double google_x) {
		this.google_x = google_x;
	}

	public Double getGoogle_y() {
		return google_y;
	}

	public void setGoogle_y(Double google_y) {
		this.google_y = google_y;
	}

	public Double getBaidu_x() {
		return baidu_x;
	}

	public void setBaidu_x(Double baidu_x) {
		this.baidu_x = baidu_x;
	}

	public Double getBaidu_y() {
		return baidu_y;
	}

	public void setBaidu_y(Double baidu_y) {
		this.baidu_y = baidu_y;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Long getLevel() {
		return level;
	}

	public void setLevel(Long level) {
		this.level = level;
	}

	public SysRegion getPregion() {
		return pregion;
	}

	public void setPregion(SysRegion pregion) {
		this.pregion = pregion;
	}
	

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getSubRegion(SysRegion region){
		String hqlArea=null;
		if(region.getLevel()>0){
			if(region.getLevel()==1){
				hqlArea=region.getId().substring(0,2);
			}else if(region.getLevel()==2){
				hqlArea=region.getId().substring(0,4);
			}else if(region.getLevel()==3){
				hqlArea=region.getId();
			}
		}
		return hqlArea;	
	}
	
	
	public String toString() {
		return "SysRegion [id=" + id + ", name=" + name + ",regions="
				+ regions + ",orderNo="+ orderNo + "]";
	}
}
