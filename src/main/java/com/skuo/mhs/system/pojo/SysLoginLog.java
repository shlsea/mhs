package com.skuo.mhs.system.pojo;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * 用户登录日志实体
* @项目名称 xzzxw
* @作者 余德山
* @包名称 com.skou.zxw.pojo
* @功能描述 管理登录日志属性
* @日期 2017年1月12日
* @版本 V1.0.0
 */
@Entity
public class SysLoginLog {
	
	/**
	 * id,主键
	 */
	@Id
	@GeneratedValue
	private Long id;
	
	/**
	 * 登录账号
	 */
	private String account;
	
	/**
	 * IP地址
	 */
	private String ip;
	
	/**
	 * 浏览器
	 */
	private String browser;
	
	/**
	 * 浏览器宽度
	 */
	private String width;
	
	/**
	 * 浏览器高度
	 */
	private String height;
	
	/**
	 * 登录时间
	 */
	private Date addTime;
	
	/**
	 * 登录状态<br>
	 * 1:成功<br>
	 * 0:失败
	 */
	private Long state;
	
	public SysLoginLog(){
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getBrowser() {
		return browser;
	}

	public void setBrowser(String browser) {
		this.browser = browser;
	}

	public String getWidth() {
		return width;
	}

	public void setWidth(String width) {
		this.width = width;
	}

	public String getHeight() {
		return height;
	}

	public void setHeight(String height) {
		this.height = height;
	}

	public Date getAddTime() {
		return addTime;
	}

	public void setAddTime(Date addTime) {
		this.addTime = addTime;
	}

	public Long getState() {
		return state;
	}

	public void setState(Long state) {
		this.state = state;
	} 
	
	
}