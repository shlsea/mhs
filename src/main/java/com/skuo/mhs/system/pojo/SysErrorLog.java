package com.skuo.mhs.system.pojo;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * 用户操作错误日志实体
* @项目名称 xzzxw
* @作者 余德山
* @包名称 com.skou.zxw.pojo
* @功能描述 管理用户操作错误日志属性
* @日期 2017年1月12日
* @版本 V1.0.0
 */
@Entity
public class SysErrorLog {


	/**
	 * id,主键
	 */
	@Id
	@GeneratedValue
	private Long id;
	
	/**
	 * 操作账号
	 */
	private String account;
	
	/**
	 * 操作名称
	 */
	private String OpName;
	
	/**
	 * 操作所属菜单
	 */
	private String menuName;
	
	/**
	 * 操作地址
	 */
	private String url;
	
	/**
	 * 操作参数
	 */
	@Type(type="text")
	private String parames;
	
	/**
	 * 错误日志
	 */
	@Type(type="text")
	private String content;
	
	/**
	 * 操作时间
	 */
	private Date addTime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getOpName() {
		return OpName;
	}

	public void setOpName(String opName) {
		OpName = opName;
	}

	public String getMenuName() {
		return menuName;
	}

	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getParames() {
		return parames;
	}

	public void setParames(String parames) {
		this.parames = parames;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getAddTime() {
		return addTime;
	}

	public void setAddTime(Date addTime) {
		this.addTime = addTime;
	}
	
	
	
}
