package com.skuo.mhs.system.service;

import org.springframework.stereotype.Service;

import com.skuo.mhs.support.CustomRepository;
import com.skuo.mhs.system.pojo.SysErrorLog;

/**
 * 
 * 
 * @ClassName: SysErrorLogRepository
 * @Description: 系统管理错误日志实现DAO
 * @Company: 四川西谷智慧科技有限公司
 * @author: 余德山
 * @date: 2017年9月1日 下午2:21:02
 * @version: V1.0.0
 */
@Service
public interface SysErrorLogRepository extends CustomRepository<SysErrorLog, Long> {

}
