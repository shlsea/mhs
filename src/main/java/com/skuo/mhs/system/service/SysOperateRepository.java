package com.skuo.mhs.system.service;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import com.skuo.mhs.support.CustomRepository;
import com.skuo.mhs.system.pojo.SysMenu;
import com.skuo.mhs.system.pojo.SysOperate;

/**
 * @ClassName: SysOperateRepository
 * @Description: 系统管理菜单操作实现DAO
 * @Company: 四川西谷智慧科技有限公司
 * @author: 余德山
 * @date: 2017年9月1日 下午3:16:30
 * @version: V1.0.0
 */
@Service
public interface SysOperateRepository extends CustomRepository<SysOperate, Long> {

	/**
	 * @Description:查询菜单权限显示按钮
	 * @param id
	 *            菜单id
	 * @return
	 * @date 2017年9月1日下午3:16:51
	 *
	 */
	@Query("select t from SysOperate t where t.state = 1 and t.isShow=1 and t.menu.id = ?1 order by t.orderList ")
	List<SysOperate> findToolBar(Long id);

	/**
	 * @Description:根据菜单查询有效操作
	 * @param id
	 *            菜单id
	 * @return
	 * @date 2017年9月1日下午3:16:56
	 *
	 */
	@Query("select t from SysOperate t where t.state = 1 and t.menu.id = ?1 order by t.orderList ")
	List<SysOperate> findByMenuId(Long id);

	/**
	 * @Description:根据菜单和操作代码联合查询操作数据
	 * @param menu
	 *            所属菜单
	 * @param code
	 *            操作编码
	 * @return
	 * @date 2017年9月1日下午3:17:03
	 *
	 */
	SysOperate findByMenuAndCode(SysMenu menu, String code);

}
