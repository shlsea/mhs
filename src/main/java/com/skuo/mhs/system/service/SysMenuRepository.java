package com.skuo.mhs.system.service;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import com.skuo.mhs.support.CustomRepository;
import com.skuo.mhs.system.pojo.SysMenu;

/**
 * 
 * 
 * @ClassName: SysMenuRepository
 * @Description: 系统管理菜单实现实现DAO
 * @Company: 四川西谷智慧科技有限公司
 * @author: 余德山
 * @date: 2017年9月1日 下午3:04:01
 * @version: V1.0.0
 */
@Service
public interface SysMenuRepository extends CustomRepository<SysMenu, Long> {

	/**
	 * 
	 * 
	 * @Description:读取有效菜单列表
	 * @param state
	 *            菜单状态
	 * @return
	 * @date 2017年9月1日下午3:04:26
	 *
	 */
	List<SysMenu> findByState(Long state);

	/**
	 * 
	 * 
	 * @Description:根据菜单等级和状态并按照排序查询
	 * @param level
	 *            菜单等级
	 * @param state
	 *            菜单状态
	 * @return
	 * @date 2017年9月1日下午3:04:38
	 *
	 */
	List<SysMenu> findMenusByMenuLevelAndStateOrderByOrderListAsc(long level, long state);

	/**
	 * 
	 * 
	 * @Description:根据父类菜单查询子菜单
	 * @param parent
	 *            父级菜单
	 * @return
	 * @date 2017年9月1日下午3:04:57
	 *
	 */
	List<SysMenu> findMenusByParentOrderByOrderListAsc(SysMenu parent);

	/**
	 * 
	 *
	 * @Description:查询所有有效菜单并安ordno排序
	 * @return
	 * @date 2017年9月1日下午3:09:52
	 *
	 */
	@Query("select u from SysMenu u where u.state = 1 order by u.ordno ")
	List<SysMenu> findMenus();

	/**
	 * 
	 *
	 * @Description:根据父类菜单查询子菜单，并按照菜单等级和整型排序字段排序
	 * @param id
	 *            父类菜单id
	 * @return
	 * @date 2017年9月1日下午3:10:02
	 *
	 */
	@Query("select u from SysMenu u where u.state = 1 and u.parent.id=?1 order by u.menuLevel,u.orderList ")
	List<SysMenu> findMenusById(Long id);

	/**
	 * 
	 * @Description:根据父级菜单和状态查询菜单列表并按照orderList排序
	 * @param parent 父级菜单
	 * @param state 菜单状态
	 * @return 
	 * @date 2017年9月22日上午10:56:20
	 */
	List<SysMenu> findMenusByParentAndStateOrderByOrderListAsc(SysMenu parent, Long state);

}
