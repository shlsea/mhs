package com.skuo.mhs.system.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.skuo.mhs.support.CustomRepository;
import com.skuo.mhs.system.pojo.SysRole;

/**
 * 
 * 
 * @ClassName: SysRoleRepository
 * @Description: 系统管理用户角色操作实现DAO
 * @Company: 四川西谷智慧科技有限公司
 * @author: 余德山
 * @date: 2017年9月1日 下午3:22:54
 * @version: V1.0.0
 */
@Service
public interface SysRoleRepository extends CustomRepository<SysRole, Long> {

	/**
	 * @Description:根据状态查询用户角色
	 * @param state
	 *            角色状态
	 * @return
	 * @date 2017年9月1日下午3:23:10
	 *
	 */
	List<SysRole> findByStateOrderByCode(Long state);

	/**
	 * @Description:根据角色代码查询角色
	 * @param code
	 *            角色代码
	 * @return
	 * @date 2017年9月1日下午3:23:38
	 *
	 */
	SysRole findByCode(String code);

}
