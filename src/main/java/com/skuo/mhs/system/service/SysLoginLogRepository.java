package com.skuo.mhs.system.service;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import com.skuo.mhs.support.CustomRepository;
import com.skuo.mhs.system.pojo.SysLoginLog;

/**
 * 系统管理登录日志实现接口类
 * 
 * @项目名称 xzzxw
 * @作者 余德山
 * @包名称 com.skou.zxw.service
 * @功能描述 系统用户登录日志数据持久化层
 * @日期 2017年1月12日
 * @版本 V1.0.0
 */
@Service
public interface SysLoginLogRepository extends CustomRepository<SysLoginLog, Long> {

	/**
	 * @Description:根据日志id修改登录日志状态
	 * @param state
	 * @param id
	 * @date 2017年9月1日下午3:10:25
	 *
	 */
	@Modifying
	@Query(" update SysLoginLog u set u.state = ?1 where u.id = ?2 ")
	void update(int state, Long id);
}
