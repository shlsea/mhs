package com.skuo.mhs.system.service;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import com.skuo.mhs.support.CustomRepository;
import com.skuo.mhs.system.pojo.SysDict;

/**
 * 
 * 
 * @ClassName: SysDictRepository
 * @Description: 数据字典实现DAO
 * @Company: 四川西谷智慧科技有限公司
 * @author: 余德山
 * @date: 2017年9月1日 下午2:17:01
 * @version: V1.0.0
 */
@Service
public interface SysDictRepository extends CustomRepository<SysDict, String> {

	/**
	 * 
	 *
	 * @Description:查询所有有效数据字典
	 * @return
	 * @date 2017年9月1日下午2:18:16
	 *
	 */
	@Query("select u from SysDict u where u.state = 1 order by u.skey ")
	List<SysDict> findMenus();

	/**
	 * 
	 *
	 * @Description:根据字典类型查询有效数据字典
	 * @param type
	 *            字典类型
	 * @return
	 * @date 2017年9月1日下午2:18:20
	 *
	 */
	@Query("select u from SysDict u where u.state = 1 and u.type=?1 order by u.skey")
	List<SysDict> findDict(String type);

	/**
	 * 
	 *
	 * @Description:根据父类字典skey查询有效子字典
	 * @param skey
	 *            父类字典skey
	 * @return
	 * @date 2017年9月1日下午2:18:25
	 *
	 */
	@Query("select u from SysDict u where u.state = 1 and u.pkey.skey=?1 order by u.skey")
	List<SysDict> findByParent(String skey);

	/**
	 * 
	 *
	 * @Description:根据父类skey值查询其下所有有效子集
	 * @param parent
	 *            父类数据字典
	 * @param state
	 *            状态
	 * @return
	 * @date 2017年9月1日下午2:18:35
	 *
	 */
	List<SysDict> findByPkeyAndStateOrderBySkeyAsc(SysDict parent, Long state);

	SysDict findBySkey(String string);
}
