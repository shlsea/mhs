package com.skuo.mhs.system.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.skuo.mhs.support.CustomRepository;
import com.skuo.mhs.system.pojo.SysManager;
import com.skuo.mhs.system.pojo.SysRole;

/**
 * @ClassName: SysManagerRepository
 * @Description: 系统管理用户实现DAO
 * @Company: 四川西谷智慧科技有限公司
 * @author: 余德山
 * @date: 2017年9月1日 下午2:17:01
 * @version: V1.0.0
 */
@Service
public interface SysManagerRepository extends CustomRepository<SysManager, Long> {

	/**
	 * 
	 * @Description:根据账号查询用户
	 * @param account
	 *            账号
	 * @return
	 * @date 2017年9月1日下午2:57:53
	 *
	 */
	SysManager findByAccount(String account);

	/**
	 * 
	 * @Description:根据角色查询有效用户
	 * @param role
	 *            用户角色
	 * @param state
	 *            状态
	 * @return
	 * @date 2017年9月1日下午3:01:49
	 *
	 */
	List<SysManager> findByRoleAndState(SysRole role, Long state);

	/**
	 * 
	 *
	 * @Description:查询用户列表排除登录用户本身及无效账号
	 * @param id
	 *            用户id
	 * @param state
	 *            用户列表
	 * @return
	 * @date 2017年9月1日下午3:02:05
	 *
	 */
	List<SysManager> findByIdNotAndState(Long id, Long state);
}
