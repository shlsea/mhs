package com.skuo.mhs.system.service;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import com.skuo.mhs.support.CustomRepository;
import com.skuo.mhs.system.pojo.SysRegion;

/**
 * 
 * 
 * @ClassName: SysRegionRepository
 * @Description: 系统地区实现DAO
 * @Company: 四川西谷智慧科技有限公司
 * @author: 余德山
 * @date: 2017年9月1日 下午3:19:26
 * @version: V1.0.0
 */
@Service
public interface SysRegionRepository extends CustomRepository<SysRegion, String> {

	/**
	 * @Description:根据父类地区查询子地区并按照地区编码排序
	 * @param pid
	 *            父类地区id
	 * @return
	 * @date 2017年9月1日下午3:20:02
	 *
	 */
	@Query("select u from SysRegion u where u.pregion.id = ?1 order by u.region ")
	List<SysRegion> findRegions(String pid);

	/**
	 * @Description:按照地区等级查询地区按照地区编码排序
	 * @param level
	 *            地区等级
	 * @return
	 * @date 2017年9月1日下午3:20:12
	 *
	 */
	@Query("select u from SysRegion u where u.level = ?1 and u.id < 800000 order by u.region")
	List<SysRegion> findByLevel(Long level);

	/**
	 * @Description:查询父类不为null的地区
	 * @return
	 * @date 2017年9月1日下午3:20:16
	 *
	 */
	List<SysRegion> findByPregionIsNullOrderByRegion();

	/**
	 * @Description:根据父类地区查询子地区并根据地区ID排序
	 * @param pregion
	 *            父类地区
	 * @return
	 * @date 2017年9月1日下午3:20:20
	 *
	 */
	List<SysRegion> findByPregionOrderById(SysRegion pregion);

}
