package com.skuo.mhs.data.res.service;

import org.springframework.stereotype.Service;

import com.skuo.mhs.data.res.pojo.DataFood;
import com.skuo.mhs.support.CustomRepository;

import java.util.List;


/**
 * @ClassName: DataFoodRepository 
 * @Description: 美食
 * @Company: 四川西谷智慧科技有限公司
 * @author: songhailing 
 * @date: 2017年9月19日 下午8:14:06 
 * @version: V1.0.0
 */
@Service
public interface DataFoodRepository extends CustomRepository<DataFood, Long> {

    /**
     * @DATE: 2017/9/7-11:43.
     * @Description:查询所有有效美食
     */
    List<DataFood> findByState(Long state);

	List<DataFood> findByBelongRestaurant(Long id);
}
