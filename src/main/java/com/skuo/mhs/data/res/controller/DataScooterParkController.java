package com.skuo.mhs.data.res.controller;

import net.sf.json.JSONObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.skuo.mhs.data.res.pojo.DataScooterPark;
import com.skuo.mhs.data.res.service.DataScooterParkRepository;
import com.skuo.mhs.form.PageForm;
import com.skuo.mhs.utils.Constant;

import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName: DataScooterParkController
 * @Description: 电瓶车站逻辑控制器
 * @Company: 四川西谷智慧科技有限公司
 * @author: 钟操
 * @date: 2017年9月4日  下午 16:13:26
 * @version: V1.0.0
 */
@Controller
@RequestMapping(value = "/scooter")
public class DataScooterParkController {

	@Autowired
	private DataScooterParkRepository dataScooterParkRepository;
	/**
	 * 跳转电瓶车站管理列表页面
	 * @Description:TODO
	 * @return
	 * @date: 2017年9月4日  下午 16:14:26
	 */
	@RequestMapping("/page.action")
	public String page(){
		return "res/scooter_list";
	}


	/**
	 * @Description: 读取电瓶车站列表
	 * @param pageForm
	 * @param dto
	 * @return:
	 * @date: 2017年9月4日 下午16:17:26
	 */
	@RequestMapping(value = "/list.action", method = RequestMethod.POST)
	@ResponseBody
	public Object list(PageForm pageForm, DataScooterPark dto) {
		Map<String, Object> map = dataScooterParkRepository.findByAuto(dto, pageForm);
		return map;
	}

	/**
	 * @Description:新增/编辑电瓶车站
	 * @param id
 	 * @param model
	 * @return:
	 * @date: 2017年9月4日  下午16:20:26
	 */
	@RequestMapping("/edit.action")
	public String edit(Long id,  Model model) {

		DataScooterPark dto = new DataScooterPark();
		if (id != null) {
			dto = dataScooterParkRepository.findOne(id);
		}
		model.addAttribute("dto", dto);
		return "res/scooter_input";
	}


	/**
	 * @Description:保存电瓶车站
	 * @param dto
	 * @return
	 * @date 2017年9月4日下午16:25:04
	 */
	@RequestMapping(value = "/save.action", method = RequestMethod.POST)
	public String save(@ModelAttribute DataScooterPark dto) {
		if (dto.getState() == null || dto.getState().equals("")) {
			dto.setState(0L);
		}
		if(dto.getId()!=null){
			DataScooterPark data = dataScooterParkRepository.getOne(dto.getId());
			BeanUtils.copyProperties(dto, data,new String[] { "id"});
			dataScooterParkRepository.save(data);

		}else {
			dataScooterParkRepository.save(dto);
		}
		return "forward:/success.action";
	}

	/**
	 * @Description:删除电瓶车站
	 * @param ids
	 * @return
	 * @date 2017年9月4日下午16:27:11
	 */
	@RequestMapping("/remove.action")
	@ResponseBody
	public Object delete(Long[] ids){
		String status = Constant.STATUS_SCUESS;
		String msg;
		try {
			for(Long id:ids){
				dataScooterParkRepository.delete(id);
			}
			msg = "删除成功！";
		} catch (Exception e) {
			status = Constant.STATUS_ERROR;
			msg = "删除失败！";
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("state", status);
		map.put("msg", msg);
		JSONObject json = JSONObject.fromObject(map);
		return json;
	}

}
