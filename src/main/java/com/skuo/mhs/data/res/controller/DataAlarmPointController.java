package com.skuo.mhs.data.res.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.skuo.mhs.data.res.pojo.DataAlarmPoint;
import com.skuo.mhs.data.res.service.DataAlarmPointRepository;
import com.skuo.mhs.form.PageForm;
import com.skuo.mhs.specs.BaseController;
import com.skuo.mhs.utils.Constant;

import net.sf.json.JSONObject;

/**
 * 
 * 
 * @ClassName: DataAlarmPointController
 * @Description: 报警点逻辑控制器
 * @Company: 四川西谷智慧科技有限公司
 * @author: 余德山
 * @date: 2017年9月4日 上午11:34:45
 * @version: V1.0.0
 */
@Controller
@RequestMapping(value = "/alarm_point")
public class DataAlarmPointController extends BaseController {

	@Autowired
	private DataAlarmPointRepository dataAlarmPointRepository;

	/**
	 * 跳转报警点管理列表页面
	 * 
	 * @Description:TODO
	 * @return
	 * @date 2017年9月4日上午11:35:53
	 */
	@RequestMapping("/page.action")
	public String page() {
		return "res/alarm_point_list";
	}

	/**
	 *
	 * @Description: 获取报警点列表数据
	 * @param pageForm
	 * @param dto
	 * @return
	 * @date: 2017年9月4日 下午2:35:06
	 */
	@RequestMapping(value = "/list.action", method = RequestMethod.POST)
	@ResponseBody
	public Object list(PageForm pageForm, DataAlarmPoint dto) {
		Map<String, Object> map = dataAlarmPointRepository.findByAuto(dto, pageForm);
		return map;
	}

	/**
	 *
	 * @Description: 新增/编辑报警点信息
	 * @param id
	 * @param model
	 * @return
	 * @date: 2017年9月4日 下午3:22:34
	 */
	@RequestMapping("/edit.action")
	public String edit(Long id, Model model) {
		DataAlarmPoint dto = new DataAlarmPoint();
		if (id != null) {
			dto = dataAlarmPointRepository.findOne(id);
		}
		model.addAttribute("dto", dto);
		return "res/alarm_point_input";
	}

	/**
	 *
	 * @Description: 保存报警点信息
	 * @param dto
	 * @return
	 * @date: 2017年9月4日 下午3:43:05
	 */
	@RequestMapping(value = "/save.action", method = RequestMethod.POST)
	public String save(@ModelAttribute DataAlarmPoint dto) {
		if (dto.getState() == null || dto.getState().equals("")) {
			dto.setState(0L);
		}
		if (dto.getId() != null) {
			DataAlarmPoint data = dataAlarmPointRepository.getOne(dto.getId());
			BeanUtils.copyProperties(dto, data, new String[] { "id" });
			dataAlarmPointRepository.save(data);

		} else {
			dataAlarmPointRepository.save(dto);
		}
		return "forward:/success.action";
	}

	/**
	 *
	 * @Description: 删除报警点信息
	 * @param ids
	 * @return
	 * @date: 2017年9月4日 下午3:50:09
	 */
	@RequestMapping("/remove.action")
	@ResponseBody
	public Object delete(Long[] ids) {
		String status = Constant.STATUS_SCUESS;
		String msg;
		try {
			for (Long id : ids) {
				dataAlarmPointRepository.delete(id);
			}
			msg = "删除成功！";
		} catch (Exception e) {
			status = Constant.STATUS_ERROR;
			msg = "删除失败！";
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("state", status);
		map.put("msg", msg);
		JSONObject json = JSONObject.fromObject(map);
		return json;
	}
}
