package com.skuo.mhs.data.res.service;

import org.springframework.stereotype.Service;

import com.skuo.mhs.data.res.pojo.DataScenic;
import com.skuo.mhs.support.CustomRepository;

import java.util.List;

/**
 * @ClassName: DataScenicRepository
 * @Description: 景区景点实现DAO
 * @Company: 四川西谷智慧科技有限公司
 * @author: 钟操
 * @date: 2017年9月4日 下午14:15:52
 * @version: V1.0.0
 */
@Service
public interface DataScenicRepository extends CustomRepository<DataScenic, Long> {

    /**
     * @Description:查询所有有效景区
     * @return
     * @date 2017年9月5日下午12:18:16
     *
     */
    List<DataScenic> findByState(Long state);
}
