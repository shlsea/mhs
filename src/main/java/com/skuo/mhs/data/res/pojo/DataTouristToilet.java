package com.skuo.mhs.data.res.pojo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * 
 * 
 * @ClassName: DataTouristToilet
 * @Description: 旅游厕所
 * @Company: 四川西谷智慧科技有限公司
 * @author: 余德山
 * @date: 2017年9月4日 上午10:58:26
 * @version: V1.0.0
 */
@Entity
public class DataTouristToilet {

	/**
	 * 主键Id
	 */
	@Id
	@GeneratedValue
	private Long id;

	/**
	 * 旅游厕所名称
	 */
	private String name;

	/**
	 * 旅游厕所地址
	 */
	private String address;

	/**
	 * 纬度
	 */
	private String lat;

	/**
	 * 经度
	 */
	private String lon;

	/**
	 * 联系人
	 */
	private String linkName;

	/**
	 * 联系电话
	 */
	private String phone;

	/**
	 * 简介
	 */
	@Type(type = "text")
	private String content;

	/**
	 * 备注
	 */
	@Type(type = "text")
	private String remark;

	/**
	 * 图片路径
	 */
	private String imgPath;
	
	/**
	 * 状态<br>
	 * 0:禁用<br>
	 * 1:启用
	 */
	private Long state;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public String getLon() {
		return lon;
	}

	public void setLon(String lon) {
		this.lon = lon;
	}

	public String getLinkName() {
		return linkName;
	}

	public void setLinkName(String linkName) {
		this.linkName = linkName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getImgPath() {
		return imgPath;
	}

	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}

	public Long getState() {
		return state;
	}

	public void setState(Long state) {
		this.state = state;
	}

	
}
