package com.skuo.mhs.data.res.controller;

import net.sf.json.JSONObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.skuo.mhs.data.res.pojo.DataSnack;
import com.skuo.mhs.data.res.service.DataSnackRepository;
import com.skuo.mhs.form.PageForm;
import com.skuo.mhs.utils.Constant;

import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName: DataSnackController
 * @Description: 冷饮小吃店逻辑控制器
 * @Company: 四川西谷智慧科技有限公司
 * @author: 钟操
 * @date: 2017年9月7日  下午 14:08:26
 * @version: V1.0.0
 */
@Controller
@RequestMapping(value = "/snack")
public class DataSnackController {

	@Autowired
	private DataSnackRepository dataSnackRepository;
	/**
	 * 跳转冷饮小吃店管理列表页面
	 * @Description:TODO
	 * @return
	 * @date: 2017年9月7日  下午 14:10:11
	 */
	@RequestMapping("/page.action")
	public String page(){
		return "res/snack_list";
	}


	/**
	 * @Description: 读取冷饮小吃店列表
	 * @param pageForm
	 * @param dto
	 * @return:
	 * @date: 2017年9月7日 下午14:12:00
	 */
	@RequestMapping(value = "/list.action", method = RequestMethod.POST)
	@ResponseBody
	public Object list(PageForm pageForm, DataSnack dto) {
		Map<String, Object> map = dataSnackRepository.findByAuto(dto, pageForm);
		return map;
	}

	/**
	 * @Description:新增/编辑冷饮小吃店
	 * @param id
 	 * @param model
	 * @return:
	 * @date: 2017年9月7日  下午14:14:36
	 */
	@RequestMapping("/edit.action")
	public String edit(Long id,  Model model) {
		DataSnack dto = new DataSnack();
		if (id != null) {
			dto = dataSnackRepository.findOne(id);
		}
		model.addAttribute("dto", dto);
		return "res/snack_input";
	}


	/**
	 * @Description:保存冷饮小吃店信息
	 * @param dto
	 * @return
	 * @date 2017年9月7日下午14:16:14
	 */
	@RequestMapping(value = "/save.action", method = RequestMethod.POST)
	public String save(@ModelAttribute DataSnack dto) {
		if (dto.getState() == null || dto.getState().equals("")) {
			dto.setState(0L);
		}
		if(dto.getId()!=null){
			DataSnack data = dataSnackRepository.getOne(dto.getId());
			BeanUtils.copyProperties(dto, data,new String[] { "id"});
			dataSnackRepository.save(data);

		}else {
			dataSnackRepository.save(dto);
		}
		return "forward:/success.action";
	}

	/**
	 * @Description:删除冷饮小吃店
	 * @param ids
	 * @return
	 * @date 2017年9月4日下午14:18:36
	 */
	@RequestMapping("/remove.action")
	@ResponseBody
	public Object delete(Long[] ids){
		String status = Constant.STATUS_SCUESS;
		String msg;
		try {
			for(Long id:ids){
				dataSnackRepository.delete(id);
			}
			msg = "删除成功！";
		} catch (Exception e) {
			status = Constant.STATUS_ERROR;
			msg = "删除失败！";
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("state", status);
		map.put("msg", msg);
		JSONObject json = JSONObject.fromObject(map);
		return json;
	}


}
