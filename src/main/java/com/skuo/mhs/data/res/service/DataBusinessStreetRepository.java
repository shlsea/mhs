package com.skuo.mhs.data.res.service;

import org.springframework.stereotype.Service;

import com.skuo.mhs.data.res.pojo.DataBusinessStreet;
import com.skuo.mhs.support.CustomRepository;

import java.util.List;

/**
 * AUTHAR:yosang.
 * COMPANY:四川西谷智慧科技有限公司.
 * DATAE:2017/9/4-14:30.
 * DESCRIBE:商业街service.
 * VERSION:V1.0.0
 */
@Service
public interface DataBusinessStreetRepository  extends CustomRepository<DataBusinessStreet, Long> {
    /**
     * @Description:查询所有有效商业街
     * @return
     * @date 2017年9月5日下午18:18:16
     */
    List<DataBusinessStreet> findByState(Long state);
}
