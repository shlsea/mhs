package com.skuo.mhs.data.res.service;

import org.springframework.stereotype.Service;

import com.skuo.mhs.data.res.pojo.DataTour;
import com.skuo.mhs.support.CustomRepository;

import java.util.List;

/**
 * @ClassName: DataTourRepository 
 * @Description: 行程纪录
 * @Company: 四川西谷智慧科技有限公司
 * @author: songhailing 
 * @date: 2017年9月19日 下午3:49:16 
 * @version: V1.0.0
 */
@Service
public interface DataTourRepository extends CustomRepository<DataTour, Long> {

    /**
     * @Description:查询所有有效行程
     * @return
     * @date 2017年9月5日下午18:18:16
     */
    List<DataTour> findByState(Long state);
}
