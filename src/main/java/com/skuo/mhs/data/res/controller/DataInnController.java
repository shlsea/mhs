package com.skuo.mhs.data.res.controller;

import net.sf.json.JSONObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.skuo.mhs.data.res.pojo.DataInn;
import com.skuo.mhs.data.res.service.DataInnRepository;
import com.skuo.mhs.form.PageForm;
import com.skuo.mhs.utils.Constant;

import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName: DataInnController
 * @Description: 民宿逻辑控制器
 * @Company: 四川西谷智慧科技有限公司
 * @author: 钟操
 * @date: 2017年9月7日  下午 13:30:26
 * @version: V1.0.0
 */
@Controller
@RequestMapping(value = "/inn")
public class DataInnController {

	@Autowired
	private DataInnRepository dataInnRepository;
	/**
	 * 跳转民宿管理列表页面
	 * @Description:TODO
	 * @return
	 * @date: 2017年9月7日  下午 13:49:26
	 */
	@RequestMapping("/page.action")
	public String page(){
		return "res/inn_list";
	}


	/**
	 * @Description: 读取民宿列表
	 * @param pageForm
	 * @param dto
	 * @return:
	 * @date: 2017年9月7日 下午13:50:26
	 */
	@RequestMapping(value = "/list.action", method = RequestMethod.POST)
	@ResponseBody
	public Object list(PageForm pageForm, DataInn dto) {
		Map<String, Object> map = dataInnRepository.findByAuto(dto, pageForm);
		return map;
	}

	/**
	 * @Description:新增/编辑民宿
	 * @param id
 	 * @param model
	 * @return:
	 * @date: 2017年9月7日  下午13:55:36
	 */
	@RequestMapping("/edit.action")
	public String edit(Long id,  Model model) {
		DataInn dto = new DataInn();
		if (id != null) {
			dto = dataInnRepository.findOne(id);
		}
		model.addAttribute("dto", dto);
		return "res/inn_input";
	}


	/**
	 * @Description:保存民宿信息
	 * @param dto
	 * @return
	 * @date 2017年9月7日下午13:58:04
	 */
	@RequestMapping(value = "/save.action", method = RequestMethod.POST)
	public String save(@ModelAttribute DataInn dto) {
		if (dto.getState() == null || dto.getState().equals("")) {
			dto.setState(0L);
		}
		if(dto.getId()!=null){
			DataInn data = dataInnRepository.getOne(dto.getId());
			BeanUtils.copyProperties(dto, data,new String[] { "id"});
			dataInnRepository.save(data);

		}else {
			dataInnRepository.save(dto);
		}
		return "forward:/success.action";
	}

	/**
	 * @Description:删除民宿
	 * @param ids
	 * @return
	 * @date 2017年9月4日下午14:26:11
	 */
	@RequestMapping("/remove.action")
	@ResponseBody
	public Object delete(Long[] ids){
		String status = Constant.STATUS_SCUESS;
		String msg;
		try {
			for(Long id:ids){
				dataInnRepository.delete(id);
			}
			msg = "删除成功！";
		} catch (Exception e) {
			status = Constant.STATUS_ERROR;
			msg = "删除失败！";
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("state", status);
		map.put("msg", msg);
		JSONObject json = JSONObject.fromObject(map);
		return json;
	}


}
