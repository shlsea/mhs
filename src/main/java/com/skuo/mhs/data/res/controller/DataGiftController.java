package com.skuo.mhs.data.res.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.skuo.mhs.data.res.pojo.DataGift;
import com.skuo.mhs.data.res.service.DataGiftRepository;
import com.skuo.mhs.form.PageForm;
import com.skuo.mhs.system.pojo.SysDict;
import com.skuo.mhs.system.service.SysDictRepository;
import com.skuo.mhs.utils.Constant;

import net.sf.json.JSONObject;

/**
 * @ClassName: DataGiftController
 * @Description: 伴手礼控制器
 * @Company: 四川西谷智慧科技有限公司
 * @author: YanHiroShi
 * @date: 2017/9/7 11:41
 * @version: V1.0.0
 */
@Controller
@RequestMapping(value = "/gift")
public class DataGiftController {

    @Autowired
    private DataGiftRepository giftRepository;

	@Autowired
	private SysDictRepository sysDictRepository;

    /**
     * @DATE: 2017/9/4-17:36.
     * @Description:跳转界面
     */
    @RequestMapping(value = "/page.action")
    public String page(){
        return "res/gift_list";
    }

    /**
     * @DATE: 2017/9/4-17:35.
     * @Description:编辑界面
     */
    @RequestMapping(value = "/edit.action")
    public String edit(Long id,Model model){
        DataGift dto = new DataGift();
        if(id!=null){
            dto = giftRepository.findOne(id);
        }
     // 读取数据字典酒店类型
        List<SysDict> giftTypes = sysDictRepository.findByParent("GiftType"); 
        model.addAttribute("dto",dto);
        model.addAttribute("giftTypes",giftTypes);
        return "res/gift_input";
    }


    /**
     * @DATE: 2017/9/4-17:36.
     * @Description:获取列表数据
     */
    @RequestMapping(value = "/list.action",method = RequestMethod.POST)
    @ResponseBody
    public Object list(PageForm pageForm, DataGift dto){
        Map<String, Object> map = giftRepository.findByAuto(dto, pageForm);
        return map;
    }

    /**
     * @DATE: 2017/9/4-17:26.
     * @Description:删除数据(单条和多条)
     */
    @RequestMapping(value = "/remove.action",method = RequestMethod.POST)
    @ResponseBody
    public Object remove(Long[] ids){
        String status = Constant.STATUS_SCUESS;
        String msg;
        try {
            for (Long id : ids) {
                giftRepository.delete(id);
            }
            msg = "删除成功！";
        } catch (Exception e) {
            status = Constant.STATUS_ERROR;
            msg = "删除失败！";
        }
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("state", status);
        map.put("msg", msg);
        JSONObject json = JSONObject.fromObject(map);
        return json;
    }

    /**
     * @DATE: 2017/9/4-17:28.
     * @Description:保存数据(单个)
     */
    @RequestMapping(value = "/save.action",method = RequestMethod.POST)
    public Object save(@ModelAttribute DataGift dto, Model model){
        model.addAttribute("dto", dto);
        if (dto.getState() == null || dto.getState().equals("")) {
            dto.setState(0L);
        }
        if (dto.getId() != null) {
            DataGift data = giftRepository.findOne(dto.getId());
            BeanUtils.copyProperties(dto, data, new String[] { "id" });
            giftRepository.save(data);
        } else {
            giftRepository.save(dto);
        }
        return "forward:/success.action";
    }


}
