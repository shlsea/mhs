package com.skuo.mhs.data.res.controller;

import net.sf.json.JSONObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import com.skuo.mhs.data.res.pojo.DataRestaurant;
import com.skuo.mhs.data.res.service.DataRestaurantRepository;
import com.skuo.mhs.form.PageForm;
import com.skuo.mhs.utils.Constant;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName: DataRestaurantController
 * @Description: 餐厅控制器
 * @Company: 四川西谷智慧科技有限公司
 * @author: YanHiroShi
 * @date: 2017/9/7 11:41
 * @version: V1.0.0
 */
@Controller
@RequestMapping(value = "/restaurant")
public class DataRestaurantController {

    @Autowired
    private DataRestaurantRepository restaurantRepository;


    /**
     * @DATE: 2017/9/4-17:36.
     * @Description:跳转界面
     */
    @RequestMapping(value = "/page.action")
    public String page(){
        return "res/restaurant_list";
    }

    /**
     * @DATE: 2017/9/4-17:35.
     * @Description:编辑界面
     */
    @RequestMapping(value = "/edit.action")
    public String edit(Long id,Model model){
        DataRestaurant dto = new DataRestaurant();
        if(id!=null){
            dto = restaurantRepository.findOne(id);
        }
        model.addAttribute("dto",dto);
        return "res/restaurant_input";
    }


    /**
     * @DATE: 2017/9/4-17:36.
     * @Description:获取列表数据
     */
    @RequestMapping(value = "/list.action",method = RequestMethod.POST)
    @ResponseBody
    public Object list(PageForm pageForm, DataRestaurant dto){
        Map<String, Object> map = restaurantRepository.findByAuto(dto, pageForm);
        return map;
    }

    /**
     * @DATE: 2017/9/4-17:26.
     * @Description:删除数据(单条和多条)
     */
    @RequestMapping(value = "/remove.action",method = RequestMethod.POST)
    @ResponseBody
    public Object remove(Long[] ids){
        String status = Constant.STATUS_SCUESS;
        String msg;
        try {
            for (Long id : ids) {
                restaurantRepository.delete(id);
            }
            msg = "删除成功！";
        } catch (Exception e) {
            status = Constant.STATUS_ERROR;
            msg = "删除失败！";
        }
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("state", status);
        map.put("msg", msg);
        JSONObject json = JSONObject.fromObject(map);
        return json;
    }

    /**
     * @DATE: 2017/9/4-17:28.
     * @Description:保存数据(单个)
     */
    @RequestMapping(value = "/save.action",method = RequestMethod.POST)
    public Object save(@ModelAttribute DataRestaurant dto, Model model){
        model.addAttribute("dto", dto);
        if (dto.getState() == null || dto.getState().equals("")) {
            dto.setState(0L);
        }
        if (dto.getId() != null) {
            DataRestaurant data = restaurantRepository.findOne(dto.getId());
            BeanUtils.copyProperties(dto, data, new String[] { "id" });
            restaurantRepository.save(data);
        } else {
            restaurantRepository.save(dto);
        }
        return "forward:/success.action";
    }

    /**
     * 行程酒店列表
     * @param region
     * @param page
     * @param rows
     * @param name
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
	@RequestMapping("/dinning_list.action")
    @ResponseBody
    public Object dinningList(String region, @RequestParam(defaultValue = "1") int page,
                              @RequestParam(defaultValue = "10") int rows, String name, HttpServletRequest request,
                              HttpServletResponse response) throws Exception {
        DataRestaurant dto = new DataRestaurant();
        PageForm form = new PageForm();
		/*if (region != null && !region.equals("")) {
			if (region.endsWith("0000")) {
				region = region.substring(0, 2);
			} else if (region.endsWith("00")) {
				region = region.substring(0, 4);
			}
			dto.setRegion(region);
			;
		}*/

        form.setLimit(rows);
        form.setOffset((page-1)*rows);
        form.setSort("id");
        form.setOrder("ASC");
        dto.setName(name);
        List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
        Map<String, Object> queyMap = restaurantRepository.findByAuto(dto, form);
        List<DataRestaurant> list = (List<DataRestaurant>) queyMap.get("rows");
        for (DataRestaurant data : list) {
            Map<String, Object> mapScen = new HashMap<String, Object>();
            mapScen.put("id", data.getId());
            mapScen.put("name", data.getName());// 酒店名称
			mapScen.put("picture", data.getImgPath());// 引导图图片
			/*String levels = null;
			if (data.getResourceLevel() != null) {
				levels = sysDictRepository.findOne(data.getResourceLevel()).getName();
			} else {
				levels = "未评等级";
			}
			mapScen.put("type", levels);// 级别*/
			mapScen.put("phone", data.getPhone());
            mapList.add(mapScen);
        }
        response.setContentType("charset=UTF-8;text/json");
        response.setCharacterEncoding("UTF-8");
        response.setHeader("Access-Control-Allow-Origin", "*");
        JSONObject obj = new JSONObject();
        obj.put("rows", mapList);
        obj.put("total", queyMap.get("total"));
        response.getWriter().write(obj.toString());
        return null;

    }

    /**
     * 根据id查询出餐饮--用于行程管理资源查询
     * @param id       资源id
     * @param response
     * @return
     * @throws IOException
     */
    @RequestMapping("/findOne.action")
    @ResponseBody
    public Object findOne(Long id, HttpServletResponse response) throws IOException {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            DataRestaurant t =restaurantRepository.findOne(id);
            map.put("id", String.valueOf(t.getId()));
            map.put("name", t.getName()); // 名称
			map.put("phone", t.getPhone() != null ? t.getPhone() : "-"); // 电话
			map.put("picture", t.getImgPath()); // 形象标识图片
			/*map.put("type", t.getResourceLevel()!=null?sysDictRepository.findOne(t.getResourceLevel()).getName():"-"); // 类型
			map.put("adviceDate", "地址:"+(t.getAddress() != null ? t.getAddress() : "-")); // 地址
			map.put("region", t.getRegion() != null ? sysRegionRepository.getOne(t.getRegion()).getShortName() : "-"); // 地区名称*/
        } catch (Exception e) {
            e.printStackTrace();
        }
        response.setContentType("charset=UTF-8;text/json");
        response.setCharacterEncoding("UTF-8");
        net.sf.json.JSONObject obj = new net.sf.json.JSONObject();
        obj.put("data", map);
        response.getWriter().write(obj.toString());
        return null;
    }
}
