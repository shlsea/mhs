package com.skuo.mhs.data.res.pojo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

import java.util.Date;

/**
 * 
 * @ClassName: DataComplain
 * @Description: 投诉实体类
 * @Company: 四川西谷智慧科技有限公司
 * @author: songhailing
 * @date: 2017年9月13日 上午10:20:41
 * @version: V1.0.0
 */
@Entity
public class DataComplain {

	/**
	 * 主键
	 */
	@Id
	@GeneratedValue
	private Long id;

	/**
	 * 投诉标题
	 */
	private String title;

	/**
	 * 视频类型
	 */
	private String type;

	/**
	 * 图片地址
	 */
	private String imgPath;

	/**
	 * 视频mp4地址
	 */
	private String videoMp4;

	/**
	 * 大小
	 */
	private String size;

	/**
	 * 排序
	 */
	private Long orderList;

	/**
	 * 简介
	 */
	@Type(type = "text")
	private String content;

	/**
	 * 状态
	 */
	private Long state;

	/**
	 * 创建时间
	 */
	private Date createTime;

	/**
	 * 更新时间
	 */
	private Date updateTime;

	/**
	 * 点击数
	 */
	private Long checkNum;

	/**
	 * 关注数
	 */
	private Long followNum;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getImgPath() {
		return imgPath;
	}

	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}

	public Long getOrderList() {
		return orderList;
	}

	public void setOrderList(Long orderList) {
		this.orderList = orderList;
	}

	public Long getState() {
		return state;
	}

	public void setState(Long state) {
		this.state = state;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Long getCheckNum() {
		return checkNum;
	}

	public void setCheckNum(Long checkNum) {
		this.checkNum = checkNum;
	}

	public Long getFollowNum() {
		return followNum;
	}

	public void setFollowNum(Long followNum) {
		this.followNum = followNum;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getVideoMp4() {
		return videoMp4;
	}

	public void setVideoMp4(String videoMp4) {
		this.videoMp4 = videoMp4;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
