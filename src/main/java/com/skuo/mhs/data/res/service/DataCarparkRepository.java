package com.skuo.mhs.data.res.service;

import org.springframework.stereotype.Service;

import com.skuo.mhs.data.res.pojo.DataCarpark;
import com.skuo.mhs.support.CustomRepository;

import java.util.List;

/**
 * @ClassName: DataCarparkRepository
 * @Description: 景区停车场信息实现DAO
 * @Company: 四川西谷智慧科技有限公司
 * @author: 钟操
 * @date: 2017年9月4日 下午14:38:52
 * @version: V1.0.0
 */
@Service
public interface DataCarparkRepository extends CustomRepository<DataCarpark, Long> {

    /**
     * @Description:查询所有有效停车场
     * @return
     * @date 2017年9月5日下午12:18:16
     */
    List<DataCarpark> findByState(Long state);
}
