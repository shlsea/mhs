package com.skuo.mhs.data.res.service;

import org.springframework.stereotype.Service;

import com.skuo.mhs.data.res.pojo.DataInn;
import com.skuo.mhs.support.CustomRepository;

import java.util.List;

/**
 * @ClassName: DataInnRepository
 * @Description: 民宿实现DAO
 * @Company: 四川西谷智慧科技有限公司
 * @author: 钟操
 * @date: 2017年9月7日 上午09:56:52
 * @version: V1.0.0
 */
@Service
public interface DataInnRepository extends CustomRepository<DataInn, Long> {

    /**
     * @Description:查询所有有效民宿
     * @return
     * @date 2017年9月7日 下午15:57:16
     *
     */
    List<DataInn> findByState(Long state);
}
