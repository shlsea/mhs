package com.skuo.mhs.data.res.pojo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * 
 * 
 * @ClassName: DataGuide
 * @Description:  导游信息
 * @Company: 四川西谷智慧科技有限公司
 * @author: songhailing
 * @date: 2017年9月4日 上午10:40:27
 * @version: V1.0.0
 */
@Entity
public class DataGuide {

	/**
	 * 主键Id
	 */
	@Id
	@GeneratedValue
	private Long id;

	/**
	 * 姓名
	 */
	private String name;

	/**
	 * 性别
	 */
	private Integer sex;

	/**
	 * 级别
	 */
	private String guideLevel;

	/**
	 * 证件编号
	 */
	private String guideCode;

	/**
	 * 语言种类
	 */
	private String languageType;

	/**
	 * 所属公司
	 */
	private String belongTo;

	/**
	 * 联系电话
	 */
	private String phone;

	/**
	 * 简介
	 */
	@Type(type = "text")
	private String content;

	/**
	 * 备注
	 */
	@Type(type = "text")
	private String remark;

	/**
	 * 图片路径
	 */
	private String imgPath;

	/**
	 * 状态<br>
	 * 0:禁用<br>
	 * 1:启用
	 */
	private Long state;

	/**
	 * 人员类型<br>
	 * 0:导游<br>
	 * 1:教练
	 */
	private Integer type;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getSex() {
		return sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}

	public String getGuideLevel() {
		return guideLevel;
	}

	public void setGuideLevel(String guideLevel) {
		this.guideLevel = guideLevel;
	}

	public String getGuideCode() {
		return guideCode;
	}

	public void setGuideCode(String guideCode) {
		this.guideCode = guideCode;
	}

	public String getLanguageType() {
		return languageType;
	}

	public void setLanguageType(String languageType) {
		this.languageType = languageType;
	}

	public String getBelongTo() {
		return belongTo;
	}

	public void setBelongTo(String belongTo) {
		this.belongTo = belongTo;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getImgPath() {
		return imgPath;
	}

	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}

	public Long getState() {
		return state;
	}

	public void setState(Long state) {
		this.state = state;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

}
