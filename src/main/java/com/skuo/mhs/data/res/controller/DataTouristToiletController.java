package com.skuo.mhs.data.res.controller;

import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.skuo.mhs.data.res.pojo.DataTouristToilet;
import com.skuo.mhs.data.res.service.DataTouristToiletRepository;
import com.skuo.mhs.form.PageForm;
import com.skuo.mhs.specs.BaseController;
import com.skuo.mhs.utils.Constant;

import net.sf.json.JSONObject;

/**
 * 
 * 
 * @ClassName: DataTouristToiletController
 * @Description: 旅游厕所逻辑控制器
 * @Company: 四川西谷智慧科技有限公司
 * @author: 余德山
 * @date: 2017年9月4日 上午11:34:45
 * @version: V1.0.0
 */
@Controller
@RequestMapping(value = "/toilet")
public class DataTouristToiletController extends BaseController{
	
	@Autowired
	private DataTouristToiletRepository dataTouristToiletRepository;

	/**
	 * 跳转旅游厕所管理列表页面
	 * 
	 * @Description:TODO
	 * @return
	 * @date 2017年9月4日上午11:35:53
	 */
	@RequestMapping("/page.action")
	public String page() {
		return "res/toilet_list";
	}
	
	/**
	 * @Description:读取旅游厕所列表
	 * @param pageForm 分页表单
	 * @param dto 查询条件
	 * @return
	 * @date 2017年9月2日下午4:57:47
	 */
	@RequestMapping(value = "/list.action", method = RequestMethod.POST)
	@ResponseBody
	public Object list(PageForm pageForm, DataTouristToilet dto) {
		Map<String, Object> map = dataTouristToiletRepository.findByAuto(dto, pageForm);
		return map;
	}
	
	
	/**
	 * @Description:编辑旅游厕所
	 * @param id 厕所Id
	 * @param model
	 * @return
	 * @date 2017年9月2日下午4:57:47
	 */
	@RequestMapping("/edit.action")
	public String edit(Long id,Model model) {
		DataTouristToilet dto= new DataTouristToilet();
		if(id!=null){
			 dto=dataTouristToiletRepository.findOne(id);
		}
		model.addAttribute("dto", dto);
		return "res/toilet_input";
	}
	
	/**
	 * @Description:保存旅游厕所
	 * @param dto 旅游厕所实体
	 * @param model
	 * @return
	 * @date 2017年9月2日下午4:57:47
	 */
	@RequestMapping(value = "/save.action", method = RequestMethod.POST)
	public String save(@ModelAttribute DataTouristToilet dto ,Model model ) {
		if (dto.getState() == null || dto.getState().equals("")) {
			dto.setState(0L);
		}
		dataTouristToiletRepository.save(dto);
		return "forward:/success.action";
	}
	
	/**
	 * @Description:删除音频
	 * @param ids
	 * @return
	 * @date 2017年9月2日下午4:57:47
	 */
	@RequestMapping("/remove.action")
	@ResponseBody
	public Object delete(Long[] ids){
		String status = Constant.STATUS_SCUESS;
		String msg;
		try {
			for(Long id:ids){
				dataTouristToiletRepository.delete(id);
			}
			msg = "删除成功！";
		} catch (Exception e) {
			status = Constant.STATUS_ERROR;
			msg = "删除失败！";
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("state", status);
		map.put("msg", msg);
		JSONObject json = JSONObject.fromObject(map);
		return json;
	} 

}
