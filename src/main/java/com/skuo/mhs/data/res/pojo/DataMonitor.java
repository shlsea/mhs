package com.skuo.mhs.data.res.pojo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * 
 * 
 * @ClassName: DataMonitor
 * @Description: 视频监控实体
 * @Company: 四川西谷智慧科技有限公司
 * @author: 余德山
 * @date: 2017年9月4日 上午10:33:26
 * @version: V1.0.0
 */
@Entity
public class DataMonitor {

	/**
	 * 主键Id
	 */
	@Id
	@GeneratedValue
	private Long id;

	/**
	 * 名称标识
	 */
	private String name;

	/**
	 * IP地址
	 */
	private String uri;

	/**
	 * 用户名称
	 */
	private String user;

	/**
	 * 密码
	 */
	private String password;

	/**
	 * 摄像头品牌
	 */
	private String brand;

	/**
	 * 所属资源类型
	 */
	private String type;

	/**
	 * 资源Id
	 */
	private String resourceId;

	/**
	 * 纬度
	 */
	private String lat;

	/**
	 * 经度
	 */
	private String lon;

	/**
	 * 备注信息
	 */
	@Column(length = 2000)
	private String remark;

	/**
	 * 状态<br>
	 * 0:禁用<br>
	 * 1:启用<br>
	 */
	private Long state;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getResourceId() {
		return resourceId;
	}

	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Long getState() {
		return state;
	}

	public void setState(Long state) {
		this.state = state;
	}

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public String getLon() {
		return lon;
	}

	public void setLon(String lon) {
		this.lon = lon;
	}
}
