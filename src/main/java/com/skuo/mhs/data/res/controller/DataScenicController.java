package com.skuo.mhs.data.res.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.skuo.mhs.data.res.pojo.DataScenic;
import com.skuo.mhs.data.res.service.DataScenicRepository;
import com.skuo.mhs.form.PageForm;
import com.skuo.mhs.utils.Constant;

import net.sf.json.JSONObject;

/**
 * @ClassName: DataScenicController
 * @Description: 景区景点逻辑控制器
 * @Company: 四川西谷智慧科技有限公司
 * @author: 钟操
 * @date: 2017年9月4日  下午 14:08:26
 * @version: V1.0.0
 */
@Controller
@RequestMapping(value = "/scenic")
public class DataScenicController {

	@Autowired
	private DataScenicRepository dataScenicRepository;
	/**
	 * 跳转景区景点管理列表页面
	 * @Description:TODO
	 * @return
	 * @date: 2017年9月4日  下午 14:10:26
	 */
	@RequestMapping("/page.action")
	public String page(){
		return "res/scenic_list";
	}


	/**
	 * @Description: 读取景区景点列表
	 * @param pageForm
	 * @param dto
	 * @return:
	 * @date: 2017年9月4日 下午14:18:26
	 */
	@RequestMapping(value = "/list.action", method = RequestMethod.POST)
	@ResponseBody
	public Object list(PageForm pageForm, DataScenic dto) {
		Map<String, Object> map = dataScenicRepository.findByAuto(dto, pageForm);
		return map;
	}

	/**
	 * @Description:新增/编辑景区景点
	 * @param id
 	 * @param model
	 * @return:
	 * @date: 2017年9月4日  下午14:20:26
	 */
	@RequestMapping("/edit.action")
	public String edit(Long id,  Model model) {

		DataScenic dto = new DataScenic();
		if (id != null) {
			dto = dataScenicRepository.findOne(id);
		}
		model.addAttribute("dto", dto);
		return "res/scenic_input";
	}


	/**
	 * @Description:保存景区景点信息
	 * @param dto
	 * @return
	 * @date 2017年9月4日下午14:25:04
	 */
	@RequestMapping(value = "/save.action", method = RequestMethod.POST)
	public String save(@ModelAttribute DataScenic dto) {
		if (dto.getState() == null || dto.getState().equals("")) {
			dto.setState(0L);
		}
		if(dto.getId()!=null){
			DataScenic data = dataScenicRepository.getOne(dto.getId());
			BeanUtils.copyProperties(dto, data,new String[] { "id"});
			dataScenicRepository.save(data);

		}else {
			dataScenicRepository.save(dto);
		}
		return "forward:/success.action";
	}

	/**
	 * @Description:删除景区景点
	 * @param ids
	 * @return
	 * @date 2017年9月4日下午14:26:11
	 */
	@RequestMapping("/remove.action")
	@ResponseBody
	public Object delete(Long[] ids){
		String status = Constant.STATUS_SCUESS;
		String msg;
		try {
			for(Long id:ids){
				dataScenicRepository.delete(id);
			}
			msg = "删除成功！";
		} catch (Exception e) {
			status = Constant.STATUS_ERROR;
			msg = "删除失败！";
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("state", status);
		map.put("msg", msg);
		JSONObject json = JSONObject.fromObject(map);
		return json;
	}

	/**
	 * 行程管理景区列表读取
	 * @param region
	 * @param page
	 * @param rows
	 * @param name
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("/scenery_list.action")
	@ResponseBody
	public Object sceneryList(String region, @RequestParam(defaultValue = "1") int page,
							  @RequestParam(defaultValue = "10") int rows, String name, HttpServletRequest request,
							  HttpServletResponse response) throws Exception {
		DataScenic dto = new DataScenic();
		PageForm form = new PageForm();
		/*if (region != null && !region.equals("")) {
			if (region.endsWith("0000")) {
				region = region.substring(0, 2);
			} else if (region.endsWith("00")) {
				region = region.substring(0, 4);
			}
			dto.setRegion(region);
			;
		}*/

		form.setLimit(rows);
		form.setOffset((page-1)*rows);
		form.setSort("id");
		form.setOrder("ASC");
		dto.setName(name);
		List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
		Map<String, Object> queyMap = dataScenicRepository.findByAuto(dto, form);
		List<DataScenic> list = (List<DataScenic>) queyMap.get("rows");
		for (DataScenic data : list) {
			Map<String, Object> mapScen = new HashMap<String, Object>();
			mapScen.put("id", data.getId());
			mapScen.put("name", data.getName());// 景区名称
			mapScen.put("picture", data.getImgPath());// 景区引导图图片
			/*String levels = null;
			if (data.getResourceLevel() != null) {
				levels = sysDictRepository.findOne(data.getResourceLevel()).getName();
			} else {
				levels = "未评等级";
			}
			mapScen.put("type", levels);// 级别*/
			mapScen.put("phone", data.getPhone());
			mapScen.put("content", data.getContent());//景点描述
			mapList.add(mapScen);
		}
		response.setContentType("charset=UTF-8;text/json");
		response.setCharacterEncoding("UTF-8");
		response.setHeader("Access-Control-Allow-Origin", "*");
		JSONObject obj = new JSONObject();
		obj.put("rows", mapList);
		obj.put("total", queyMap.get("total"));
		return obj;

	}
	/**
	 * 根据id查询出景区景点--用于行程管理资源查询
	 * @param id       资源id
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@RequestMapping("/findOne.action")
	@ResponseBody
	public Object findOne(Long id, HttpServletResponse response) throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();

		try {
			DataScenic t = dataScenicRepository.findOne(id);
			map.put("id", String.valueOf(t.getId()));
			map.put("name", t.getName()); // 名称 != null ? t.getPhone() : "-"
			map.put("phone", t.getPhone() != null ? t.getPhone() : "-"); // 电话
			map.put("picture", t.getImgPath()); // 形象标识图片
			map.put("content", t.getContent()); // 景点描述
			//map.put("address", t.getAddress() != null ? t.getAddress() : "-"); // 地址
			//map.put("region", t.getRegion() != null ? sysRegionRepository.getOne(t.getRegion()).getShortName() : "-"); // 地区名称
			//String level = t.getResourceLevel() != null ? sysDictRepository.getOne(t.getResourceLevel()).getName() : "-";
			//map.put("type",  level); // 类型
			//map.put("adviceDate", "建议游玩:"+StringUtils.replaceHtml((t.getBestSeason() != null ? t.getBestSeason() : "-"))); // 建议游玩时间
			//map.put("adviceDate", "建议游玩:"+(t.getAdvicedate() != null ? t.getAdvicedate() : "-")); // 建议游玩时间
		} catch (Exception e) {
			e.printStackTrace();
		}
		response.setContentType("charset=UTF-8;text/json");
		response.setCharacterEncoding("UTF-8");
		net.sf.json.JSONObject obj = new net.sf.json.JSONObject();
		obj.put("data", map);
		response.getWriter().write(obj.toString());
		return null;
	}



}
