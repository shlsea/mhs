package com.skuo.mhs.data.res.pojo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * 
 * 
 * @ClassName: DataScenic
 * @Description:  景点信息
 * @Company: 四川西谷智慧科技有限公司
 * @author: 余德山
 * @date: 2017年9月4日 上午10:40:27
 * @version: V1.0.0
 */
@Entity
public class DataScenic {

	/**
	 * 主键Id
	 */
	@Id
	@GeneratedValue
	private Long id;

	/**
	 * 景点名称
	 */
	private String name;

	/**
	 * 景点地址
	 */
	private String address;

	/**
	 * 最佳游玩时间
	 */
	private String playtime;

	/**
	 * 语音讲解地址
	 */
	private String voicePath;

	/**
	 * 纬度
	 */
	private String lat;

	/**
	 * 经度
	 */
	private String lon;

	/**
	 * 联系人
	 */
	private String linkName;

	/**
	 * 联系电话
	 */
	private String phone;

	/**
	 * 副文本内容
	 */
	@Type(type = "text")
	private String content;

	/**
	 * 备注
	 */
	@Type(type = "text")
	private String remark;

	/**
	 * 图片路径
	 */
	private String imgPath;
	
	/**
	 * 720封面路径
	 */
	private String imgPathFor720;

	/**
	 * 状态<br>
	 * 0:禁用<br>
	 * 1:启用
	 */
	private Long state;
	
	/**
	 * 720路径
	 */
	private String urlOf720;
	
	/**
	 * 简介
	 */
	private String summary;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public String getLon() {
		return lon;
	}

	public void setLon(String lon) {
		this.lon = lon;
	}

	public String getLinkName() {
		return linkName;
	}

	public void setLinkName(String linkName) {
		this.linkName = linkName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getImgPath() {
		return imgPath;
	}

	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}

	public String getPlaytime() {
		return playtime;
	}

	public void setPlaytime(String playtime) {
		this.playtime = playtime;
	}

	public String getVoicePath() {
		return voicePath;
	}

	public void setVoicePath(String voicePath) {
		this.voicePath = voicePath;
	}

	public Long getState() {
		return state;
	}

	public void setState(Long state) {
		this.state = state;
	}

	public String getUrlOf720() {
		return urlOf720;
	}

	public void setUrlOf720(String urlOf720) {
		this.urlOf720 = urlOf720;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getImgPathFor720() {
		return imgPathFor720;
	}

	public void setImgPathFor720(String imgPathFor720) {
		this.imgPathFor720 = imgPathFor720;
	}
	
}
