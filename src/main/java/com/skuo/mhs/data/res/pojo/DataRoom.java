package com.skuo.mhs.data.res.pojo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * 
 * 
 * @ClassName: DataRoom
 * @Description: 酒店房间
 * @Company: 四川西谷智慧科技有限公司
 * @author: 余德山
 * @date: 2017年9月4日 上午11:03:41
 * @version: V1.0.0
 */
@Entity
public class DataRoom {

	/**
	 * 主键Id
	 */
	@Id
	@GeneratedValue
	private Long id;

	/**
	 * 房间名称
	 */
	private String roomName;

	/**
	 * 图片
	 */
	private String imgPath;

	/**
	 * 人均花费
	 */
	private String cost;

	/**
	 * 服务及措施
	 */
	private String service;

	/**
	 * 简介
	 */
	@Type(type = "text")
	private String content;

	/**
	 * 备注
	 */
	@Type(type = "text")
	private String remark;

	/**
	 * 所属酒店
	 */
	private Long belongHotel;

	/**
	 * 房间类型<br>
	 * 1:单人房<br>
	 * 2:双人房<br>
	 * 3.豪华套房
	 */
	private String roomType;

	/**
	 * 状态<br>
	 * 0:禁用<br>
	 * 1:启用
	 */
	private Long state;
	
	/**
	 * 简介
	 */
	private String summary;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public String getImgPath() {
		return imgPath;
	}

	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}

	public String getCost() {
		return cost;
	}

	public void setCost(String cost) {
		this.cost = cost;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}


	public String getRoomType() {
		return roomType;
	}

	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}

	public Long getState() {
		return state;
	}

	public void setState(Long state) {
		this.state = state;
	}

	public Long getBelongHotel() {
		return belongHotel;
	}

	public void setBelongHotel(Long belongHotel) {
		this.belongHotel = belongHotel;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

}
