package com.skuo.mhs.data.res.controller;

import net.sf.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.skuo.mhs.data.res.pojo.*;
import com.skuo.mhs.data.res.service.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName: DataScenicController
 * @Description: 景区景点逻辑控制器
 * @Company: 四川西谷智慧科技有限公司
 * @author: 钟操
 * @date: 2017月9日4 下午 14:08:26
 * @version: V1.0.0
 */
@Controller
@RequestMapping(value = "/resources")
public class DataResourcesController {

	@Autowired
	private DataScenicRepository dataScenicRepository;
	@Autowired
	private DataCarparkRepository dataCarparkRepository;
	@Autowired
	private DataAlarmPointRepository dataAlarmPointRepository;
	@Autowired
	private DataBusinessStreetRepository dataBusinessStreetRepository;
	@Autowired
	private DataRescueStationRepository dataRescueStationRepository;
	@Autowired
	private DataShoppingStreetRepository dataShoppingStreetRepository;
	@Autowired
	private DataTouristToiletRepository dataTouristToiletRepository;
	@Autowired
	private DataWharfRepository dataWharfRepository;
	@Autowired
	private DataScooterParkRepository dataScooterParkRepository;
	@Autowired
	private DataEmerSiteRepository dataEmerSiteRepository;
	@Autowired
	private DataHotelRepository dataHotelRepository;
	@Autowired
	private DataInnRepository dataInnRepository;
	@Autowired
	private DataSnackRepository dataSnackRepository;
	@Autowired
	private DataActingRepository dataActingRepository;
	@Autowired
	private DataMallRepository dataMallRepository;
	@Autowired
	private DataBarRepository dataBarRepository;
	@Autowired
	private DataRestaurantRepository dataRestaurantRepository;



	/**
	 * @Description: 景区设施-景区监控 查询所有有效资源
	 * @return
	 * @date 2017年9月5日下午11:26:11
	 */
	@RequestMapping(value = "/findAll.action", method = RequestMethod.POST)
	@ResponseBody
	public Object findAll(String type) {

		List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
		if(type.equals("ResourceType_1")){ //景区景点
			List<DataScenic> scenics = dataScenicRepository.findByState(1L);
			for (DataScenic data : scenics) {
				Map<String,Object> map=new HashMap<String,Object>();
				map.put("id", data.getId());
				map.put("name", data.getName());
				list.add(map);
			}
		}else if(type.equals("ResourceType_2")){ //停车场
			List<DataCarpark> carparks = dataCarparkRepository.findByState(1L);
			for (DataCarpark data : carparks) {
				Map<String,Object> map=new HashMap<String,Object>();
				map.put("id", data.getId());
				map.put("name", data.getName());
				list.add(map);
			}
		}else if(type.equals("ResourceType_3")){//电瓶车站
			List<DataScooterPark> scooterParks = dataScooterParkRepository.findByState(1L);
			for (DataScooterPark data : scooterParks) {
				Map<String,Object> map=new HashMap<String,Object>();
				map.put("id", data.getId());
				map.put("name", data.getName());
				list.add(map);
			}
		}else if(type.equals("ResourceType_4")){//应急场所
			List<DataEmerSite> emerSites = dataEmerSiteRepository.findByState(1L);
			for (DataEmerSite data : emerSites) {
				Map<String,Object> map=new HashMap<String,Object>();
				map.put("id", data.getId());
				map.put("name", data.getName());
				list.add(map);
			}
		}else if(type.equals("ResourceType_5")){//报警点
			List<DataAlarmPoint> points = dataAlarmPointRepository.findByState(1L);
			for (DataAlarmPoint data : points) {
				Map<String,Object> map=new HashMap<String,Object>();
				map.put("id", data.getId());
				map.put("name", data.getName());
				list.add(map);
			}
		}else if(type.equals("ResourceType_6")){//旅游厕所
			List<DataTouristToilet> toilets = dataTouristToiletRepository.findByState(1L);
			for (DataTouristToilet data : toilets) {
				Map<String,Object> map=new HashMap<String,Object>();
				map.put("id", data.getId());
				map.put("name", data.getName());
				list.add(map);
			}
		}else if(type.equals("ResourceType_7")){//救护点
			List<DataRescueStation> stations = dataRescueStationRepository.findByState(1L);
			for (DataRescueStation data : stations) {
				Map<String,Object> map=new HashMap<String,Object>();
				map.put("id", data.getId());
				map.put("name", data.getName());
				list.add(map);
			}
		}else if(type.equals("ResourceType_8")){//码头
			List<DataWharf> wharf = dataWharfRepository.findByState(1L);
			for (DataWharf data : wharf) {
				Map<String,Object> map=new HashMap<String,Object>();
				map.put("id", data.getId());
				map.put("name", data.getName());
				list.add(map);
			}
		}else if(type.equals("ResourceType_9")){//商业街
			List<DataBusinessStreet> streets = dataBusinessStreetRepository.findByState(1L);
			for (DataBusinessStreet data : streets) {
				Map<String,Object> map=new HashMap<String,Object>();
				map.put("id", data.getId());
				map.put("name", data.getName());
				list.add(map);
			}
		}else { //购物场所
			List<DataShoppingStreet> shoppingStreets = dataShoppingStreetRepository.findByState(1L);
			for (DataShoppingStreet data : shoppingStreets) {
				Map<String,Object> map=new HashMap<String,Object>();
				map.put("id", data.getId());
				map.put("name", data.getName());
				list.add(map);
			}
		}
		JSONArray json = JSONArray.fromObject(list);
		return json;
	}



	/**
	 * @Description: 图片库- 查询所有有效资源
	 * @return
	 * @date 2017年9月5日下午11:26:11
	 */
	@RequestMapping(value = "/findResourcesAll.action", method = RequestMethod.POST)
	@ResponseBody
	public Object findResourcesAll(String type) {

		List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
		if(type.equals("PicResourceType_1")){ //景区景点
			List<DataScenic> scenics = dataScenicRepository.findByState(1L);
			for (DataScenic data : scenics) {
				Map<String,Object> map=new HashMap<String,Object>();
				map.put("id", data.getId());
				map.put("name", data.getName());
				list.add(map);
			}
		}else if(type.equals("PicResourceType_2")){ //停车场
			List<DataCarpark> carparks = dataCarparkRepository.findByState(1L);
			for (DataCarpark data : carparks) {
				Map<String,Object> map=new HashMap<String,Object>();
				map.put("id", data.getId());
				map.put("name", data.getName());
				list.add(map);
			}
		}else if(type.equals("PicResourceType_3")){//电瓶车站
			List<DataScooterPark> scooterParks = dataScooterParkRepository.findByState(1L);
			for (DataScooterPark data : scooterParks) {
				Map<String,Object> map=new HashMap<String,Object>();
				map.put("id", data.getId());
				map.put("name", data.getName());
				list.add(map);
			}
		}else if(type.equals("PicResourceType_4")){//应急场所
			List<DataEmerSite> emerSites = dataEmerSiteRepository.findByState(1L);
			for (DataEmerSite data : emerSites) {
				Map<String,Object> map=new HashMap<String,Object>();
				map.put("id", data.getId());
				map.put("name", data.getName());
				list.add(map);
			}
		}else if(type.equals("PicResourceType_5")){//报警点
			List<DataAlarmPoint> points = dataAlarmPointRepository.findByState(1L);
			for (DataAlarmPoint data : points) {
				Map<String,Object> map=new HashMap<String,Object>();
				map.put("id", data.getId());
				map.put("name", data.getName());
				list.add(map);
			}
		}else if(type.equals("PicResourceType_6")){//旅游厕所
			List<DataTouristToilet> toilets = dataTouristToiletRepository.findByState(1L);
			for (DataTouristToilet data : toilets) {
				Map<String,Object> map=new HashMap<String,Object>();
				map.put("id", data.getId());
				map.put("name", data.getName());
				list.add(map);
			}
		}else if(type.equals("PicResourceType_7")){//救护点
			List<DataRescueStation> stations = dataRescueStationRepository.findByState(1L);
			for (DataRescueStation data : stations) {
				Map<String,Object> map=new HashMap<String,Object>();
				map.put("id", data.getId());
				map.put("name", data.getName());
				list.add(map);
			}
		}else if(type.equals("PicResourceType_8")){//码头
			List<DataWharf> wharf = dataWharfRepository.findByState(1L);
			for (DataWharf data : wharf) {
				Map<String,Object> map=new HashMap<String,Object>();
				map.put("id", data.getId());
				map.put("name", data.getName());
				list.add(map);
			}
		}else if(type.equals("PicResourceType_9")){//商业街
			List<DataBusinessStreet> streets = dataBusinessStreetRepository.findByState(1L);
			for (DataBusinessStreet data : streets) {
				Map<String,Object> map=new HashMap<String,Object>();
				map.put("id", data.getId());
				map.put("name", data.getName());
				list.add(map);
			}
		}else if(type.equals("PicResourceType_10")){ //购物场所
			List<DataShoppingStreet> shoppingStreets = dataShoppingStreetRepository.findByState(1L);
			for (DataShoppingStreet data : shoppingStreets) {
				Map<String,Object> map=new HashMap<String,Object>();
				map.put("id", data.getId());
				map.put("name", data.getName());
				list.add(map);
			}
		}else if(type.equals("PicResourceType_11")){ //商务酒店
			List<DataHotel> hotels = dataHotelRepository.findByStateAndType(1L,1l);
			for (DataHotel data : hotels) {
				Map<String,Object> map=new HashMap<String,Object>();
				map.put("id", data.getId());
				map.put("name", data.getName());
				list.add(map);
			}
		}else if(type.equals("PicResourceType_12")){ //主题酒店
			List<DataHotel> hotels = dataHotelRepository.findByStateAndType(1L,2l);
			for (DataHotel data : hotels) {
				Map<String,Object> map=new HashMap<String,Object>();
				map.put("id", data.getId());
				map.put("name", data.getName());
				list.add(map);
			}
		}else if(type.equals("PicResourceType_13")){ //民宿
			List<DataInn> inn = dataInnRepository.findByState(1L);
			for (DataInn data : inn) {
				Map<String,Object> map=new HashMap<String,Object>();
				map.put("id", data.getId());
				map.put("name", data.getName());
				list.add(map);
			}
		}else if(type.equals("PicResourceType_14")){ //餐厅
			List<DataRestaurant> restaurants = dataRestaurantRepository.findByState(1L);
			for (DataRestaurant data : restaurants) {
				Map<String,Object> map=new HashMap<String,Object>();
				map.put("id", data.getId());
				map.put("name", data.getName());
				list.add(map);
			}
		}else if(type.equals("PicResourceType_15")){ //小吃冷饮
			List<DataSnack> snacks = dataSnackRepository.findByState(1L);
			for (DataSnack data : snacks) {
				Map<String,Object> map=new HashMap<String,Object>();
				map.put("id", data.getId());
				map.put("name", data.getName());
				list.add(map);
			}
		}else if(type.equals("PicResourceType_16")){ //酒吧
			List<DataBar> bars = dataBarRepository.findByState(1L);
			for (DataBar data : bars) {
				Map<String,Object> map=new HashMap<String,Object>();
				map.put("id", data.getId());
				map.put("name", data.getName());
				list.add(map);
			}
		}else if(type.equals("PicResourceType_17")){ //演出场所
			List<DataActing> actings = dataActingRepository.findByState(1L);
			for (DataActing data : actings) {
				Map<String,Object> map=new HashMap<String,Object>();
				map.put("id", data.getId());
				map.put("name", data.getName());
				list.add(map);
			}
		}else { //特色商店
			List<DataMall> malls = dataMallRepository.findByState(1L);
			for (DataMall data : malls) {
				Map<String,Object> map=new HashMap<String,Object>();
				map.put("id", data.getId());
				map.put("name", data.getName());
				list.add(map);
			}
		}
		JSONArray json = JSONArray.fromObject(list);
		return json;
	}
}
