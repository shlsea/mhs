package com.skuo.mhs.data.res.service;

import org.springframework.stereotype.Service;

import com.skuo.mhs.data.res.pojo.DataBar;
import com.skuo.mhs.support.CustomRepository;

import java.util.List;

/**
 * @ClassName: DataBarRepository
 * @Description: 酒吧service
 * @Company: 四川西谷智慧科技有限公司
 * @author: YanHiroShi
 * @date: 2017/9/7 10:42
 * @version: V1.0.0
 */
@Service
public interface DataBarRepository extends CustomRepository<DataBar, Long> {

    /**
     * @DATE: 2017/9/7-10:52.
     * @Description:查询所有有效酒吧
     */
    List<DataBar> findByState(Long state);
}
