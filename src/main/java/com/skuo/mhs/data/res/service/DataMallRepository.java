package com.skuo.mhs.data.res.service;

import org.springframework.stereotype.Service;

import com.skuo.mhs.data.res.pojo.DataMall;
import com.skuo.mhs.support.CustomRepository;

import java.util.List;

/**
 * @ClassName: DataMallRepository
 * @Description: 特色商店实现DAO
 * @Company: 四川西谷智慧科技有限公司
 * @author: 钟操
 * @date: 2017年9月7日 下午14:26:52
 * @version: V1.0.0
 */
@Service
public interface DataMallRepository extends CustomRepository<DataMall, Long> {

    /**
     * @Description:查询所有有效特色商店
     * @return
     * @date 2017年9月7日 下午15:57:16
     *
     */
    List<DataMall> findByState(Long state);
}
