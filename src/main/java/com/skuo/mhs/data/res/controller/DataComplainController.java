package com.skuo.mhs.data.res.controller;

import net.sf.json.JSONObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.skuo.mhs.data.res.pojo.DataComplain;
import com.skuo.mhs.data.res.service.DataComplainRepository;
import com.skuo.mhs.form.PageForm;
import com.skuo.mhs.utils.Constant;

import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName: DataComplainController
 * @Description: 投诉逻辑控制器
 * @Company: 四川西谷智慧科技有限公司
 * @author: 钟操
 * @date: 2017年9月7日  下午 13:30:26
 * @version: V1.0.0
 */
@Controller
@RequestMapping(value = "/complain")
public class DataComplainController {

	@Autowired
	private DataComplainRepository dataComplainRepository;
	/**
	 * 跳转投诉管理列表页面
	 * @Description:TODO
	 * @return
	 * @date: 2017年9月7日  下午 13:49:26
	 */
	@RequestMapping("/page.action")
	public String page(){
		return "res/complain_list";
	}


	/**
	 * @Description: 读取投诉列表
	 * @param pageForm
	 * @param dto
	 * @return:
	 * @date: 2017年9月13日 下午13:50:26
	 */
	@RequestMapping(value = "/list.action", method = RequestMethod.POST)
	@ResponseBody
	public Object list(PageForm pageForm, DataComplain dto) {
		Map<String, Object> map = dataComplainRepository.findByAuto(dto, pageForm);
		return map;
	}

	/**
	 * @Description:新增/编辑投诉
	 * @param id
 	 * @param model
	 * @return:
	 * @date: 2017年9月13日  下午13:55:36
	 */
	@RequestMapping("/edit.action")
	public String edit(Long id,  Model model) {
		DataComplain dto = new DataComplain();
		if (id != null) {
			dto = dataComplainRepository.findOne(id);
		}
		model.addAttribute("dto", dto);
		return "res/complain_input";
	}


	/**
	 * @Description:保存投诉信息
	 * @param dto
	 * @return
	 * @date 2017年9月13日 下午13:58:04
	 */
	@RequestMapping(value = "/save.action", method = RequestMethod.POST)
	public String save(@ModelAttribute DataComplain dto) {
		if (dto.getState() == null || dto.getState().equals("")) {
			dto.setState(0L);
		}
		if(dto.getId()!=null){
			DataComplain data = dataComplainRepository.getOne(dto.getId());
			BeanUtils.copyProperties(dto, data,new String[] { "id"});
			dataComplainRepository.save(data);

		}else {
			dataComplainRepository.save(dto);
		}
		return "forward:/success.action";
	}

	/**
	 * @Description:删除投诉
	 * @param ids
	 * @return
	 * @date 2017年9月13日 下午14:26:11
	 */
	@RequestMapping("/remove.action")
	@ResponseBody
	public Object delete(Long[] ids){
		String status = Constant.STATUS_SCUESS;
		String msg;
		try {
			for(Long id:ids){
				dataComplainRepository.delete(id);
			}
			msg = "删除成功！";
		} catch (Exception e) {
			status = Constant.STATUS_ERROR;
			msg = "删除失败！";
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("state", status);
		map.put("msg", msg);
		JSONObject json = JSONObject.fromObject(map);
		return json;
	}


}
