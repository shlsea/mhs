package com.skuo.mhs.data.res.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.skuo.mhs.data.res.pojo.DataComplain;
import com.skuo.mhs.support.CustomRepository;

/**
 * @ClassName: DataComplainRepository 
 * @Description: 投诉建议类
 * @Company: 四川西谷智慧科技有限公司
 * @author: songhailing 
 * @date: 2017年9月13日 上午10:23:52 
 * @version: V1.0.0
 */
@Service
public interface DataComplainRepository extends CustomRepository<DataComplain, Long> {

    /**
     * @Description:查询所有有效投诉信息
     * @return
     * @date 2017年9月7日 下午15:57:16
     *
     */
    List<DataComplain> findByState(Long state);
}
