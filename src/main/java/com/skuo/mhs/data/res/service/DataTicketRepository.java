package com.skuo.mhs.data.res.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.skuo.mhs.data.res.pojo.DataTicket;
import com.skuo.mhs.support.CustomRepository;

/**
 * @ClassName: DataTicketRepository
 * @Description: 票务实现DAO
 * @Company: 四川西谷智慧科技有限公司
 * @author: 钟操
 * @date: 2017年9月7日 下午14:26:52
 * @version: V1.0.0
 */
@Service
public interface DataTicketRepository extends CustomRepository<DataTicket, Long> {

    /**
     * @Description:查询所有有效门票类型
     * @return
     * @date 2017年9月7日 下午15:57:16
     *
     */
    List<DataTicket> findByState(Long state);
}
