package com.skuo.mhs.data.res.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.skuo.mhs.data.res.pojo.DataEmerSite;
import com.skuo.mhs.data.res.service.DataEmerSiteRepository;
import com.skuo.mhs.form.PageForm;
import com.skuo.mhs.utils.Constant;

import net.sf.json.JSONObject;

/**
 *
 * @ClassName: DataEmerSiteController.java
 * @Description: 应急场所逻辑控制器
 * @Company: 四川西谷智慧科技有限公司
 * @author: 蔡磊
 * @date: 2017年9月4日 下午2:29:17
 * @version: V1.0.0
 */
@Controller
@RequestMapping(value = "/emer_site")
public class DataEmerSiteController {

	@Autowired
	private DataEmerSiteRepository dataEmerSiteRepository;

	/**
	 *
	 * @Description: 跳转应急场所管理列表界面
	 * @return
	 * @date: 2017年9月4日 下午2:30:28
	 */
	@RequestMapping("/page.action")
	public String page() {
		return "res/emer_site_list";
	}

	/**
	 *
	 * @Description: 获取应急场所列表数据
	 * @param pageForm
	 * @param dto
	 * @return
	 * @date: 2017年9月4日 下午2:35:06
	 */
	@RequestMapping(value = "/list.action", method = RequestMethod.POST)
	@ResponseBody
	public Object list(PageForm pageForm, DataEmerSite dto) {
		Map<String, Object> map = dataEmerSiteRepository.findByAuto(dto, pageForm);
		return map;
	}

	/**
	 *
	 * @Description: 新增/编辑应急场所信息
	 * @param id
	 * @param model
	 * @return
	 * @date: 2017年9月4日 下午3:22:34
	 */
	@RequestMapping("/edit.action")
	public String edit(Long id, Model model) {
		DataEmerSite dto = new DataEmerSite();
		if (id != null) {
			dto = dataEmerSiteRepository.findOne(id);
		}
		model.addAttribute("dto", dto);
		return "res/emer_site_input";
	}

	/**
	 *
	 * @Description: 保存应急场所信息
	 * @param dto
	 * @return
	 * @date: 2017年9月4日 下午3:43:05
	 */
	@RequestMapping(value = "/save.action", method = RequestMethod.POST)
	public String save(@ModelAttribute DataEmerSite dto) {
		if (dto.getState() == null || dto.getState().equals("")) {
			dto.setState(0L);
		}
		if (dto.getId() != null) {
			DataEmerSite data = dataEmerSiteRepository.getOne(dto.getId());
			BeanUtils.copyProperties(dto, data, new String[] { "id" });
			dataEmerSiteRepository.save(data);

		} else {
			dataEmerSiteRepository.save(dto);
		}
		return "forward:/success.action";
	}
	
	/**
	 *
	 * @Description: 删除应急场所信息
	 * @param ids
	 * @return
	 * @date: 2017年9月4日 下午3:50:09
	 */
	@RequestMapping("/remove.action")
	@ResponseBody
	public Object delete(Long[] ids){
		String status = Constant.STATUS_SCUESS;
		String msg;
		try {
			for(Long id:ids){
				dataEmerSiteRepository.delete(id);
			}
			msg = "删除成功！";
		} catch (Exception e) {
			status = Constant.STATUS_ERROR;
			msg = "删除失败！";
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("state", status);
		map.put("msg", msg);
		JSONObject json = JSONObject.fromObject(map);
		return json;
	}
}
