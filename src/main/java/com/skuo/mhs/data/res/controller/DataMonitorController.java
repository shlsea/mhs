package com.skuo.mhs.data.res.controller;

import net.sf.json.JSONObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.skuo.mhs.data.res.pojo.DataMonitor;
import com.skuo.mhs.data.res.service.DataMonitorRepository;
import com.skuo.mhs.form.PageForm;
import com.skuo.mhs.system.pojo.SysDict;
import com.skuo.mhs.system.service.SysDictRepository;
import com.skuo.mhs.utils.Constant;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName: DataMonitorController
 * @Description: 视频监控逻辑控制器
 * @Company: 四川西谷智慧科技有限公司
 * @author: 钟操
 * @date: 2017年9月4日  下午 14:33:26
 * @version: V1.0.0
 */
@Controller
@RequestMapping(value = "/monitor")
@SuppressWarnings("unchecked")
public class DataMonitorController {

	@Autowired
	private DataMonitorRepository dataMonitorRepository;
	@Autowired
	private SysDictRepository sysDictRepository;
	/**
	 * 跳转视频监控管理列表页面
	 * @Description:TODO
	 * @return
	 * @date: 2017年9月4日  下午 14:34:26
	 */
	@RequestMapping("/page.action")
	public String page(){
		return "res/monitor_list";
	}


	/**
	 * @Description: 读取视频监控列表
	 * @param pageForm
	 * @param dto
	 * @return:
	 * @date: 2017年9月4日 下午14:36:26
	 */
	@RequestMapping(value = "/list.action", method = RequestMethod.POST)
	@ResponseBody
	public Object list(PageForm pageForm, DataMonitor dto) {
		Map<String, Object> queyrMap = dataMonitorRepository.findByAuto(dto, pageForm);
		Map<String, Object> map = new HashMap<String, Object>();
		List<DataMonitor> list = (List<DataMonitor>) queyrMap.get("rows");
		List<Map<String, Object>> _list = new ArrayList<Map<String, Object>>();
		for (DataMonitor data : list) {
			Map<String, Object> _map = new HashMap<String, Object>();
			_map.put("id", data.getId());
			_map.put("name", data.getName());
			_map.put("user", data.getUser());
			_map.put("brand", data.getBrand());
			_map.put("lat", data.getLat());
			_map.put("lon", data.getLon());
			String resources=null;
			if(data.getType().equals("ResourceType_1")){
				resources="景区景点";
			}else if(data.getType().equals("ResourceType_2")){
				resources="停车场";
			}else if(data.getType().equals("ResourceType_3")){
				resources="电瓶车站";
			}else if(data.getType().equals("ResourceType_4")){
				resources="应急场所";
			}else if(data.getType().equals("ResourceType_5")){
				resources="报警点";
			}else if(data.getType().equals("ResourceType_6")){
				resources="旅游厕所";
			}else if(data.getType().equals("ResourceType_7")){
				resources="救助点";
			}else if(data.getType().equals("ResourceType_8")){
				resources="码头";
			}else if(data.getType().equals("ResourceType_9")){
				resources="商业街";
			}else {
				resources="购物场所";
			}
			_map.put("type", resources);
			_map.put("state", data.getState());
			_list.add(_map);
		}
		map.put("rows", _list);
		map.put("total", queyrMap.get("total"));
		return map;
	}

	/**
	 * @Description:新增/编辑视频监控
	 * @param id
 	 * @param model
	 * @return:
	 * @date: 2017年9月4日  下午14:38:26
	 */
	@RequestMapping("/edit.action")
	public String edit(Long id,  Model model) {
		List<SysDict> resourceType = sysDictRepository.findByParent("ResourceType"); // 读取数据字典资源类型
		DataMonitor dto = new DataMonitor();
		if (id != null) {
			dto = dataMonitorRepository.findOne(id);
		}
		model.addAttribute("dto", dto);
		model.addAttribute("resourceType", resourceType);
		return "res/monitor_input";
	}


	/**
	 * @Description:保存视频监控
	 * @param dto
	 * @return
	 * @date 2017年9月4日下午14:25:04
	 */
	@RequestMapping(value = "/save.action", method = RequestMethod.POST)
	public String save(@ModelAttribute DataMonitor dto){
		if (dto.getState() == null || dto.getState().equals("")) {
			dto.setState(0L);
		}
		if(dto.getId()!=null){
			DataMonitor data = dataMonitorRepository.getOne(dto.getId());
			BeanUtils.copyProperties(dto, data,new String[] { "id"});
			dataMonitorRepository.save(data);

		}else {
			dataMonitorRepository.save(dto);
		}
		return "forward:/success.action";
	}

	/**
	 * @Description:删除视频监控
	 * @param ids
	 * @return
	 * @date 2017年9月4日下午14:40:11
	 */
	@RequestMapping("/remove.action")
	@ResponseBody
	public Object delete(Long[] ids){
		String status = Constant.STATUS_SCUESS;
		String msg;
		try {
			for(Long id:ids){
				dataMonitorRepository.delete(id);
			}
			msg = "删除成功！";
		} catch (Exception e) {
			status = Constant.STATUS_ERROR;
			msg = "删除失败！";
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("state", status);
		map.put("msg", msg);
		JSONObject json = JSONObject.fromObject(map);
		return json;
	}


}
