package com.skuo.mhs.data.res.service;

import org.springframework.stereotype.Service;

import com.skuo.mhs.data.res.pojo.DataAlarmPoint;
import com.skuo.mhs.support.CustomRepository;

import java.util.List;

/**
 * 
 * 
 * @ClassName: DataAlarmPointRepository
 * @Description: 报警点实现DAO
 * @Company: 四川西谷智慧科技有限公司
 * @author: 余德山
 * @date: 2017年9月4日 上午11:31:52
 * @version: V1.0.0
 */
@Service
public interface DataAlarmPointRepository extends CustomRepository<DataAlarmPoint, Long> {
    /**
     * @Description:查询所有有效商业街
     * @return
     * @date 2017年9月5日下午18:18:16
     */
    List<DataAlarmPoint> findByState(Long state);
  
}
