package com.skuo.mhs.data.res.controller;

import net.sf.json.JSONObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.skuo.mhs.data.res.pojo.DataCarpark;
import com.skuo.mhs.data.res.service.DataCarparkRepository;
import com.skuo.mhs.form.PageForm;
import com.skuo.mhs.utils.Constant;

import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName: DataCarparkController
 * @Description: 景区停车场信息逻辑控制器
 * @Company: 四川西谷智慧科技有限公司
 * @author: 钟操
 * @date: 2017年9月4日  下午 14:40:26
 * @version: V1.0.0
 */
@Controller
@RequestMapping(value = "/carpark")
public class DataCarparkController {

	@Autowired
	private DataCarparkRepository dataCarparkRepository;
	/**
	 * 跳转景区停车场信息管理列表页面
	 * @Description:TODO
	 * @return
	 * @date: 2017年9月4日  下午 14:10:26
	 */
	@RequestMapping("/page.action")
	public String page(){
		return "res/carpark_list";
	}


	/**
	 * @Description: 读取景区停车场信息列表
	 * @param pageForm
	 * @param dto
	 * @return:
	 * @date: 2017年9月4日 下午14:18:26
	 */
	@RequestMapping(value = "/list.action", method = RequestMethod.POST)
	@ResponseBody
	public Object list(PageForm pageForm, DataCarpark dto) {
		Map<String, Object> map = dataCarparkRepository.findByAuto(dto, pageForm);
		return map;
	}

	/**
	 * @Description:新增/编辑景区停车场信息
	 * @param id
 	 * @param model
	 * @return:
	 * @date: 2017年9月4日  下午14:20:26
	 */
	@RequestMapping("/edit.action")
	public String edit(Long id,  Model model) {

		DataCarpark dto = new DataCarpark();
		if (id != null) {
			dto = dataCarparkRepository.findOne(id);
		}
		model.addAttribute("dto", dto);
		return "res/carpark_input";
	}


	/**
	 * @Description:景区停车场信息
	 * @param dto
	 * @return
	 * @date 2017年9月4日下午14:25:04
	 */
	@RequestMapping(value = "/save.action", method = RequestMethod.POST)
	public String save(@ModelAttribute DataCarpark dto) {
		if (dto.getState() == null || dto.getState().equals("")) {
			dto.setState(0L);
		}
		if(dto.getId()!=null){
			DataCarpark data = dataCarparkRepository.getOne(dto.getId());
			BeanUtils.copyProperties(dto, data,new String[] { "id"});
			dataCarparkRepository.save(data);

		}else {
			dataCarparkRepository.save(dto);
		}
		return "forward:/success.action";
	}

	/**
	 * @Description:删除景区停车场信息
	 * @param ids
	 * @return
	 * @date 2017年9月4日下午14:26:11
	 */
	@RequestMapping("/remove.action")
	@ResponseBody
	public Object delete(Long[] ids){
		String status = Constant.STATUS_SCUESS;
		String msg;
		try {
			for(Long id:ids){
				dataCarparkRepository.delete(id);
			}
			msg = "删除成功！";
		} catch (Exception e) {
			status = Constant.STATUS_ERROR;
			msg = "删除失败！";
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("state", status);
		map.put("msg", msg);
		JSONObject json = JSONObject.fromObject(map);
		return json;
	}

}
