package com.skuo.mhs.data.res.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.skuo.mhs.data.res.pojo.DataTicketDetail;
import com.skuo.mhs.support.CustomRepository;

/**
 * @ClassName: DataTicketRepository
 * @Description: 票务详情所实现DAO
 * @Company: 四川西谷智慧科技有限公司
 * @author: 钟操
 * @date: 2017年9月7日 下午14:26:52
 * @version: V1.0.0
 */
@Service
public interface DataTicketDetailRepository extends CustomRepository<DataTicketDetail, Long> {

    /**
     * @Description:查询所有有效票务套餐
     * @return
     * @date 2017年9月7日 下午15:57:16
     *
     */
    List<DataTicketDetail> findByState(Long state);

    /**
     * @Description:根据票务id获取票务对象
     * @param id
     * @return 
     * @date 2017年9月19日下午5:17:44
     */
	List<DataTicketDetail> findByParentId(Long id);
}
