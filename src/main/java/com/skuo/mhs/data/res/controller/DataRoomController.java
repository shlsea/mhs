package com.skuo.mhs.data.res.controller;

import net.sf.json.JSONObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.skuo.mhs.data.res.pojo.DataHotel;
import com.skuo.mhs.data.res.pojo.DataRoom;
import com.skuo.mhs.data.res.service.DataHotelRepository;
import com.skuo.mhs.data.res.service.DataRoomRepository;
import com.skuo.mhs.form.PageForm;
import com.skuo.mhs.system.pojo.SysDict;
import com.skuo.mhs.system.service.SysDictRepository;
import com.skuo.mhs.utils.Constant;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName: DataThemeRoomController
 * @Description: 主题房间逻辑控制器
 * @Company: 四川西谷智慧科技有限公司
 * @author: 钟操
 * @date: 2017年9月7日  上午 10:40:26
 * @version: V1.0.0
 */
@Controller
@RequestMapping(value = "/room")
public class DataRoomController {

	@Autowired
	private DataRoomRepository dataRoomRepository;

	@Autowired
	private DataHotelRepository dataHotelRepository;
	
	@Autowired
	private SysDictRepository sysDictRepository;
	
	/**
	 * 跳转主题房间管理列表页面
	 * @Description:TODO
	 * @return
	 * @date: 2017年9月7日  上午 10:41:16
	 */
	@RequestMapping("/page.action")
	public String page( ){
		return "res/room_list";
	}


	/**
	 * @Description: 读取主题房间列表
	 * @param pageForm
	 * @param dto
	 * @return:
	 * @date: 2017年9月7日  上午 10:41:16
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/list.action", method = RequestMethod.POST)
	@ResponseBody
	public Object list(PageForm pageForm, DataRoom dto) {
		Map<String, Object> queyrMap = dataRoomRepository.findByAuto(dto, pageForm);
		Map<String, Object> map = new HashMap<String, Object>();
		List<DataRoom> list = (List<DataRoom>) queyrMap.get("rows");
		List<Map<String, Object>> _list = new ArrayList<>();
		for (DataRoom data : list) {
			Map<String, Object> _map = new HashMap<String, Object>();
			_map.put("id", data.getId());
			_map.put("roomName", data.getRoomName());
			_map.put("imgPath", data.getImgPath());
			_map.put("cost", data.getCost());
			_map.put("state", data.getState());
			_map.put("summary", data.getSummary());
			String roomType="";
			if("RoomType2".equals(data.getRoomType())){
				roomType="豪华套房";
			}else if("RoomType1".equals(data.getRoomType())){
				roomType="双人间";
			}else {
				roomType="单人间";
			}
			_map.put("roomType", roomType);
			_map.put("state", data.getState());
			_list.add(_map);
		}
		map.put("rows", _list);
		map.put("total", queyrMap.get("total"));
		return map;
	}

	/**
	 * @Description:新增/编辑主题房间
	 * @param id
 	 * @param model
	 * @return:
	 * @date: 2017年9月7日  上午 10:43:50
	 */
	@RequestMapping("/edit.action")
	public String edit(Long id,  Model model) {
		DataRoom dto = new DataRoom();
		if (id != null) {
			dto = dataRoomRepository.findOne(id);
		}
		List<SysDict> roomTypes = sysDictRepository.findByParent("RoomType");
		List<DataHotel> hotels = dataHotelRepository.findByState(1L);
		model.addAttribute("dto", dto);
		model.addAttribute("hotels", hotels);
		model.addAttribute("roomTypes", roomTypes);
		return "res/room_input";
	}


	/**
	 * @Description:保存主题房间信息
	 * @param dto
	 * @return
	 * @date: 2017年9月7日  上午 10:47:36
	 */
	@RequestMapping(value = "/save.action", method = RequestMethod.POST)
	public String save(@ModelAttribute DataRoom dto) {
		if (dto.getState() == null || dto.getState().equals("")) {
			dto.setState(0L);
		}
		if(dto.getId()!=null){
			DataRoom data = dataRoomRepository.getOne(dto.getId());
			BeanUtils.copyProperties(dto, data,new String[] { "id"});
			dataRoomRepository.save(data);

		}else {
			dataRoomRepository.save(dto);
		}
		return "forward:/success.action";
	}

	/**
	 * @Description:删除主题房间
	 * @param ids
	 * @return
	 * @date: 2017年9月7日  上午 10:50:11
	 */
	@RequestMapping("/remove.action")
	@ResponseBody
	public Object delete(Long[] ids){
		String status = Constant.STATUS_SCUESS;
		String msg;
		try {
			for(Long id:ids){
				dataRoomRepository.delete(id);
			}
			msg = "删除成功！";
		} catch (Exception e) {
			status = Constant.STATUS_ERROR;
			msg = "删除失败！";
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("state", status);
		map.put("msg", msg);
		JSONObject json = JSONObject.fromObject(map);
		return json;
	}


}
