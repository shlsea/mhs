package com.skuo.mhs.data.res.controller;

import net.sf.json.JSONObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.skuo.mhs.data.res.pojo.DataMall;
import com.skuo.mhs.data.res.service.DataMallRepository;
import com.skuo.mhs.form.PageForm;
import com.skuo.mhs.utils.Constant;

import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName: DataMallController
 * @Description: 特色商店逻辑控制器
 * @Company: 四川西谷智慧科技有限公司
 * @author: 钟操
 * @date: 2017年9月7日  下午 13:30:26
 * @version: V1.0.0
 */
@Controller
@RequestMapping(value = "/mall")
public class DataMallController {

	@Autowired
	private DataMallRepository dataMallRepository;
	/**
	 * 跳转特色商店管理列表页面
	 * @Description:TODO
	 * @return
	 * @date: 2017年9月7日  下午 13:49:26
	 */
	@RequestMapping("/page.action")
	public String page(){
		return "res/mall_list";
	}


	/**
	 * @Description: 读取特色商店列表
	 * @param pageForm
	 * @param dto
	 * @return:
	 * @date: 2017年9月7日 下午13:50:26
	 */
	@RequestMapping(value = "/list.action", method = RequestMethod.POST)
	@ResponseBody
	public Object list(PageForm pageForm, DataMall dto) {
		Map<String, Object> map = dataMallRepository.findByAuto(dto, pageForm);
		return map;
	}

	/**
	 * @Description:新增/编辑特色商店
	 * @param id
 	 * @param model
	 * @return:
	 * @date: 2017年9月7日  下午13:55:36
	 */
	@RequestMapping("/edit.action")
	public String edit(Long id,  Model model) {
		DataMall dto = new DataMall();
		if (id != null) {
			dto = dataMallRepository.findOne(id);
		}
		model.addAttribute("dto", dto);
		return "res/mall_input";
	}


	/**
	 * @Description:保存特色商店信息
	 * @param dto
	 * @return
	 * @date 2017年9月7日 下午13:58:04
	 */
	@RequestMapping(value = "/save.action", method = RequestMethod.POST)
	public String save(@ModelAttribute DataMall dto) {
		if (dto.getState() == null || dto.getState().equals("")) {
			dto.setState(0L);
		}
		if(dto.getId()!=null){
			DataMall data = dataMallRepository.getOne(dto.getId());
			BeanUtils.copyProperties(dto, data,new String[] { "id"});
			dataMallRepository.save(data);

		}else {
			dataMallRepository.save(dto);
		}
		return "forward:/success.action";
	}

	/**
	 * @Description:删除特色商店
	 * @param ids
	 * @return
	 * @date 2017年9月4日 下午14:26:11
	 */
	@RequestMapping("/remove.action")
	@ResponseBody
	public Object delete(Long[] ids){
		String status = Constant.STATUS_SCUESS;
		String msg;
		try {
			for(Long id:ids){
				dataMallRepository.delete(id);
			}
			msg = "删除成功！";
		} catch (Exception e) {
			status = Constant.STATUS_ERROR;
			msg = "删除失败！";
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("state", status);
		map.put("msg", msg);
		JSONObject json = JSONObject.fromObject(map);
		return json;
	}


}
