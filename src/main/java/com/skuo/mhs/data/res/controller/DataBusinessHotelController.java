package com.skuo.mhs.data.res.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.skuo.mhs.data.res.pojo.DataHotel;
import com.skuo.mhs.data.res.service.DataHotelRepository;
import com.skuo.mhs.form.PageForm;
import com.skuo.mhs.system.pojo.SysDict;
import com.skuo.mhs.system.service.SysDictRepository;
import com.skuo.mhs.utils.Constant;

import net.sf.json.JSONObject;

/**
 * @ClassName: DataBusinessHotelController
 * @Description: 商务酒店逻辑控制器
 * @Company: 四川西谷智慧科技有限公司
 * @author: 钟操
 * @date: 2017年9月7日 上午 10:40:26
 * @version: V1.0.0
 */
@Controller
@RequestMapping(value = "/business_hotel")
public class DataBusinessHotelController {

	@Autowired
	private DataHotelRepository dataHotelRepository;
	@Autowired
	private SysDictRepository sysDictRepository;
	
	/**
	 * 跳转商务酒店管理列表页面
	 * @Description:TODO
	 * @return
	 * @date: 2017年9月7日 上午 10:41:16
	 */
	@RequestMapping("/page.action")
	public String page( Model model){
		List<SysDict> hotelLevel = sysDictRepository.findByParent("HotelLevel"); // 读取数据字典星级
		model.addAttribute("levels", hotelLevel);
		return "res/business_hotel_list";
	}


	/**
	 * @Description: 读取商务酒店列表
	 * @param pageForm
	 * @param dto
	 * @return:
	 * @date: 2017年9月7日 上午 10:41:16
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/list.action", method = RequestMethod.POST)
	@ResponseBody
	public Object list(PageForm pageForm, DataHotel dto) {
		Map<String, Object> queyrMap = dataHotelRepository.findByAuto(dto, pageForm);
		Map<String, Object> map = new HashMap<String, Object>();
		List<DataHotel> list = (List<DataHotel>) queyrMap.get("rows");
		List<Map<String, Object>> _list = new ArrayList<>();
		for (DataHotel data : list) {
			Map<String, Object> _map = new HashMap<String, Object>();
			if (data.getType()!=1){
				continue;
			}
			_map.put("id", data.getId());
			_map.put("name", data.getName());
			_map.put("address", data.getAddress());
			_map.put("linkName", data.getLinkName());
			_map.put("lat", data.getLat());
			_map.put("lon", data.getLon());
			String level=null;
			if("HotelLevel5".equals(data.getLevel())){
				level="五星级";
			}else if("HotelLevel4".equals(data.getLevel())){
				level="四星级";
			}else if("HotelLevel3".equals(data.getLevel())){
				level="三星级";
			}else if("HotelLevel2".equals(data.getLevel())){
				level="二星级";
			}else if("HotelLevel1".equals(data.getLevel())){
				level="一星级";
			}else {
				level="未评等级";
			}
			_map.put("level", level);
			_map.put("state", data.getState());
			_list.add(_map);
		}
		map.put("rows", _list);
		map.put("total", queyrMap.get("total"));
		return map;
	}

	/**
	 * @Description:新增/编辑商务酒店
	 * @param id
 	 * @param model
	 * @return:
	 * @date: 2017年9月7日 上午 10:43:50
	 */
	@RequestMapping("/edit.action")
	public String edit(Long id,  Model model) {
		List<SysDict> hotelLevel = sysDictRepository.findByParent("HotelLevel"); // 读取数据字典酒店类型
		DataHotel dto = new DataHotel();
		if (id != null) {
			dto = dataHotelRepository.findOne(id);
		}
		model.addAttribute("dto", dto);
		model.addAttribute("hotelLevel", hotelLevel);
		return "res/business_hotel_input";
	}


	/**
	 * @Description:保存商务酒店信息
	 * @param dto
	 * @return
	 * @date: 2017年9月7日 上午 10:47:36
	 */
	@RequestMapping(value = "/save.action", method = RequestMethod.POST)
	public String save(@ModelAttribute DataHotel dto) {
		if (dto.getState() == null || dto.getState().equals("")) {
			dto.setState(0L);
		}
		if(dto.getId()!=null){
			DataHotel data = dataHotelRepository.getOne(dto.getId());
			BeanUtils.copyProperties(dto, data,new String[] { "id"});
			dataHotelRepository.save(data);

		}else {
			dataHotelRepository.save(dto);
		}
		return "forward:/success.action";
	}

	/**
	 * @Description:删除商务酒店
	 * @param ids
	 * @return
	 * @date: 2017年9月7日 上午 10:50:11
	 */
	@RequestMapping("/remove.action")
	@ResponseBody
	public Object delete(Long[] ids){
		String status = Constant.STATUS_SCUESS;
		String msg;
		try {
			for(Long id:ids){
				dataHotelRepository.delete(id);
			}
			msg = "删除成功！";
		} catch (Exception e) {
			status = Constant.STATUS_ERROR;
			msg = "删除失败！";
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("state", status);
		map.put("msg", msg);
		JSONObject json = JSONObject.fromObject(map);
		return json;
	}


	/**
	 * 行程酒店列表
	 * @param region
	 * @param page
	 * @param rows
	 * @param name
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("/hotel_list.action")
	@ResponseBody
	public Object hotelList(String region, @RequestParam(defaultValue = "1") int page,
							@RequestParam(defaultValue = "10") int rows, String name, HttpServletRequest request,
							HttpServletResponse response) throws Exception {
		DataHotel dto = new DataHotel();
		PageForm form = new PageForm();
		/*if (region != null && !region.equals("")) {
			if (region.endsWith("0000")) {
				region = region.substring(0, 2);
			} else if (region.endsWith("00")) {
				region = region.substring(0, 4);
			}
			dto.setRegion(region);
			;
		}*/

		form.setLimit(rows);
		form.setOffset((page-1)*rows);
		form.setSort("id");
		form.setOrder("ASC");
		dto.setName(name);
		List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
		Map<String, Object> queyMap = dataHotelRepository.findByAuto(dto, form);
		List<DataHotel> list = (List<DataHotel>) queyMap.get("rows");
		for (DataHotel data : list) {
			Map<String, Object> mapScen = new HashMap<String, Object>();
			mapScen.put("id", data.getId());
			mapScen.put("name", data.getName());// 酒店名称
			mapScen.put("picture", data.getImgPath());// 酒店引导图图片
			String levels = null;
			if (data.getLevel() != null) {
				levels = sysDictRepository.findOne(data.getLevel()).getName();
			} else {
				levels = "未评等级";
			}
			mapScen.put("type", levels);// 级别
			mapScen.put("phone", data.getPhone());
			mapList.add(mapScen);
		}
		response.setContentType("charset=UTF-8;text/json");
		response.setCharacterEncoding("UTF-8");
		response.setHeader("Access-Control-Allow-Origin", "*");
		JSONObject obj = new JSONObject();
		obj.put("rows", mapList);
		obj.put("total", queyMap.get("total"));
		response.getWriter().write(obj.toString());
		return null;

	}

	/**
	 * 根据id查询出宾馆酒店--用于行程管理资源查询
	 * @param id       资源id
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@RequestMapping("/findOne.action")
	@ResponseBody
	public Object findOne(Long id, HttpServletResponse response) throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			DataHotel t =dataHotelRepository.findOne(id);
			map.put("id", String.valueOf(t.getId()));
			map.put("name", t.getName()); // 名称
			map.put("phone", t.getPhone() != null ? t.getPhone() : "-"); // 电话
			map.put("type", t.getLevel()!=null?sysDictRepository.findOne(t.getLevel()).getName():"-"); // 类型
			map.put("picture", t.getImgPath()); // 形象标识图片
			/*map.put("adviceDate", "地址:"+(t.getAddress() != null ? t.getAddress() : "-")); // 地址
			map.put("region", t.getRegion() != null ? sysRegionRepository.getOne(t.getRegion()).getShortName() : "-"); // 地区名称*/
		} catch (Exception e) {
			e.printStackTrace();
		}
		response.setContentType("charset=UTF-8;text/json");
		response.setCharacterEncoding("UTF-8");
		net.sf.json.JSONObject obj = new net.sf.json.JSONObject();
		obj.put("data", map);
		response.getWriter().write(obj.toString());
		return null;
	}

}
