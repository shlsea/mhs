package com.skuo.mhs.data.res.service;

import org.springframework.stereotype.Service;

import com.skuo.mhs.data.res.pojo.DataActing;
import com.skuo.mhs.support.CustomRepository;

import java.util.List;

/**
 * @ClassName: DataActingRepository
 * @Description: 演艺场所实现DAO
 * @Company: 四川西谷智慧科技有限公司
 * @author: 钟操
 * @date: 2017年9月7日 下午14:26:52
 * @version: V1.0.0
 */
@Service
public interface DataActingRepository extends CustomRepository<DataActing, Long> {

    /**
     * @Description:查询所有有效演艺场所
     * @return
     * @date 2017年9月7日 下午15:57:16
     *
     */
    List<DataActing> findByState(Long state);
}
