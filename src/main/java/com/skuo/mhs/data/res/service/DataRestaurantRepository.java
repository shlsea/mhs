package com.skuo.mhs.data.res.service;

import org.springframework.stereotype.Service;

import com.skuo.mhs.data.res.pojo.DataRestaurant;
import com.skuo.mhs.support.CustomRepository;

import java.util.List;

/**
 * @ClassName: DataRestaurantRepository
 * @Description: 餐厅service
 * @Company: 四川西谷智慧科技有限公司
 * @author: YanHiroShi
 * @date: 2017/9/7 11:42
 * @version: V1.0.0
 */

@Service
public interface DataRestaurantRepository extends CustomRepository<DataRestaurant, Long> {

    /**
     * @DATE: 2017/9/7-11:43.
     * @Description:查询所有有效餐厅
     */
    List<DataRestaurant> findByState(Long state);
}
