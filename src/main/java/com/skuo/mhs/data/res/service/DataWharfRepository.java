package com.skuo.mhs.data.res.service;

import org.springframework.stereotype.Service;

import com.skuo.mhs.data.res.pojo.DataWharf;
import com.skuo.mhs.support.CustomRepository;

import java.util.List;

/**
 * AUTHAR:yosang.
 * COMPANY:四川西谷智慧科技有限公司.
 * DATAE:2017/9/4-14:29.
 * DESCRIBE:码头service.
 * VERSION:V1.0.0
 */
@Service
public interface DataWharfRepository extends CustomRepository<DataWharf, Long> {

    /**
     * @Description:查询所有有效码头
     * @return
     * @date 2017年9月5日下午18:18:16
     */
    List<DataWharf> findByState(Long state);
}
