package com.skuo.mhs.data.res.service;

import java.util.List;

import com.skuo.mhs.data.res.pojo.DataEmerSite;
import com.skuo.mhs.support.CustomRepository;

/**
 *
 * @ClassName: DataEmerSiteRepository.java
 * @Description: 应急场所实现DAO
 * @Company: 四川西谷智慧科技有限公司
 * @author: 蔡磊
 * @date: 2017年9月4日 下午2:28:04
 * @version: V1.0.0
 */
public interface DataEmerSiteRepository extends CustomRepository<DataEmerSite, Long> {
    /**
     * @Description:查询所有有效应急场所
     * @return
     * @date 2017年9月5日下午18:18:16
     */
    List<DataEmerSite> findByState(Long state);
}
