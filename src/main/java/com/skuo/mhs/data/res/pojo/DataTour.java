package com.skuo.mhs.data.res.pojo;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * @ClassName: DataTour
 * @Description:行程纪录
 * @Company: 四川西谷智慧科技有限公司
 * @author: songhailing
 * @date: 2017年9月19日 下午3:46:09
 * @version: V1.0.0
 */
@Entity
public class DataTour {

	/**
	 * 主键Id
	 */
	@Id
	@GeneratedValue
	private Long id;

	/**
	 * 旅游人数
	 */
	private Long peopleNum;

	/**
	 * 持续天数
	 */
	private Long stayDays;

	/**
	 * 旅游主题
	 */
	private String tripTheme;

	/**
	 * 联系电话
	 */
	private String phone;

	/**
	 * 行程花费
	 */
	private String cost;

	/**
	 * 备注
	 */
	@Type(type = "text")
	private String remark;

	/**
	 * 状态<br>
	 * 0:禁用<br>
	 * 1:启用
	 */
	private Long state;

	/**
	 * 添加时间
	 */
	private Date addDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getPeopleNum() {
		return peopleNum;
	}

	public void setPeopleNum(Long peopleNum) {
		this.peopleNum = peopleNum;
	}

	public Long getStayDays() {
		return stayDays;
	}

	public void setStayDays(Long stayDays) {
		this.stayDays = stayDays;
	}

	public String getTripTheme() {
		return tripTheme;
	}

	public void setTripTheme(String tripTheme) {
		this.tripTheme = tripTheme;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCost() {
		return cost;
	}

	public void setCost(String cost) {
		this.cost = cost;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Long getState() {
		return state;
	}

	public void setState(Long state) {
		this.state = state;
	}

	public Date getAddDate() {
		return addDate;
	}

	public void setAddDate(Date addDate) {
		this.addDate = addDate;
	}

}
