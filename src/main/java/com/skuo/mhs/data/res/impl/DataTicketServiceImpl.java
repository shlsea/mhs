package com.skuo.mhs.data.res.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.skuo.mhs.data.res.pojo.DataTicket;
import com.skuo.mhs.utils.StringUtils;

/**
 * @ClassName: DataTicketServiceImpl 
 * @Description: 票务系统的实现接口
 * @Company: 四川西谷智慧科技有限公司
 * @author: songhailing 
 * @date: 2017年9月16日 下午4:36:00 
 * @version: V1.0.0
 */
@Service
public class DataTicketServiceImpl {

	/**
     * 循环遍历当前栏目及子栏目
     * @param dto
     * @return
     */
    public Map<String,Object> findByParent(DataTicket dto){
    	Map<String,Object> map = new HashMap<String,Object>();
    	  map.put("id", dto.getId());
          map.put("name", StringUtils.transString(dto.getName()));
          map.put("summary", StringUtils.transString(dto.getSummary()));
//          map.put("parentId", dto.getParentId());
          map.put("imgPath", StringUtils.transString(dto.getImgPath()));
//          map.put("content", StringUtils.transString(dto.getContent()));
          map.put("price", StringUtils.transString(dto.getPrice()));
          if(dto.getChildren().size()>0){
              List<Map<String,Object>> list=new ArrayList<Map<String,Object>>();
              for(DataTicket data:dto.getChildren()){
                  Map<String,Object> dataMap=findByParent(data);
                  list.add(dataMap);
              }
              map.put("child", list);
          }
        return map;
    }

}
