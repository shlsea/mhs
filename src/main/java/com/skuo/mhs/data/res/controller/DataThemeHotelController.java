package com.skuo.mhs.data.res.controller;

import net.sf.json.JSONObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.skuo.mhs.data.res.pojo.DataHotel;
import com.skuo.mhs.data.res.service.DataHotelRepository;
import com.skuo.mhs.form.PageForm;
import com.skuo.mhs.utils.Constant;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName: DataThemeHotelController
 * @Description: 主题酒店逻辑控制器
 * @Company: 四川西谷智慧科技有限公司
 * @author: 钟操
 * @date: 2017年9月7日  上午 10:40:26
 * @version: V1.0.0
 */
@Controller
@RequestMapping(value = "/theme_hotel")
public class DataThemeHotelController {

	@Autowired
	private DataHotelRepository dataHotelRepository;
	
	
	
	/**
	 * 跳转主题酒店管理列表页面
	 * @Description:TODO
	 * @return
	 * @date: 2017年9月7日  上午 10:41:16
	 */
	@RequestMapping("/page.action")
	public String page( ){
		return "res/theme_hotel_list";
	}


	/**
	 * @Description: 读取主题酒店列表
	 * @param pageForm
	 * @param dto
	 * @return:
	 * @date: 2017年9月7日  上午 10:41:16
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/list.action", method = RequestMethod.POST)
	@ResponseBody
	public Object list(PageForm pageForm, DataHotel dto) {
		Map<String, Object> queyrMap = dataHotelRepository.findByAuto(dto, pageForm);
		Map<String, Object> map = new HashMap<String, Object>();
		List<DataHotel> list = (List<DataHotel>) queyrMap.get("rows");
		List<Map<String, Object>> _list = new ArrayList<>();
		for (DataHotel data : list) {
			Map<String, Object> _map = new HashMap<String, Object>();
			if (data.getType()!=2){
				continue;
			}
			_map.put("id", data.getId());
			_map.put("name", data.getName());
			_map.put("address", data.getAddress());
			_map.put("linkName", data.getLinkName());
			_map.put("lat", data.getLat());
			_map.put("lon", data.getLon());
			String level=null;
			if("HotelLevel5".equals(data.getLevel())){
				level="五星级";
			}else if("HotelLevel4".equals(data.getLevel())){
				level="四星级";
			}else if("HotelLevel3".equals(data.getLevel())){
				level="三星级";
			}else if("HotelLevel2".equals(data.getLevel())){
				level="二星级";
			}else if("HotelLevel1".equals(data.getLevel())){
				level="一星级";
			}else {
				level="未评等级";
			}
			_map.put("level", level);
			_map.put("state", data.getState());
			_list.add(_map);
		}
		map.put("rows", _list);
		map.put("total", queyrMap.get("total"));
		return map;
	}

	/**
	 * @Description:新增/编辑主题酒店
	 * @param id
 	 * @param model
	 * @return:
	 * @date: 2017年9月7日  上午 10:43:50
	 */
	@RequestMapping("/edit.action")
	public String edit(Long id,  Model model) {
		DataHotel dto = new DataHotel();
		if (id != null) {
			dto = dataHotelRepository.findOne(id);
		}
		model.addAttribute("dto", dto);
		return "res/theme_hotel_input";
	}


	/**
	 * @Description:保存主题酒店信息
	 * @param dto
	 * @return
	 * @date: 2017年9月7日  上午 10:47:36
	 */
	@RequestMapping(value = "/save.action", method = RequestMethod.POST)
	public String save(@ModelAttribute DataHotel dto) {
		if (dto.getState() == null || dto.getState().equals("")) {
			dto.setState(0L);
		}
		if(dto.getId()!=null){
			DataHotel data = dataHotelRepository.getOne(dto.getId());
			BeanUtils.copyProperties(dto, data,new String[] { "id"});
			dataHotelRepository.save(data);

		}else {
			dataHotelRepository.save(dto);
		}
		return "forward:/success.action";
	}

	/**
	 * @Description:删除主题酒店
	 * @param ids
	 * @return
	 * @date: 2017年9月7日  上午 10:50:11
	 */
	@RequestMapping("/remove.action")
	@ResponseBody
	public Object delete(Long[] ids){
		String status = Constant.STATUS_SCUESS;
		String msg;
		try {
			for(Long id:ids){
				dataHotelRepository.delete(id);
			}
			msg = "删除成功！";
		} catch (Exception e) {
			status = Constant.STATUS_ERROR;
			msg = "删除失败！";
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("state", status);
		map.put("msg", msg);
		JSONObject json = JSONObject.fromObject(map);
		return json;
	}


}
