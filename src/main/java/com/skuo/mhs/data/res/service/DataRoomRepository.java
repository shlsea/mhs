package com.skuo.mhs.data.res.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.skuo.mhs.data.res.pojo.DataRoom;
import com.skuo.mhs.support.CustomRepository;

/**
 * @ClassName: DataRoomRepository
 * @Description: 房间实现DAO
 * @Company: 四川西谷智慧科技有限公司
 * @author: 钟操
 * @date: 2017年9月7日 上午09:56:52
 * @version: V1.0.0
 */
@Service
public interface DataRoomRepository extends CustomRepository<DataRoom, Long> {

    /**
     * @Description: 查询所有所属房间
     * @return
     * @date 2017年9月7日 下午15:57:16
     *
     */
    List<DataRoom> findByBelongHotelAndState(Long id,Long state);
}
