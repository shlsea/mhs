package com.skuo.mhs.data.res.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.skuo.mhs.data.res.pojo.DataGuide;
import com.skuo.mhs.support.CustomRepository;

/**
 * @ClassName: DataGuideRepository 
 * @Description: 导游查询接口
 * @Company: 四川西谷智慧科技有限公司
 * @author: songhailing 
 * @date: 2017年9月12日 下午5:05:59 
 * @version: V1.0.0
 */
@Service
public interface DataGuideRepository extends CustomRepository<DataGuide, Long> {

    /**
     * @Description:查询有效的导游信息
     * @return
     * @date 2017年9月5日下午12:18:16
     *
     */
    List<DataGuide> findByState(Long state);

}
