package com.skuo.mhs.data.res.service;

import org.springframework.stereotype.Service;

import com.skuo.mhs.data.res.pojo.DataMonitor;
import com.skuo.mhs.support.CustomRepository;

/**
 * @ClassName: DataMonitorRepository
 * @Description: 视频监控实现DAO
 * @Company: 四川西谷智慧科技有限公司
 * @author: 钟操
 * @date: 2017年9月4日 下午14:31:52
 * @version: V1.0.0
 */
@Service
public interface DataMonitorRepository extends CustomRepository<DataMonitor, Long> {

}
