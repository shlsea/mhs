package com.skuo.mhs.data.res.controller;

import net.sf.json.JSONObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.skuo.mhs.data.res.pojo.DataTour;
import com.skuo.mhs.data.res.service.DataTourRepository;
import com.skuo.mhs.form.PageForm;
import com.skuo.mhs.utils.Constant;

import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName: DataTourController
 * @Description: 行程定制控制器
 * @Company: 四川西谷智慧科技有限公司
 * @author: YanHiroShi
 * @date: 2017/9/7 10:33
 * @version: V1.0.0
 */
@Controller
@RequestMapping(value = "/tour")
public class DataTourController {

    @Autowired
    private DataTourRepository tourRepository;


    /**
     * @DATE: 2017/9/4-17:36.
     * @Description:跳转界面
     */
    @RequestMapping(value = "/page.action")
    public String page(){
        return "res/tour_list";
    }

    /**
     * @DATE: 2017/9/4-17:35.
     * @Description:编辑界面
     */
    @RequestMapping(value = "/edit.action")
    public String edit(Long id,Model model){
        DataTour dto = new DataTour();
        if(id!=null){
            dto = tourRepository.findOne(id);
        }
        model.addAttribute("dto",dto);
        return "res/tour_input";
    }


    /**
     * @DATE: 2017/9/4-17:36.
     * @Description:获取列表数据
     */
    @RequestMapping(value = "/list.action",method = RequestMethod.POST)
    @ResponseBody
    public Object list(PageForm pageForm, DataTour dto){
        Map<String, Object> map = tourRepository.findByAuto(dto, pageForm);
        return map;
    }

    /**
     * @DATE: 2017/9/4-17:26.
     * @Description:删除数据(单条和多条)
     */
    @RequestMapping(value = "/remove.action",method = RequestMethod.POST)
    @ResponseBody
    public Object remove(Long[] ids){
        String status = Constant.STATUS_SCUESS;
        String msg;
        try {
            for (Long id : ids) {
                tourRepository.delete(id);
            }
            msg = "删除成功！";
        } catch (Exception e) {
            status = Constant.STATUS_ERROR;
            msg = "删除失败！";
        }
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("state", status);
        map.put("msg", msg);
        JSONObject json = JSONObject.fromObject(map);
        return json;
    }

    /**
     * @DATE: 2017/9/4-17:28.
     * @Description:保存数据(单个)
     */
    @RequestMapping(value = "/save.action",method = RequestMethod.POST)
    public Object save(@ModelAttribute DataTour dto, Model model){
        model.addAttribute("dto", dto);
        if (dto.getState() == null || dto.getState().equals("")) {
            dto.setState(0L);
        }
        if (dto.getId() != null) {
            DataTour data = tourRepository.findOne(dto.getId());
            BeanUtils.copyProperties(dto, data, new String[] { "id" });
            tourRepository.save(data);
        } else {
            tourRepository.save(dto);
        }
        return "forward:/success.action";
    }

}
