package com.skuo.mhs.data.res.service;

import org.springframework.stereotype.Service;

import com.skuo.mhs.data.res.pojo.DataRescueStation;
import com.skuo.mhs.support.CustomRepository;

import java.util.List;

/**
 * AUTHAR:yosang.
 * COMPANY:四川西谷智慧科技有限公司.
 * DATAE:2017/9/4-14:31.
 * DESCRIBE:救助点service.
 * VERSION:V1.0.0
 */
@Service
public interface DataRescueStationRepository extends CustomRepository<DataRescueStation, Long> {
    /**
     * @Description:查询所有有效救助点
     * @return
     * @date 2017年9月5日下午18:18:16
     */
    List<DataRescueStation> findByState(Long state);
}
