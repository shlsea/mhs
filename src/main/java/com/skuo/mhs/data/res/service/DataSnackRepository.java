package com.skuo.mhs.data.res.service;

import org.springframework.stereotype.Service;

import com.skuo.mhs.data.res.pojo.DataSnack;
import com.skuo.mhs.support.CustomRepository;

import java.util.List;

/**
 * @ClassName: DataSnackRepository
 * @Description: 冷饮小吃店实现DAO
 * @Company: 四川西谷智慧科技有限公司
 * @author: 钟操
 * @date: 2017年9月7日 上午09:56:52
 * @version: V1.0.0
 */
@Service
public interface DataSnackRepository extends CustomRepository<DataSnack, Long> {

    /**
     * @Description:查询所有有效冷饮小吃
     * @return
     * @date 2017年9月7日 下午15:57:16
     *
     */
    List<DataSnack> findByState(Long state);
}
