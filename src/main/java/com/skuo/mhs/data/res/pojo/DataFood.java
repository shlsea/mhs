package com.skuo.mhs.data.res.pojo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * @ClassName: DataFood
 * @Description: 美食信息
 * @Company: 四川西谷智慧科技有限公司
 * @author: songhailing
 * @date: 2017年9月19日 下午8:12:36
 * @version: V1.0.0
 */
@Entity
public class DataFood {

	/**
	 * 主键Id
	 */
	@Id
	@GeneratedValue
	private Long id;

	/**
	 * 美食名称
	 */
	private String name;

	/**
	 * 图片
	 */
	private String imgPath;

	/**
	 * 价格
	 */
	private String cost;

	/**
	 * 美食介绍
	 */
	@Type(type = "text")
	private String content;

	/**
	 * 备注
	 */
	@Type(type = "text")
	private String remark;

	/**
	 * 状态<br>
	 * 0:禁用<br>
	 * 1:启用
	 */
	private Long state;

	/**
	 * 所属餐厅
	 */
	private Long belongRestaurant;
	

	/**
	 * 简介
	 */
	private String summary;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImgPath() {
		return imgPath;
	}

	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}

	public String getCost() {
		return cost;
	}

	public void setCost(String cost) {
		this.cost = cost;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Long getState() {
		return state;
	}

	public void setState(Long state) {
		this.state = state;
	}

	public Long getBelongRestaurant() {
		return belongRestaurant;
	}

	public void setBelongRestaurant(Long belongRestaurant) {
		this.belongRestaurant = belongRestaurant;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}


}
