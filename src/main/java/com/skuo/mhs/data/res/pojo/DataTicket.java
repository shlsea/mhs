package com.skuo.mhs.data.res.pojo;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;

import org.hibernate.annotations.Type;

/**
 * @ClassName: DataTicket
 * @Description: 门票主信息
 * @Company: 四川西谷智慧科技有限公司
 * @author: songhailing
 * @date: 2017年9月19日 下午5:09:34
 * @version: V1.0.0
 */
@Entity
public class DataTicket {

	/**
	 * 主键Id
	 */
	@Id
	@GeneratedValue
	private Long id;

	/**
	 * 门票名称
	 */
	private String name;

	/**
	 * 图片
	 */
	private String imgPath;

	/**
	 * 地址
	 */
	private String address;

	/**
	 * 门票价格
	 */
	private String price;

	/**
	 * 内容
	 */
	@Type(type = "text")
	private String content;

	/**
	 * 备注
	 */
	@Type(type = "text")
	private String remark;

	/**
	 * 状态<br>
	 * 0:禁用<br>
	 * 1:启用
	 */
	private Long state;

	/**
	 * 子集合
	 */
	@Transient
	private List<DataTicket> children = new ArrayList<DataTicket>(0);
	
	/**
	 * 简介
	 */
	private String summary;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImgPath() {
		return imgPath;
	}

	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Long getState() {
		return state;
	}

	public void setState(Long state) {
		this.state = state;
	}

	public List<DataTicket> getChildren() {
		return children;
	}

	public void setChildren(List<DataTicket> children) {
		this.children = children;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

}
