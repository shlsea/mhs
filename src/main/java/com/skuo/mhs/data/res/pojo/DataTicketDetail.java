package com.skuo.mhs.data.res.pojo;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;

import org.hibernate.annotations.Type;
/**
 * @ClassName: DataTicketDetail 
 * @Description: 门票详细信息
 * @Company: 四川西谷智慧科技有限公司
 * @author: songhailing 
 * @date: 2017年9月19日 下午5:09:05 
 * @version: V1.0.0
 */
@Entity
public class DataTicketDetail {

	/**
	 * 主键Id
	 */
	@Id
	@GeneratedValue
	private Long id;

	/**
	 * 门票套餐名称
	 */
	private String name;

	/**
	 * 图片
	 */
	private String imgPath;


	/**
	 * 门票价格
	 */
	private String price;

	/**
	 * 简介
	 */
	@Type(type = "text")
	private String content;

	/**
	 * 备注
	 */
	@Type(type = "text")
	private String remark;

	/**
	 * 状态<br>
	 * 0:禁用<br>
	 * 1:启用
	 */
	private Long state;

	/**
	 * 父id
	 */
	private Long parentId;

	/**
	 * 子集合
	 */
	@Transient
	private List<DataTicketDetail> children = new ArrayList<DataTicketDetail>(0);

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImgPath() {
		return imgPath;
	}

	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}


	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Long getState() {
		return state;
	}

	public void setState(Long state) {
		this.state = state;
	}



	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public List<DataTicketDetail> getChildren() {
		return children;
	}

	public void setChildren(List<DataTicketDetail> children) {
		this.children = children;
	}

}
