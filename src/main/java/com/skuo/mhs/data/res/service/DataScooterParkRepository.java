package com.skuo.mhs.data.res.service;

import org.springframework.stereotype.Service;

import com.skuo.mhs.data.res.pojo.DataScooterPark;
import com.skuo.mhs.support.CustomRepository;

import java.util.List;

/**
 * @ClassName: DataScooterParkRepository
 * @Description: 电瓶车站实现DAO
 * @Company: 四川西谷智慧科技有限公司
 * @author: 钟操
 * @date: 2017年9月4日 下午16:11:52
 * @version: V1.0.0
 */
@Service
public interface DataScooterParkRepository extends CustomRepository<DataScooterPark, Long> {

    /**
     * @Description:查询所有有效电瓶车站
     * @return
     * @date 2017年9月5日下午12:18:16
     *
     */
    List<DataScooterPark> findByState(Long state);
}
