package com.skuo.mhs.data.res.controller;

import net.sf.json.JSONObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.skuo.mhs.data.res.pojo.DataShoppingStreet;
import com.skuo.mhs.data.res.service.DataShoppingStreetRepository;
import com.skuo.mhs.form.PageForm;
import com.skuo.mhs.utils.Constant;

import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName: DataShoppingStreetController
 * @Description: 购物场所控制器
 * @Company: 四川西谷智慧科技有限公司
 * @author: YanHiroShi
 * @date: 2017/9/4 16:17
 * @version: V1.0.0
 */
@Controller
@RequestMapping(value = "/shopping_street")
public class DataShoppingStreetController {

	@Autowired
	private DataShoppingStreetRepository shoppingStreetRepository;

	/**
	 * @DATE: 2017/9/4-17:15.
	 * @Description:跳转界面
	 */
	@RequestMapping(value = "/page.action")
	public String page() {
		return "res/shopingstreet_list";
	}

	/**
	 * @DATE: 2017/9/4-17:15.
	 * @Description:跳转编辑界面
	 */
	@RequestMapping(value = "/edit.action")
	public String edit(Long id, Model model) {
		DataShoppingStreet shoppingStreet = new DataShoppingStreet();
		if (id != null) {
			shoppingStreet = shoppingStreetRepository.findOne(id);
		}
		model.addAttribute("dto", shoppingStreet);
		return "res/shoppingstreet_input";
	}

	/**
	 * @DATE: 2017/9/4-17:15.
	 * @Description:获取数据
	 */
	@RequestMapping(value = "/list.action", method = RequestMethod.POST)
	@ResponseBody
	public Object list(PageForm pageForm, DataShoppingStreet dto) {
		Map<String, Object> map = shoppingStreetRepository.findByAuto(dto, pageForm);
		return map;
	}


	/**
	 * @DATE: 2017/9/4-17:15.
	 * @Description:删除数据(单条和多条)
	 */
	@RequestMapping(value = "/remove.action", method = RequestMethod.POST)
	@ResponseBody
	public Object remove(Long[] ids) {
		String status = Constant.STATUS_SCUESS;
		String msg;
		try {
			for (Long id : ids) {
				shoppingStreetRepository.delete(id);
			}
			msg = "删除成功！";
		} catch (Exception e) {
			status = Constant.STATUS_ERROR;
			msg = "删除失败！";
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("state", status);
		map.put("msg", msg);
		JSONObject json = JSONObject.fromObject(map);
		return json;
	}


    /**
     * @DATE: 2017/9/4-17:15.
     * @Description:保存数据(单个)
     */
    @RequestMapping(value = "/save.action",method = RequestMethod.POST)
    public Object save(@ModelAttribute DataShoppingStreet dto,Model model){
        model.addAttribute("dto", dto);
		if (dto.getState() == null || dto.getState().equals("")) {
			dto.setState(0L);
		}
        if (dto.getId() != null) {
            DataShoppingStreet data = shoppingStreetRepository.findOne(dto.getId());
            BeanUtils.copyProperties(dto, data, new String[] { "id" });
            shoppingStreetRepository.save(data);
        } else {
            shoppingStreetRepository.save(dto);
        }
        return "forward:/success.action";
    }

}