package com.skuo.mhs.data.res.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.skuo.mhs.data.res.pojo.DataHotel;
import com.skuo.mhs.support.CustomRepository;

/**
 * @ClassName: DataHotelRepository
 * @Description: 酒店实现DAO
 * @Company: 四川西谷智慧科技有限公司
 * @author: 钟操
 * @date: 2017年9月7日 上午09:56:52
 * @version: V1.0.0
 */
@Service
public interface DataHotelRepository extends CustomRepository<DataHotel, Long> {

    /**
     * @Description: 查询所有有效酒店
     * @return
     * @date 2017年9月7日 下午15:57:16
     *
     */
    List<DataHotel> findByStateAndType(Long state,Long type);

	List<DataHotel> findByState(Long l);
}
