package com.skuo.mhs.data.res.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.skuo.mhs.data.res.pojo.DataGuide;
import com.skuo.mhs.data.res.service.DataGuideRepository;
import com.skuo.mhs.form.PageForm;
import com.skuo.mhs.system.pojo.SysDict;
import com.skuo.mhs.system.service.SysDictRepository;
import com.skuo.mhs.utils.Constant;

import net.sf.json.JSONObject;

/**
 * @ClassName: DataGuideController 
 * @Description: 导游控制器
 * @Company: 四川西谷智慧科技有限公司
 * @author: songhailing 
 * @date: 2017年9月12日 下午5:10:03 
 * @version: V1.0.0
 */
@Controller
@RequestMapping(value = "/guide")
public class DataGuideController {

	@Autowired
	private DataGuideRepository dataGuideRepository;
	@Autowired
	private SysDictRepository sysDictRepository;

	/**
	 * @Description:跳转导游管理列表页面
	 * @return
	 * @date: 2017年9月12日 下午 14:10:26
	 */
	@RequestMapping("/page.action")
	public String page() {
		return "res/guide_list";
	}

	/**
	 * @Description: 读取导游列表
	 * @param pageForm
	 * @param dto
	 * @return:
	 * @date: 2017年9月12日 下午14:18:26
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/list.action", method = RequestMethod.POST)
	@ResponseBody
	public Object list(PageForm pageForm, DataGuide dto) {
		Map<String, Object> queryMap = dataGuideRepository.findByAuto(dto, pageForm);
		Map<String, Object> map = new HashMap<String, Object>();
		List<DataGuide> list = (List<DataGuide>) queryMap.get("rows");
		List<Map<String, Object>> _list = new ArrayList<>();
		for (DataGuide data : list) {
			Map<String, Object> _map = new HashMap<String, Object>();
			/*if (data.getType()!=1){
				continue;
			}*/
			_map.put("id", data.getId());
			_map.put("name", data.getName());
			_map.put("sex", data.getSex()==1?"男":"女");
			String guideLevel="";
			if("GuideLevel3".equals(data.getGuideLevel())){
				guideLevel="特级";
			}else if("GuideLevel2".equals(data.getGuideLevel())){
				guideLevel="高级";
			}else if("GuideLevel1".equals(data.getGuideLevel())){
				guideLevel="中级";
			}else {
				guideLevel="初级";
			}
			_map.put("guideLevel", guideLevel);
			_map.put("guideCode", data.getGuideCode());
			
			String languageType="";
			if("Language5".equals(data.getLanguageType())){
				languageType="德语";
			}else if("Language4".equals(data.getLanguageType())){
				languageType="法语";
			}else if("Language3".equals(data.getLanguageType())){
				languageType="韩语";
			}else if("Language2".equals(data.getLanguageType())){
				languageType="日语";
			}else if("Language1".equals(data.getLanguageType())){
				languageType="普通话";
			}else {
				languageType="英语";
			}
			_map.put("languageType", languageType);
			_map.put("type", data.getType()==1?"教练":"导游");
			_map.put("state", data.getState());
			_list.add(_map);
		}
		map.put("rows", _list);
		map.put("total", queryMap.get("total"));
		return map;
		
	}

	/**
	 * @Description:新增/编辑导游
	 * @param id
	 * @param model
	 * @return:
	 * @date: 2017年9月12日 下午14:20:26
	 */
	@RequestMapping("/edit.action")
	public String edit(Long id, Model model) {
		DataGuide dto = new DataGuide();
		if (id != null) {
			dto = dataGuideRepository.findOne(id);
		}
		List<SysDict> guideLevels = sysDictRepository.findByParent("GuideLevel");
		List<SysDict> languages = sysDictRepository.findByParent("Language");
		model.addAttribute("dto", dto);
		model.addAttribute("guideLevels", guideLevels);
		model.addAttribute("languages", languages);
		return "res/guide_input";
	}

	/**
	 * @Description:保存导游信息
	 * @param dto
	 * @return
	 * @date 2017年9月12日下午14:25:04
	 */
	@RequestMapping(value = "/save.action", method = RequestMethod.POST)
	public String save(@ModelAttribute DataGuide dto) {
		if (dto.getSex() == null ) {
			dto.setSex(0);
		}
		if (dto.getState() == null || dto.getState().equals("")) {
			dto.setState(0L);
		}
		if (dto.getType() == null || dto.getType().equals("")) {
			dto.setType(0);
		}
		if (dto.getId() != null) { 
			DataGuide data = dataGuideRepository.getOne(dto.getId());
			BeanUtils.copyProperties(dto, data, new String[] { "id" });
			dataGuideRepository.save(data);

		} else {
			dataGuideRepository.save(dto);
		}
  		return "forward:/success.action";
	}

	/**
	 * @Description:删除导游
	 * @param ids
	 * @return
	 * @date 2017年9月12日下午14:26:11
	 */
	@RequestMapping("/remove.action")
	@ResponseBody
	public Object delete(Long[] ids) {
		String status = Constant.STATUS_SCUESS;
		String msg;
		try {
			for (Long id : ids) {
				dataGuideRepository.delete(id);
			}
			msg = "删除成功！";
		} catch (Exception e) {
			status = Constant.STATUS_ERROR;
			msg = "删除失败！";
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("state", status);
		map.put("msg", msg);
		JSONObject json = JSONObject.fromObject(map);
		return json;
	}

}
