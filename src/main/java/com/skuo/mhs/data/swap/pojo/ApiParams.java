package com.skuo.mhs.data.swap.pojo;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * 
 * @ClassName: ApiParams
 * @Description: 接口说明参数
 * @Company: 四川西谷智慧科技有限公司
 * @author: 余德山
 * @date: 2017年9月6日 下午3:45:17
 * @version: V1.0.0
 */
@Entity
public class ApiParams {

	/**
	 * 主键
	 */
	@Id
	@GeneratedValue
	private Long id;

	/**
	 * 参数名称
	 */
	private String title;

	/**
	 * 参数类型<br>
	 * 1:请求参数<br>
	 * 2:响应参数
	 */
	private Long type;

	/**
	 * 是否必填<br>
	 * 0:否<br>
	 * 1:是
	 */
	private Long required;

	/**
	 * 备注
	 */
	private String remark;

	/**
	 * 参数类型
	 */
	private String paramType;

	/**
	 * 所属接口
	 */
	@ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinColumn(name = "_explain", nullable = true, insertable = true, updatable = true)
	private ApiExplain explain;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Long getType() {
		return type;
	}

	public void setType(Long type) {
		this.type = type;
	}

	public Long getRequired() {
		return required;
	}

	public void setRequired(Long required) {
		this.required = required;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getParamType() {
		return paramType;
	}

	public void setParamType(String paramType) {
		this.paramType = paramType;
	}

	public ApiExplain getExplain() {
		return explain;
	}

	public void setExplain(ApiExplain explain) {
		this.explain = explain;
	}

}
