package com.skuo.mhs.data.swap.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.skuo.mhs.data.swap.pojo.ApiChannel;
import com.skuo.mhs.data.swap.service.ApiChannelRepository;
import com.skuo.mhs.utils.Constant;

/**
 *
 * @ClassName: ApiChannelServiceImpl.java
 * @Description: 文章栏目具体实现类
 * @Company: 四川西谷智慧科技有限公司
 * @author: 蔡磊
 * @date: 2017年9月7日 上午10:36:07
 * @version: V1.0.0
 */
@Service
public class ApiChannelServiceImpl {

	@Autowired
	private ApiChannelRepository apiChannelRepository;

	/**
	 *
	 * @Description: 保存新闻栏目
	 * @param data
	 * @return
	 * @date: 2017年9月7日 上午10:38:21
	 */
	public ApiChannel save(ApiChannel data) {

		Long level = 0L;
		String channel = null;
		if (data.getChannelId() == null || data.getChannelId().equals("")) {
			ApiChannel parent = apiChannelRepository.findOne("0000000000000000000");
			if (data.getParent() == null || data.getParent().getChannelId().equals("")) {
			} else {
				parent = apiChannelRepository.findOne(data.getParent().getChannelId());
				level = parent.getLevel() + 1;
				channel = parent.getChannelId();
			}
			String channelId = getChannel(channel, level);
			data.setChannelId(channelId);
			data.setParent(parent);
			data.setLevel(level);
		} else {
			ApiChannel cha = apiChannelRepository.findOne(data.getChannelId());
			data.setLevel(cha.getLevel());
			data.setParent(cha.getParent());
		}
		Long i = data.getOrderList() > 0 ? data.getOrderList() : 1;
		// 强制转换字符排序
		char c1 = (char) (i + 96);
		// 拼接字符串排序
		String ordno = (data.getParent() != null ? data.getParent().getOrdno() : "") + c1 + ",";
		data.setOrdno(ordno);

		data = apiChannelRepository.save(data);
		// 获取当前新闻栏目子栏目
		List<ApiChannel> channels = apiChannelRepository.findChannelsByParentOrderByOrderListAsc(data);
		if (!channels.isEmpty()) {
			for (ApiChannel dto : channels) {
				// 递归保存子栏目
				save(dto);
			}

		}
		return data;

	}

	/**
	 *
	 * @Description: 新增时获取新闻栏目ID
	 * @param channelId
	 * @param level
	 * @return
	 * @date: 2017年9月7日 上午10:38:40
	 */
	private String getChannel(String channelId, Long level) {
		String str = "";
		if (channelId == null) {
			str = "0001000000000000";
		} else {
			ApiChannel channel = apiChannelRepository.findOne(channelId);
			if (!channel.getChildren().isEmpty()) {
				for (ApiChannel dto : channel.getChildren()) {
					channelId = dto.getChannelId();
				}
			}
			int length = 4 + Integer.parseInt(String.valueOf(level - 1)) * 3;
			String ch = channelId.substring(0, length);
			str = String.valueOf(Long.parseLong(ch) + 1) + Constant.CHANNEL.substring(length);
			str = Constant.CHANNEL.substring(0, Constant.CHANNEL.length() - str.length()) + str;
		}
		return str;
	}

	 /**
     * 循环遍历当前栏目及子栏目
     * @param dto
     * @return
     */
    public Map<String,Object> findByParent(ApiChannel dto){
        Map<String,Object> map=new HashMap<String,Object>();
        map.put("id", dto.getChannelId());
        map.put("name", dto.getChannelName());
        map.put("imgPath", dto.getImgPath());
        if(dto.getChildren().size()>0){
            List<Map<String,Object>> list=new ArrayList<Map<String,Object>>();
            for(ApiChannel data:dto.getChildren()){
                Map<String,Object> dataMap=findByParent(data);
                list.add(dataMap);
            }
            map.put("child", list);
        }
        return map;
    }
}
