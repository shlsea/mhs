package com.skuo.mhs.data.swap.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.skuo.mhs.data.swap.pojo.ApiApplication;
import com.skuo.mhs.data.swap.service.ApiApplicationRepository;
import com.skuo.mhs.form.PageForm;
import com.skuo.mhs.system.pojo.SysDict;
import com.skuo.mhs.system.service.SysDictRepository;
import com.skuo.mhs.utils.Constant;
import com.skuo.mhs.utils.DateUtils;

import net.sf.json.JSONObject;

/**
 * 
 * @ClassName: ApiApplicationController
 * @Description: API应用管理逻辑控制器
 * @Company: 四川西谷智慧科技有限公司
 * @author: 余德山
 * @date: 2017年9月22日 下午3:30:52
 * @version: V1.0.0
 */
@Controller
@RequestMapping(value = "/application")
public class ApiApplicationController {

	@Autowired
	private ApiApplicationRepository apiApplicationRepository;

	@Autowired
	private SysDictRepository sysDictRepository;

	@RequestMapping(value = "/page.action")
	public String page() {
		return "api/application_list";
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/list.action", method = RequestMethod.POST)
	@ResponseBody
	public Object list(PageForm pageForm, ApiApplication dto) {
		Map<String, Object> dataMap = apiApplicationRepository.findByAuto(dto, pageForm);
		Map<String, Object> map = new HashMap<String, Object>();
		List<ApiApplication> list = (List<ApiApplication>) dataMap.get("rows");
		List<Map<String, Object>> _list = new ArrayList<Map<String, Object>>();
		for (ApiApplication data : list) {
			Map<String, Object> _map = new HashMap<String, Object>();
			_map.put("id", data.getId());
			_map.put("name", data.getName());
			_map.put("state", data.getState());
			_map.put("member.name", data.getMember() != null ? data.getMember().getName() : "");
			_map.put("type", data.getType());
			_map.put("ip", data.getIp());
			_map.put("skey", data.getSkey());
			_map.put("addDate", DateUtils.format(data.getAddDate(), "yyyy-MM-dd HH:mm:ss"));
			_list.add(_map);
		}
		map.put("rows", _list);
		map.put("total", dataMap.get("total"));
		return map;

	}

	/**
	 * @Description:新增/编辑
	 * @param id
	 *             * @param model
	 * @return:
	 * @date: 2017年9月13日 下午15:20:26
	 */
	@RequestMapping(value = "/edit.action")
	public String edit(Long id, Model model) {
		ApiApplication application = new ApiApplication();
		List<SysDict> appsorts = sysDictRepository.findByParent("Appsorts");// 应用分类
		List<SysDict> applied = sysDictRepository.findByParent("Applied");// 应用平台
		if (id != null) {
			application = apiApplicationRepository.findOne(id);
		}
		model.addAttribute("dto", application);
		model.addAttribute("appsorts", appsorts);
		model.addAttribute("applied", applied);
		return "api/application_input";
	}

	/**
	 * @Description:保存信息
	 * @param dto
	 * @return
	 * @date 2017年9月13日下午15:25:04
	 */
	@RequestMapping(value = "/save.action", method = RequestMethod.POST)
	public Object save(@ModelAttribute ApiApplication dto, Model model) {
		model.addAttribute("dto", dto);
		if (dto.getState() == null || dto.getState().equals("")) {
			dto.setState(0L);
		}
		dto.setAddDate(new Date());
		if (dto.getId() != null) {
			ApiApplication data = apiApplicationRepository.findOne(dto.getId());
			BeanUtils.copyProperties(dto, data, new String[] { "id" });
			apiApplicationRepository.save(data);
		} else {
			apiApplicationRepository.save(dto);
		}
		return "forward:/success.action";
	}

	/**
	 * @Description:删除
	 * @param ids
	 * @return
	 * @date 2017年9月13日下午15:26:11
	 */
	@RequestMapping(value = "/remove.action", method = RequestMethod.POST)
	@ResponseBody
	public Object remove(Long[] ids) {
		String status = Constant.STATUS_SCUESS;
		String msg;
		try {
			for (Long id : ids) {
				apiApplicationRepository.delete(id);
			}
			msg = "删除成功！";
		} catch (Exception e) {
			status = Constant.STATUS_ERROR;
			msg = "删除失败！";
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("state", status);
		map.put("msg", msg);
		JSONObject json = JSONObject.fromObject(map);
		return json;
	}
}
