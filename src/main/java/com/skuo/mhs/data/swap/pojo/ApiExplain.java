package com.skuo.mhs.data.swap.pojo;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

/**
 * 
 * @ClassName: ApiExplain
 * @Description: 接口说明
 * @Company: 四川西谷智慧科技有限公司
 * @author: 余德山
 * @date: 2017年9月6日 下午3:33:48
 * @version: V1.0.0
 */
@Entity
public class ApiExplain {

	/**
	 * 主键
	 */
	@Id
	@GeneratedValue
	private Long id;

	/**
	 * 接口名称
	 */
	private String title;

	/**
	 * 接口访问地址
	 */
	private String url;

	/**
	 * 接口说明
	 */
	private String remark;

	/**
	 * 方法
	 */
	public String mothed;

	/**
	 * 示例
	 */
	private String sample;

	/**
	 * 接口状态 0:禁用<br>
	 * 1:启用
	 */
	private Long state;

	/**
	 * 接口参数
	 */
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST, mappedBy = "explain")
	@OrderBy("id")
	private Set<ApiParams> chlidren = new HashSet<ApiParams>(0);

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getMothed() {
		return mothed;
	}

	public void setMothed(String mothed) {
		this.mothed = mothed;
	}

	public String getSample() {
		return sample;
	}

	public void setSample(String sample) {
		this.sample = sample;
	}

	public Long getState() {
		return state;
	}

	public void setState(Long state) {
		this.state = state;
	}

	public Set<ApiParams> getChlidren() {
		return chlidren;
	}

	public void setChlidren(Set<ApiParams> chlidren) {
		this.chlidren = chlidren;
	}


}
