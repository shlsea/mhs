package com.skuo.mhs.data.swap.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.skuo.mhs.data.swap.pojo.ApiChannel;
import com.skuo.mhs.data.swap.pojo.ApiNews;
import com.skuo.mhs.data.swap.service.ApiChannelRepository;
import com.skuo.mhs.data.swap.service.ApiNewsRepository;
import com.skuo.mhs.form.PageForm;
import com.skuo.mhs.utils.Constant;

import net.sf.json.JSONObject;

/**
 *
 * @ClassName: ApiNewsController.java
 * @Description: 文章内容逻辑控制器
 * @Company: 四川西谷智慧科技有限公司
 * @author: 蔡磊
 * @date: 2017年9月7日 上午11:27:48
 * @version: V1.0.0
 */
@Controller
@RequestMapping(value = "/api_news")
@SuppressWarnings("unchecked")
public class ApiNewsController {

	@Autowired
	private ApiNewsRepository apiNewsRepository;
	@Autowired
	private ApiChannelRepository apiChannelRepository;

	/**
	 * 
	 * @Description:跳转文章内容管理页面
	 * @return
	 * @date 2017年9月6日上午9:22:00
	 */
	@RequestMapping(value = "/page.action")
	public String page() {
		return "api/news_list";
	}

	/**
	 * 
	 * @Description:读取文章内容列表
	 * @param pageForm
	 *            分页表单
	 * @param dto
	 *            查询实体
	 * @return
	 * @date 2017年9月6日上午9:22:05
	 */
	@RequestMapping(value = "/list.action", method = RequestMethod.POST)
	@ResponseBody
	public Object list(PageForm pageForm, ApiNews dto) {
		if (dto.getChannelId() != null) {
			ApiChannel channel = apiChannelRepository.findOne(dto.getChannelId());
			if (channel != null && channel.getLevel() > 0) {
				Integer level = Integer.parseInt(String.valueOf(channel.getLevel()));
				String channelId = channel.getChannelId().substring(0, 4 + (level - 1) * 3);
				dto.setChannelId(channelId);
			} else {
				dto.setChannelId(null);
			}
		}
		Map<String, Object> dataMap = apiNewsRepository.findByAuto(dto, pageForm);
		Map<String, Object> map = new HashMap<String, Object>();
		List<ApiNews> list = (List<ApiNews>) dataMap.get("rows");
		List<Map<String, Object>> _list = new ArrayList<Map<String, Object>>();
		SimpleDateFormat yf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		for (ApiNews data : list) {
			Map<String, Object> _map = new HashMap<String, Object>();
			_map.put("id", data.getId());
			_map.put("channelId", data.getChannelId() != null
					? apiChannelRepository.findOne(data.getChannelId()).getChannelName() : "");
			_map.put("title", data.getTitle());
			_map.put("state", data.getState());
			_map.put("isTop", data.getIsTop());
			_map.put("author", data.getAuthor());
			_map.put("orderList", data.getOrderList());
			_map.put("source", data.getSource());
			_map.put("type", data.getType());
			_map.put("addDate", data.getAddDate() != null ? yf.format(data.getAddDate()) : "");
			_list.add(_map);
		}
		map.put("rows", _list);
		map.put("total", dataMap.get("total"));
		return map;
	}

	/**
	 * 
	 * @Description:编辑文章内容
	 * @param id
	 *            文章内容Id
	 * @param request
	 * @param model
	 * @return
	 * @date 2017年9月6日上午9:24:05
	 */
	@RequestMapping("/edit.action")
	public String edit(Long id, HttpServletRequest request, Model model) {
		ApiNews dto = new ApiNews();
		if (id != null) {
			dto = apiNewsRepository.findOne(id);
		}
		model.addAttribute("dto", dto);
		return "api/news_input";
	}

	/**
	 * 
	 * @Description:保存文章内容
	 * @param dto
	 *            文章内容实体
	 * @param model
	 * @param request
	 * @return
	 * @date 2017年9月6日上午11:02:35
	 */
	@RequestMapping(value = "/save.action", method = RequestMethod.POST)
	public String save(@ModelAttribute ApiNews dto, Model model, HttpServletRequest request) {
		dto.setState(0L);
		if (dto.getId() == null) {
			dto.setAddDate(new Date());
			apiNewsRepository.save(dto);
		} else {
			ApiNews data = apiNewsRepository.findOne(dto.getId());
			dto.setUpdateDate(new Date());
			BeanUtils.copyProperties(dto, data,
					new String[] { "id", "addDate", "pubDate", "orderList", "checkNum", "followNum", "shareNum" });
			apiNewsRepository.save(data);
		}
		return "forward:/success.action";
	}

	/**
	 * 
	 * @Description:删除文章内容
	 * @param ids
	 *            文章内容Id数组
	 * @return
	 * @date 2017年9月6日上午11:23:52
	 */
	@RequestMapping("/remove.action")
	@ResponseBody
	public Object delete(Long[] ids) {
		String status = Constant.STATUS_SCUESS;
		String msg;
		try {
			for (Long id : ids) {
				apiNewsRepository.delete(id);
			}
			msg = "删除成功！";
		} catch (Exception e) {
			status = Constant.STATUS_ERROR;
			msg = "删除失败！";
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("state", status);
		map.put("msg", msg);
		JSONObject json = JSONObject.fromObject(map);
		return json;
	}

	/**
	 * 
	 * @Description:审核文章内容
	 * @param ids
	 *            文章内容id数组
	 * @return
	 * @date 2017年9月6日下午2:29:01
	 */
	@RequestMapping("/check.action")
	@ResponseBody
	public Object check(Long[] ids) {
		String status = Constant.STATUS_SCUESS;
		String msg;
		try {
			for (Long id : ids) {
				ApiNews data = apiNewsRepository.findOne(id);
				data.setState(1L);
				apiNewsRepository.save(data);
			}
			msg = "审核成功！";
		} catch (Exception e) {
			status = Constant.STATUS_ERROR;
			msg = "审核失败！";
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("state", status);
		map.put("msg", msg);
		JSONObject json = JSONObject.fromObject(map);
		return json;
	}

	/**
	 * 
	 * @Description:发布文章内容
	 * @param ids
	 *            文章内容id数组
	 * @return
	 * @date 2017年9月6日下午2:29:01
	 */
	@RequestMapping("/repeat.action")
	@ResponseBody
	public Object repeat(Long[] ids) {
		String status = Constant.STATUS_SCUESS;
		String msg;
		try {
			for (Long id : ids) {
				ApiNews data = apiNewsRepository.findOne(id);
				data.setState(2L);
				data.setPubDate(new Date());
				apiNewsRepository.save(data);
			}
			msg = "发布成功！";
		} catch (Exception e) {
			status = Constant.STATUS_ERROR;
			msg = "发布失败！";
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("state", status);
		map.put("msg", msg);
		JSONObject json = JSONObject.fromObject(map);
		return json;
	}

	/**
	 * 
	 * @Description:撤销文章内容
	 * @param ids
	 *            文章内容id数组
	 * @return
	 * @date 2017年9月6日下午2:29:01
	 */
	@RequestMapping("/trash.action")
	@ResponseBody
	public Object trash(Long[] ids) {
		String status = Constant.STATUS_SCUESS;
		String msg;
		try {
			for (Long id : ids) {
				ApiNews data = apiNewsRepository.findOne(id);
				data.setState(3L);
				apiNewsRepository.save(data);
			}
			msg = "撤销成功！";
		} catch (Exception e) {
			status = Constant.STATUS_ERROR;
			msg = "撤销失败！";
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("state", status);
		map.put("msg", msg);
		JSONObject json = JSONObject.fromObject(map);
		return json;
	}
}
