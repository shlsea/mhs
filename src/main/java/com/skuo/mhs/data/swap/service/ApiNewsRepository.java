package com.skuo.mhs.data.swap.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.skuo.mhs.data.swap.pojo.ApiNews;
import com.skuo.mhs.support.CustomRepository;

/**
 *
 * @ClassName: ApiNewsRepository.java
 * @Description: 文章实现DAO
 * @Company: 四川西谷智慧科技有限公司
 * @author: 蔡磊
 * @date: 2017年9月7日 上午11:20:42
 * @version: V1.0.0
 */
@Service
public interface ApiNewsRepository extends CustomRepository<ApiNews, Long> {

	/**
	 *
	 * @Description: 查询当前栏目下的文章
	 * @param id
	 * @return
	 * @date: 2017年9月12日 下午3:06:42
	 */
	List<ApiNews> findByChannelId(String id);
}
