package com.skuo.mhs.data.swap.pojo;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

/**
 * 
 * @ClassName: CmsNews
 * @Description: 新闻内容实体
 * @Company: 四川西谷智慧科技有限公司
 * @author: 余德山
 * @date: 2017年9月5日 下午3:27:12
 * @version: V1.0.0
 */
@Entity
public class ApiNews {

	/**
	 * 主键Id
	 */
	@Id
	@GeneratedValue
	private Long id;

	/**
	 * 新闻标题
	 */
	private String title;

	/**
	 * 副标题
	 */
	private String subTitle;

	/**
	 * 作者
	 */
	private String author;

	/**
	 * 是否图片新闻
	 */
	private Long isImg;

	/**
	 * 图片路径
	 */
	private String imgPath;

	/**
	 * 所属新闻栏目
	 */
	private String channelId;

	/**
	 * 新闻来源
	 */
	private String source;

	/**
	 * 新闻类型
	 */
	private Long type;

	/**
	 * 新闻内容
	 */
	@Type(type = "text")
	private String content;

	/**
	 * 摘要
	 */
	@Type(type = "text")
	private String remark;

	/**
	 * 点击数
	 */
	private Long checkNum;

	/**
	 * 关注数
	 */
	private Long followNum;

	/**
	 * 分享数
	 */
	private Long shareNum;

	/**
	 * 排序
	 */
	private Long orderList;

	/**
	 * 是否置顶
	 */
	private Long isTop;

	/**
	 * 新增时间
	 */
	private Date addDate;

	/**
	 * 更新时间
	 */
	private Date updateDate;

	/**
	 * 发布时间
	 */
	private Date pubDate;

	/**
	 * 新闻状态
	 */
	private Long state;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getSubTitle() {
		return subTitle;
	}

	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}

	public Long getIsImg() {
		return isImg;
	}

	public void setIsImg(Long isImg) {
		this.isImg = isImg;
	}

	public String getImgPath() {
		return imgPath;
	}

	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}

	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Long getCheckNum() {
		return checkNum;
	}

	public void setCheckNum(Long checkNum) {
		this.checkNum = checkNum;
	}

	public Long getFollowNum() {
		return followNum;
	}

	public void setFollowNum(Long followNum) {
		this.followNum = followNum;
	}

	public Long getShareNum() {
		return shareNum;
	}

	public void setShareNum(Long shareNum) {
		this.shareNum = shareNum;
	}

	public Long getOrderList() {
		return orderList;
	}

	public void setOrderList(Long orderList) {
		this.orderList = orderList;
	}

	public Long getIsTop() {
		return isTop;
	}

	public void setIsTop(Long isTop) {
		this.isTop = isTop;
	}

	public Date getAddDate() {
		return addDate;
	}

	public void setAddDate(Date addDate) {
		this.addDate = addDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Date getPubDate() {
		return pubDate;
	}

	public void setPubDate(Date pubDate) {
		this.pubDate = pubDate;
	}

	public Long getState() {
		return state;
	}

	public void setState(Long state) {
		this.state = state;
	}

	public Long getType() {
		return type;
	}

	public void setType(Long type) {
		this.type = type;
	}

}
