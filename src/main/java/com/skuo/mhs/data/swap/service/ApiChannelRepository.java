package com.skuo.mhs.data.swap.service;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import com.skuo.mhs.data.swap.pojo.ApiChannel;
import com.skuo.mhs.support.CustomRepository;

/**
 *
 * @ClassName: ApiChannelRepository.java
 * @Description: 文章栏目实现DAO
 * @Company: 四川西谷智慧科技有限公司
 * @author: 蔡磊
 * @date: 2017年9月7日 上午10:25:00
 * @version: V1.0.0
 */
@Service
public interface ApiChannelRepository extends CustomRepository<ApiChannel, String> {

	/**
	 *
	 * @Description: 查询所有未删除栏目并按照字符排序
	 * @return
	 * @date: 2017年9月7日 上午10:32:48
	 */
	@Query("select u from ApiChannel u where u.state !=-1 order by u.ordno ")
	List<ApiChannel> findChannels();

	/**
	 *
	 * @Description: 根据父类栏目查询子栏目
	 * @param data
	 * @return
	 * @date: 2017年9月7日 上午10:32:34
	 */
	List<ApiChannel> findChannelsByParentOrderByOrderListAsc(ApiChannel data);
}
