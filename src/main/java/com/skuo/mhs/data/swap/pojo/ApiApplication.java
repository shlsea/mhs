package com.skuo.mhs.data.swap.pojo;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

/**
 * 
* @ClassName: ApiApplication 
* @Description: 
* @Company: 四川西谷智慧科技有限公司
* @author: 余德山 
* @date: 2017年9月6日 下午3:04:36 
* @version: V1.0.0
 */
@Entity
public class ApiApplication {

	/**
	 * 主键
	 */
	@Id
	@GeneratedValue
	private Long id;
	
	/**
	 * 应用名称
	 */
	private String name;
	
	/**
	 * key值
	 */
	private String skey;
	
	/**
	 * 应用类型
	 */
	private String type;
	
	/**
	 * 应用logo
	 */
	private String logo;
	
	/**
	 * 添加时间
	 */
	private Date addDate;
	
	/**
	 * 调用地址
	 */
	private String ip;
	
	/**
	 * 备注
	 */
	private String remark;
	
	/**
	 * 所属用户
	 */
	@ManyToOne(fetch = FetchType.LAZY, cascade ={CascadeType.PERSIST,CascadeType.MERGE})
	@JoinColumn(name = "member", nullable = true, insertable = true, updatable = true)
	private ApiMember member;
	
	/**
	 * 应用接口权限
	 */
	@ManyToMany(cascade={CascadeType.REFRESH},fetch=FetchType.EAGER)
	private Set<ApiExplain> explains;
	
	/**
	 * 状态
	 * -1:删除<br>
	 * 0:待审核<br>
	 * 1:审核通过<br>
	 * 2:冻结
	 * 
	 */
	private Long state;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSkey() {
		return skey;
	}

	public void setSkey(String skey) {
		this.skey = skey;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public Date getAddDate() {
		return addDate;
	}

	public void setAddDate(Date addDate) {
		this.addDate = addDate;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public ApiMember getMember() {
		return member;
	}

	public void setMember(ApiMember member) {
		this.member = member;
	}

	public Set<ApiExplain> getExplains() {
		return explains;
	}

	public void setExplains(Set<ApiExplain> explains) {
		this.explains = explains;
	}

	public Long getState() {
		return state;
	}

	public void setState(Long state) {
		this.state = state;
	}
	
	
}
