package com.skuo.mhs.data.swap.pojo;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * 
* @ClassName: ApiMember 
* @Description: API接口用户管理
* @Company: 四川西谷智慧科技有限公司
* @author: 余德山 
* @date: 2017年9月6日 下午2:51:53 
* @version: V1.0.0
 */
@Entity
public class ApiMember {

	/**
	 * 主键
	 */
	@Id
	@GeneratedValue
	private Long id;
	
	/**
	 * 用户姓名
	 */
	private String name;
	
	/**
	 * QQ
	 */
	private String qq;
	
	/**
	 * 所属地区
	 */
	private String region;
	
	/**
	 * 账号
	 */
	private String account;
	
	/**
	 * 密码
	 */
	private String password;
	
	/**
	 * 电话
	 */
	private String phone;
	
	/**
	 * 地址
	 */
	private String address;
	
	/**
	 * 电子邮箱
	 */
	private String email;
	
	/**
	 * 类型
	 * 1:个人<br>
	 * 2:企业<br>
	 * 3:政府机构
	 */
	private Long type;
	
	/**
	 * 备注
	 */
	private String remark;
	
	/**
	 * 签名
	 */
	private String sign;
	
	/**
	 * 状态
	 * 0:待审核<br>
	 * 1:审核通过<br>
	 * -1:冻结<br>
	 */
	private Long state;
	
	/**
	 * 添加时间
	 */
	private Date addDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getType() {
		return type;
	}

	public void setType(Long type) {
		this.type = type;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Long getState() {
		return state;
	}

	public void setState(Long state) {
		this.state = state;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public Date getAddDate() {
		return addDate;
	}

	public void setAddDate(Date addDate) {
		this.addDate = addDate;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}
	
	
	
}
