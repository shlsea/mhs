package com.skuo.mhs.data.swap.service;

import com.skuo.mhs.data.swap.pojo.ApiApplication;
import com.skuo.mhs.support.CustomRepository;

public interface ApiApplicationRepository extends CustomRepository< ApiApplication, Long>{

}
