package com.skuo.mhs.data.swap.controller;



import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.skuo.mhs.data.swap.pojo.ApiMember;
import com.skuo.mhs.data.swap.service.ApiMemberRepository;
import com.skuo.mhs.form.PageForm;
import com.skuo.mhs.system.pojo.SysRegion;
import com.skuo.mhs.system.service.SysRegionRepository;
import com.skuo.mhs.utils.Constant;
import com.skuo.mhs.utils.Md5Tool;
import com.skuo.mhs.utils.UUIDUtils;

import net.sf.json.JSONObject;

/**
 * 
 * @ClassName: ApiMemberController
 * @Description: 接口用户管理逻辑控制器
 * @Company: 四川西谷智慧科技有限公司
 * @author: 余德山
 * @date: 2017年9月6日 下午3:55:10
 * @version: V1.0.0
 */
@Controller
@RequestMapping(value = "/member")
@SuppressWarnings("unchecked")
public class ApiMemberController {

	@Autowired
	private ApiMemberRepository apiMemberRepository;
	@Autowired
	private SysRegionRepository sysRegionRepository;

	/**
	 * 
	 * @Description:跳转接口用户管理页面
	 * @return
	 * @date 2017年9月7日下午2:06:29
	 */
	@RequestMapping(value = "/page.action")
	public String page() {
		return "api/member_list";
	}

	/**
	 * 
	 * @Description:读取接口用户列表
	 * @param pageForm
	 *            分页表单
	 * @param dto
	 *            查询条件
	 * @return
	 * @date 2017年9月6日下午3:58:05
	 */
	@RequestMapping(value = "/list.action", method = RequestMethod.POST)
	@ResponseBody
	public Object list(PageForm pageForm, ApiMember dto) {
		if (dto.getRegion() != null && !dto.getRegion().equals("")) {
			SysRegion region = sysRegionRepository.findOne(dto.getRegion());
			if (region != null) {
				dto.setRegion(region.getSubRegion(region));
			}
		}
		Map<String, Object> dataMap = apiMemberRepository.findByAuto(dto, pageForm);
		Map<String, Object> map = new HashMap<String, Object>();
		List<ApiMember> list = (List<ApiMember>) dataMap.get("rows");
		List<Map<String, Object>> _list = new ArrayList<Map<String, Object>>();
		for (ApiMember data : list) {
			Map<String, Object> _map = new HashMap<String, Object>();
			_map.put("id", data.getId());
			_map.put("name", data.getName());
			_map.put("state", data.getState());
			_map.put("account", data.getAccount());
			_map.put("phone", data.getPhone());
			_map.put("address", data.getAddress());
			_map.put("sign", data.getSign());
			_map.put("region",
					data.getRegion() != null ? sysRegionRepository.findOne(data.getRegion()).getFullName() : null);
			_list.add(_map);
		}
		map.put("rows", _list);
		map.put("total", dataMap.get("total"));
		return map;
	}

	/**
	 * 
	 * @Description:编辑接口用户
	 * @param id
	 *            接口用户Id
	 * @param request
	 * @param model
	 * @return
	 * @date 2017年9月7日下午2:27:59
	 */
	@RequestMapping("/edit.action")
	public String edit(Long id, HttpServletRequest request, Model model) {
		ApiMember dto = new ApiMember();
		String areacode = "";
		if (id != null) {
			dto = apiMemberRepository.findOne(id);
			if (dto.getRegion() != null && !dto.getRegion().equals("")) {
				SysRegion region = sysRegionRepository.findOne(dto.getRegion());
				areacode = region != null ? region.getOrderNo() : "";
			}
		}
		model.addAttribute("dto", dto);
		model.addAttribute("area", areacode);
		return "api/member_input";
	}

	/**
	 * 
	 * @Description:保存接口用户
	 * @param dto
	 *            接口用户实体
	 * @param model
	 * @param request
	 * @return
	 * @date 2017年9月7日下午2:28:03
	 */
	@RequestMapping(value = "/save.action", method = RequestMethod.POST)
	public String save(@ModelAttribute ApiMember dto, Model model, HttpServletRequest request) {
		dto.setState(0L);
		String pwd = Md5Tool.getMd5(dto.getPassword());
		if (dto.getId() != null) {
			ApiMember data = apiMemberRepository.findOne(dto.getId());
			if (!dto.getPassword().equals(data.getPassword())) {
				dto.setPassword(pwd);
			}
			BeanUtils.copyProperties(dto, data, new String[] { "id", "account","sign","addDate" });
			apiMemberRepository.save(data);

		} else {
			dto.setAddDate(new Date());
			dto.setPassword(pwd);
			String sign=UUIDUtils.random();
			dto.setSign(sign);
			apiMemberRepository.save(dto);
		}
		return "forward:/success.action";
	}

	/**
	 * 
	 * @Description:启用接口用户账号
	 * @param ids
	 *            接口用户账号Id数组
	 * @return
	 * @date 2017年9月7日下午2:28:06
	 */
	@RequestMapping("/check.action")
	@ResponseBody
	public Object check(Long[] ids) {
		String status = Constant.STATUS_SCUESS;
		String msg;
		try {
			for (Long id : ids) {
				ApiMember user = apiMemberRepository.findOne(id);
				user.setState(1L);
				apiMemberRepository.save(user);
			}
			msg = "启用成功！";
		} catch (Exception e) {
			status = Constant.STATUS_ERROR;
			msg = "启用失败！";
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("state", status);
		map.put("msg", msg);
		JSONObject json = JSONObject.fromObject(map);
		return json;
	}

	/**
	 * 
	 * @Description:冻结用户账号
	 * @param ids
	 *            接口用户账号id数组
	 * @return
	 * @date 2017年9月7日下午2:28:14
	 */
	@RequestMapping("/remove.action")
	@ResponseBody
	public Object delete(Long[] ids) {
		String status = Constant.STATUS_SCUESS;
		String msg;
		try {
			for (Long id : ids) {
				ApiMember user = apiMemberRepository.findOne(id);
				user.setState(-1L);
				apiMemberRepository.save(user);
			}
			msg = "冻结成功！";
		} catch (Exception e) {
			status = Constant.STATUS_ERROR;
			msg = "冻结失败！";
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("state", status);
		map.put("msg", msg);
		JSONObject json = JSONObject.fromObject(map);
		return json;
	}

	/**
	 * 
	 * @Description:根据账号判断用户是否存在
	 * @param account
	 *            用户账号
	 * @return
	 * @date 2017年9月7日下午2:28:18
	 */
	@RequestMapping(value = "/findOne.action", method = RequestMethod.POST)
	@ResponseBody
	public Object findOne(String account) {
		boolean is = true;
		ApiMember data = apiMemberRepository.findByAccount(account);
		if (data != null) {
			is = false;
		}
		return is;
	}

}
