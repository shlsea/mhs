package com.skuo.mhs.data.swap.service;

import com.skuo.mhs.data.swap.pojo.ApiMember;
import com.skuo.mhs.support.CustomRepository;

/**
 * 
 * @ClassName: ApiMemberRepository
 * @Description: 接口用户实现DAO
 * @Company: 四川西谷智慧科技有限公司
 * @author: 余德山
 * @date: 2017年9月6日 下午3:53:38
 * @version: V1.0.0
 */
public interface ApiMemberRepository extends CustomRepository<ApiMember, Long> {

	/**
	 * 
	 * @Description:根据账号查询用户
	 * @param account
	 *            账号
	 * @return
	 * @date 2017年9月7日下午2:27:08
	 */
	ApiMember findByAccount(String account);

}
