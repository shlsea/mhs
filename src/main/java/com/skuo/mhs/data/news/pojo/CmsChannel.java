package com.skuo.mhs.data.news.pojo;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

/**
 * 
 * @ClassName: CmsChannel
 * @Description: 新闻栏目
 * @Company: 四川西谷智慧科技有限公司
 * @author: 余德山
 * @date: 2017年9月4日 下午5:18:28
 * @version: V1.0.0
 */
@Entity
public class CmsChannel {

	/**
	 * 栏目编号，主键
	 */
	@Id
	@Column(name = "channel_id", nullable = false, length = 16)
	private String channelId;

	/**
	 * 栏目名称
	 */
	private String channelName;

	/**
	 * 栏目代码
	 */
	private String tag;

	/**
	 * 栏目引导图片
	 */
	private String imgPath;

	/**
	 * 栏目类型<br>
	 * 1:普通栏目<br>
	 * 2:附件栏目<br>
	 * 3:连接栏目
	 */
	private Long type;

	/**
	 * 栏目等级
	 */
	@Column(name = "channel_level")
	private Long level;

	/**
	 * 数字排序
	 */
	private Long orderList;

	/**
	 * 字符串排序
	 */
	private String ordno;

	/**
	 * 栏目状态
	 */
	private Long state;

	/**
	 * 父类栏目
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "parent", nullable = true, insertable = true, updatable = true)
	private CmsChannel parent;

	/**
	 * 子栏目集合
	 */
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST, mappedBy = "parent")
	@OrderBy("orderList")
	private Set<CmsChannel> children = new HashSet<CmsChannel>(0);

	/**
	 * 子标题
	 */
	private String subTitle;

	/**
	 * 发布时间
	 */
	private Date publishTime;

	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	public String getChannelName() {
		return channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getImgPath() {
		return imgPath;
	}

	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}

	public Long getType() {
		return type;
	}

	public void setType(Long type) {
		this.type = type;
	}

	public Long getLevel() {
		return level;
	}

	public void setLevel(Long level) {
		this.level = level;
	}

	public Long getOrderList() {
		return orderList;
	}

	public void setOrderList(Long orderList) {
		this.orderList = orderList;
	}

	public String getOrdno() {
		return ordno;
	}

	public void setOrdno(String ordno) {
		this.ordno = ordno;
	}

	public Long getState() {
		return state;
	}

	public void setState(Long state) {
		this.state = state;
	}

	public CmsChannel getParent() {
		return parent;
	}

	public void setParent(CmsChannel parent) {
		this.parent = parent;
	}

	public Set<CmsChannel> getChildren() {
		return children;
	}

	public void setChildren(Set<CmsChannel> children) {
		this.children = children;
	}

	public String getSubTitle() {
		return subTitle;
	}

	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}

	public Date getPublishTime() {
		return publishTime;
	}

	public void setPublishTime(Date publishTime) {
		this.publishTime = publishTime;
	}

}
