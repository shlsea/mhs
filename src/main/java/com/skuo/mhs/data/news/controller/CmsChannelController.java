package com.skuo.mhs.data.news.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.skuo.mhs.data.news.impl.CmsChannelServiceImpl;
import com.skuo.mhs.data.news.pojo.CmsChannel;
import com.skuo.mhs.data.news.service.CmsChannelRepository;
import com.skuo.mhs.form.PageForm;
import com.skuo.mhs.utils.Constant;

import net.sf.json.JSONObject;

@Controller
@RequestMapping(value = "/channel")
@SuppressWarnings("unchecked")
public class CmsChannelController {

	@Autowired
	private CmsChannelRepository cmsChannelRepository;

	@Autowired
	private CmsChannelServiceImpl cmsChannelServiceImpl;

	/**
	 * 
	 * @Description:跳转新闻栏目管理列表页面
	 * @return
	 * @date 2017年9月4日下午5:42:16
	 */
	@RequestMapping(value = "/page.action")
	public String page() {
		return "cms/channel_list";
	}

	/**
	 * 
	 * @Description:读取新闻栏目列表
	 * @param pageForm
	 * @param dto
	 * @return
	 * @date 2017年9月4日下午5:42:39
	 */
	@RequestMapping(value = "/list.action", method = RequestMethod.POST)
	@ResponseBody
	public Object list(PageForm pageForm, CmsChannel dto) {
		String _id = dto.getParent().getChannelId();
		if (_id != null && !_id.equals("")) {
			CmsChannel parent = cmsChannelRepository.findOne(_id);
			dto.setParent(parent);
		} else {
			dto.setParent(null);
		}
		Map<String, Object> dataMap = cmsChannelRepository.findByAuto(dto, pageForm);
		Map<String, Object> map = new HashMap<String, Object>();
		List<CmsChannel> list = (List<CmsChannel>) dataMap.get("rows");
		List<Map<String, Object>> _list = new ArrayList<Map<String, Object>>();
		for (CmsChannel data : list) {
			Map<String, Object> _map = new HashMap<String, Object>();
			_map.put("id", data.getChannelId());
			_map.put("channelId", data.getChannelId());
			_map.put("channelName", data.getChannelName());
			_map.put("state", data.getState());
			_map.put("level", data.getLevel());
			_map.put("orderList", data.getOrderList());
			_map.put("type", data.getType());
			_map.put("tag", data.getTag());
			_map.put("parent.channelId", data.getParent() != null ? data.getParent().getChannelName() : null);
			_list.add(_map);
		}
		map.put("rows", _list);
		map.put("total", dataMap.get("total"));
		return map;
	}

	/**
	 * 
	 * @Description:编辑新闻栏目
	 * @param id
	 *            栏目id
	 * @param request
	 * @param model
	 * @return
	 * @date 2017年9月4日下午5:49:38
	 */
	@RequestMapping("/edit.action")
	public String edit(String id, HttpServletRequest request, Model model) {
		CmsChannel dto = new CmsChannel();
		if (id != null) {
			dto = cmsChannelRepository.findOne(id);
		}
		model.addAttribute("dto", dto);
		return "cms/channel_input";
	}

	/**
	 *  
	 * 
	 * @Description:保存新闻栏目
	 * @param dto
	 *            新闻栏目实体
	 * @param model
	 * @param request
	 * @return
	 * @date 2017年9月4日下午5:49:56
	 */
	@RequestMapping(value = "/save.action", method = RequestMethod.POST)
	public String save(@ModelAttribute CmsChannel dto, Model model, HttpServletRequest request) {
		cmsChannelServiceImpl.save(dto);
		return "forward:/success.action";
	}

	/**
	 * 
	 * @Description:删除新闻栏目
	 * @param ids
	 *            栏目id数组
	 * @return
	 * @date 2017年9月4日下午5:51:08
	 */
	@RequestMapping("/remove.action")
	@ResponseBody
	public Object delete(String[] ids) {
		String status = Constant.STATUS_SCUESS;
		String msg;
		try {
			for (String id : ids) {
				cmsChannelRepository.delete(id);
			}
			msg = "删除成功！";
		} catch (Exception e) {
			status = Constant.STATUS_ERROR;
			msg = "删除失败！";
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("state", status);
		map.put("msg", msg);
		JSONObject json = JSONObject.fromObject(map);
		return json;
	}

	/**
	 * 
	 * @Description:查询所有未删除栏目
	 * @return
	 * @date 2017年9月5日上午9:32:32
	 */
	@RequestMapping(value = "/findAll.action", method = RequestMethod.POST)
	@ResponseBody
	public Object findAll() {
		List<CmsChannel> list = new ArrayList<CmsChannel>();
		List<CmsChannel> channels = cmsChannelRepository.findChannels();
		for (CmsChannel channel : channels) {
			channel.setChildren(null);
			;
			channel.setParent(null);
			list.add(channel);
		}
		return list;
	}

	/**
	 * 
	 * @Description:获取栏目树形结构
	 * @return
	 * @date 2017年9月5日下午4:59:03
	 */
	@RequestMapping(value = "/getTree.action", method = RequestMethod.POST)
	@ResponseBody
	public Object getTree() {
		List<Map<String, Object>> treeList = new ArrayList<Map<String, Object>>();
		List<CmsChannel> list = cmsChannelRepository.findChannels();
		for (CmsChannel data : list) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("id", data.getChannelId());
			map.put("pId", data.getParent() != null ? data.getParent().getChannelId() : null);
			map.put("name", data.getChannelName());
			map.put("open", data.getLevel() < 2 ? true : false);
			treeList.add(map);
		}

		JSONObject obj = new JSONObject();
		obj.put("treeNodes", treeList);
		return obj.toString();
	}
}
