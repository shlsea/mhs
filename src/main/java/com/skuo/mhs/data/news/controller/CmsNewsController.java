package com.skuo.mhs.data.news.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.skuo.mhs.data.news.pojo.CmsChannel;
import com.skuo.mhs.data.news.pojo.CmsNews;
import com.skuo.mhs.data.news.service.CmsChannelRepository;
import com.skuo.mhs.data.news.service.CmsNewsRepository;
import com.skuo.mhs.form.PageForm;
import com.skuo.mhs.system.pojo.SysDict;
import com.skuo.mhs.system.service.SysDictRepository;
import com.skuo.mhs.utils.Constant;

import net.sf.json.JSONObject;

/**
 * 
 * @ClassName: CmsNewsController
 * @Description: 资讯新闻逻辑控制器
 * @Company: 四川西谷智慧科技有限公司
 * @author: 余德山
 * @date: 2017年9月6日 上午9:21:38
 * @version: V1.0.0
 */
@Controller
@RequestMapping(value = "/news")
@SuppressWarnings("unchecked")
public class CmsNewsController {

	@Autowired
	private CmsNewsRepository cmsNewsRepository;
	@Autowired
	private CmsChannelRepository cmsChannelRepository;

	@Autowired
	private SysDictRepository sysDictRepository;

	/**
	 * 
	 * @Description:跳转新闻管理页面
	 * @return
	 * @date 2017年9月6日上午9:22:00
	 */
	@RequestMapping(value = "/page.action")
	public String page() {
		return "cms/news_list";
	}

	/**
	 * 
	 * @Description:读取新闻列表
	 * @param pageForm
	 *            分页表单
	 * @param dto
	 *            查询实体
	 * @return
	 * @date 2017年9月6日上午9:22:05
	 */
	@RequestMapping(value = "/list.action", method = RequestMethod.POST)
	@ResponseBody
	public Object list(PageForm pageForm, CmsNews dto) {
		if (dto.getChannelId() != null) {
			CmsChannel channel = cmsChannelRepository.findOne(dto.getChannelId());
			if (channel != null && channel.getLevel() > 0) {
				Integer level = Integer.parseInt(String.valueOf(channel.getLevel()));
				String channelId = channel.getChannelId().substring(0, 4 + (level - 1) * 3);
				dto.setChannelId(channelId);
			} else {
				dto.setChannelId(null);
			}
		}
		Map<String, Object> dataMap = cmsNewsRepository.findByAuto(dto, pageForm);
		Map<String, Object> map = new HashMap<String, Object>();
		List<CmsNews> list = (List<CmsNews>) dataMap.get("rows");
		List<Map<String, Object>> _list = new ArrayList<Map<String, Object>>();
		SimpleDateFormat yf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		for (CmsNews data : list) {
			Map<String, Object> _map = new HashMap<String, Object>();
			_map.put("id", data.getId());
			_map.put("channelId", data.getChannelId() != null
					? cmsChannelRepository.findOne(data.getChannelId()).getChannelName() : "");
			_map.put("title", data.getTitle());
			_map.put("state", data.getState());
			_map.put("isTop", data.getIsTop());
			_map.put("author", data.getAuthor());
			_map.put("orderList", data.getOrderList());
			String source = "";
			if ("NewsSource3".equals(data.getSource())) {
				source = "腾讯新闻";
			} else if ("NewsSource2".equals(data.getSource())) {
				source = "新浪微博";
			} else if ("NewsSource1".equals(data.getSource())) {
				source = "凤凰网";
			} else {
				source = "新华社";
			}
			_map.put("source", source);
			String newsType = "";
			if ("newsType6".equals(data.getNewsType())) {
				newsType = "初识栏目";
			} else if ("newsType5".equals(data.getNewsType())) {
				newsType = "App底部广告";
			} else if ("newsType4".equals(data.getNewsType())) {
				newsType = "App中部广告";
			} else if ("newsType3".equals(data.getNewsType())) {
				newsType = "App顶部广告";
			} else if ("newsType2".equals(data.getNewsType())) {
				newsType = "企业文化";
			} else if ("newsType1".equals(data.getNewsType())) {
				newsType = "园区公告";
			} else {
				newsType = "景区动态";
			}
			_map.put("newsType", newsType);
			_map.put("addDate", data.getAddDate() != null ? yf.format(data.getAddDate()) : yf.format(new Date()));
			_list.add(_map);
		}
		map.put("rows", _list);
		map.put("total", dataMap.get("total"));
		return map;
	}

	/**
	 * 
	 * @Description:编辑新闻
	 * @param id
	 *            新闻Id
	 * @param request
	 * @param model
	 * @return
	 * @date 2017年9月6日上午9:24:05
	 */
	@RequestMapping("/edit.action")
	public String edit(Long id, HttpServletRequest request, Model model) {
		CmsNews dto = new CmsNews();
		if (id != null) {
			dto = cmsNewsRepository.findOne(id);
		}
		List<SysDict> newsSources = sysDictRepository.findByParent("NewsSource");
		List<SysDict> newsTypes = sysDictRepository.findByParent("NewsType");
		model.addAttribute("dto", dto);
		model.addAttribute("newsTypes", newsTypes);
		model.addAttribute("newsSources", newsSources);
		return "cms/news_input";
	}

	/**
	 * 
	 * @Description:保存新闻
	 * @param dto
	 *            新闻实体
	 * @param model
	 * @param request
	 * @return
	 * @date 2017年9月6日上午11:02:35
	 */
	@RequestMapping(value = "/save.action", method = RequestMethod.POST)
	public String save(@ModelAttribute CmsNews dto, Model model, HttpServletRequest request) {
		dto.setState(0L);
		if (dto.getId() == null) {
			dto.setAddDate(new Date());
			cmsNewsRepository.save(dto);
		} else {
			CmsNews data = cmsNewsRepository.findOne(dto.getId());
			dto.setUpdateDate(new Date());
			BeanUtils.copyProperties(dto, data,
					new String[] { "id", "addDate", "pubDate", "orderList", "checkNum", "followNum", "shareNum" });
			cmsNewsRepository.save(data);
		}
		return "forward:/success.action";
	}

	/**
	 * 
	 * @Description:删除新闻
	 * @param ids
	 *            新闻Id数组
	 * @return
	 * @date 2017年9月6日上午11:23:52
	 */
	@RequestMapping("/remove.action")
	@ResponseBody
	public Object delete(Long[] ids) {
		String status = Constant.STATUS_SCUESS;
		String msg;
		try {
			for (Long id : ids) {
				cmsNewsRepository.delete(id);
			}
			msg = "删除成功！";
		} catch (Exception e) {
			status = Constant.STATUS_ERROR;
			msg = "删除失败！";
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("state", status);
		map.put("msg", msg);
		JSONObject json = JSONObject.fromObject(map);
		return json;
	}

	/**
	 * 
	 * @Description:审核新闻
	 * @param ids
	 *            新闻id数组
	 * @return
	 * @date 2017年9月6日下午2:29:01
	 */
	@RequestMapping("/check.action")
	@ResponseBody
	public Object check(Long[] ids) {
		String status = Constant.STATUS_SCUESS;
		String msg;
		try {
			for (Long id : ids) {
				CmsNews data = cmsNewsRepository.findOne(id);
				data.setState(1L);
				cmsNewsRepository.save(data);
			}
			msg = "审核成功！";
		} catch (Exception e) {
			status = Constant.STATUS_ERROR;
			msg = "审核失败！";
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("state", status);
		map.put("msg", msg);
		JSONObject json = JSONObject.fromObject(map);
		return json;
	}

	/**
	 * 
	 * @Description:发布新闻
	 * @param ids
	 *            新闻id数组
	 * @return
	 * @date 2017年9月6日下午2:29:01
	 */
	@RequestMapping("/repeat.action")
	@ResponseBody
	public Object repeat(Long[] ids) {
		String status = Constant.STATUS_SCUESS;
		String msg;
		try {
			for (Long id : ids) {
				CmsNews data = cmsNewsRepository.findOne(id);
				data.setState(2L);
				data.setPubDate(new Date());
				cmsNewsRepository.save(data);
			}
			msg = "发布成功！";
		} catch (Exception e) {
			status = Constant.STATUS_ERROR;
			msg = "发布失败！";
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("state", status);
		map.put("msg", msg);
		JSONObject json = JSONObject.fromObject(map);
		return json;
	}

	/**
	 * 
	 * @Description:撤销新闻
	 * @param ids
	 *            新闻id数组
	 * @return
	 * @date 2017年9月6日下午2:29:01
	 */
	@RequestMapping("/trash.action")
	@ResponseBody
	public Object trash(Long[] ids) {
		String status = Constant.STATUS_SCUESS;
		String msg;
		try {
			for (Long id : ids) {
				CmsNews data = cmsNewsRepository.findOne(id);
				data.setState(3L);
				cmsNewsRepository.save(data);
			}
			msg = "撤销成功！";
		} catch (Exception e) {
			status = Constant.STATUS_ERROR;
			msg = "撤销失败！";
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("state", status);
		map.put("msg", msg);
		JSONObject json = JSONObject.fromObject(map);
		return json;
	}
}
