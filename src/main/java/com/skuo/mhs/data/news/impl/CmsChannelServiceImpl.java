package com.skuo.mhs.data.news.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.skuo.mhs.data.news.pojo.CmsChannel;
import com.skuo.mhs.data.news.service.CmsChannelRepository;
import com.skuo.mhs.utils.Constant;
import com.skuo.mhs.utils.StringUtils;

/**
 * 
 * @ClassName: CmsChannelServiceImpl
 * @Description: 新闻栏目具体实现类
 * @Company: 四川西谷智慧科技有限公司
 * @author: 余德山
 * @date: 2017年9月5日 下午3:10:47
 * @version: V1.0.0
 */
@Service
public class CmsChannelServiceImpl {

	@Autowired
	private CmsChannelRepository cmsChannelRepository;

	/**
	 * 
	 * @Description:保存新闻栏目
	 * @param data
	 *            新闻栏目实体
	 * @return
	 * @date 2017年9月5日下午3:11:09
	 */
	public CmsChannel save(CmsChannel data) {

		Long level = 0L;
		String channel = null;
		if (data.getChannelId() == null || data.getChannelId().equals("")) {
			CmsChannel parent = cmsChannelRepository.findOne("0000000000000000000");
			if (data.getParent() == null || data.getParent().getChannelId().equals("")) {
			} else {
				parent = cmsChannelRepository.findOne(data.getParent().getChannelId());
				level = parent.getLevel() + 1;
				channel = parent.getChannelId();
			}
			String channelId = getChannel(channel, level);
			data.setChannelId(channelId);
			data.setParent(parent);
			data.setLevel(level);
		} else {
			CmsChannel cha = cmsChannelRepository.findOne(data.getChannelId());
			data.setLevel(cha.getLevel());
			data.setParent(cha.getParent());
		}
		Long i = data.getOrderList() > 0 ? data.getOrderList() : 1;
		// 强制转换字符排序
		char c1 = (char) (i + 96);
		// 拼接字符串排序
		String ordno = (data.getParent() != null ? data.getParent().getOrdno() : "") + c1 + ",";
		data.setOrdno(ordno);

		data = cmsChannelRepository.save(data);
		// 获取当前新闻栏目子栏目
		List<CmsChannel> channels = cmsChannelRepository.findChannelsByParentOrderByOrderListAsc(data);
		if (!channels.isEmpty()) {
			for (CmsChannel dto : channels) {
				// 递归保存子栏目
				save(dto);
			}

		}
		return data;

	}

	/**
	 * 
	 * @Description:新增时获取新闻栏目ID
	 * @param channelId
	 *            父类栏目ID
	 * @param level
	 *            栏目等级
	 * @return
	 * @date 2017年9月5日下午3:11:48
	 */
	private String getChannel(String channelId, Long level) {
		String str = "";
		if (channelId == null) {
			str = "0001000000000000";
		} else {
			CmsChannel channel = cmsChannelRepository.findOne(channelId);
			if (!channel.getChildren().isEmpty()) {
				for (CmsChannel dto : channel.getChildren()) {
					channelId = dto.getChannelId();
				}
			}
			int length = 4 + Integer.parseInt(String.valueOf(level - 1)) * 3;
			String ch = channelId.substring(0, length);
			str = String.valueOf(Long.parseLong(ch) + 1) + Constant.CHANNEL.substring(length);
			str = Constant.CHANNEL.substring(0, Constant.CHANNEL.length() - str.length()) + str;
		}
		return str;
	}

	 /**
     * 循环遍历当前栏目及子栏目
     * @param dto
     * @return
     */
    public Map<String,Object> findByParent(CmsChannel dto){
        Map<String,Object> map=new HashMap<String,Object>();
        map.put("id", StringUtils.transString(dto.getChannelId()));
        map.put("name", StringUtils.transString(dto.getChannelName()));
        map.put("imgPath", StringUtils.transString(dto.getImgPath()));
        map.put("subTitle", StringUtils.transString(dto.getSubTitle()));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        map.put("publishTime", dto.getPublishTime()==null?sdf.format(new Date()):sdf.format(dto.getPublishTime()));
        if(dto.getChildren().size()>0){
            List<Map<String,Object>> list=new ArrayList<Map<String,Object>>();
            for(CmsChannel data:dto.getChildren()){
                Map<String,Object> dataMap=findByParent(data);
                list.add(dataMap);
            }
            map.put("child", list);
        }
        return map;
    }
    /**
     * 循环遍历当前栏目及子栏目(初识梅花山专用)
     * @param dto
     * @return
     */
    public Map<String,Object> findByParentForTitle(CmsChannel dto){
    	Map<String,Object> map=new HashMap<String,Object>();
    	map.put("id", StringUtils.transString(dto.getChannelId()));
    	map.put("name", StringUtils.transString(dto.getChannelName()));
    	map.put("imgPath", StringUtils.transString(dto.getImgPath()));
//    	map.put("subTitle", StringUtils.transString(dto.getSubTitle()));
//    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//    	map.put("publishTime", dto.getPublishTime()==null?sdf.format(new Date()):sdf.format(dto.getPublishTime()));
    	if(dto.getChildren().size()>0){
    		List<Map<String,Object>> list=new ArrayList<Map<String,Object>>();
    		for(CmsChannel data:dto.getChildren()){
    			Map<String,Object> dataMap=findByParent(data);
    			list.add(dataMap);
    		}
    		map.put("child", list);
    	}
    	return map;
    }

}
