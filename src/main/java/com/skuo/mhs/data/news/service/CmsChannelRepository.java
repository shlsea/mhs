package com.skuo.mhs.data.news.service;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import com.skuo.mhs.data.news.pojo.CmsChannel;
import com.skuo.mhs.support.CustomRepository;

/**
 * 
 * @ClassName: CmsChannelRepository
 * @Description: 新闻栏目实现DAO
 * @Company: 四川西谷智慧科技有限公司
 * @author: 余德山
 * @date: 2017年9月4日 下午5:29:30
 * @version: V1.0.0
 */
@Service
public interface CmsChannelRepository extends CustomRepository<CmsChannel, String> {

	/**
	 *
	 * @Description:查询所有未删除栏目并按照字符排序
	 * @return
	 * @date 2017年9月5日上午9:31:30
	 */
	@Query("select u from CmsChannel u where u.state !=-1 order by u.ordno ")
	List<CmsChannel> findChannels();

	/**
	 * 
	 * @Description:根据父类栏目查询子栏目
	 * @param data
	 *            父类栏目
	 * @return
	 * @date 2017年9月5日下午3:31:30
	 */
	List<CmsChannel> findChannelsByParentOrderByOrderListAsc(CmsChannel data);


	/**
	 * @DATE: 2017/9/12-17:28.
	 * @Description:查询单个数据信息
	 */
	CmsChannel findByChannelId(String id);
}
