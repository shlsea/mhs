package com.skuo.mhs.data.news.service;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import com.skuo.mhs.data.news.pojo.CmsNews;
import com.skuo.mhs.form.PageForm;
import com.skuo.mhs.support.CustomRepository;

import java.util.List;

/**
 * 
 * @ClassName: CmsChannelRepository
 * @Description: 新闻实现DAO
 * @Company: 四川西谷智慧科技有限公司
 * @author: 余德山
 * @date: 2017年9月4日 下午5:29:30
 * @version: V1.0.0
 */
@Service
public interface CmsNewsRepository extends CustomRepository<CmsNews, Long> {

	/**
	 * @DATE: 2017/9/11-16:44.
	 * @Description:查询新闻(分页)
	 */
	List<CmsNews> findByChannelId(String id, PageForm form);

	/**
	 * @DATE: 2017/9/12-18:02.
	 * @Description:根据新闻频道编号查询单个新闻纪录
	 */
	CmsNews findByChannelId(String id);


	/**
	 * @Description: 利用isTop排序
	 * @return
	 * @date: 2017年9月22日 下午2:37:06
	 */
	@Query("select u from CmsNews u where u.channelId=? order by u.addDate desc")
	List<CmsNews> findNews(String channelId);
}
