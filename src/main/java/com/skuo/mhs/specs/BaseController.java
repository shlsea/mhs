package com.skuo.mhs.specs;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import com.skuo.mhs.utils.CookieUtils;

/**
 * 
 * 
 * @ClassName: BaseController
 * @Description: 公共逻辑控制器主要设置访问成功，错误跳转地址
 * @Company: 四川西谷智慧科技有限公司
 * @author: 余德山
 * @date: 2017年9月1日 下午1:35:42
 * @version: V1.0.0
 */
public class BaseController {

	private static final String REDIRECTURL = "redirectUrl";
	private static final String ACTIONMESSAGES = "actionMessages";
	private static final String ERRORMESSAGES = "errorMessages";

	/**
	 * 
	 * 跳转成功页面
	 * 
	 * @return
	 * @date 2017年8月31日上午10:53:57
	 *
	 */
	protected String success() {
		return "redirect:success.action";
	}

	/**
	 * 
	 *
	 * 跳转成功页面并设置返回上一次访问路径
	 * 
	 * @param request
	 *            请求信息
	 * @return
	 * @date 2017年8月31日上午10:54:27
	 *
	 */
	protected String success(HttpServletRequest request) {
		Cookie cookie = CookieUtils.getCookie(request, "_LAST_URL_");
		if (cookie != null) {
			String redirectURL = cookie.getValue();
			return "redirect:success.action?" + REDIRECTURL + "=" + redirectURL;
		}
		return "redirect:success.action";
	}

	/**
	 * 
	 *
	 * 返回成功页面设置提示成功信息
	 * 
	 * @param request
	 *            请求信息
	 * @param actionMessages
	 *            成功提示
	 * @return
	 * @date 2017年8月31日上午10:55:33
	 *
	 */
	protected String success(HttpServletRequest request, String actionMessages) {
		String message = "";
		try {
			message = URLEncoder.encode(actionMessages, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		Cookie cookie = CookieUtils.getCookie(request, "_LAST_URL_");
		if (cookie != null) {
			String redirectURL = cookie.getValue();
			return "redirect:success.action?" + REDIRECTURL + "=" + redirectURL + "&" + ACTIONMESSAGES + "=" + message;
		}
		return "redirect:success.action?" + ACTIONMESSAGES + "=" + message;
	}

	/**
	 * 
	 *
	 * 返回成功页面并提供返回成功路径
	 * 
	 * @param redirectURL
	 *            跳转方法
	 * @param request
	 *            请求信息
	 * @return
	 * @date 2017年8月31日上午10:56:09
	 *
	 */
	protected String success(String redirectURL, HttpServletRequest request) {
		try {
			redirectURL = URLEncoder.encode(redirectURL, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return "redirect:success.action?" + REDIRECTURL + "=" + redirectURL;
	}

	/**
	 * 
	 *
	 * 返回成功页面，并提示成功操作信息
	 * 
	 * @param redirectURL
	 *            访问成功路径
	 * @param request
	 *            请求信息
	 * @param actionMessages
	 *            成功提示信息
	 * @return
	 * @date 2017年8月31日上午10:56:51
	 *
	 */
	protected String success(String redirectURL, HttpServletRequest request, String actionMessages) {
		String message = "";
		try {
			message = URLEncoder.encode(actionMessages, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		try {
			redirectURL = URLEncoder.encode(redirectURL, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return "redirect:success.action?" + REDIRECTURL + "=" + redirectURL + "&" + ACTIONMESSAGES + "=" + message;
	}

	/**
	 * 
	 *
	 * 返回错误页面
	 * 
	 * @return
	 * @date 2017年8月31日上午10:58:48
	 *
	 */
	protected String error() {
		return "redirect:error.action";
	}

	protected String error(HttpServletRequest request) {
		return "redirect:error.action";
	}

	/**
	 * 
	 *
	 * 返回错误页面并提示错误信息
	 * 
	 * @param request
	 *            请求信息
	 * @param errorMessages
	 *            错误提示
	 * @return
	 * @date 2017年8月31日上午10:59:08
	 *
	 */
	protected String error(HttpServletRequest request, String errorMessages) {
		String message = "";
		try {
			message = URLEncoder.encode(errorMessages, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return "redirect:error.action?" + ERRORMESSAGES + "=" + message;
	}

	/**
	 * 
	 *
	 * 提示错误信息后返回访问地址
	 * 
	 * @param redirectURL
	 *            访问地址
	 * @param request
	 *            请求信息
	 * @return
	 * @date 2017年8月31日上午10:59:50
	 *
	 */
	protected String error(String redirectURL, HttpServletRequest request) {
		try {
			redirectURL = URLEncoder.encode(redirectURL, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return "redirect:error.action?" + REDIRECTURL + "=" + redirectURL;
	}

	/**
	 * 
	 *
	 * 提示错误信息后返回访问地址
	 * 
	 * @param redirectURL
	 *            访问地址
	 * @param request
	 *            请求信息
	 * @param errorMessages
	 *            错误提示
	 * @return
	 * @date 2017年8月31日上午11:00:41
	 *
	 */
	protected String error(String redirectURL, HttpServletRequest request, String errorMessages) {
		String message = "";
		try {
			message = URLEncoder.encode(errorMessages, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		try {
			redirectURL = URLEncoder.encode(redirectURL, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return "redirect:error.action?" + REDIRECTURL + "=" + redirectURL + "&" + ERRORMESSAGES + "=" + message;
	}

	/**
	 * @Description:无权限操作跳转
	 * @return
	 * @date 2017年9月1日下午1:43:11
	 *
	 */
	protected String noright() {
		return "redirect:noright.action";
	}

}
