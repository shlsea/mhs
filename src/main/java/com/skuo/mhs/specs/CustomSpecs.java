package com.skuo.mhs.specs;

import static com.google.common.collect.Iterables.toArray;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.Attribute;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.SingularAttribute;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.ReflectionUtils;
import org.springframework.util.StringUtils;

/**
 * 
 * 自定义Repository实现<br>
 * 适用任何实体
 * 
 * @ClassName: CustomSpecs
 * @Description: 自定义Repository实现
 * @Company: 四川西谷智慧科技有限公司
 * @author: 余德山
 * @date: 2017年9月1日 下午1:36:04
 * @version: V1.0.0
 */
@SuppressWarnings("hiding")
public class CustomSpecs {

	/**
	 * 定义一个返回值为Specification的方法byAuto,这里使用的是泛型T,所以这个Specification是可以用于任意实体的,
	 * <br>
	 * 它接受的参数是entityManager和当前包含值作为查询条件的实体对象。
	 * 
	 * @param entityManager
	 *            实体对象
	 * @param example
	 * @return
	 */
	public static <T> Specification<T> byAuto(final EntityManager entityManager, final T example) {

		/**
		 * 获得当前实体对象类型
		 */
		@SuppressWarnings("unchecked")
		final Class<T> type = (Class<T>) example.getClass();

		return new Specification<T>() {

			@Override
			public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

				/**
				 * 新建Predicate列表存储构造的查询条件
				 */
				List<Predicate> predicates = new ArrayList<>();

				/**
				 * 获得实体类的EntityType,我们可以从EntityType获得实体类的属性
				 */
				EntityType<T> entity = entityManager.getMetamodel().entity(type);

				/**
				 * 对实体类的所有属性做循环
				 */
				for (Attribute<T, ?> attr : entity.getDeclaredAttributes()) {

					/**
					 * 获得实体类对象摸一个属性的值
					 */
					Object attrValue = getValue(example, attr);
					if (attrValue != null) {

						/**
						 * 当前属性值为字符类型的时候
						 */
						if (attr.getJavaType() == String.class) {

							/**
							 * 若单且字符不为空的情况下
							 */
							if (!StringUtils.isEmpty(attrValue)) {

								/**
								 * 构造当前属性like(前后%)属性值查询条件，并添加到条件列表中
								 */
								if (attr.getName().equals("region")) {
									predicates.add(cb.like(root.get(attribute(entity, attr.getName(), String.class)),
											patternPre((String) attrValue)));
								} else {
									predicates.add(cb.like(root.get(attribute(entity, attr.getName(), String.class)),
											pattern((String) attrValue)));
								}

							}
						} else if (attr.getJavaType() == Set.class) {

						} else {
							predicates.add(cb.equal(root.get(attribute(entity, attr.getName(), attrValue.getClass())),
									attrValue));
						}
					}

				}

				/**
				 * 将条件列表转换成Predicate
				 */
				return predicates.isEmpty() ? cb.conjunction() : cb.and(toArray(predicates, Predicate.class));
			}

			/**
			 * 拖过反射获得实体类对象对应属性的属性值
			 */

			private <T> Object getValue(T example, Attribute<T, ?> attr) {
				return ReflectionUtils.getField((Field) attr.getJavaMember(), example);
			}

			/**
			 * 获得实体类的当前属性的SingularAttribute,SingularAttribute包含的是实体类的某个单独属性
			 */

			private <E, T> SingularAttribute<T, E> attribute(EntityType<T> entity, String fieldName,
					Class<E> fieldClass) {
				return entity.getDeclaredSingularAttribute(fieldName, fieldClass);
			}

		};

	}

	/**
	 * 构造like的查询模式,即前后加%
	 * 
	 * @param str
	 * @return
	 */
	static private String pattern(String str) {
		return "%" + str + "%";
	}

	/**
	 * 构造like前缀模糊查询
	 * 
	 * @param str
	 * @return
	 */
	static private String patternPre(String str) {
		return str + "%";
	}

	/**
	 * 构造like后缀模糊查询
	 * 
	 * @param str
	 * @return
	 */
	@SuppressWarnings("unused")
	static private String pattNext(String str) {
		return "%" + str;
	}
}
