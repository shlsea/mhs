package com.skuo.mhs.map.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 地图控制器
 * @author zxs
 *
 */
@Controller
@RequestMapping(value = "/map")
public class MapController {

	/**
	 * 跳转到地图页面
	 * @return
	 */
	@RequestMapping("/mainMap.action")
	public String mainMap(HttpServletRequest request, HttpServletResponse response, ModelMap model){
		return "map/main_map";
	}
	
}
