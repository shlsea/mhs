package com.skuo.mhs.lucene;

import org.apache.lucene.document.Document;

/**
 * 
 * 
 * @ClassName: DocumentUtils
 * @Description: 收录检索内容
 * @Company: 四川西谷智慧科技有限公司
 * @author: 余德山
 * @date: 2017年9月1日 下午1:33:48
 * @version: V1.0.0
 */
public class DocumentUtils {

	// //新闻
	// public static Document articleNewsContent(ZxNews article,
	// HttpServletRequest request) {
	// Document doc = new Document();
	// doc.add(new Field("id", String.valueOf(article.getId()),
	// TextField.TYPE_STORED));
	// doc.add(new Field("title", article.getTitle(), TextField.TYPE_STORED));
	// doc.add(new Field("content",
	// StringUtils.replaceHtml(article.getContent()), TextField.TYPE_STORED));
	// doc.add(new Field("menuId", String.valueOf(article.getMenu().getId()),
	// TextField.TYPE_STORED));
	// doc.add(new Field("name", article.getMenu().getName(),
	// TextField.TYPE_STORED));
	// return doc;
	// }

	/**
	 * 
	 *
	 * @Description:检索结果转换为实体对象
	 * @param doc
	 * @return
	 * @date 2017年9月1日下午1:34:02
	 *
	 */
	public static SearchData documentNewsContent(Document doc) {
		SearchData article = new SearchData();
		article.setId(Long.valueOf(doc.get("id")));
		article.setTitle(doc.get("title"));
		article.setContent(doc.get("content"));
		article.setMenu(Long.valueOf(doc.get("menuId")));
		article.setName(doc.get("name"));
		return article;
	}

}
