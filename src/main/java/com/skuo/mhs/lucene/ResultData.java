package com.skuo.mhs.lucene;

import java.util.List;

/**
 * 
* 
* @ClassName: ResultData 
* @Description: 检索查询结果集
* @Company: 四川西谷智慧科技有限公司
* @author: 余德山 
* @date: 2017年9月1日 下午1:35:19 
* @version: V1.0.0
 */
public class ResultData {

	/**
	 * 查询结果总条数
	 */
	private int total;
	/**
	 * 查询结果数据
	 */
	private List<SearchData> rows;

	public ResultData() {
		super();
	}

	public ResultData(int total, List<SearchData> rows) {
		super();
		this.total = total;
		this.rows = rows;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public List<SearchData> getRows() {
		return rows;
	}

	public void setRows(List<SearchData> rows) {
		this.rows = rows;
	}

}
