package com.skuo.mhs.lucene;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import com.skuo.mhs.utils.Configure;

import java.nio.file.Paths;

/**
 * 
* 
* @ClassName: LuceneUtils 
* @Description: 获取分词器和索引位置
* @Company: 四川西谷智慧科技有限公司
* @author: 余德山 
* @date: 2017年9月1日 下午1:35:07 
* @version: V1.0.0
 */
public class LuceneUtils {
	private static Logger logger = Logger.getLogger(LuceneUtils.class);
	private static Directory directory;
	private static Analyzer analyzer;

	@Autowired
	private Environment env;

	static {
		try {
			// 获取配置文件
			Configure config = new Configure("application.properties");
			// 获取配置文件Lucene存放路径
			String indexfile = config.getValue("lucene.path");
			directory = FSDirectory.open(Paths.get(indexfile));
			analyzer = new StandardAnalyzer();
		} catch (Exception e) {
			logger.error("LuceneUtils error!", e);
		}
	}

	public static Directory getDirectory() {
		return directory;
	}

	public static Analyzer getAnalyzer() {
		return analyzer;
	}

	public static void closeIndexWriter(IndexWriter indexWriter) {
		if (indexWriter != null) {
			try {
				indexWriter.close();
			} catch (Exception e2) {
				logger.error("indexWriter.close error", e2);
			}
		}
	}

	public String getPath() {
		return env.getProperty("lucene.path");
	}

}
