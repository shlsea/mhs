package com.skuo.mhs.lucene;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

/**
 * 
 * 
 * @ClassName: Article
 * @Description: 检索内容实体类(不存数据库)
 * @Company: 四川西谷智慧科技有限公司
 * @author: 余德山
 * @date: 2017年9月1日 下午1:33:35
 * @version: V1.0.0
 */
public class Article {

	/***
	 * 检索id
	 */
	@Id
	@GeneratedValue
	private String id;
	/***
	 * 检索标题
	 */
	private String title;

	/***
	 * 检索内容
	 */
	private String content;

	/***
	 * 链接地址
	 */
	private String url;

	/***
	 * 添加时间
	 */
	private Date addTime;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Date getAddTime() {
		return addTime;
	}

	public void setAddTime(Date addTime) {
		this.addTime = addTime;
	}

}
