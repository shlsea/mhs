package com.skuo.mhs.lucene;

import org.apache.log4j.Logger;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.*;
import org.apache.lucene.search.highlight.*;
import org.apache.lucene.search.highlight.Scorer;
import java.util.ArrayList;
import java.util.List;

/**
 * 
* 
* @ClassName: IndexDao 
* @Description: Lucene工具对索引进行管理
* @Company: 四川西谷智慧科技有限公司
* @author: 余德山 
* @date: 2017年9月1日 下午1:34:57 
* @version: V1.0.0
 */
public class IndexDao {

	private static Logger logger = Logger.getLogger(IndexDao.class);

	//

	/**
	 * 新闻方法 保持索引
	 * 
	 * @param article
	 * @param request
	 */
	// public static void save(ZxNews article, HttpServletRequest request) {
	// Document doc = DocumentUtils.articleNewsContent(article, request);
	// IndexWriter indexWriter = null;
	// try {
	// IndexWriterConfig config = new
	// IndexWriterConfig(LuceneUtils.getAnalyzer());
	// indexWriter = new IndexWriter(LuceneUtils.getDirectory(), config);
	// indexWriter.addDocument(doc);
	// } catch (Exception e) {
	// logger.error("IndexDao.save error", e);
	// } finally {
	// LuceneUtils.closeIndexWriter(indexWriter);
	// }
	// }

	/**
	 * 删除索引
	 * 
	 * @param id
	 */
	public static void deleteNewsContent(String id) {
		IndexWriter indexWriter = null;
		try {
			Term term = new Term("id", String.valueOf(id));
			IndexWriterConfig config = new IndexWriterConfig(LuceneUtils.getAnalyzer());
			indexWriter = new IndexWriter(LuceneUtils.getDirectory(), config);
			indexWriter.deleteDocuments(term);// 删除含有指定term的所有文档
		} catch (Exception e) {
			logger.error("IndexDao.save error", e);
		} finally {
			LuceneUtils.closeIndexWriter(indexWriter);
		}
	}

	/***
	 * 更新索引
	 * 
	 * @param article
	 */
	// public static void update(ZxNews article, HttpServletRequest request) {
	// Document doc = DocumentUtils.articleNewsContent(article, request);
	// IndexWriter indexWriter = null;
	// try {
	// Term term = new Term("id", article.getId().toString());
	// IndexWriterConfig config = new
	// IndexWriterConfig(LuceneUtils.getAnalyzer());
	// indexWriter = new IndexWriter(LuceneUtils.getDirectory(), config);
	// indexWriter.updateDocument(term, doc);// 先删除，后创建。
	// } catch (Exception e) {
	// logger.error("IndexDao.save error", e);
	// } finally {
	// LuceneUtils.closeIndexWriter(indexWriter);
	// }
	// }

	/***
	 * 查询索引 新闻
	 * 
	 * @param keyWord
	 *            查询条件
	 * @param firstResult
	 *            开始条数
	 * @param maxResult
	 *            每页展示条数
	 * @return
	 */
	public ResultData searchSiteContent(String keyWord, int firstResult, int maxResult) {
		List<SearchData> list = new ArrayList<SearchData>();
		try {
			DirectoryReader ireader = DirectoryReader.open(LuceneUtils.getDirectory());
			// 2、第二步，创建搜索器
			IndexSearcher isearcher = new IndexSearcher(ireader);

			// 3、第三步，类似SQL，进行关键字查询
			String[] fields = { "title", "content" };
			QueryParser parser = new MultiFieldQueryParser(fields, LuceneUtils.getAnalyzer());
			Query query = parser.parse(keyWord);
			BooleanQuery.Builder builder = new BooleanQuery.Builder();
			builder.add(query, BooleanClause.Occur.MUST);
			TopDocs topDocs = isearcher.search(builder.build(), firstResult + maxResult);
			// 总记录数
			int count = topDocs.totalHits;
			// System.out.println("总记录数为：" + topDocs.totalHits);
			ScoreDoc[] hits = topDocs.scoreDocs;
			// 高亮
			Formatter formatter = new SimpleHTMLFormatter("<font color='red'>", "</font>");
			Scorer source = new QueryScorer(query);
			Highlighter highlighter = new Highlighter(formatter, source);
			highlighter.setTextFragmenter(new SimpleFragmenter(400));// 设置每次返回的字符数
			int endIndex = Math.min(firstResult + maxResult, hits.length);
			for (int i = firstResult; i < endIndex; i++) {
				Document hitDoc = isearcher.doc(hits[i].doc);
				SearchData article = DocumentUtils.documentNewsContent(hitDoc);
				String title = highlighter.getBestFragment(LuceneUtils.getAnalyzer(), "title", hitDoc.get("title"));
				if (title != null) {
					article.setTitle(title);
				}
				String text = highlighter.getBestFragment(LuceneUtils.getAnalyzer(), "content", hitDoc.get("content"));
				if (text != null) {
					article.setContent(text);
				}
				list.add(article);
			}
			ireader.close();
			return new ResultData(count, list);
		} catch (Exception e) {
			logger.error("IndexDao.search error", e);
		}
		return null;
	}

}
