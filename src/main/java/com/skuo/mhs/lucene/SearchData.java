package com.skuo.mhs.lucene;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.util.Date;

/**
 * 
 * 
 * @ClassName: SearchData
 * @Description: 检索查询结果存放实体
 * @Company: 四川西谷智慧科技有限公司
 * @author: 余德山
 * @date: 2017年9月1日 下午1:35:29
 * @version: V1.0.0
 */
public class SearchData {

	/**
	 * 主键
	 */
	@Id
	@GeneratedValue
	private Long id;

	/**
	 * 名称
	 */
	private String name;
	/**
	 * 标题
	 */
	private String title;

	/**
	 * 摘要
	 */
	@Length(max = 500)
	private String remark;

	/**
	 * 新闻内容
	 */
	@Type(type = "text")
	private String content;

	/**
	 * 是否图片新闻
	 */
	private Long isImg;

	/**
	 * 图片路径
	 */
	private String imgPath;

	/**
	 * 创建时间
	 */
	private Date createTime;

	/**
	 * 发布时间
	 */
	private Date pubTime;

	/**
	 * 点击数
	 */
	private Long checkNum;

	/**
	 * 是否置顶
	 * 
	 */
	private Long isTop;

	/**
	 * 作者
	 */
	private String author;

	/**
	 * 来源
	 */
	private String source;

	/**
	 * 排序
	 */
	private Long orderList;

	/**
	 * 关注数
	 */
	private Long followNum;

	/**
	 * 状态
	 */
	private Long state;

	/**
	 * 所属栏目
	 */
	private Long menu;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getPubTime() {
		return pubTime;
	}

	public void setPubTime(Date pubTime) {
		this.pubTime = pubTime;
	}

	public Long getCheckNum() {
		return checkNum;
	}

	public void setCheckNum(Long checkNum) {
		this.checkNum = checkNum;
	}

	public Long getState() {
		return state;
	}

	public void setState(Long state) {
		this.state = state;
	}

	public Long getMenu() {
		return menu;
	}

	public void setMenu(Long menu) {
		this.menu = menu;
	}

	public Long getIsImg() {
		return isImg;
	}

	public void setIsImg(Long isImg) {
		this.isImg = isImg;
	}

	public String getImgPath() {
		return imgPath;
	}

	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}

	public Long getIsTop() {
		return isTop;
	}

	public void setIsTop(Long isTop) {
		this.isTop = isTop;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public Long getOrderList() {
		return orderList;
	}

	public void setOrderList(Long orderList) {
		this.orderList = orderList;
	}

	public Long getFollowNum() {
		return followNum;
	}

	public void setFollowNum(Long followNum) {
		this.followNum = followNum;
	}

}
