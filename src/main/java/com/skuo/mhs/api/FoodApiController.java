package com.skuo.mhs.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.skuo.mhs.data.res.pojo.DataFood;
import com.skuo.mhs.data.res.pojo.DataRestaurant;
import com.skuo.mhs.data.res.service.DataFoodRepository;
import com.skuo.mhs.data.res.service.DataRestaurantRepository;
import com.skuo.mhs.form.PageForm;
import com.skuo.mhs.media.service.MediaPictureRepository;
import com.skuo.mhs.utils.PaginationUtils;
import com.skuo.mhs.utils.StringUtils;

/**
 * 
 * @ClassName: FoodApiController
 * @Description: 酒店对外接口
 * @Company: 四川西谷智慧科技有限公司
 * @author: songhailing
 * @date: 2017年9月12日 下午2:28:27
 * @version: V1.0.0
 */
@Controller
@RequestMapping(value = "/food-api")
public class FoodApiController {

	@Autowired
	private DataRestaurantRepository dataRestaurantRepository;

	@Autowired
	private DataFoodRepository dataFoodRepository;

	@Autowired
	private MediaPictureRepository mediaPictureRepository;

	/**
	 * @Description: 读取景区景点列表
	 * @param pageForm
	 * @param dto
	 * @return:
	 * @date: 2017年9月4日 下午14:18:26
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@ResponseBody
	public Object list(PageForm pageForm, DataRestaurant dto, HttpServletRequest request) {
		List<DataRestaurant> list = dataRestaurantRepository.findByState(1L);
		list = (List<DataRestaurant>) PaginationUtils.pagination(list, pageForm.getOffset(), pageForm.getLimit());
		/*
		 * List<MediaPicture> findAll = mediaPictureRepository.findAll();
		 * List<String> pictures = new ArrayList<String>();
		 */
		Map<String, Object> map = new HashMap<String, Object>();
		List<Map<String, Object>> _list = new ArrayList<Map<String, Object>>();
		for (DataRestaurant data : list) {
			Map<String, Object> _mMap = new HashMap<String, Object>();
			_mMap.put("id", data.getId());
			_mMap.put("name", StringUtils.transString(data.getName()));
			_mMap.put("summary", StringUtils.transString(data.getSummary()));
			_mMap.put("imgPath", StringUtils.transString(data.getImgPath()));
			// _mMap.put("cost", StringUtils.transString(data.getCost()));
			/*
			 * String type = ""; Long type2 = data.getType(); if(type2==2){
			 * type="主题酒店"; }else{ type="商务酒店"; } _mMap.put("type", type); for
			 * (MediaPicture picture : findAll) { if ((data.getId() +
			 * "").equals(picture.getResourceId())) {
			 * pictures.add(StringUtils.transString(picture.getResourceId())); }
			 * } _mMap.put("pictures", pictures);
			 */
			_list.add(_mMap);
		}
		map.put("rows", _list);
		map.put("total", list.size());
		if (list.size() == 0) {
			map.put("state", 0);
		} else {
			map.put("state", 1);
		}
		return map;
	}

	/**
	 * @Description: 获取餐馆下的美食信息
	 * @param pageForm
	 * @param dto
	 * @return:
	 * @date: 2017年9月4日 下午14:18:26
	 */
	@RequestMapping(value = "/findFoods", method = RequestMethod.GET)
	@ResponseBody
	public Object findFoods(Long id, HttpServletRequest request) {
		// pageForm.setLimit(10);
		DataRestaurant restaurant = dataRestaurantRepository.findOne(id);

		/*
		 * List<MediaPicture> findAll = mediaPictureRepository.findAll();
		 * List<String> pictures = new ArrayList<String>();
		 */
		List<DataFood> foods = dataFoodRepository.findByBelongRestaurant(id);
		Map<String, Object> _mMap = new HashMap<String, Object>();
		if (restaurant != null && restaurant.getState() != 0L) {
			_mMap.put("id", restaurant.getId());
			_mMap.put("restaurantName", StringUtils.transString(restaurant.getName()));
			_mMap.put("restaurantContent", StringUtils.transString(restaurant.getContent()));
			_mMap.put("imgPath", StringUtils.transString(restaurant.getImgPath()));
			// 去掉不必要的字段影响
			List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
			for (DataFood food : foods) {
				Map<String, Object> _mMap1 = new HashMap<String, Object>();
				_mMap1.put("id", food.getId());
				_mMap1.put("foodName", StringUtils.transString(food.getName()));
				// _mMap1.put("summary",
				// StringUtils.transString(food.getSummary()));
				_mMap1.put("foodImgPath", StringUtils.transString(food.getImgPath()));
				// _mMap1.put("price", StringUtils.transString(food.getCost()));
				list.add(_mMap1);
			}
			/*
			 * for (MediaPicture picture : findAll) { if ((restaurant.getId() +
			 * "").equals(picture.getResourceId())) {
			 * pictures.add(StringUtils.transString(picture.getResourceId())); }
			 * } _mMap.put("pictures", pictures);
			 */
			_mMap.put("children", list);
			_mMap.put("state", 1);
		} else {
			_mMap.put("state", 0);
		}
		return _mMap;
	}

	/**
	 * @Description: 获取一种美食信息
	 * @param pageForm
	 * @param dto
	 * @return:
	 * @date: 2017年9月4日 下午14:18:26
	 */
	@RequestMapping(value = "/findOne", method = RequestMethod.GET)
	@ResponseBody
	public Object findOne(Long id, HttpServletRequest request) {
		// pageForm.setLimit(10);
		DataFood food = dataFoodRepository.findOne(id);
		Map<String, Object> _mMap = new HashMap<String, Object>();
		if (food != null && food.getState() != 0L) {
			_mMap.put("id", food.getId());
			_mMap.put("cost", StringUtils.transString(food.getCost()));
			_mMap.put("content", StringUtils.transString(food.getContent()));
			_mMap.put("imgPath", StringUtils.transString(food.getImgPath()));
			_mMap.put("state", 1);
		} else {
			_mMap.put("state", 0);
		}
		return _mMap;
	}
}
