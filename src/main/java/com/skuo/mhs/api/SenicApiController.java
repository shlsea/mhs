package com.skuo.mhs.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.skuo.mhs.data.res.pojo.DataScenic;
import com.skuo.mhs.data.res.service.DataScenicRepository;
import com.skuo.mhs.form.PageForm;
import com.skuo.mhs.media.pojo.MediaPicture;
import com.skuo.mhs.media.service.MediaPictureRepository;
import com.skuo.mhs.utils.PaginationUtils;
import com.skuo.mhs.utils.StringUtils;

/**
 * 
 * @ClassName: SenicApiController
 * @Description: 景区对外接口
 * @Company: 四川西谷智慧科技有限公司
 * @author: songhailing
 * @date: 2017年9月12日 下午2:28:27
 * @version: V1.0.0
 */
@Controller
@RequestMapping(value = "/scenic-api")
public class SenicApiController {

	@Autowired
	private DataScenicRepository dataScenicRepository;

	@Autowired
	private MediaPictureRepository mediaPictureRepository;

	/**
	 * @Description: 读取景区景点列表
	 * @param pageForm
	 * @param dto
	 * @return:
	 * @date: 2017年9月4日 下午14:18:26
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/listAll", method = RequestMethod.GET)
	@ResponseBody
	public Object list(PageForm pageForm, DataScenic dto, HttpServletRequest request) {
		List<DataScenic> list = dataScenicRepository.findByState(1L);
		list = (List<DataScenic>) PaginationUtils.pagination(list, pageForm.getOffset(), pageForm.getLimit());
		/*
		 * List<MediaPicture> findAll = mediaPictureRepository.findAll();
		 * List<String> pictures = new ArrayList<String>();
		 */
		Map<String, Object> map = new HashMap<String, Object>();
		List<Map<String, Object>> _list = new ArrayList<Map<String, Object>>();
		for (DataScenic data : list) {
			Map<String, Object> _mMap = new HashMap<String, Object>();
			_mMap.put("id", data.getId());
			_mMap.put("name", StringUtils.transString(data.getName()));
			// _mMap.put("content",StringUtils.transString(data.getContent()));
			// _mMap.put("urlOf720",
			// StringUtils.transString(data.getUrlOf720()));
			_mMap.put("imgPath", StringUtils.transString(data.getImgPath()));
			_mMap.put("summary", StringUtils.transString(data.getSummary()));
			/*
			 * for (MediaPicture picture : findAll) {
			 * if((data.getId()+"").equals(picture.getResourceId())){
			 * pictures.add(StringUtils.transString(picture.getResourceId())); }
			 * } _mMap.put("pictures", pictures);
			 */
			_list.add(_mMap);
		}
		map.put("rows", _list);
		map.put("total", list.size());
		if (list.size() == 0) {
			map.put("state", 0);
		} else {
			map.put("state", 1);
		}
		return map;
	}

	/**
	 * @Description: 读取景区景点详情
	 * @param pageForm
	 * @param dto
	 * @return:
	 * @date: 2017年9月4日 下午14:18:26
	 */
	@RequestMapping(value = "/findOne", method = RequestMethod.GET)
	@ResponseBody
	public Object findOne(Long id, HttpServletRequest request) {
		// pageForm.setLimit(10);
		DataScenic scenic = dataScenicRepository.findOne(id);
		List<MediaPicture> findAll = mediaPictureRepository.findAll();
		List<String> pictures = new ArrayList<String>();
		Map<String, Object> _mMap = new HashMap<String, Object>();
		if (scenic != null && scenic.getState() != 0L) {
			_mMap.put("id", scenic.getId());
			_mMap.put("name", StringUtils.transString(scenic.getName()));
			_mMap.put("content", StringUtils.transString(scenic.getContent()));
			_mMap.put("urlOf720", StringUtils.transString(scenic.getUrlOf720()));
			_mMap.put("imgPath", StringUtils.transString(scenic.getImgPath()));
			_mMap.put("imgPathFor720", StringUtils.transString(scenic.getImgPathFor720()));
			_mMap.put("voicePath", StringUtils.transString(scenic.getVoicePath()));
			for (MediaPicture picture : findAll) {
				if ((scenic.getId() + "").equals(picture.getResourceId())) {
					pictures.add(StringUtils.transString(picture.getResourceId()));
				}
			}
			_mMap.put("pictures", pictures);
			_mMap.put("state", 1);
		} else {
			_mMap.put("state", 0);
		}
		return _mMap;
	}

}
