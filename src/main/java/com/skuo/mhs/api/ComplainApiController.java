package com.skuo.mhs.api;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.skuo.mhs.data.res.pojo.DataComplain;
import com.skuo.mhs.data.res.service.DataComplainRepository;
import com.skuo.mhs.form.PageForm;
import com.skuo.mhs.utils.Configure;
import com.skuo.mhs.utils.Constant;
import com.skuo.mhs.utils.FFmpegUtil;
import com.skuo.mhs.utils.FileMap;
import com.skuo.mhs.utils.PaginationUtils;
import com.skuo.mhs.utils.StringUtils;
import com.skuo.mhs.utils.UploadUtil;

import net.sf.json.JSONObject;

/**
 * 
 * @ClassName: ComplainApiController
 * @Description: 景区对外接口
 * @Company: 四川西谷智慧科技有限公司
 * @author: songhailing
 * @date: 2017年9月12日 下午2:28:27
 * @version: V1.0.0
 */
@Controller
@RequestMapping(value = "/complain-api")
public class ComplainApiController {

	@Autowired
	private DataComplainRepository dataComplainRepository;

	/**
	 * @Description: 读取投诉建议列表
	 * @param pageForm
	 * @param dto
	 * @return:
	 * @date: 2017年9月4日 下午14:18:26
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/listAll", method = RequestMethod.GET)
	@ResponseBody
	public Object list(PageForm pageForm, DataComplain dto, HttpServletRequest request) {
		List<DataComplain> list = dataComplainRepository.findByState(1L);
		list = (List<DataComplain>) PaginationUtils.pagination(list, pageForm.getOffset(), pageForm.getLimit());
		Map<String, Object> map = new HashMap<String, Object>();
		List<Map<String, Object>> _list = new ArrayList<Map<String, Object>>();
		for (DataComplain data : list) {
			Map<String, Object> _mMap = new HashMap<String, Object>();
			_mMap.put("id", data.getId());
			_mMap.put("title", StringUtils.transString(data.getTitle()));
			_mMap.put("content", StringUtils.transString(data.getContent()));
//			_mMap.put("videoMp4", StringUtils.transString(data.getVideoMp4()));
			_mMap.put("imgPath", StringUtils.transString(data.getImgPath()));
			// _mMap.put("state", data.getState()==1?"已处理":"未处理");
			_list.add(_mMap);
		}
		map.put("rows", _list);
		map.put("total", list.size());
		if (list.size() == 0) {
			map.put("state", 0);
		} else {
			map.put("state", 1);
		}

		return map;
	}

	/**
	 * @Description:保存投诉信息
	 * @param dto
	 * @return
	 * @date 2017年9月13日 下午13:58:04
	 */
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	@ResponseBody
	public Object save(@ModelAttribute DataComplain dto) {
		Map<String, Object> map = new HashMap<String, Object>();
		// request
		String message = "操作成功！";
		try {
			if (dto.getState() == null || dto.getState().equals("")) {
				dto.setState(1L);
			}
			if (dto.getId() != null) {
				DataComplain data = dataComplainRepository.getOne(dto.getId());
				BeanUtils.copyProperties(dto, data, new String[] { "id" });
				dataComplainRepository.save(data);

			} else {
				dataComplainRepository.save(dto);
			}
			map.put("state", 1);
		} catch (Exception e) {
			message = "数据异常，请核对之后提交";
			map.put("state", 0);
		}
		map.put("message", message);
		return map;
	}

	/**
	 * @Description:上传图片、文本文件
	 * @param file
	 * @param request
	 * @return
	 * @date 2017年9月13日下午2:27:34
	 */
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	@ResponseBody
	public Object upload(@RequestParam(value = "file", required = false) MultipartFile[] file,
			HttpServletRequest request) {

		String savePath = request.getServletContext().getRealPath("/");
		String status = Constant.STATUS_SCUESS;
		String path = "";
		for (MultipartFile myfile : file) {
			if (!myfile.isEmpty()) {

				String suffix = myfile.getOriginalFilename()
						.substring(myfile.getOriginalFilename().lastIndexOf(".") + 1).toLowerCase();
				String fileType = FileMap.getType(suffix);
				if (fileType == null || fileType.equals("")) {
					fileType = "file";
				}
				try {
					path = UploadUtil.writeFile(myfile.getOriginalFilename(), savePath, myfile.getInputStream());
				} catch (IOException e) {
					status = Constant.STATUS_ERROR;
				}

			}
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("state", status);
		map.put("path", path);
		JSONObject json = JSONObject.fromObject(map);
		return json;

	}

	/**
	 * @Description:音频、视频上传
	 * @param file
	 * @param request
	 * @return
	 * @date 2017年9月13日下午2:29:06
	 */
	@RequestMapping(value = "/mediaUpload", method = RequestMethod.POST)
	@ResponseBody
	public Object mediaUpload(@RequestParam(value = "file", required = false) MultipartFile[] file,
			HttpServletRequest request) {
		Properties props = System.getProperties();
		String os = props.getProperty("os.name");
		String savePath = request.getServletContext().getRealPath("/");
		String path = "";
		String status = Constant.STATUS_SCUESS;
		for (MultipartFile myfile : file) {
			if (!myfile.isEmpty()) {

				String suffix = myfile.getOriginalFilename()
						.substring(myfile.getOriginalFilename().lastIndexOf(".") + 1).toLowerCase();

				String fileType = FileMap.getType(suffix);
				if (fileType == null || fileType.equals("")) {
					fileType = "file";
				}

				try {
					path = UploadUtil.writeFile(myfile.getOriginalFilename(), savePath, myfile.getInputStream());
					String loadPath = savePath + path.substring(1);

					String cmdUrl = "";

					// 读取配置文件
					Configure config = new Configure("application.properties");

					// 读取ffmpeg地址
					String ffmpeg = config.getValue("ffmpeg.path");

					// 判断是否为Windows系统
					if (os.contains("Windows")) {
						cmdUrl = savePath + "tools/ffprobe.exe";
					} else {
						cmdUrl = ffmpeg;
					}
					FFmpegUtil futil = new FFmpegUtil(cmdUrl, loadPath);
					//times = futil.StringTime(futil.getRuntime());

					if (fileType.equals("video")) {
						String flvPath = "", mp4Path = "";
						if (suffix.equals("flv")) {

							// flv转mp4格式视频
							flvPath = loadPath;
							mp4Path = loadPath.substring(0, loadPath.lastIndexOf(".")) + ".mp4";
							futil.getChange(mp4Path);
						} else {
							// mp4转flv格式视频
							mp4Path = loadPath;
							flvPath = loadPath.substring(0, loadPath.lastIndexOf(".")) + ".flv";
							futil.getChange(flvPath);
						}

						// mp4存放路径
						path = "/" + mp4Path.substring(savePath.length());

						// flv存放路径
						//flv = "/" + flvPath.substring(savePath.length());
					}
				} catch (IOException e) {
					status = Constant.STATUS_ERROR;
				}
			}
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("state", status);
		map.put("path", path);
		// map.put("flvPath", flv);
		// map.put("times", times);
		JSONObject json = JSONObject.fromObject(map);
		return json;
	}

}
