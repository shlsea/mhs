package com.skuo.mhs.api;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.skuo.mhs.data.res.pojo.DataTour;
import com.skuo.mhs.data.res.service.DataTourRepository;
import com.skuo.mhs.form.PageForm;
import com.skuo.mhs.utils.PaginationUtils;
import com.skuo.mhs.utils.StringUtils;

/**
 * 
 * @ClassName: TourApiController
 * @Description: 景区对外接口
 * @Company: 四川西谷智慧科技有限公司
 * @author: songhailing
 * @date: 2017年9月12日 下午2:28:27
 * @version: V1.0.0
 */
@Controller
@RequestMapping(value = "/tour-api")
public class TourApiController {

	@Autowired
	private DataTourRepository dataTourRepository;

	/**
	 * @Description: 读取行程列表列表
	 * @param pageForm
	 * @param dto
	 * @return:
	 * @date: 2017年9月4日 下午14:18:26
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/listAll", method = RequestMethod.GET)
	@ResponseBody
	public Object list(PageForm pageForm, DataTour dto, HttpServletRequest request) {
		List<DataTour> list = dataTourRepository.findByState(1L);
		list = (List<DataTour>) PaginationUtils.pagination(list, pageForm.getOffset(), pageForm.getLimit());
		Map<String, Object> map = new HashMap<String, Object>();
		List<Map<String, Object>> _list = new ArrayList<Map<String, Object>>();
		for (DataTour data : list) {
			Map<String, Object> _mMap = new HashMap<String, Object>();
			_mMap.put("id", data.getId());
			_mMap.put("tripTheme", StringUtils.transString(data.getTripTheme()));
			_mMap.put("phone", StringUtils.transString(data.getPhone()));
			_mMap.put("cost", StringUtils.transString(data.getCost()));
			_mMap.put("remark", StringUtils.transString(data.getRemark()));
			_mMap.put("peopleNum", data.getPeopleNum() == null ? 1 : data.getPeopleNum());
			_mMap.put("stayDays", data.getStayDays() == null ? 1 : data.getStayDays());
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			_mMap.put("addDate", data.getAddDate() == null ? "" : sdf.format(data.getAddDate()));

			_list.add(_mMap);
		}
		map.put("rows", _list);
		map.put("total", list.size());
		if (list.size() == 0) {
			map.put("state", 0);
		} else {
			map.put("state", 1);
		}
		return map;
	}

	/**
	 * @Description:保存行程信息
	 * @param dto
	 * @return
	 * @date 2017年9月13日 下午13:58:04
	 */
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	@ResponseBody
	public Object save(@ModelAttribute DataTour dto) {
		Map<String, Object> map = new HashMap<String, Object>();
		// request
		String message = "操作成功！";
		try {
			if (dto.getState() == null || dto.getState().equals("")) {
				dto.setState(1L);
			}
			if (dto.getId() != null) {
				DataTour data = dataTourRepository.getOne(dto.getId());
				BeanUtils.copyProperties(dto, data, new String[] { "id" });
				dataTourRepository.save(data);

			} else {
				dto.setAddDate(new Date());
				dataTourRepository.save(dto);
			}
			map.put("state", 1);
		} catch (Exception e) {
			message = "数据异常，请核对之后提交";
			map.put("state", 0);
		}
		map.put("message", message);
		return map;
	}

}
