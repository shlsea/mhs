package com.skuo.mhs.api;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.skuo.mhs.data.res.impl.DataTicketServiceImpl;
import com.skuo.mhs.data.res.pojo.DataTicket;
import com.skuo.mhs.data.res.pojo.DataTicketDetail;
import com.skuo.mhs.data.res.service.DataTicketDetailRepository;
import com.skuo.mhs.data.res.service.DataTicketRepository;
import com.skuo.mhs.form.PageForm;
import com.skuo.mhs.utils.PaginationUtils;
import com.skuo.mhs.utils.StringUtils;

/**
 * 
 * @ClassName: TicketApiController
 * @Description: 票务对外接口
 * @Company: 四川西谷智慧科技有限公司
 * @author: songhailing
 * @date: 2017年9月12日 下午2:28:27
 * @version: V1.0.0
 */
@Controller
@RequestMapping(value = "/ticket-api")
public class TicketApiController {

	@Autowired
	private DataTicketServiceImpl ticketService;

	@Autowired
	private DataTicketRepository dataTicketRepository;
	@Autowired
	private DataTicketDetailRepository dataTicketDetailRepository;

	/**
	 * @DATE: 2017/9/11-16:39.
	 * @Description:父类下的子菜单
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/list")
	@ResponseBody
	public Object getList(PageForm pageForm, HttpServletResponse response) throws IOException {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		List<DataTicket> findAll = dataTicketRepository.findByState(1L);
		findAll = (List<DataTicket>) PaginationUtils.pagination(findAll, pageForm.getOffset(), pageForm.getLimit());
		// 去掉不必要的字段
		for (DataTicket dataTicket : findAll) {
			Map<String, Object> map = ticketService.findByParent(dataTicket);
			list.add(map);
		}
		response.setContentType("charset=UTF-8;text/json");
		response.setCharacterEncoding("UTF-8");
		response.setHeader("Access-Control-Allow-Origin", "*");
		JSONObject obj = new JSONObject();
		obj.put("data", list);
		obj.put("total", findAll.size());
		if (findAll.size() == 0) {
			obj.put("state", 0);
		} else {
			obj.put("state", 1);
		}
		return obj;
	}

	/**
	 * @Description: 获取一个门票类型下的所有门票信息
	 * @param pageForm
	 * @param dto
	 * @return:
	 * @date: 2017年9月4日 下午14:18:26
	 */
	@RequestMapping(value = "/findOne", method = RequestMethod.GET)
	@ResponseBody
	public Object findOne(Long id, HttpServletRequest request) {
		// pageForm.setLimit(10);
		DataTicket ticket = dataTicketRepository.findOne(id);
		List<DataTicketDetail> findAll = dataTicketDetailRepository.findByParentId(id);
		Map<String, Object> _mMap = new HashMap<String, Object>();
		if (ticket != null && ticket.getState() != 0L) {
			_mMap.put("id", ticket.getId());
			_mMap.put("name", StringUtils.transString(ticket.getName()));
			_mMap.put("content", StringUtils.transString(ticket.getContent()));
			_mMap.put("address", StringUtils.transString(ticket.getAddress()));
			_mMap.put("imgPath", StringUtils.transString(ticket.getImgPath()));
			// 去掉不必要的字段影响
			List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
			for (DataTicketDetail dataTicketDetail : findAll) {
				Map<String, Object> _mMap1 = new HashMap<String, Object>();
				_mMap1.put("ticketName", StringUtils.transString(dataTicketDetail.getName()));
				_mMap1.put("ticketContent", StringUtils.transString(dataTicketDetail.getContent()));
				_mMap1.put("price", StringUtils.transString(dataTicketDetail.getPrice()));
				list.add(_mMap1);
			}
			_mMap.put("children", list);
			_mMap.put("state", 1);
		} else {
			_mMap.put("state", 0);
		}
		return _mMap;
	}

}
