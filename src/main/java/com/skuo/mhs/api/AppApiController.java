package com.skuo.mhs.api;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.skuo.mhs.data.news.impl.CmsChannelServiceImpl;
import com.skuo.mhs.data.news.pojo.CmsChannel;
import com.skuo.mhs.data.news.pojo.CmsNews;
import com.skuo.mhs.data.news.service.CmsChannelRepository;
import com.skuo.mhs.data.news.service.CmsNewsRepository;
import com.skuo.mhs.utils.StringUtils;

/**
 * @ClassName: UnderstandApiController
 * @Description: 初识梅花山控制器
 * @Company: 四川西谷智慧科技有限公司
 * @author: YanHiroShi
 * @date: 2017/9/12 16:14
 * @version: V1.0.0
 */
@Controller
@RequestMapping(value = "/api-getInfo")
public class AppApiController {
	@Autowired
	private CmsChannelRepository channelRepository;

	@Autowired
	private CmsChannelServiceImpl channelService;

	@Autowired
	private CmsNewsRepository newsRepository;

	/**
	 * @DATE: 2017/9/12-16:27.
	 * @Description:根据code获取不同的信息(APP)
	 */
	@RequestMapping(value = "/menu/{code}")
	@ResponseBody
	public Object getInfo(@PathVariable String code) {
		CmsChannel channel = channelRepository.findOne(code);
		Map<String, Object> map = new HashMap<>();
		if (channel != null) {
			map = channelService.findByParentForTitle(channel);
			map.put("state", 1);
		} else {
			map.put("state", 0);
		}
		return map;
	}

	/**
	 * @DATE: 2017/9/12-17:21.
	 * @Description:根据channelID查询news
	 */
	@RequestMapping(value = "/detail/{code}")
	@ResponseBody
	public Object getOneInfo(@PathVariable String code) {
		CmsNews data = newsRepository.findByChannelId(code);
		Map<String, Object> _mMap = new HashMap<String, Object>();
		if (data != null) {
			// _mMap.put("id", data.getId());
			// _mMap.put("title", StringUtils.transString(data.getTitle()));
			// _mMap.put("subTitle",
			// StringUtils.transString(data.getSubTitle()));
			String content = data.getContent();
			if (content != null && content.indexOf("<img src=\"/") != -1) {
				content = content.replaceAll("<img src=\"/", "<img src=\"http://174677bk23.iask.in:14151/");
			}
			_mMap.put("content", StringUtils.transString(content));
			_mMap.put("state", 1);
			// _mMap.put("channelId",
			// StringUtils.transString(data.getChannelId()));
			// _mMap.put("imgPath", StringUtils.transString(data.getImgPath()));
			// SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd
			// HH:mm:ss");
			// _mMap.put("addDate", data.getAddDate() == null ? sdf.format(new
			// Date()) : sdf.format(data.getAddDate()));
			// _mMap.put("remark", StringUtils.transString(data.getRemark()));
		} else {
			_mMap.put("state", 0);
		}
		return _mMap;
	}

}
