package com.skuo.mhs.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.skuo.mhs.data.res.pojo.DataGift;
import com.skuo.mhs.data.res.service.DataGiftRepository;
import com.skuo.mhs.form.PageForm;
import com.skuo.mhs.utils.PaginationUtils;
import com.skuo.mhs.utils.StringUtils;

/**
 * 
 * @ClassName: GiftApiController
 * @Description: 伴手礼对外接口
 * @Company: 四川西谷智慧科技有限公司
 * @author: songhailing
 * @date: 2017年9月12日 下午2:28:27
 * @version: V1.0.0
 */
@Controller
@RequestMapping(value = "/gift-api")
public class GiftApiController {

	@Autowired
	private DataGiftRepository dataGiftRepository;


	/**
	 * @Description: 读取景区景点列表
	 * @param pageForm
	 * @param dto
	 * @return:
	 * @date: 2017年9月4日 下午14:18:26
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/listAll", method = RequestMethod.GET)
	@ResponseBody
	public Object list1(PageForm pageForm, DataGift dto, HttpServletRequest request) {
		List<DataGift> list = dataGiftRepository.findByState(1L);
		list = (List<DataGift>) PaginationUtils.pagination(list, pageForm.getOffset(), pageForm.getLimit());
		Map<String, Object> map = new HashMap<String, Object>();
		List<Map<String, Object>> _list = new ArrayList<Map<String, Object>>();
		for (DataGift data : list) {
			Map<String, Object> _mMap = new HashMap<String, Object>();
			_mMap.put("id", data.getId());
			_mMap.put("name", StringUtils.transString(data.getName()));
			_mMap.put("summary", StringUtils.transString(data.getSummary()));
			_mMap.put("imgPath", StringUtils.transString(data.getImgPath()));
			_mMap.put("cost", StringUtils.transString(data.getCost()));
			String type = "";
			
			if ("GiftType2".equals(data.getGiftType())) {
				type = "类别三";
			}else if ("GiftType1".equals(data.getGiftType())) {
				type = "类别二";
			} else {
				type = "类别一";
			}
			_mMap.put("typeName", type);
			_mMap.put("giftType", data.getGiftType());
			_list.add(_mMap);
		}
		map.put("rows", _list);
		map.put("total", list.size());
		if (list.size() == 0) {
			map.put("state", 0);
		} else {
			map.put("state", 1);
		}
		return map;
	}

	/**
	 * @Description: 读取景区景点详情
	 * @param pageForm
	 * @param dto
	 * @return:
	 * @date: 2017年9月4日 下午14:18:26
	 */
	@RequestMapping(value = "/findOne", method = RequestMethod.GET)
	@ResponseBody
	public Object findOne(Long id, HttpServletRequest request) {
		// pageForm.setLimit(10);
		DataGift gift = dataGiftRepository.findOne(id);
		/*
		 * List<MediaPicture> findAll = mediaPictureRepository.findAll();
		 * List<String> pictures = new ArrayList<String>();
		 */
		Map<String, Object> _mMap = new HashMap<String, Object>();
		if (gift != null && gift.getState() != 0L) {
			_mMap.put("id", gift.getId());
			_mMap.put("name", StringUtils.transString(gift.getName()));
			_mMap.put("content", StringUtils.transString(gift.getContent()));
			_mMap.put("imgPath", StringUtils.transString(gift.getImgPath()));
			/*
			 * for (MediaPicture picture : findAll) { if ((gift.getId() +
			 * "").equals(picture.getResourceId())) {
			 * pictures.add(StringUtils.transString(picture.getResourceId())); }
			 * } _mMap.put("pictures", pictures);
			 */
			_mMap.put("state", 1);
		} else {
			_mMap.put("state", 0);
		}
		return _mMap;
	}

}
