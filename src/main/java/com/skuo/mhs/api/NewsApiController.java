package com.skuo.mhs.api;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.skuo.mhs.data.news.impl.CmsChannelServiceImpl;
import com.skuo.mhs.data.news.pojo.CmsChannel;
import com.skuo.mhs.data.news.pojo.CmsNews;
import com.skuo.mhs.data.news.service.CmsChannelRepository;
import com.skuo.mhs.data.news.service.CmsNewsRepository;
import com.skuo.mhs.form.PageForm;
import com.skuo.mhs.utils.PaginationUtils;
import com.skuo.mhs.utils.StringUtils;

/**
 * @ClassName: NewsApiController
 * @Description: 前端新闻API控制器 w@Company: 四川西谷智慧科技有限公司
 * @author: YanHiroShi
 * @date: 2017/9/11 15:48
 * @version: V1.0.0
 */
@Controller
@RequestMapping(value = "/api-news")
public class NewsApiController {
	@Autowired
	private CmsChannelServiceImpl channelService;

	@Autowired
	private CmsChannelRepository cmsChannelRepository;

	@Autowired
	private CmsNewsRepository cmsNewsRepository;

	
	public  static final String  CHANNEL_ID="0004000000000000";
	
	
	/**
	 * @DATE: 2017/9/11-16:39.
	 * @Description:父类下的子菜单
	 */
	@RequestMapping(value = "/second_level")
	@ResponseBody
	public Object second_level(String id, HttpServletRequest request, String jsoncallback, HttpServletResponse response)
			throws IOException {
		CmsChannel channel = cmsChannelRepository.findOne(id);
		Map<String, Object> map = channelService.findByParent(channel);
		response.setContentType("charset=UTF-8;text/json");
		response.setCharacterEncoding("UTF-8");
		response.setHeader("Access-Control-Allow-Origin", "*");
		JSONObject obj = new JSONObject();
		obj.put("data", map);
		return obj;
	}

	/**
	 * @throws IOException
	 * @DATE: 2017/9/11-16:41.
	 * @Description:查询新闻
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/getNews")
	@ResponseBody
	public Object getNews(PageForm pageForm,  HttpServletResponse response) throws IOException {
		List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
		// Long type = 1L;
		Date date = new Date();
		long time = date.getTime();
		Date yesterday = new Date(time - 1000 * 3600 * 24);
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
		String format = sdf1.format(yesterday);
		List<CmsNews> newsList = new ArrayList<CmsNews>();
			// 如果没有媒体类型，默认查全量
		newsList = cmsNewsRepository.findNews(CHANNEL_ID);
		

		for (CmsNews apiNews : newsList) {
			Map<String, Object> map = new HashMap<String, Object>();
			// 时间过滤
			// if (apiNews.getAddDate() != null &&
			// format.compareTo((sdf1.format(apiNews.getAddDate()))) <= 0) {
			map.put("id", apiNews.getId());
			map.put("title", StringUtils.transString(apiNews.getTitle()));
			// map.put("subTitle",
			// StringUtils.transString(apiNews.getSubTitle()));
			map.put("imgPath", StringUtils.transString(apiNews.getImgPath()));
			// map.put("channelId",
			// StringUtils.transString(apiNews.getChannelId()));
			// map.put("content",
			// StringUtils.transString(apiNews.getContent()));
			map.put("remark", StringUtils.transString(apiNews.getRemark()));
			// map.put("checkNum", apiNews.getCheckNum());
			// map.put("followNum", apiNews.getFollowNum());
			// map.put("shareNum", apiNews.getShareNum());
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			map.put("addDate", apiNews.getAddDate() == null ? "" : sdf.format(apiNews.getAddDate()));

			String newsTypeName = "";
			if ("newsType6".equals(apiNews.getNewsType())) {
				newsTypeName = "初识栏目";
			} else if ("newsType5".equals(apiNews.getNewsType())) {
				newsTypeName = "App底部广告";
			} else if ("newsType4".equals(apiNews.getNewsType())) {
				newsTypeName = "App中部广告";
			} else if ("newsType3".equals(apiNews.getNewsType())) {
				newsTypeName = "App顶部广告";
			} else if ("newsType2".equals(apiNews.getNewsType())) {
				newsTypeName = "企业文化";
			} else if ("newsType1".equals(apiNews.getNewsType())) {
				newsTypeName = "园区公告";
			} else {
				newsTypeName = "景区动态";
			}
			map.put("newsType", apiNews.getNewsType());
			map.put("newsTypeName", newsTypeName);
			String source = "";
			if ("NewsSource3".equals(apiNews.getSource())) {
				source = "腾讯新闻";
			} else if ("NewsSource2".equals(apiNews.getSource())) {
				source = "新浪微博";
			} else if ("NewsSource1".equals(apiNews.getSource())) {
				source = "凤凰网";
			} else {
				source = "新华社";
			}
			map.put("source", source);
			mapList.add(map);
			// }
		}
		int size = mapList.size();
		mapList = (List<Map<String, Object>>) PaginationUtils.pagination(mapList, pageForm.getOffset(),
				pageForm.getLimit());

		response.setContentType("charset=UTF-8;text/json");
		response.setCharacterEncoding("UTF-8");
		response.setHeader("Access-Control-Allow-Origin", "*");
		JSONObject obj = new JSONObject();
		obj.put("data", mapList);
		obj.put("total", size);
		if (newsList.size() == 0) {
			obj.put("state", 0);
		} else {
			obj.put("state", 1);
		}

		return obj;
	}

	/**
	 * @throws IOException
	 * @DATE: 2017/9/11-16:41.
	 * @Description:查询新闻
	 */
	@RequestMapping(value = "/findOne", method = RequestMethod.GET)
	@ResponseBody
	public Object findOne(Long id) throws IOException {
		CmsNews cmsNews = cmsNewsRepository.findOne(id);
		Map<String, Object> map = new HashMap<String, Object>();
		if (cmsNews != null && cmsNews.getState() != 0L) {
			map.put("id", cmsNews.getId());
			map.put("title", StringUtils.transString(cmsNews.getTitle()));
			map.put("imgPath", StringUtils.transString(cmsNews.getImgPath()));
			map.put("url", StringUtils.transString(cmsNews.getUrl()));
			String content = cmsNews.getContent();
			if (content != null && content.indexOf("<img src=\"/") != -1) {
				content = content.replaceAll("<img src=\"/", "<img src=\"http://174677bk23.iask.in:14151/");
			}
			map.put("content", StringUtils.transString(content));

			map.put("state", 1);
		} else {
			map.put("state", 0);
		}
		return map;
	}

	/**
	 * @Description: 获取首页的数据
	 * @param pageForm
	 * @param newsType
	 * @param response
	 * @return
	 * @throws IOException
	 * @date: 2017年9月23日 下午5:57:25
	 */
	@RequestMapping(value = "/getMainNews")
	@ResponseBody
	public Object getMainNews(HttpServletResponse response) throws IOException {
		List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();

		// 顶部
		List<Map<String, Object>> topList = new ArrayList<Map<String, Object>>();
		// 中部
		List<Map<String, Object>> middleList = new ArrayList<Map<String, Object>>();
		// 底部
		List<Map<String, Object>> footList = new ArrayList<Map<String, Object>>();

		List<CmsNews> newsList = cmsNewsRepository.findNews(CHANNEL_ID);
		for (CmsNews apiNews : newsList) {
			Map<String, Object> map = new HashMap<String, Object>();
			map = transToMap(apiNews);
			// 时间过滤
			// if (apiNews.getAddDate() != null &&
			// format.compareTo((sdf1.format(apiNews.getAddDate()))) <= 0) {

			mapList.add(map);
			// }
		}

		for (Map<String, Object> map : mapList) {
			if ("NewsType5".equals((String) map.get("newsType"))) {
				topList.add(map);
			} else if ("NewsType4".equals((String) map.get("newsType"))) {
				middleList.add(map);
			} else if ("NewsType3".equals((String) map.get("newsType"))) {
				footList.add(map);
			}
		}
		response.setContentType("charset=UTF-8;text/json");
		response.setCharacterEncoding("UTF-8");
		response.setHeader("Access-Control-Allow-Origin", "*");
		JSONObject obj = new JSONObject();
		obj.put("top", topList);
		obj.put("middle", middleList);
		obj.put("foot", footList);
		if (topList.size() == 0 && middleList.size() == 0 && footList.size() == 0) {
			obj.put("state", 0);
		} else {
			obj.put("state", 1);
		}
		return obj;
	}

	/**
	 * 
	 * @Description: 转换封装前端返回结果
	 * @param apiNews
	 * @return
	 * @date: 2017年9月23日 下午6:11:56
	 */
	private Map<String, Object> transToMap(CmsNews apiNews) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", apiNews.getId());
		map.put("title", StringUtils.transString(apiNews.getTitle()));
		// map.put("subTitle",
		// StringUtils.transString(apiNews.getSubTitle()));
		map.put("imgPath", StringUtils.transString(apiNews.getImgPath()));
		// map.put("channelId",
		// StringUtils.transString(apiNews.getChannelId()));
		// map.put("content",
		// StringUtils.transString(apiNews.getContent()));
		map.put("remark", StringUtils.transString(apiNews.getRemark()));
		// map.put("checkNum", apiNews.getCheckNum());
		// map.put("followNum", apiNews.getFollowNum());
		// map.put("shareNum", apiNews.getShareNum());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		map.put("addDate", apiNews.getAddDate() == null ? "" : sdf.format(apiNews.getAddDate()));

		String newsTypeName = "";
		if ("NewsType6".equals(apiNews.getNewsType())) {
			newsTypeName = "初识栏目";
		} else if ("NewsType5".equals(apiNews.getNewsType())) {
			newsTypeName = "App底部广告";
		} else if ("NewsType4".equals(apiNews.getNewsType())) {
			newsTypeName = "App中部广告";
		} else if ("NewsType3".equals(apiNews.getNewsType())) {
			newsTypeName = "App顶部广告";
		} else if ("NewsType2".equals(apiNews.getNewsType())) {
			newsTypeName = "企业文化";
		} else if ("NewsType1".equals(apiNews.getNewsType())) {
			newsTypeName = "园区公告";
		} else {
			newsTypeName = "景区动态";
		}
		map.put("newsType", apiNews.getNewsType());
		map.put("newsTypeName", newsTypeName);
		String source = "";
		if ("NewsSource3".equals(apiNews.getSource())) {
			source = "腾讯新闻";
		} else if ("NewsSource2".equals(apiNews.getSource())) {
			source = "新浪微博";
		} else if ("NewsSource1".equals(apiNews.getSource())) {
			source = "凤凰网";
		} else {
			source = "新华社";
		}
		map.put("source", source);
		return map;
	}

}
