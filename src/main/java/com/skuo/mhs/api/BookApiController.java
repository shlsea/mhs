package com.skuo.mhs.api;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 
 * @ClassName: BookApiController
 * @Description: 景区对外接口
 * @Company: 四川西谷智慧科技有限公司
 * @author: songhailing
 * @date: 2017年9月12日 下午2:28:27
 * @version: V1.0.0
 */
@Controller
@RequestMapping(value = "/book-api")
public class BookApiController {
	/**
	 * 系统路径
	 */
	public static final String URL = "http://174677bk23.iask.in:14151";

	/**
	 * @Description: 宝典获取路径
	 * @param pageForm
	 * @param dto
	 * @return:
	 * @date: 2017年9月4日 下午14:18:26
	 */
	@RequestMapping(value = "/getUrl", method = RequestMethod.GET)
	@ResponseBody
	public Object getUrl() {
		// pageForm.setLimit(10);
		String path = "/travel.html";
		Map<String, Object> map = new HashMap<String, Object>();
		System.out.println("path:" + URL + path);
		map.put("state", 1);
		map.put("path", URL + path);

		return map;
	}
	/**
	 * @Description: 主题视频获取路径
	 * @param pageForm
	 * @param dto
	 * @return:
	 * @date: 2017年9月4日 下午14:18:26
	 */
	@RequestMapping(value = "/getThemeVideo", method = RequestMethod.GET)
	@ResponseBody
	public Object getThemeVideo() {
		// pageForm.setLimit(10);
		String path = "/upload/video/2017/09/24/1506182986395.mp4";
		Map<String, Object> map = new HashMap<String, Object>();
		System.out.println("path:" + URL + path);
		map.put("state", 1);
		map.put("path", URL + path);
		
		return map;
	}

}
