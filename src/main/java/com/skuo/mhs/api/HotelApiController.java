package com.skuo.mhs.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.skuo.mhs.data.res.pojo.DataHotel;
import com.skuo.mhs.data.res.pojo.DataRoom;
import com.skuo.mhs.data.res.service.DataHotelRepository;
import com.skuo.mhs.data.res.service.DataRoomRepository;
import com.skuo.mhs.form.PageForm;
import com.skuo.mhs.media.service.MediaPictureRepository;
import com.skuo.mhs.utils.PaginationUtils;
import com.skuo.mhs.utils.StringUtils;

/**
 * 
 * @ClassName: HotelApiController
 * @Description: 酒店对外接口
 * @Company: 四川西谷智慧科技有限公司
 * @author: songhailing
 * @date: 2017年9月12日 下午2:28:27
 * @version: V1.0.0
 */
@Controller
@RequestMapping(value = "/hotel-api")
public class HotelApiController {

	@Autowired
	private DataHotelRepository dataHotelRepository;

	@Autowired
	private DataRoomRepository dataRoomRepository;

	@Autowired
	private MediaPictureRepository mediaPictureRepository;

	/**
	 * @Description: 读取酒店列表
	 * @param pageForm
	 * @param dto
	 * @return:
	 * @date: 2017年9月4日 下午14:18:26
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@ResponseBody
	public Object list1(PageForm pageForm, DataHotel dto, HttpServletRequest request) {
		List<DataHotel> list = dataHotelRepository.findByState(1L);
		list = (List<DataHotel>) PaginationUtils.pagination(list, pageForm.getOffset(), pageForm.getLimit());
		/*
		 * List<MediaPicture> findAll = mediaPictureRepository.findAll();
		 * List<String> pictures = new ArrayList<String>();
		 */
		Map<String, Object> map = new HashMap<String, Object>();
		List<Map<String, Object>> _list = new ArrayList<Map<String, Object>>();
		for (DataHotel data : list) {
			Map<String, Object> _mMap = new HashMap<String, Object>();
			_mMap.put("id", data.getId());
			_mMap.put("name", StringUtils.transString(data.getName()));
			_mMap.put("summary", StringUtils.transString(data.getSummary()));
			_mMap.put("imgPath", StringUtils.transString(data.getImgPath()));
			_mMap.put("cost", StringUtils.transString(data.getCost()));
			/*
			 * String type = ""; Long type2 = data.getType(); if(type2==2){
			 * type="主题酒店"; }else{ type="商务酒店"; } _mMap.put("type", type); for
			 * (MediaPicture picture : findAll) { if ((data.getId() +
			 * "").equals(picture.getResourceId())) {
			 * pictures.add(StringUtils.transString(picture.getResourceId())); }
			 * } _mMap.put("pictures", pictures);
			 */
			_list.add(_mMap);
		}
		map.put("rows", _list);
		map.put("total", list.size());
		if (list.size() == 0) {
			map.put("state", 0);
		} else {
			map.put("state", 1);
		}
		return map;
	}

	/**
	 * @Description: 获取一个酒店的所有房间信息
	 * @param pageForm
	 * @param dto
	 * @return:
	 * @date: 2017年9月4日 下午14:18:26
	 */
	@RequestMapping(value = "/findRooms", method = RequestMethod.GET)
	@ResponseBody
	public Object findRooms(Long id, HttpServletRequest request) {
		// pageForm.setLimit(10);
		DataHotel hotel = dataHotelRepository.findOne(id);

		/*
		 * List<MediaPicture> findAll = mediaPictureRepository.findAll();
		 * List<String> pictures = new ArrayList<String>();
		 */
		List<DataRoom> rooms = dataRoomRepository.findByBelongHotelAndState(id, 1L);
		Map<String, Object> _mMap = new HashMap<String, Object>();
		if (hotel != null && hotel.getState() != 0L) {
			_mMap.put("id", hotel.getId());
			_mMap.put("hotelName", StringUtils.transString(hotel.getName()));
			_mMap.put("address", StringUtils.transString(hotel.getAddress()));
			_mMap.put("hotelContent", StringUtils.transString(hotel.getContent()));
			_mMap.put("imgPath", StringUtils.transString(hotel.getImgPath()));
			// 去掉不必要的字段影响
			List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
			for (DataRoom room : rooms) {
				Map<String, Object> _mMap1 = new HashMap<String, Object>();
				_mMap1.put("id", room.getId());
				_mMap1.put("roomName", StringUtils.transString(room.getRoomName()));
				_mMap1.put("summary", StringUtils.transString(room.getSummary()));
				_mMap1.put("roomImgPath", StringUtils.transString(room.getImgPath()));
				_mMap1.put("price", StringUtils.transString(room.getCost()));

				String type = "";
				if ("RoomType2".equals(room.getRoomType())) {
					type = "豪华套房";
				} else if ("RoomType1".equals(room.getRoomType())) {
					type = "双人间";
				} else {
					type = "单人间";
				}
				_mMap1.put("type", type);
				list.add(_mMap1);
			}
			/*
			 * for (MediaPicture picture : findAll) { if ((hotel.getId() +
			 * "").equals(picture.getResourceId())) {
			 * pictures.add(StringUtils.transString(picture.getResourceId())); }
			 * } _mMap.put("pictures", pictures);
			 */
			_mMap.put("children", list);
			_mMap.put("state", 1);
		} else {
			_mMap.put("state", 0);

		}
		return _mMap;
	}

	/**
	 * @Description: 读取单个房间信息
	 * @param pageForm
	 * @param dto
	 * @return:
	 * @date: 2017年9月4日 下午14:18:26
	 */
	@RequestMapping(value = "/findOne", method = RequestMethod.GET)
	@ResponseBody
	public Object findOne(Long id, HttpServletRequest request) {
		// pageForm.setLimit(10);
		DataRoom room = dataRoomRepository.findOne(id);
		Map<String, Object> _mMap = new HashMap<String, Object>();
		if (room != null && room.getState() != 0L) {
			_mMap.put("id", room.getId());
			_mMap.put("service", StringUtils.transString(room.getService()));
			_mMap.put("content", StringUtils.transString(room.getContent()));
			_mMap.put("imgPath", StringUtils.transString(room.getImgPath()));
			_mMap.put("state", 1);
		} else {
			_mMap.put("state", 0);

		}
		return _mMap;
	}
}
