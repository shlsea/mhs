package com.skuo.mhs.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.skuo.mhs.form.PageForm;
import com.skuo.mhs.trip.pojo.CmsTrip;
import com.skuo.mhs.trip.pojo.CmsTripDay;
import com.skuo.mhs.trip.pojo.CmsTripSource;
import com.skuo.mhs.trip.pojo.CmsTripTraffic;
import com.skuo.mhs.trip.service.CmsTripDayRepository;
import com.skuo.mhs.trip.service.CmsTripRepository;
import com.skuo.mhs.trip.service.CmsTripSourceRepository;
import com.skuo.mhs.trip.service.CmsTripTrafficRepository;
import com.skuo.mhs.utils.PaginationUtils;
import com.skuo.mhs.utils.StringUtils;

/**
 * @ClassName: TripApiController
 * @Description: 行程对外API
 * @Company: 四川西谷智慧科技有限公司
 * @author: YanHiroShi
 * @date: 2017/9/13 10:23
 * @version: V1.0.0
 */
@Controller
@RequestMapping(value = "/trip-api")
public class TripApiController {
	@Autowired
	private CmsTripRepository tripRepository;
	@Autowired
	private CmsTripDayRepository tripDayRepository;
	@Autowired
	private CmsTripSourceRepository tripSourceRepository;
	@Autowired
	private CmsTripTrafficRepository tripTrafficRepository;

	/**
	 * @DATE: 2017/9/13-10:38.
	 * @Description:获取所有行程信息(分页)
	 */
	@ResponseBody
	@RequestMapping(value = "/listTrip-page", method = RequestMethod.GET)
	public Object listAllTrip(PageForm pageForm, CmsTrip dto, HttpServletRequest request) {
		Map<String, Object> tripMap = tripRepository.findByAuto(dto, pageForm);
		Map<String, Object> map = new HashMap<>();
		List<CmsTrip> tripList = (List<CmsTrip>) tripMap.get("rows");
		List<Map<String, Object>> _list = new ArrayList<>();
		for (CmsTrip item : tripList) {
			Map<String, Object> _map = new HashMap<>();
			_map.put("id", item.getId());
			_map.put("name", StringUtils.transString(item.getName()));
			_map.put("days", item.getDays());
			_map.put("startAddress", StringUtils.transString(item.getStartAddress()));
			_map.put("endAddress", StringUtils.transString(item.getEndAddress()));
			_map.put("content", StringUtils.transString(item.getContent()));

			List<Map<String, Object>> _tripDayList = new ArrayList<>();
			List<CmsTripDay> tripDayList = tripDayRepository.findByTripId(item.getId());
			for (CmsTripDay element : tripDayList) {
				Map<String, Object> tripDetailMap = new HashMap<>();
				tripDetailMap.put("id", element.getId());
				tripDetailMap.put("day", element.getDay());
				tripDetailMap.put("destination", StringUtils.transString(element.getDestination()));
				tripDetailMap.put("remark", StringUtils.transString(element.getRemark()));
				_tripDayList.add(tripDetailMap);
				List<CmsTripSource> tripSourceList = tripSourceRepository.findByDayId(element.getId());
				List<Map<String, Object>> _tripSourceList = new ArrayList<>();
				for (CmsTripSource n : tripSourceList) {
					Map<String, Object> tripSourceMap = new HashMap<>();
					tripSourceMap.put("id", n.getId());
					tripSourceMap.put("dataId", StringUtils.transString(n.getDataId()));
					tripSourceMap.put("imgPath", StringUtils.transString(n.getImgPath()));
					tripSourceMap.put("name", StringUtils.transString(n.getName()));
					tripSourceMap.put("regionName", StringUtils.transString(n.getRegionName()));
					tripSourceMap.put("remark", StringUtils.transString(n.getRemark()));
					tripSourceMap.put("tag", StringUtils.transString(n.getTag()));
					List<CmsTripTraffic> tripTrafficList = n.getTraffic();
					List<Map<String, Object>> _tripTrafficList = new ArrayList<>();
					for (CmsTripTraffic m : tripTrafficList) {
						Map<String, Object> tripTrafficMap = new HashMap<String, Object>();
						if (m != null) {
							tripTrafficMap.put("way", m.getWay());
							tripTrafficMap.put("fromArea", StringUtils.transString(m.getFromArea()));
							tripTrafficMap.put("toArea", StringUtils.transString(m.getToArea()));
							tripTrafficMap.put("trafficSequence", StringUtils.transString(m.getTrafficSequence()));
							tripTrafficMap.put("sdateh", m.getSdateh());
							tripTrafficMap.put("sdatem", m.getSdatem());
							tripTrafficMap.put("edateh", m.getEdateh());
							tripTrafficMap.put("edatem", m.getEdatem());
							tripTrafficMap.put("edays", m.getEdays());
							_tripTrafficList.add(tripTrafficMap);
						}
					}
					tripSourceMap.put("tripTraffic", _tripTrafficList);
					_tripSourceList.add(tripSourceMap);
				}
				tripDetailMap.put("tripSource", _tripSourceList);
			}
			_map.put("tripDays", _tripDayList);
			_list.add(_map);
		}

		map.put("total", tripMap.get("total"));
		map.put("data", _list);
		return map;
	}

	/**
	 * @Description:分页查询行程线路
	 * @param pageForm
	 * @param dto
	 * @param request
	 * @return
	 * @date 2017年9月20日上午11:04:46
	 */
	@SuppressWarnings("unchecked")
	@ResponseBody
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public Object list(PageForm pageForm, CmsTrip dto, HttpServletRequest request) {
		// 只获取第一层的图片路径、线路亮点、第四次层的线路行程点
		List<CmsTrip> tripList = tripRepository.findByState(1L);
		tripList = (List<CmsTrip>) PaginationUtils.pagination(tripList, pageForm.getOffset(), pageForm.getLimit());
		Map<String, Object> map = new HashMap<>();
		List<Map<String, Object>> _list = new ArrayList<>();
		for (CmsTrip item : tripList) {
			Map<String, Object> _map = new HashMap<>();
			_map.put("id", item.getId());
			_map.put("name", StringUtils.transString(item.getName()));
			_map.put("days", item.getDays());
			_map.put("imgPath", StringUtils.transString(item.getImgPath()));
			_map.put("remark", StringUtils.transString(item.getRemark()));
			// 所有目的地的总集合
			List<String> tripTargets = new ArrayList<String>();
			List<CmsTripDay> tripDayList = tripDayRepository.findByTripId(item.getId());
			for (CmsTripDay element : tripDayList) {
				List<CmsTripSource> tripSourceList = tripSourceRepository.findByDayId(element.getId());
				for (CmsTripSource n : tripSourceList) {
					tripTargets.add(StringUtils.transString(n.getRegionName()));
				}
			}
			// 总集合中的地名数量不要超过3个
			if (tripTargets.size() > 3) {
				tripTargets = tripTargets.subList(0, 3);
			}
			_map.put("tripTargets", tripTargets);
			_list.add(_map);
		}

		map.put("total", tripList.size());
		map.put("data", _list);
		if (tripList.size() == 0) {
			map.put("state", 0);
		} else {
			map.put("state", 1);
		}
		return map;
	}

	/**
	 * @Description:查询单条记录
	 * @param pageForm
	 * @param dto
	 * @param request
	 * @return
	 * @date 2017年9月20日上午11:04:46
	 */
	@ResponseBody
	@RequestMapping(value = "/findOne", method = RequestMethod.GET)
	public Object findOne(Long id, HttpServletRequest request) {
		// 只获取第一层的图片路径、线路亮点、第四次层的线路行程点

		CmsTrip item = tripRepository.findOne(id);
		Map<String, Object> _map = new HashMap<>();
		if (item != null && item.getState() != 0L) {
			_map.put("id", item.getId());
			_map.put("name", StringUtils.transString(item.getName()));
			_map.put("days", item.getDays());
			_map.put("imgPath", StringUtils.transString(item.getImgPath()));
			_map.put("content", StringUtils.transString(item.getContent()));
			// 所有目的地的总集合
			List<Map<String, Object>> tripTargets = new ArrayList<Map<String, Object>>();
			List<CmsTripDay> tripDayList = tripDayRepository.findByTripId(item.getId());
			for (CmsTripDay element : tripDayList) {
				List<CmsTripSource> tripSourceList = tripSourceRepository.findByDayId(element.getId());
				for (CmsTripSource n : tripSourceList) {
					Map<String, Object> tripSourceMap = new HashMap<>();
					tripSourceMap.put("id", n.getId());
					// tripSourceMap.put("dataId",
					// StringUtils.transString(n.getDataId()));
					tripSourceMap.put("scenicImgPath", StringUtils.transString(n.getImgPath()));
					// tripSourceMap.put("name",
					// StringUtils.transString(n.getName()));
					tripSourceMap.put("regionName", n.getRegionName() == null ? n.getName() : n.getRegionName());
					// tripSourceMap.put("remark",
					// StringUtils.transString(n.getRemark()));
					tripSourceMap.put("scenicContent", StringUtils.transString(n.getContent()));
					// tripSourceMap.put("tag",
					// StringUtils.transString(n.getTag()));
					tripTargets.add(tripSourceMap);
				}
			}
			_map.put("tripTargets", tripTargets);
			_map.put("state", 1);
		} else {
			_map.put("state", 0);
		}
		return _map;
	}

}
