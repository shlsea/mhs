package com.skuo.mhs.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.skuo.mhs.data.res.pojo.DataGuide;
import com.skuo.mhs.data.res.service.DataGuideRepository;
import com.skuo.mhs.form.PageForm;
import com.skuo.mhs.utils.PaginationUtils;
import com.skuo.mhs.utils.StringUtils;

/**
 * 
 * @ClassName: GuideApiController
 * @Description: 景区对外接口
 * @Company: 四川西谷智慧科技有限公司
 * @author: songhailing
 * @date: 2017年9月12日 下午2:28:27
 * @version: V1.0.0
 */
@Controller
@RequestMapping(value = "/guide-api")
public class GuideApiController {

	@Autowired
	private DataGuideRepository dataGuideRepository;

	/**
	 * @Description: 读取景区景点列表
	 * @param pageForm
	 * @param dto
	 * @return:
	 * @date: 2017年9月4日 下午14:18:26
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/listAll", method = RequestMethod.GET)
	@ResponseBody
	public Object list1(PageForm pageForm, DataGuide dto, HttpServletRequest request) {
		List<DataGuide> list = dataGuideRepository.findByState(1L);
		list = (List<DataGuide>) PaginationUtils.pagination(list, pageForm.getOffset(), pageForm.getLimit());
		Map<String, Object> map = new HashMap<String, Object>();
		List<Map<String, Object>> _list = new ArrayList<Map<String, Object>>();
		for (DataGuide data : list) {
			Map<String, Object> _mMap = new HashMap<String, Object>();
			_mMap.put("id", data.getId());
			_mMap.put("name", StringUtils.transString(data.getName()));
			_mMap.put("sex", data.getSex() == 1 ? "男" : "女");

			String guideLevel = "";
			if ("GuideLevel3".equals(data.getGuideLevel())) {
				guideLevel = "特级";
			} else if ("GuideLevel2".equals(data.getGuideLevel())) {
				guideLevel = "高级";
			} else if ("GuideLevel1".equals(data.getGuideLevel())) {
				guideLevel = "中级";
			} else {
				guideLevel = "初级";
			}
			_mMap.put("guideLevel", guideLevel);
			String languageType = "";
			if ("Language5".equals(data.getLanguageType())) {
				languageType = "德语";
			} else if ("Language4".equals(data.getLanguageType())) {
				languageType = "法语";
			} else if ("Language3".equals(data.getLanguageType())) {
				languageType = "韩语";
			} else if ("Language2".equals(data.getLanguageType())) {
				languageType = "日语";
			} else if ("Language1".equals(data.getLanguageType())) {
				languageType = "普通话";
			} else {
				languageType = "英语";
			}
			_mMap.put("languageType", languageType);
			_mMap.put("guideCode", StringUtils.transString(data.getGuideCode()));
			// _mMap.put("belongTo",StringUtils.transString(data.getBelongTo()));
			_mMap.put("imgPath", StringUtils.transString(data.getImgPath()));
			_mMap.put("imgPath", StringUtils.transString(data.getImgPath()));

			_list.add(_mMap);
		}
		map.put("rows", _list);
		map.put("total", list.size());
		if (list.size() == 0) {
			map.put("state", 0);
		} else {
			map.put("state", 1);
		}
		return map;
	}

}
