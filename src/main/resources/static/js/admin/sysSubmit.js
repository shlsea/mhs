$(document).ready( function() {
	
	//下拉框改变值的时候验证
	$(".selectpicker-validate").change(function(){
		var value = $(this).val();
		var message = $(this).attr("message");
		if (message == undefined || message == '') {
			message = '必须选择该项！';
		}
		if (value == ''||value=='null') {
			$(this).parent().find("button").css({'border-color': '#ffa8a8', 'background-color': '#fff3f3'});
			$(this).parent().tooltip({
				position: 'right',    
				content: '<span style="color:red;font-weight: bold; font-size: 14px;font-family: 微软雅黑">'+ message +'</span>',
			});
		} else {
			$(this).parent().find("button").css({'border-color': '#ccc', 'background-color': '#fff'});
			try {
				$(this).parent().tooltip("destroy");
			}catch (e) {
				
			}
		}
	});
	
	//进入页面先不提示验证，鼠标点进与移除后进行验证
	$('.easyui-validatebox').validatebox('disableValidation')
    .focus(function () { $(this).validatebox('enableValidation'); })
    .blur(function () { $(this).validatebox('validate');});
	//初始化表单验证
	/* $("#validatorForm").bootstrapValidator({submitHandler: function(validator, form, submitButton) {  
	        validator.defaultSubmit();  
	 	}
	 });*/
	//提交表单
	$("#submitButton").click(function(){
		//提交表单时开启文本框验证
		$('.easyui-validatebox').validatebox('enableValidation');
		var flag = validateSelect();
		var $form=$("#validatorForm");
		if($form.form('validate') && flag){
			//防止点击提交进行多次保存
			layer.load(0, {shade: [0.5,'#333']});
			$form.submit();
		}
	});

	// 页面返回按钮
	$("#returnButton").click(function(){
		location.href = "/lastVisited.action";
	});
	
	/***
	 * 重置表单
	 */
	$("#resetButton").click(function(){
		$("#validatorForm").data('bootstrapValidator').resetForm();
	});
});

function validateSelect() {
	var selects = $(".selectpicker-validate");
	var flag = true;
	if (selects.length == 0) {
		return flag;
	}
	$.each(selects, function(i, item) {
		var value = $(item).val();
		var message = $(item).attr("message");
		if (message == undefined || message == '') {
			message = '必须选择该项！';
		}
		if (value == ''||value=='null') {
			flag = false;
			console.info($(item).parent().find("button").html());
			$(item).parent().find("button").css({'border-color': '#ffa8a8', 'background-color': '#fff3f3'});
			$(item).parent().tooltip({
				position: 'right',    
				content: '<span style="color:red;font-weight: bold; font-size: 14px;font-family: 微软雅黑">'+ message +'</span>',
			});
			
		} else {
			flag = true;
			$(item).parent().find("button").css({'border-color': '#ccc', 'background-color': '#fff'});
			try {
				$(item).parent().tooltip("destroy");
			}catch (e) {
				
			}
		}
	});
	return flag;
}


function fillForm($form,data){
	var jsonObj = data;
	  if (typeof data === 'string') {
	    jsonObj = $.parseJSON(data);
	  }
	  for (var key in jsonObj) {  //遍历json字符串
		    var objtype = jsonObjType(jsonObj[key]); // 获取值类型

		     if (objtype === "array") { //如果是数组，一般都是数据库中多对多关系

		      var obj1 = jsonObj[key];
		      for (var arraykey in obj1) {
		        //alert(arraykey + jsonObj[arraykey]);
		        var arrayobj = obj1[arraykey];
		        for (var smallkey in arrayobj) {
		          setCkb(key, arrayobj[smallkey]);
		          break;
		        }
		      }
		    } else if (objtype === "object") { //如果是对象，啥都不错，大多数情况下，会有 xxxId 这样的字段作为外键表的id

		    } else if (objtype === "string") { //如果是字符串
		      var str = jsonObj[key];
		      var date = new Date(str);
		      /*if (date.getDay()) {  //这种判断日期是本人懒，不想写代码了，大家慎用。
		        $("[name=" + key + "]", $form).val(date.format("yyyy-MM-dd"));
		        continue;
		      }*/

		      var tagobjs = $("[name=" + key + "]", $form);
		      if ($(tagobjs[0]).attr("type") == "radio") {//如果是radio控件 
		        $.each(tagobjs, function (keyobj,value) {
		          if ($(value).attr("value") == jsonObj[key]) {
		            value.checked = true;
		          }
		        });
		        continue;
		      }
		      
             if ($(tagobjs[0]).attr("type") == "checkbox") {//如果是checkbox控件
                 $.each(tagobjs, function (keyobj,value) {
                    if (jsonObj[key].indexOf($(value).attr("value"))!=-1) {
                         value.checked = true;
                     }
                 });
                 continue;
             }


		      $("[name=" + key + "]", $form).val(jsonObj[key]);
		      
		    } else { //其他的直接赋值
		      $("[name=" + key + "]", $form).val(jsonObj[key]);
		    }

		  }
}

//根据name填充$form:填充对象 data：填充JSON数据 prefix：填充标签name前缀名字
function fillFormNew($form, data, prefix){
	var jsonObj = data;
	if (typeof data === 'string') {
		jsonObj = $.parseJSON(data);
	}
	for (var key in jsonObj) {  //遍历json字符串
		var objtype = jsonObjType(jsonObj[key]); // 获取值类型
			if (objtype === "array") { //如果是数组，一般都是数据库中多对多关系
			    var obj1 = jsonObj[key];
			    for (var arraykey in obj1) {
			    	var arrayobj = obj1[arraykey];
			        for (var smallkey in arrayobj) {
			        	setCkbNew(key, arrayobj[smallkey], prefix);
			        	break;
			        }
			    }
		    } else if (objtype === "object") { //如果是对象，啥都不错，大多数情况下，会有 xxxId 这样的字段作为外键表的id

		    } else if (objtype === "string") { //如果是字符串
		    	var str = jsonObj[key];
		    	var date = new Date(str);
		    	var tagobjs = $("[name=" + prefix + key + "]", $form);
		    	if ($(tagobjs[0]).attr("type") == "radio") {//如果是radio控件 
		    		$.each(tagobjs, function (keyobj,value) {
		    			if ($(value).attr("value") == jsonObj[key]) {
		    				value.checked = true;
		    			}
		    		});
		    		continue;
		    	}
		    	if ($(tagobjs[0]).attr("type") == "checkbox") {//如果是checkbox控件
		    		$.each(tagobjs, function (keyobj,value) {
		    			if (jsonObj[key].indexOf($(value).attr("value"))!=-1) {
		    				value.checked = true;
		    			}
		    		});
		    		continue;
		    	}
		    	if ($(tagobjs[0]).attr("type") == "text") {
		    		$("[name=" + prefix + key + "]", $form).val(jsonObj[key]);
		    	}
		    	$("[name=" + prefix + key + "]", $form).html(jsonObj[key]);
		    } else { //其他的直接赋值
		    	if ($(tagobjs[0]).attr("type") == "text") {
		    		$("[name=" + prefix + key + "]", $form).val(jsonObj[key]);
		    	}
		    	$("[name=" + prefix + key + "]", $form).html(jsonObj[key]);
		    }

		  }
}

 function jsonObjType(obj) {
  if (typeof obj === "object") {
	    var teststr = JSON.stringify(obj);
	    if (teststr[0] == '{' && teststr[teststr.length - 1] == '}') return "class";
	    if (teststr[0] == '[' && teststr[teststr.length - 1] == ']') return "array";
	  }
	  return typeof obj;
	}
 
function setCkb(name, value) {
  $("[name=" + name + "][val=" + value + "]").attr("checked", "checked");
}

function setCkbNew(name, value, prefix) {
	$("[name="+ prefix + name + "][val=" + value + "]").attr("checked", "checked");
}

//图片上传初始化(单张图片)
function initImgInput(ctrlName, uploadUrl) {    
    var control = $('#' + ctrlName); 
    control.fileinput({
        language: 'zh', //设置语言
        uploadUrl: uploadUrl, //上传的地址
        allowedFileExtensions : ['jpg', 'png','gif'],//接收的文件后缀
        uploadAsync: true,
        overwriteInitial : false,
        initialCaption: "请选择上传图片文件(jpg、png、gif格式)",
        showUpload: true, //是否显示上传按钮
        showCaption: true,//是否显示标题
        showPreview:false,
        showCancel: false,
        showClose: false,
        browseOnZoneClick: false,
        maxFileCount: 1,
        browseClass: "btn btn-primary", //按钮样式             
        previewFileIcon: "<i class='glyphicon glyphicon-king'></i>",
        slugCallback : function(filename) {
            return filename.replace('(', '_').replace(']', '_');
        }
    });
}

//视频上传初始化
function initVideoInput(ctrlName, uploadUrl) {
    var control = $('#' + ctrlName);

    control.fileinput({
        language: 'zh', //设置语言
        uploadUrl: uploadUrl, //上传的地址
        allowedFileExtensions : ['flv','mp4'],//接收的文件后缀
        uploadAsync: true,
        overwriteInitial : false,
        initialCaption: "请选择上传视频文件flv、mp4格式",
        showUpload: true, //是否显示上传按钮
        showCaption: true,//是否显示标题
        showPreview:false,
        showCancel: false,
        showClose: false,
        browseOnZoneClick: false,
        maxFileCount: 1,
        msgValidationError :'文件格式错误',
        browseClass: "btn btn-primary", //按钮样式
        previewFileIcon: "<i class='glyphicon glyphicon-king'></i>",
        slugCallback : function(filename) {
            return filename.replace('(', '_').replace(']', '_');
        }
    });
}
// 音频上传初始化
function initVoiceInput(ctrlName, uploadUrl) {
    var control = $('#' + ctrlName);

    control.fileinput({
        language: 'zh', //设置语言
        uploadUrl: uploadUrl, //上传的地址
        allowedFileExtensions : ['wma','wav' ,'mp3'],//接收的文件后缀
        uploadAsync: true,
        overwriteInitial : false,
        initialCaption: "请选择上传音频文件wma,wav,mp3格式",
        showUpload: true, //是否显示上传按钮
        showCaption: true,//是否显示标题
        showPreview:false,
        showCancel: false,
        showClose: false,
        browseOnZoneClick: false,
        maxFileCount: 1,
        browseClass: "btn btn-primary", //按钮样式
        previewFileIcon: "<i class='glyphicon glyphicon-king'></i>",
        slugCallback : function(filename) {
            return filename.replace('(', '_').replace(']', '_');
        }
    });
}
// zip上传初始化
function initZipInput(ctrlName, uploadUrl) {
    var control = $('#' + ctrlName);

    control.fileinput({
        language: 'zh', //设置语言
        uploadUrl: uploadUrl, //上传的地址
        allowedFileExtensions : ['zip'],//接收的文件后缀
        uploadAsync: true,
        overwriteInitial : false,
        initialCaption: "请选择上传压缩文件zip格式",
        showUpload: true, //是否显示上传按钮
        showCaption: true,//是否显示标题
        showPreview:false,
        showCancel: false,
        showClose: false,
        browseOnZoneClick: false,
        maxFileCount: 1,
        browseClass: "btn btn-primary", //按钮样式
        previewFileIcon: "<i class='glyphicon glyphicon-king'></i>",
        slugCallback : function(filename) {
            return filename.replace('(', '_').replace(']', '_');
        }
    });
}

function initFileInput(ctrlName, uploadUrl) {
    var control = $('#' + ctrlName);

    control.fileinput({
        language: 'zh', //设置语言
        uploadUrl: uploadUrl, //上传的地址
        allowedFileExtensions : ['zip','doc','docx','ppt','pptx','xls','xlsx','txt','rar','tar'],//接收的文件后缀
        uploadAsync: true,
        overwriteInitial : false,
        initialCaption: "请选择上传文件",
        showUpload: true, //是否显示上传按钮
        showCaption: true,//是否显示标题
        showPreview:false,
        showCancel: false,
        showClose: false,
        browseOnZoneClick: false,
        maxFileCount: 1,
        browseClass: "btn btn-primary", //按钮样式
        previewFileIcon: "<i class='glyphicon glyphicon-king'></i>",
        slugCallback : function(filename) {
            return filename.replace('(', '_').replace(']', '_');
        }
    });
}