var cookitInfo;
var vcode=0;


var Cookie = function(name, value, options) {
	// 如果第二个参数存在
	if (typeof value != 'undefined') {
		options = options || {};
		if (value === null) {
			// 设置失效时间
			options.expires = -1;
		}
		var expires = '';
		// 如果存在事件参数项，并且类型为 number，或者具体的时间，那么分别设置事件
		if (options.expires && (typeof options.expires == 'number' || options.expires.toUTCString)) {
			var date;
			if (typeof options.expires == 'number') {
				date = new Date();
				date.setTime(date.getTime() + (options.expires * 24 * 60 * 60 * 1000));
			} else {
				date = options.expires;
			}
			expires = '; expires=' + date.toUTCString();
		}
		var path = options.path ? '; path=' + options.path : '', // 设置路径
			domain = options.domain ? '; domain=' + options.domain : '', // 设置域
			secure = options.secure ? '; secure' : ''; // 设置安全措施，为 true 则直接设置，否则为空

		// 把所有字符串信息都存入数组，然后调用 join() 方法转换为字符串，并写入 Cookie 信息
		document.cookie = [name, '=', encodeURIComponent(value), expires, path, domain, secure].join('');
	} else { // 如果第二个参数不存在
		var CookieValue = null;
		if (document.cookie && document.cookie != '') {
			var Cookie = document.cookie.split(';');
			for (var i = 0; i < Cookie.length; i++) {
				var Cookie = (Cookie[i] || "").replace( /^\s+|\s+$/g, "");
				if (Cookie.substring(0, name.length + 1) == (name + '=')) {
					CookieValue = decodeURIComponent(Cookie.substring(name.length + 1));
					break;
				}
			}
		}
		return CookieValue;
	}
}


$(function(){
	/*if(Cookie("userInfo")){
		cookitInfo=eval(Cookie("userInfo"))[0];
		vcode=cookitInfo.vCode;

		if(window.location.pathname.indexOf('login')>0){
			$(".head_log").show();
			$(".head_user").html(cookitInfo.userName+"您好，您已成功登陆！");
				$(".log_box").remove();
		}
		$("#user_sp").html(cookitInfo.userName);
	}
	else{
		if(window.location.pathname.indexOf('login')<0){
			location.href="login.html";
		}
	}
	//获取模块列表

	//退出登陆
	$("#exit").click(function(){
		var options={expires:-1};
		Cookie("userInfo",null,options);
		location.href="login.html";
	});

	输入框默认样式
	fobl('.s_input_fb');*/
	
});

function placeHolder(that){
	var $this = $(that);
	var defaultVal = $this.val();    
	$this.focus(function(){
		if($(this).val() == defaultVal){
			$(this).addClass('active').val('');
		}
	});
	$this.blur(function(){
		if($(this).val() == ''){
			$(this).val(defaultVal).removeClass('active');
		}
	});
}

function fobl(that){
	var $this = $(that);
	$this.focus(function(){
		$(this).addClass('active');
	});
	$this.blur(function(){
		$(this).removeClass('active');
	});

}


function radio($cur){
	var radioName = $cur.find('.radio').attr('name');
	$('label[name="'+ radioName +'"]').removeClass('active').siblings('input[name="'+ radioName +'"]').removeAttr('checked','checked');
	$cur.find('.radio').addClass('active').siblings('input[name="'+ radioName +'"]').attr('checked', 'checked');
	$cur.parent().addClass('active').siblings().removeClass('active');
}

if ( !Array.prototype.forEach ) {

	Array.prototype.forEach = function forEach( callback, thisArg ) {

		var T, k;

		if ( this == null ) {
			throw new TypeError( "this is null or not defined" );
		}
		var O = Object(this);
		var len = O.length >>> 0;
		if ( typeof callback !== "function" ) {
			throw new TypeError( callback + " is not a function" );
		}
		if ( arguments.length > 1 ) {
			T = thisArg;
		}
		k = 0;

		while( k < len ) {

			var kValue;
			if ( k in O ) {

				kValue = O[ k ];
				callback.call( T, kValue, k, O );
			}
			k++;
		}
	};
}
/*获取URL参数*/
function getParam(paramName) {
	paramValue = "";
	isFound = false;
	if (this.location.search.indexOf("?") == 0
		&& this.location.search.indexOf("=") > 1) {
		arrSource = unescape(this.location.search).substring(1,
			this.location.search.length).split("&");
		i = 0;
		while (i < arrSource.length && !isFound) {
			if (arrSource[i].indexOf("=") > 0) {
				if (arrSource[i].split("=")[0].toLowerCase() == paramName
						.toLowerCase()) {
					paramValue = arrSource[i].split("=")[1];
					isFound = true;
				}
			}
			i++;
		}
	}
	return paramValue;
}
//统一ajaxJson
function getAjaxJson(params,type, url,func) {

	//$.getJSON(url+"?callback=?",params,function(data) {
	//	func(data)
	//});
	$.ajax({
		cache: true,
		type: type,
		url:url,
		data:params,
	    dataType: "jsonp",
		async: false,
		error: function(request) {
			layer.msg('接口错误', {
				icon: 6,
				shift: 6,    // 出现的动画效果
				time: 1500 //2秒关闭（如果不配置，默认是3秒）
			}, function(){
				//do something
			});

		},
		success: function(json) {

			func(json);
		}
	});
}

function getShow(field,type,values){
	var str='';
	switch (type.toLowerCase()){
		case "string":
			str+='<input class="s_input s_input_fb w318" type="text" va  name="'+field+'" id="'+field+'"/>';
			break;
		case "images":
			break;
		case "radio":
			if(values && values.length>0){
				values.forEach(function(value,ind){
					var radio_s='',label_s='';
					if(ind==0){
						radio_s='checked="checked"';
						label_s='active';
					}
					str+='<span class="radio_wrap"><input class="deRadio" type="radio"  value="'+value.val+'" name="'+field+'" '+radio_s+'><label class="radio '+label_s+'"  ></label>'+value.name+'</span>';
				});
			}
			break;
		case "checkbox":
			if(values && values.length>0){
				values.forEach(function(value,ind){
					var radio_s='',label_s='';
					if(ind==0){
						radio_s='checked="checked"';
						label_s='active';
					}
					str+='<span class="checkbox_wrap"><input class="check_init" type="checkbox" value="'+value.val+'" name="'+field+'" '+radio_s+'><label class="checkbox '+label_s+'"></label>'+value.name+'</span>';
				});
			}
			break;
		case "drop":
			if(values && values.length>0){
				drop+=","+field;
				str+='<select class="'+field+'" name="'+field+'" style="display: none">';
				values.forEach(function(value){
					str+='<option value="'+value.val+'">'+value.name+'</option>';
				});
				str+='</select>';
			}
			break;
		case "textarea":
			str+='<textarea class="fom_texarea" name="'+field+'"></textarea>';
			break;
		case "date":
			break;
		case "area":
			break;
		case  "images":
			break;


	}
	return str;

}
function wrapH(){
	var hh = $(window).height();
	$(".wrap_con").height(hh - $(".posi_box").height() - 15);
	var ww=$(window).width()-45;
	$(".tit_pos").css("width",ww+"px");
	$("#warp_box").css("width",ww+"px");
}
