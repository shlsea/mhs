$(function() {
	// 浮动计算
	$(window).bind("scroll", function() {
		var scrollTo = $('#survey_tools');
		scrollTo.css('margin-top', ($(document).scrollTop()) + 'px');
	});
	// 左侧工具点击展示隐藏
	$('.sel_title').click(
			function() {
				if ($(this).siblings('ul').css('display') == 'none') {
					$(this).find('.sel_op').removeClass('icon-angle-down')
							.addClass('icon-angle-up');
					$(this).addClass('inactive');
					$(this).siblings('ul').slideDown(100).children('li');
					if ($(this).parents('li').siblings('li').children('ul')
							.css('display') == 'block') {
						$(this).parents('li').siblings('li').children('ul')
								.parent('li').children('span').removeClass(
										'inactive');
						$(this).parents('li').siblings('li').children('ul')
								.slideUp(100);
						$(this).parents('li').siblings('li').children('ul')
								.parent('li').find('.sel_op').removeClass(
										'icon-angle-up').addClass(
										'icon-angle-down');
					}
				} else {
					$(this).find('.sel_op').removeClass('icon-angle-up')
							.addClass('icon-angle-down');
					$(this).removeClass('inactive');
					$(this).siblings('ul').slideUp(100);

				}
			});

	// 单选
	$('#blank').click(function() {
		layer.open({
			type : 1,
			title : "<i class='icon-circle-blank'></i>单选题",
			closeBtn : false,
			area : ['700px','500px'],
			shade : 0.1,
			zIndex :250,
			id : 'sel',
			btn : [ '保存', '取消' ],
			moveType : 1,
			content : $('#radio_edit'),
			yes : function(index, layer) {
				/*$('.easyui-validatebox').validatebox('disableValidation')
			    .focus(function () { $(this).validatebox('enableValidation'); })
			    .blur(function () { $(this).validatebox('validate');});*/
				
				var $form=$("#radio_form");
				$form.find('.easyui-validatebox').validatebox();
				if($form.form('validate')){
					//防止点击提交进行多次保存
					layer.load(0, {shade: [0.5,'#333']});
					console.info($form.serialize());
					//$form.submit();
				}
			},
			cancel : function(index, layer) {
				layer.close(index);
			}
		});
	});
	
	//单选新增选项
	$('#radio_add').click(function(){
		var size=$('#radio_item tr').length;
		var html='<tr>';
		html+='<td	class="form-group">';
		html+='<input type="text" name="itme['+size+'].serial" class="form-control easyui-validatebox"  autocomplete="off" validate="{maxlength:5,messages:{maxlength:\'序号至多为5个字符\'}}" />';
		html+='</td>';
		html+='<td class="form-group">';
		html+='<input type="text" name="itme['+size+'].title" class="form-control easyui-validatebox"  autocomplete="off" validate="{{maxlength:300,messages:{maxlength:\'选项至多为300个字符\'}}" />';
		html+='</td>';
		html+='<td class="form-group">';
		html+='<input type="text" name="itme['+size+'].value" class="form-control easyui-validatebox"  autocomplete="off" validate="maxlength:10,messages:{maxlength:\'选项值至多为10个字符\'}}" />';
		html+='</td>';
		html+='<td class="form-group">';
		html+='<input type="text" name="itme['+size+'].orderList" class="form-control easyui-validatebox"  autocomplete="off" validate="{validType:\'integerNum\',messages:{integerNum:\'输入的内容必须是大于等于0的整数\'}}" />';
		html+='</td>';
		html+='<td class="form-group  radio_op">';
		html+='<i class="icon-remove"></i>';
		html+='</td>';
		html+='</tr>';
		$("#radio_item").append(html);
	});
	
	//删除选项
	$('#radio_item').on('click','.icon-remove',function(){
		var size=$('#radio_item tr').length;
		if(size<3){
			layer.alert('至少保留两个选项', {icon: 5});
		}else{
			$(this).parent().parent().remove();
		}
	});
});