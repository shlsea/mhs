$(function(){
	
	
	//点击一级菜单切换出二三四级菜单
	
		  $("#nav_list").empty();
		  $.ajax({
			   type: "post",
			   url: "sys_menu/menu.action",
			   data: "id=1",
			   dateType : 'json',
			   success: function(data){
				   if(data.length>0){
					   //如果二级菜单下有子菜单显示左侧菜单
					   $("#sidebar").show();
					   
					   $(".main-content").css("margin-left","");
					   
					   $.each(data,function(index,item){
						   //获取三级菜单
						   var html="";
						   html+='<li id="menu_'+item.id+'">';
						   html+='<a  href="javascript:void(0)" name="opartion.action?pid='+item.id+'" title="'+item.name+'">';
						   html+='<i class="'+item.icon+'"></i>';
						   html+='<span class="menu-text">'+item.name+'</span>';
						   html+='</a>';
						   html+='</li>';
						   $("#nav_list").append(html);
						   //根据三级菜单获取四级菜单
						   $.ajax({
							   type: "post",
							   url: "sys_menu/menu.action",
							   data: "id="+item.id,
							   dateType : 'json',
							   success: function(result){
								  //根据三级菜单是否有子菜单变更三级变化
								   if(result.length>0){
									   var html="";
									   $("#menu_"+item.id +" a").addClass("dropdown-toggle");
									   $("#menu_"+item.id +" a").append('<b class="arrow icon-angle-down"></b>');
									   html+='<ul class="submenu">';
									   //轮询出四级菜单
									   $.each(result,function(i,n){
										   html+='<li id="menu_'+n.id+'"><a href="javascript:void(0)" name="opartion.action?pid='+n.id+'" title="'+n.name+'" >';
										   html+='<span class="menu-text">'+n.name+'</span></a>';
										   html+='</li>'
										   $.ajax({
											   type: "post",
											   url: "sys_menu/menu.action",
											   data: "id="+n.id,
											   dateType : 'json',
											   success: function(data){
												 var html="";
												   if(data.length>0){
													   $("#menu_"+n.id +" a").addClass("dropdown-toggle");
													   $("#menu_"+n.id +" a").append('<b class="arrow icon-angle-down"></b>');
													   html+='<ul class="submenu">';
													   $.each(data,function(j,ind){
														   html+='<li class="home"><a href="javascript:void(0)" name="opartion.action?pid='+ind.id+'" title="'+ind.name+'" class="iframeurl" >';
														   html+=ind.name+'</a></li>';
													   });
													   html+='</ul>';
													   $("#menu_"+n.id).append(html);
												   }else{
													   $("#menu_"+n.id +" a").addClass("iframeurl");
													   $("#menu_"+n.id).addClass("home"); 
												   }
												  
											   }
										   });
										 
										 
									   }); 
									   html+='</ul>';
									   $("#menu_"+item.id).append(html);
								   }else{
									   $("#menu_"+item.id +" a").addClass("iframeurl");
									   $("#menu_"+item.id).addClass("home");
									  
								   }
							   }
						}); 
					   }); 
				   }else{
					 //如果二级菜单下无子菜单隐藏左侧菜单
					   $("#sidebar").hide();
					   $(".main-content").css("margin-left",'0'); 
					   $("#iframe").attr("src", 'opartion.action?pid='+id).ready();
				   }
				 
				  
			   }
		});
		  
	
	//切换菜单样式
	$('#nav_list').on('click','.home',function() {
		$('#nav_list').find('li.home').removeClass('active');
		$(this).addClass('active');
	});


	//退出操作
	$('#Exit_system').on('click', function() {
		layer.confirm('是否确定退出系统？', {
			btn : [ '是', '否' ],//按钮
			icon : 2,
		}, function() {
			location.href = "logout.action";

		});
	});
	
	//面包屑导航
	$("#nav_list").on('click','.iframeurl',function(){
		var cid = $(this).attr("name");
		var cname = $(this).attr("title");
		$("#iframe").attr("src", cid).ready();
		$("#Bcrumbs").attr("href", cid).ready();
		$(".Current_page a").attr('href',cid).ready();
		$(".Current_page").attr('name', cid);
		$(".Current_page").html(cname).css({
			"color" : "#333333",
			"cursor" : "pointer"
		}).ready();
		$("#parentIframe").html('<span class="parentIframe iframeurl"> </span>')
				.css("display", "none")
				.ready();
		$("#parentIfour").html('').css("display", "none").ready();
	});
	
	//默认加载出一级菜单后触发加载二级菜单
	  $(".top_menu li").first().click();
	  $('#nav_list li.home').first().click();
});