	$(document).ready(function(){
		if(menus_!=null && menus_!="undefined"){
			menus=menus_;
		}
		
		if(ops_!=null && ops_!="undefined"){
			ops=ops_;
		}
		$.ajax({
			async:false,
			cache:false,
			type:"POST",
			dataType:"json",
			url:"/sys_role/getTree.action",
			error:function(data){
			treeNodes =data.treeNodes;
			
			},
			success:function(data){
				treeNodes = data.treeNodes;
				$.fn.zTree.init($("#treeMenu"), {
					check: {
						enable: true,
						chkStyle: "checkbox"
					},
					view: {
							addDiyDom: function(treeId, treeNode) {
								var op=treeNode.op;
								var html = [];
								html.push('<i>');
								$.each(op,function(index,item){
									var _op = op[index];
									 _ops= ops.split(",");
									var _check=($.inArray(_op.id+"-"+_op.code,_ops));
									var checked="checked='checked'";
									if(_check==-1){
										checked="";
									}
									
									html.push('<label class="tree_label"><input class="operates" '+checked+' type="checkbox" name="operates"  value="'+_op.id+'"></input><i class="tree_i">'+_op.name+'</i></label>');
								});
								html.push('</i>');	
								
								var aObj = $("#" + treeNode.tId);
								aObj.append(html.join(""));
								_menus= ops.split(",");
								
							}
					},
					callback:{
						onCheck:zTreeBeforeCheck
					},
					data: {
						
							simpleData: {
								enable: true,
								id: data.id,
								pId: data.pid,
								
							}
						
					}
			}, treeNodes);
			}
			});
		
		//选择权限联动
		$(".chk").on('click',function(){
			var obj=$(this);
			var check= $(this).parent().find("input[name='operates']");
			var isChecked=!(obj.hasClass("checkbox_true_full_focus"));
			$.each(check,function(index,item){
				
				if(isChecked)
				$(this).prop({checked:true});
				else
				$(this).removeProp("checked");	
			});
		});
		checkNode();
	});
	
	function checkNode() {
		
        var treeObj = $.fn.zTree.getZTreeObj("treeMenu");
        var nodes = treeObj.getNodes();
        _menus= menus.split(",");
        for(var i=1;i<_menus.length;i++){
        	if(_menus[i]>0){
        		var node=treeObj.getNodeByParam("id",_menus[i],null);
            	treeObj.checkNode(node, true, false);
            	$("#menu").append('<input type="hidden"  name="menus" value="'+node.id+'"/>');
        	}
        	
        }
        
    }
	
	function zTreeBeforeCheck(treeId, treeNode) {
		changeClick(treeNode.id);
	}
	
	//选择权限联动
	function changeClick(id){
		$("#menu").html("");
		var treeObj = $.fn.zTree.getZTreeObj("treeMenu");
		var nodes = treeObj.getCheckedNodes(true);
		
		$.each(nodes,function(index,item){
			$("#menu").append('<input type="hidden"  name="menus" value="'+item.id+'"/>');
		});
		
	}