
//菜单选择
function addMenu(id, value){
	$.ajax({
        type : 'post',
        url : '/sys_menu/findAll.action',
        dateType : 'json',
        success: function(data){
        	var tempAjax="<option style='padding-left:20px;' value=''>请选择所属菜单</option>";
            $.each(data,function(i,n){
            	var _left=10*n.menuLevel+20;
                var sel = "";
                if (n.id == value) {
                    sel = "selected='selected'";
                }
               tempAjax += "<option "+ sel +" style='padding-left:"+_left+"px;' value='"+n.id+"'>"+n.name+"</option>";

            });
            $("#"+id).empty();
            $("#"+id).append(tempAjax);
            $("#"+id).selectpicker('render');
            $("#"+id).selectpicker('refresh');
           
        }
   });
}


//地区选择
function selRegion(id,_id,p,value) {
    $.ajax({
        type : 'post',
        url : '/sys_region/findByPid.action?pid='+p,
        dateType : 'json',
        success: function(data){
            var tempAjax="<option style='padding-left:20px;' value=''>请选择地区</option>";
            $.each(data,function(i,n){
                var sel = "";
                if (n.id == value) {
                    sel = "selected='selected'";
                }
                tempAjax += "<option "+ sel +" value='"+n.id+"'>"+n.name+"</option>";
            });
            $("#"+id).empty();
            $("#"+id).append(tempAjax);
            $("#"+id).selectpicker({
                maxOptions: 1,
                noneSelectedText:'请选择地区',
                showTick: true,
                size:6
            });
           
            $("#"+id).selectpicker('refresh');
            $("#"+id).selectpicker('render');
            $("#"+id).selectpicker('val',value);
        }
    });
}

// 选择父类字典
function selDict(id, value){
    //$("#"+id)
    $.ajax({
        type : 'post',
        url : '/sys_dict/findAll.action',
        dateType : 'json',
        success: function(data){
            var tempAjax="<option style='padding-left:20px;' value=''>请选择所属字典</option>";
            $.each(data,function(i,n){
                var _left=10*n.level+20;
                var sel = "";
                //console.info(n.skey);
                if (n.skey == value) {
                    sel = "selected='selected'";
                }
                tempAjax += "<option "+ sel +" style='padding-left:"+_left+"px;' value='"+n.skey+"'>"+n.name+"</option>";

            });
            //console.info(tempAjax);
            $("#"+id).empty();
            $("#"+id).append(tempAjax);
            $("#"+id).selectpicker('render');
            $("#"+id).selectpicker('refresh');
        }
    });
}

function selScenic(id, value){
    $.ajax({
        type : 'post',
        url : '/re_scenic/findAll.action',
        dateType : 'json',
        success: function(data){
            var tempAjax="<option style='padding-left:20px;' value=''>请选择所属景区</option>";
            $.each(data,function(i,n){
             
                var sel = "";
                //console.info(n.skey);
                if (n.id == value) {
                    sel = "selected='selected'";
                }
                tempAjax += "<option "+ sel +"  value='"+n.id+"'>"+n.name+"</option>";

            });
            $("#"+id).empty();
            $("#"+id).append(tempAjax);
            $("#"+id).selectpicker('render');
            $("#"+id).selectpicker('refresh');
        }
    });
}

//根据父类数据字典查询子集
function selDictByParent(id,skey,value){
    $.ajax({
        type : 'post',
        url : '/sys_dict/findByPkey.action?skey='+skey,
        dateType : 'json',
        success: function(data){
            var tempAjax="<option style='padding-left:20px;' value=''>请选择所属标签类型</option>";
            $.each(data,function(i,n){
              
                var sel = "";
                if (n.skey == value) {
                    sel = "selected='selected'";
                }
                tempAjax += "<option "+ sel +"  value='"+n.skey+"'>"+n.name+"</option>";

            });
            $("#"+id).empty();
            $("#"+id).append(tempAjax);
            $("#"+id).selectpicker('render');
            $("#"+id).selectpicker('refresh');
        }
    });
}

function selChannel(id,noMsg,value){
	$.ajax({
        type : 'post',
        url : '/channel/findAll.action',
        dateType : 'json',
        success: function(data){
        	var tempAjax="<option style='padding-left:20px;' value=''>"+noMsg+"</option>";
            $.each(data,function(i,n){
            	var _left=10*n.level+20;
                var sel = "";
                if (n.channelId == value) {
                    sel = "selected='selected'";
                }
               tempAjax += "<option "+ sel +" style='padding-left:"+_left+"px;' value='"+n.channelId+"'>"+n.channelName+"</option>";

            });
            $("#"+id).empty();
            $("#"+id).append(tempAjax);
            $("#"+id).selectpicker('render');
            $("#"+id).selectpicker('refresh');
           
        }
   });
}

function setApiChannel(id,noMsg,value){
	$.ajax({
        type : 'post',
        url : '/api_channel/findAll.action',
        dateType : 'json',
        success: function(data){
        	var tempAjax="<option style='padding-left:20px;' value=''>"+noMsg+"</option>";
            $.each(data,function(i,n){
            	var _left=10*n.level+20;
                var sel = "";
                if (n.channelId == value) {
                    sel = "selected='selected'";
                }
               tempAjax += "<option "+ sel +" style='padding-left:"+_left+"px;' value='"+n.channelId+"'>"+n.channelName+"</option>";

            });
            $("#"+id).empty();
            $("#"+id).append(tempAjax);
            $("#"+id).selectpicker('render');
            $("#"+id).selectpicker('refresh');
           
        }
   });
}

function selTypeByResourceId(skey,noMsg,type,value){
    $.ajax({
        type : 'post',
        url : '/resources/findAll.action?type='+type,
        dateType : 'json',
        success: function(data){
            var tempAjax="<option style='padding-left:20px;' value=''>"+noMsg+"</option>";
            $.each(data,function(i,n){
                var _left=10*n.level+20;
                var sel = "";
                if (n.id == value) {
                    sel = "selected='selected'";
                }
                tempAjax += "<option "+ sel +" style='padding-left:"+_left+"px;' value='"+n.id+"'>"+n.name+"</option>";

            });
            $("#"+skey).empty();
            $("#"+skey).append(tempAjax);
            $("#"+skey).selectpicker('render');
            $("#"+skey).selectpicker('refresh');

        }
    });
}

function selResourceTypeById(skey,noMsg,type,value){
    $.ajax({
        type : 'post',
        url : '/resources/findResourcesAll.action?type='+type,
        dateType : 'json',
        success: function(data){
            var tempAjax="<option style='padding-left:20px;' value=''>"+noMsg+"</option>";
            $.each(data,function(i,n){
                var _left=10*n.level+20;
                var sel = "";
                if (n.id == value) {
                    sel = "selected='selected'";
                }
                tempAjax += "<option "+ sel +" style='padding-left:"+_left+"px;' value='"+n.id+"'>"+n.name+"</option>";

            });
            $("#"+skey).empty();
            $("#"+skey).append(tempAjax);
            $("#"+skey).selectpicker('render');
            $("#"+skey).selectpicker('refresh');

        }
    });
}
function escape2Html(str) { 
	 var arrEntities={'lt':'<','gt':'>','nbsp':' ','amp':'&','quot':'"','deg':'°','prime':'′','ldquo':'“','rdquo':'”'}; 
	 return str.replace(/&(lt|gt|nbsp|amp|quot|deg|prime|ldquo|rdquo);/ig,function(all,t){return arrEntities[t];}); 
} 
