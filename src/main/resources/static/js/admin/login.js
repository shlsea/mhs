 $(document).ready(function() {

	 		document.onkeydown = function(e){
	 			var ev = document.all ? window.event : e;
			    if(ev.keyCode==13) { 
		 			$("#login_btn").click();  
		 		}
	 		}
            if(!$.support.leadingWhitespace){
                $(".arr").css({"bottom":"150px"});
            }
            $("#login_btn").click(function() {
                var userName = $("input[name='username']").val();
                var password = $("input[name='pwd']").val();
                var verify = $("input[name='verify']").val();
                if(userName==null ||userName==""||$.trim(userName)==""){
                	layer.alert('请输入用户名!', {icon: 0});
                	return false;
                }
                if(password==null ||password==""||$.trim(password)==""){
                	layer.alert('请输入密码!', {icon: 0});
                	return false;
                }
                if(verify==null ||verify==""||$.trim(verify)==""){
                	layer.alert('请输入验证码!', {icon: 0});
                	return false;
                }
                var parameter = {username: userName, password: password, captcha: verify};
                getAjaxJson(parameter, "post","checkLogin?callback=?", getUserInfo);
            });
            
            //刷新验证码
            $("#verifyImage").click(function() {
				var timestamp = (new Date()).valueOf();
				$(this).attr("src", "code?timestamp=" + timestamp)
			});
            
           
        });
 
        function getUserInfo(data){
        	if(data.status==-1){
            	layer.alert('验证码已过期!', {icon: 2});
            }else if(data.status==0){
            	layer.alert('验证码错误!', {icon: 2});
            }else if(data.status==1){
            	layer.alert('用户不存在!', {icon: 2});
            }else if(data.status==2){
            	layer.alert('用户名或密码错误!', {icon: 2});
            }else if(data.status==3){
            	layer.alert('账号已停用!', {icon: 2});
            }else if(data.status==4){
            	 //var cookie_value="[{userName:'"+data.username+"',vCode:'"+data.vcode+"'}]";
                 //Cookie("userInfo",cookie_value);
                 location.href="main.action";
            }
        	$("#verifyImage").click();
        }
        
       