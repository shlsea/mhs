
$(function () {

   

	 var H1=$('.menu_day').height();
     var H2=$('#resouce').height();
     var H= H1>H2?H1:H2;
  	if(H>700){
  		$('.menu_detailed').css('height',H+200);
  		$("#main_right").css('height',H+200);
  	}else{
  		$('.menu_detailed').css('height',800);
  		$("#main_right").css('height',800);
  	}
  	
    var $citypicker3 = $('#city-picker3');
   
    var width=$("body").width();
    $(".main_left").width(width-530);
    $(window).resize(function() {
        var width=$("body").width();
        $(".main_left").width(width-530);
    });

    $("#btn_type span").on('click',function(){
        $("#btn_type .btn-primary").removeClass("btn-primary").addClass("btn-default");
        $(this).removeClass("btn-default");
        $(this).addClass("btn-primary");
        $("#inputId").val(""); // 清空搜索输入框的值
        $("#scenic_list").empty(); // 清空动态添加数据
        $(".pagination").empty(); // 清空页码
        getList(); // 动态添加数据
    });

    //行程天数切换
    $(".dday").on('click','.day_index,.day_adr',function(){
    	
        $(".dday .current").removeClass("current").addClass("other");
        $(this).parent().removeClass("current");
        $(this).parent().removeClass("other").addClass("current");
        var title=$(this).parent().find(".day_index").html();
        var id=$(this).parent().parent().attr("name");
        $(".menu_detailed .active").removeClass("active").addClass("hid_day");
        $(".menu_detailed  li[name='"+id+"']").find(".day-index").html(title);
        $(".menu_detailed  li[name='"+id+"']").removeClass("hid_day").addClass("active");
        var H1=$('.menu_day').height();
        var H2=$('#resouce').height();
        var H= H1>H2?H1:H2;
     	if(H>700){
     		$('.menu_detailed').css('height',H+200);
     		$("#main_right").css('height',H+200);
     	}else{
     		$('.menu_detailed').css('height',700);
     		$("#main_right").css('height',700);
     	}
    });



    //删除日程
    $(".dday").on('click','.day_remove',function(){
    	
        var id=$(this).parent().parent().attr("name");
        var $parent_class=$(this).parent().attr("class");
        var index_=$(this).parent().parent().index();
        var length=$(".dday li").length;
        if(length==1){
            layer.alert('至少保留一天日程', {icon: 0});
            return false;
        }
        layer.confirm('是否确认删除选中日程？', {
            btn: ['确认','取消'],
            icon : 0
        }, function(index){
            layer.close(index);
            $.ajax({
                type : 'post',
                url: '../trip_day/delete?id=' + id,
                dateType : 'json',
                success: function(data){
                    if(data.state=='1'){
                        layer.alert('操作成功', {icon: 6});
                        index_=index_+1;
                        if($parent_class=='current'){

                            if($(".dday li").length<index_){
                                $(".dday li:eq("+index_+")").find(".day_index").click();
                            }else{
                                $(".dday li:eq("+(index_-2)+")").find(".day_index").click();
                            }
                        }
                        $(".dday li:gt("+(index_-1)+")").each(function(i){
                            $(this).find(".day_index").html("第"+(index_+i)+"天");
                        });
                        $(".dday li[name='"+id+"']").remove();
                        $(".dday .current").find(".day_index").click();
                    }
                }
            });


        });

    });

    //新增一日行程
    $("#addOneDay").on('click',function(){
        var id=$("#trip_id").val();
        var day=$(".dday li").length+1;
        $.ajax({
            type : 'post',
            url: '../trip/day_save.action?id=' + id + '&day=' + day ,
            dateType : 'json',
            success: function(data){
            	
                $(".dday .current").removeClass("current").addClass("other");
                var html='<li name="'+data.id+'">';
                html+='<span class="add">';
                html+='	<i class="icon-add"></i>';
                html+='</span>';
                html+='<div class="current">';
                html+='<p class="day_index">第'+data.day+'天</p>';
                html+='<p class="day_adr">暂无安排</p>';
                html+='<span class="day_remove">';
                html+='<i class="glyphicon glyphicon-trash" ></i>';
                html+='</span>';
                html+='</div>';
                html+='</li>';
                $(".dday").append(html);
                $(".menu_detailed .active").removeClass("active").addClass("hid_day");
                var str='<li name="'+data.id+'" class="active">';
                str+='<div class="day_hearder">';
                str+='<span class="day-index ng-binding">第'+data.day+'天</span>';
                str+='</div>';
                str+='<div class="day_line">';
                str+='<p class="region_title"></p>';
                str+='</div>';
                str+='<ul class="source">';
                str+='</ul>';
                str+='<div class="resource_add">';
                str+='<div>';
                str+='<p>';
                str+='<button  type="button" class="scenery btn btn-primary btn-xs"><i class="icon-add"></i>景点</button>';
                str+='<button  type="button" class="dining btn btn-primary btn-xs"><i class="icon-add"></i>餐馆</button>';
                str+='<button  type="button" class="traffic btn btn-primary btn-xs"><i class="icon-add"></i>城际交通</button>';
                str+='<button  type="button" class="hotel btn btn-primary btn-xs"><i class="icon-add"></i>酒店</button>';
                str+='</p>';
                str+='</div>';
                str+='<div class="text_area">';
                str+='<textarea   placeholder="负责任地介绍这一天的行程亮点和小提醒" class="form-control" rows="3"></textarea>';
                str+='</div>';
                str+='</div>';
                str+='</li>';
                $("#resouce").append(str);
                var H1=$('.menu_day').height();
                var H2=$('#resouce').height();
                var H= H1>H2?H1:H2;
             	if(H>700){
             		$('.menu_detailed').css('height',H+200);
             		$("#main_right").css('height',H+200);
             	}else{
             		$('.menu_detailed').css('height',700);
             		$("#main_right").css('height',700);
             	}
            }
        });

    });

    //插入行程
    $(".dday").on('click','.icon-add',function(){
        var id=$(this).parent().parent().attr("name");
        var index_=$(this).parent().parent().index();
      
        $.ajax({
            type : 'post',
            url: '../trip/insert_save.action?id=' + id ,
            dateType : 'json',
            success: function(data){
                $(".dday .current").removeClass("current").addClass("other");
                var html='<li name="'+data.id+'">';
                html+='<span class="add">';
                html+='	<i class="icon-add"></i>';
                html+='</span>';
                html+='<div class="current">';
                html+='<p class="day_index">第'+data.day+'天</p>';
                html+='<p class="day_adr">暂无安排</p>';
                html+='<span class="day_remove">';
                html+='<i class="glyphicon glyphicon-trash" ></i>';
                html+='</span>';
                html+='</div>';
                html+='</li>';
                $(".dday li[name='"+id+"']").before(html);
                $(".dday li:gt("+index_+")").each(function(i){
                    $(this).find(".day_index").html("第"+(2+index_+i)+"天");
                });
                $(".menu_detailed .active").removeClass("active").addClass("hid_day");
                var str='<li name="'+data.id+'" class="active">';
                str+='<div class="day_hearder">';
                str+='<span class="day-index ng-binding">第'+data.day+'天</span>';
                str+='</div>';
                str+='<div class="day_line">';
                str+='<p class="region_title"></p>';
                str+='</div>';
                str+='<ul class="source">';
                str+='</ul>';
                str+='<div class="resource_add">';
                str+='<div>';
                str+='<p>';
                str+='<button  type="button" class="scenery btn btn-primary btn-xs"><i class="icon-add"></i>景点</button>';
                str+='<button  type="button" class="dining btn btn-primary btn-xs"><i class="icon-add"></i>餐馆</button>';
                str+='<button  type="button" class="traffic btn btn-primary btn-xs"><i class="icon-add"></i>城际交通</button>';
                str+='<button  type="button" class="hotel btn btn-primary btn-xs"><i class="icon-add"></i>酒店</button>';
                str+='</p>';
                str+='</div>';
                str+='<div class="text_area">';
                str+='<textarea   placeholder="负责任地介绍这一天的行程亮点和小提醒" class="form-control" rows="3"></textarea>';
                str+='</div>';
                str+='</div>';
                str+='</li>';
                $("#resouce").append(str);
                var H1=$('.menu_day').height();
                var H2=$('#resouce').height();
                var H= H1>H2?H1:H2;
             	if(H>700){
             		$('.menu_detailed').css('height',H+200);
             		$("#main_right").css('height',H+200);
             	}else{
             		$('.menu_detailed').css('height',700);
             		$("#main_right").css('height',700);
             	}
            }
        });
    });

    // 名称搜索
    $("#searchId").on("click", function(){
        getList();
    });

    //将资源添加到日程中
    $(".list_main").on('click','.add-to-plan',function(){
        var source_id=$(this).find("i").attr("data-size");
        var type=$("#btn_type .btn-primary").attr("resourceType");
        var dayId=$(".menu_detailed .active").attr("name");
        var source_type=1;
        var $obj= $(".active li[al='"+source_id+"']");
       
        //if($obj.attr("al")==source_id && $obj.attr("cl")==type && type === "scenery_list"){
        //	layer.alert('该行程已添加，不能再次添加', {icon: 0});
        //	return false;
        //}
        if(type === "scenery_list") {
            source_type=1;
            url = '/scenic/findOne.action';
        /*} else if (type === "hotel_list") {
            source_type=2;
            url = '/business_hotel/findOne.action';
        } else if (type === "dinning_list") {
            source_type=3;
            url = '/restaurant/findOne.action';*/
        }
        
        $.ajax({
            type : 'post',
            url: url+'?id=' + source_id ,
            dateType : 'json',
            success: function(data){
                if(data.data.id!=null){
                	var regionName=null;
                	if (type === "scenery_list"){
                		var $span=$(".active .region_title").find("span").last();
                		
                		if($span.html()!=undefined){
                        	if($span.html().indexOf(data.data.region)==-1){
                        		$(".active .region_title").append('<span>—'+data.data.name+'</span>');
                        		regionName=data.data.region;
                        	}
                			
                		}else{
                			$(".active .region_title").append('<span>'+data.data.name+'</span>');
                			regionName=data.data.region;
                		}
                		var region=$(".active .region_title").html();
                    	$(".current .day_adr").html(region.replace(/<[^>]+>/g,""));
                    	$(".current .day_adr").attr("title",region.replace(/<[^>]+>/g,""));
                    	
                	}
                	var img=data.data.picture;
                	if(img==null||img=='null'){
                		img='/images/default.png';
                	}
                    $.ajax({
                        type : 'post',
                        url: '../trip_source/save',
                        data:{
                        	'dataId':source_id,
                        	'day.id':dayId,
                        	'way':0,
                        	'type':source_type,
                        	'name':data.data.name,
                        	'tag':data.data.adviceDate,
                        	'regionName':data.data.name,
                        	'content':data.data.content,
                        	'imgPath':data.data.picture
                        },
                        dateType : 'json',
                        success: function(result){
                            if(result.id>0){
                            	var len=$(".menu_detailed .active").find("li[class='obj']").length+1;
                            	var length= $(".menu_detailed .active").find("li").length;
                            	if(length==0){
	                                var html='<li  class="obj" name="'+result.id+'" al="'+source_id+'" cl="'+type+'">';
	                                html+='<div class="cls_resouce">';
	                                html+='<div class="resouce_content">';
	                                html+='<div>';
	                                html+='<div class="resouce_img">';
	                                html+='<img src="'+img+'"/>';
	                                html+='<span class="index">'+len+'</span>';
	                                html+='</div>';
	                                html+='<div class="sub_content">';
	                                html+='<p class="remove-to-plan"><i class="glyphicon glyphicon-trash"  title="删除"></i></p>';
	                                html+='<p class="resouce_name">'+data.data.name+'</p>';
	                                html+='<p class="resouce_address">'+data.data.adviceDate+'</p>';
	                                html+='</div>';
	                                html+='</div>';
	                                html+='<div class="sub_remark">';
	                                html+='<p><span>备注</span><i class="icon-pencil"></i></p>';
	                                html+='<p class="remark" style="display:none">';
	                                html+='<textarea class="form-control" style="width:100%" rows="6"></textarea>';
	                                html+='<button type="button" class="btn btn-primary btn-xs">确认</button>';
	                                html+='</p>';
	                                html+='</div>';
	                                html+='</div>';
	                                html+='</div>';
	                                html+='</li>';
                        	}else{
                        		 
                            		var html='<li class="obj" name="'+result.id+'" al="'+source_id+'" cl="'+type+'">';
                            		html+='<ul>';
                            		html+='<li>';
                        			html+='<div class="traffic-type">';
                    				html+='<div class="resouce_content">';
                					html+='<div>';
            						html+='<p class="resouce_address">暂无交通方案</p>';
        							html+='</div>';
    								html+='</div>';
									html+='<div>';
									html+='</div>';
									html+='</div>';
									html+='</li>';
									html+='</ul>';
                            		html+='<div class="cls_resouce">';
	                                html+='<div class="resouce_content">';
	                                html+='<div>';
	                                html+='<div class="resouce_img">';
	                                html+='<img src="'+img+'"/>';
	                                html+='<span class="index">'+len+'</span>';
	                                html+='</div>';
	                                html+='<div class="sub_content">';
	                                html+='<p class="remove-to-plan"><i class="glyphicon glyphicon-trash"  title="删除"></i></p>';
	                                html+='<p class="resouce_name">'+data.data.name+'</p>';
	                                html+='<p class="resouce_address">'+data.data.adviceDate+'</p>';
	                                html+='</div>';
	                                html+='</div>';
	                                html+='<div class="sub_remark">';
	                                html+='<p><span>备注</span><i class="icon-pencil"></i></p>';
	                                html+='<p class="remark" style="display:none">';
	                                html+='<textarea class="form-control" style="width:100%" rows="6"></textarea>';
	                                html+='<button type="button" class="btn btn-primary btn-xs">确认</button>';
	                                html+='</p>';
	                                html+='</div>';
	                                html+='</div>';
	                                html+='</div>';
	                                html+='</li>';
                            	}
                                $(".menu_detailed .active").find(".source").append(html);
                                var H1=$('.menu_day').height();
                                var H2=$('#resouce').height();
                                var H= H1>H2?H1:H2;
                             	if(H>700){
                             		$('.menu_detailed').css('height',H+200);
                             		$("#main_right").css('height',H+200);
                             	}else{
                             		$('.menu_detailed').css('height',700);
                             		$("#main_right").css('height',700);
                             	}
                            }
                        }
                    });

                }
            }
        });

    });
    //将资源添加到日程中结束
    
    //点击展开行程点备注
    $("#resouce").on('click','.icon-pencil',function(){
    	$(this).parent().hide();
    	$(this).parent().parent().find(".remark").show();
    });
    //保存行程点备注
    $("#resouce").on('click','.remark .btn',function(){
    	var text=$(this).parent().find(".form-control").val();
    	var $obj=$(this).parent();
    	id=$(this).parent().parent().parent().parent().parent().attr("name");
    	 $.ajax({
             type : 'post',
             url: '../trip_source/update?id=' + id,
             dateType : 'json',
             data:"&remark="+text,
             success: function(data){
            	 if(data.state=='1'){
            		 $obj.parent().find("span").text(text);
            		 $obj.hide();
            		 $obj.parent().find(".icon-pencil").parent().show();
            	 }else{
            		 layer.alert('操作失败', {icon: 0});
            	 }
             }
    	 });
    });
    
    //日程点备注删除
    $("#resouce").on('click','.glyphicon-trash',function(){
    	 var id=$(this).parent().parent().parent().parent().parent().parent().attr("name");
    	 var index=$(this).parent().parent().parent().parent().parent().parent().index();
    	 $.ajax({
             type : 'post',
             url: '../trip_source/delete?id=' + id,
             dateType : 'json',
             success: function(data){
            	 if(data.state=='1'){
            		 var length=$(".active .obj").length;
            		 if(length==1){
            			$(".active .region_title").html("");
                     	$(".current .day_adr").html("暂无安排");
                     	$(".current .day_adr").attr("title","暂无安排");
                     	$(".active li[name='"+id+"']").remove();
            		 }else{
            			if(index==0){
            				$(".active .obj:eq(1)").find("ul").remove();
            			}
            			$(".active .obj:gt("+index+")").each(function(i){
            				var ind=$(this).index();
                             $(this).find(".index").html(ind);
                        });
        				
            			$(".active li[name='"+id+"']").remove();
            			
            			
            		 }
            		
            	 }else{
            		 layer.alert('操作失败', {icon: 0});
            	 }
             }
    	 });
    });
    
    //日程备注新增
    $("#resouce").on('focusout','.text_area .form-control',function(){
    	var $obj=$(this).parent().parent().parent();
    	var text=$(this).val();
    	var id=$obj.attr("name");
    	 $.ajax({
             type : 'post',
             url: '../trip_day/update?id=' + id,
             data:"&remark="+text,
             dateType : 'json',
             success: function(data){
            	 if(data.state=='0'){
            		 layer.alert('操作失败', {icon: 0});
            	 }
             }
    	 });
    });
    
    //日程备注新增
    $(".text_area").on('focusout','.form-control',function(){
    	var $obj=$(this).parent().parent().parent();
    	var text=$(this).val();
    	var id=$obj.attr("name");
    	 $.ajax({
             type : 'post',
             url: '../trip_day/update?id=' + id,
             dateType : 'json',
             data:"&remark="+text,
             success: function(data){
            	 if(data.state=='0'){
            		 layer.alert('操作失败', {icon: 0});
            	 }
             }
    	 });
    });
    
    //新增景区景点
    $(".resource_add").on('click','.scenery',function(){
    	var id=$(this).parent().parent().parent().parent().attr("name");
		$("#sceneryDayId").val(id);
    	 layer.open({
    		 	type: 1,
    	        zIndex :199,
    	        scrollbar: false,
    	        title:'自定义景区景点',
    	        area: '500px',
    	        content: $('#sceneryInfo'),
    	        btn: ['保存', '取消'],
    	        resize:false,
    	        yes: function(index, layer0){
    	            //按钮保存的回调
    	        	var data=$("#sceneryForm").serialize();
    	        	var name=$("#sceneryName").val();
    	        	if($.trim(name)==''){
    	        		layer.msg('请输入景区名称');
    	        		return false;
    	        	}
    	        	 $.ajax({
    	                 type : 'post',
    	                 url: '../trip_source/saveSource',
    	                 data:data,
    	                 dateType : 'json',
    	                 success: function(data){
    	                	 if(data.state=='1'){
    	                		 insertHtml(data.id,data.name,data.remark,data.type);
    	                		 $('#sceneryForm')[0].reset();
    	                		 layer.close(index);
    	                	 }else{
    	                		 layer.alert('操作失败', {icon: 0});
    	                	 }
    	                 }
    	        	 });
    	        	
    	        },
    	        cancel: function(index, layer0){
    	 			//按钮取消的回调
    	 			layer.close(index);
	 			}
    	    });
    });
    
    //新增餐饮场所
    $(".resource_add").on('click','.dining',function(){
    	var id=$(this).parent().parent().parent().parent().attr("name");
		$("#diningDayId").val(id);
    	 layer.open({
    		 	type: 1,
    	        zIndex :199,
    	        scrollbar: false,
    	        title:'自定义餐饮场所',
    	        area: '500px',
    	        content: $('#diningInfo'),
    	        btn: ['保存', '取消'],
    	        resize:false,
    	        yes: function(index, layer0){
    	            //按钮保存的回调
    	        	var name=$("#diningName").val();
    	        	if($.trim(name)==''){
    	        		layer.msg('请输入餐饮场所名称');
    	        		return false;
    	        	}
	        		var data=$("#diningForm").serialize();
       	        	$.ajax({
       	                 type : 'post',
       	                 url: '../trip_source/saveSource',
       	                 data:data,
       	                 dateType : 'json',
       	                 success: function(data){
       	                	 if(data.state=='1'){
       	                		 insertHtml(data.id,data.name,data.remark,data.type);
       	                		$('#diningForm')[0].reset();
       	                		 layer.close(index);
       	                	 }else{
       	                		 layer.alert('操作失败', {icon: 0});
       	                	 }
       	                 }
       	        	});
    	        	
    	        	
    	        	
    	        },
    	        cancel: function(index, layer0){
    	 			//按钮取消的回调
    	 			layer.close(index);
	 			}
    	    });
    });
    
    //新增宾馆酒店
    $(".resource_add").on('click','.hotel',function(){
    	var id=$(this).parent().parent().parent().parent().attr("name");
		$("#hotelDayId").val(id);
    	 layer.open({
    		 	type: 1,
    	        zIndex :199,
    	        scrollbar: false,
    	        title:'自定义宾馆酒店',
    	        area: '500px',
    	        content: $('#hotelInfo'),
    	        btn: ['保存', '取消'],
    	        resize:false,
    	        yes: function(index, layer0){
    	            //按钮保存的回调
    	        	var name=$("#hotelName").val();
    	        	if($.trim(name)==''){
    	        		layer.msg('请输入宾馆酒店名称');
    	        		return false;
    	        	}
    	        	var data=$("#hotelForm").serialize();
    	        	 $.ajax({
    	                 type : 'post',
    	                 url: '../trip_source/saveSource',
    	                 data:data,
    	                 dateType : 'json',
    	                 success: function(data){
    	                	 if(data.state=='1'){
    	                		 insertHtml(data.id,data.name,data.remark,data.type);
    	                		 $('#hotelForm')[0].reset();
    	                		 layer.close(index);
    	                	 }else{
    	                		 layer.alert('操作失败', {icon: 0});
    	                	 }
    	                 }
    	        	 });
    	        	
    	        },
    	        cancel: function(index, layer0){
    	 			//按钮取消的回调
    	 			layer.close(index);
	 			}
    	    });
    });
    
    $(".resource_add").on('click','.traffic',function(){
    	var id=$(this).parent().parent().parent().parent().attr("name");
		$("#trafficDayId").val(id);
    	 layer.open({
    		 	type: 1,
    	        zIndex :199,
    	        scrollbar: false,
    	        title:'交通工具',
    	        area: '700px',
    	        content: $('#trafficInfo'),
    	        btn: ['保存', '取消'],
    	        resize:false,
    	        yes: function(index, layer0){
    	            //按钮保存的回调
    	        	var data=$("#trafficForm").serialize();
    	        	var way=$("#way").selectpicker('val');
    	        	var fromArea=$("#fromArea").val();
    	        	var toArea=$("#toArea").val();
    	        	if(way=="" || fromArea=="" ||toArea==""){
    	        		layer.msg('请输入必填信息');
    	        		return false;
    	        	}
    	        	 $.ajax({
    	                 type : 'post',
    	                 url: '../trip_traffic/save',
    	                 data:data,
    	                 dateType : 'json',
    	                 success: function(data){
    	                	 if(data.state=='1'){
    	                		 insertHtml(data.id,data.name,data.remark,data.type);
    	                		 $('#trafficForm')[0].reset();
    	                		 layer.close(index);
    	                	 }else{
    	                		 layer.alert('操作失败', {icon: 0});
    	                	 }
    	                 }
    	        	 });
    	        	
    	        },
    	        cancel: function(index, layer0){
    	 			//按钮取消的回调
    	 			layer.close(index);
	 			}
    	    });
    });
    
    
  //完成行程
    $("#Info_wanc").on('click',function(){
    	var UEDITOR_HOME_URL = "/";
    	UE.getEditor('content');
    	UE.getEditor('remark');
        layer.open({
            type: 1,
            zIndex :20,
            scrollbar: false,
            shadeClose:true,
            btn: ['保存', '取消'],
            title:'完善行程信息',
            area: ['1100px','700px'],
            content: $('#info'),
            resize:false,
	        yes: function(index, layer0){
	            //按钮保存的回调
	        	var data=$("#submitFrom").serialize();
	        	 $.ajax({
	                 type : 'post',
	                 url: '../trip/save_detail',
	                 data:data,
	                 dateType : 'json',
	                 success: function(data){
	                	 if(data.state=='1'){
	                		 location.href = "/lastVisited.action";
	                	 }else{
	                		 layer.alert('操作失败', {icon: 0});
	                	 }
	                 }
	        	 });
	        	
	        },
	        cancel: function(index, layer0){
	 			//按钮取消的回调
	 			layer.close(index);
 			}
        });
    });
    
});

function resourcememo(item) {
    //修改备注事件
    var val = $(item).text();
    $(item).next().children("textarea").val(val);
    $(item).hide();
    $(item).next().show();
}
//备注确认按钮事件
function resourcememook(item) {
    var val = $(item).prev("textarea").val();
    $(item).parent().prev().text(val);
    $(item).parent().hide();
    $(item).parent().prev().show();
}

var sel_region;
var url; // 资源列表跳转url
function changeRegion(){
    getList();
}
function getList(){
	 var region=$("#endAddress").val();
	    sel_region=region;
   
    addResource(sel_region,{
        page: 1,
        rows: 10
    });

}

// 添加资源列表
function addResource(regionId, params) {
	
    var search = $("#inputId").val(); // 获取搜索输入框的值
    function renderResource(option) {
    	 var type=$("#btn_type .btn-primary").attr("resourceType");
    	    if (type === "scenery_list") {
    	        url = '/scenic/'+type+'.action';
/*    	    } else if (type === "hotel_list") {
    	        url = '/business_hotel/'+type+'.action';
    	    } else if (type === "dinning_list") {
    	        url = '/restaurant/'+type+'.action';*/
    	    }
        $.ajax({
            type : 'post',
            url: url + '?region=' + regionId + '&page=' + option.page + '&rows=' + option.rows + '&name=' + search,
            dateType : 'json',
            success: function(data){
                var str = '';
                $("#scenic_list").empty();
                $.each(data.rows, function(i, n){
                    var picture=n.picture;
                    if(n.picture==null || n.picture==""){
                        picture="/images/default.png";
                    }
                    str ="<li><div class='col-sm-4 list_item'><div class='item_img'><img src='"+ picture+"'/></div>" +
                        "<div class='item_title'><p class='item_name'>"+ n.name +"</p>" +

                        "<p class='item_phone'>联系电话:"+ n.phone +"</p>" +
                        "<span class='add-to-plan'><i class='glyphicon glyphicon-plus' data-size='"+ n.id+"'  title='加入行程'></i></span> </div> </div></li>";
                    $("#scenic_list").append(str);
                    tools.pageTurn({
                        selector: '.pagination',
                        total:Math.ceil(data.total/params.rows),
                        currPage: option.page,
                        limits: option.rows,
                        click: renderResource,
                        params: params
                    });
                });
            }
        })
    }

    renderResource({
        page: params.page,
        rows: params.rows
    });
}



function insertHtml(id,name,remark,type){
	var len=$(".menu_detailed .active").find("li[class='obj']").length+1;
	var length= $(".menu_detailed .active").find("li").length;
	if(length==0){
        var html='<li  class="obj" name="'+id+'" cl="'+type+'">';
        html+='<div class="cls_resouce">';
        html+='<div class="resouce_content">';
        html+='<div>';
        html+='<div class="resouce_img">';
        html+='<img src="/images/default.png"/>';
        html+='<span class="index">'+len+'</span>';
        html+='</div>';
        html+='<div class="sub_content">';
        html+='<p class="remove-to-plan"><i class="glyphicon glyphicon-trash"  title="删除"></i></p>';
        html+='<p class="resouce_name">'+name+'</p>';
        html+='<p class="resouce_address"></p>';
        html+='</div>';
        html+='</div>';
        html+='<div class="sub_remark">';
        html+='<p><span>'+remark+'</span><i class="icon-pencil"></i></p>';
        html+='<p class="remark" style="display:none">';
        html+='<textarea class="form-control" rows="3">'+remark+'</textarea>';
        html+='<button type="button" class="btn btn-primary btn-xs">确认</button>';
        html+='</p>';
        html+='</div>';
        html+='</div>';
        html+='</div>';
        html+='</li>';
	}else{
		
		var html='<li class="obj" name="'+id+'"  cl="'+type+'">';
		html+='<ul>';
		html+='<li>';
		html+='<div class="traffic-type">';
		html+='<div class="resouce_content">';
		html+='<div>';
		html+='<p class="resouce_address">暂无交通方案</p>';
		html+='</div>';
		html+='</div>';
		html+='<div>';
		html+='</div>';
		html+='</div>';
		html+='</li>';
		html+='</ul>';
        html+='<div class="cls_resouce">';
        html+='<div class="resouce_content">';
        html+='<div>';
        html+='<div class="resouce_img">';
        html+='<img src="/images/default.png"/>';
        html+='<span class="index">'+len+'</span>';
        html+='</div>';
        html+='<div class="sub_content">';
        html+='<p class="remove-to-plan"><i class="glyphicon glyphicon-trash"  title="删除"></i></p>';
        html+='<p class="resouce_name">'+name+'</p>';
        html+='<p class="resouce_address"></p>';
        html+='</div>';
        html+='</div>';
        html+='<div class="sub_remark">';
        html+='<p><span>'+remark+'</span><i class="icon-pencil"></i></p>';
        html+='<p class="remark" style="display:none">';
        html+='<textarea class="form-control" rows="3">'+remark+'</textarea>';
        html+='<button type="button" class="btn btn-primary btn-xs">确认</button>';
        html+='</p>';
        html+='</div>';
        html+='</div>';
        html+='</div>';
        html+='</li>';
	}
	$(".menu_detailed .active").find(".source").append(html);
    var H1=$('.menu_day').height();
    var H2=$('#resouce').height();
    var H= H1>H2?H1:H2;
 	if(H>700){
 		$('.menu_detailed').css('height',H+200);
 		$("#main_right").css('height',H+200);
 	}else{
 		$('.menu_detailed').css('height',700);
 		$("#main_right").css('height',700);
 	}
}

