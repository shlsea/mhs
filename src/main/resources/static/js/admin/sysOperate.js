$().ready(
		function() {

			// 列表查询按钮
			var $searchBtn = $("#btn_search");

			// 新增按钮
			var $addBtn = $('#addBtn');

			// 编辑按钮
			var $editBtn = $('#editBtn');

			// 查看按钮
			var $viewBtn = $('#viewBtn');

			// 用户新闻栏目授权
			var $wrenchBtn = $('#wrenchBtn');

			//发布
			var $repeatBtn = $("#repeatBtn");

			// 删除按钮
			var $removeBtn = $('#removeBtn');
			// 审核按钮(新闻)
			var $checkBtn = $('#checkBtn');
			// 待审核按钮(新闻)
			var $pendingBtn = $('#pendingBtn');
			// 撤销按钮
			var $trashBtn = $('#trashBtn');
			// 调整评分
			var $adjustBtn = $('#adjustBtn');
			// 撤销
			var $replyBtn = $('#replyBtn');

			/*******************************************************************
			 * 列表查询方法
			 */
			$searchBtn.click(function() {
				searchForm();
			});

			/*******************************************************************
			 * 新增方法
			 */
			$addBtn.click(function() {
				// 获取地址
				var url = $(this).attr("name");
				location.href = url;
			});

			/*******************************************************************
			 * 编辑方法
			 */
			$editBtn.click(function() {
				// 获取地址
				var url = $(this).attr("name");
				// 获取选中行
				var selectRow = getListSelect();
				// 获取选中行的id
				var selectDataId = $.map(selectRow, function(row) {
					return row.id;
				});

				if (selectRow.length != 1) {
					layer.open({
						title : "温馨提示",
						content : "请选中一项数据"
					});
				} else {
					location.href = url + '&id=' + selectDataId;
				}
			});

			/**
			 * 查看方法
			 */
			$viewBtn.click(function() {
				// 获取地址
				var url = $(this).attr("name");
				// 获取选中行
				var selectRow = getListSelect();
				// 获取选中行的id
				var selectDataId = $.map(selectRow, function(row) {
					return row.id;
				});

				if (selectRow.length != 1) {
					layer.open({
						title : "温馨提示",
						content : "请选中一项数据"
					});
				} else {
					location.href = url + '&id=' + selectDataId;
				}
			});

			/*******************************************************************
			 * 用户新闻栏目授权
			 */
			$wrenchBtn.click(function() {
				// 获取地址
				var url = $(this).attr("name");
				// 获取选中行
				var selectRow = getListSelect();
				// 获取选中行的id
				var selectDataId = $.map(selectRow, function(row) {
					return row.id;
				});

				if (selectRow.length != 1) {
					layer.open({
						title : "温馨提示",
						content : "请选中一项数据"
					});
				} else {
					location.href = url + '&id=' + selectDataId;
				}
			});
			
			/**
			 * 发布
			 */
			$repeatBtn.click(function() {
				// 获取地址
				var url = $(this).attr("name");
				// 获取选中行
				var selectRow = getListSelect();
				// 获取选中行的id
				var selectDataId = $.map(selectRow, function(row) {
					return row.id;
				});
				if (selectRow.length == 0) {
					layer.open({
						title : "温馨提示",
						content : "请选择所要发布数据"
					});
				} else {
					layer.confirm('确定发布选中<font color="red">' + selectRow.length
							+ '</font>条数据？', {
						btn : [ '确定', '取消' ],
						shade : false
					// 不显示遮罩
					}, function(index) {
						$.ajax({
							type : 'post',
							url : url + '&ids=' + selectDataId,
							dateType : 'json',
							success : function(data) {
								layer.open({
									title : "温馨提示",
									content : data.msg
								});
								searchForm();
							}
						});
						layer.close(index);
					});
				}
			});
			/**
			 * 撤销数据
			 */
			$replyBtn.click(function() {
				// 获取地址
				var url = $(this).attr("name");
				// 获取选中行
				var selectRow = getListSelect();
				// 获取选中行的id
				var selectDataId = $.map(selectRow, function(row) {
					return row.id;
				});
				if (selectRow.length == 0) {
					layer.open({
						title : "温馨提示",
						content : "请选择所要撤销数据"
					});
				} else {
					layer.confirm('确定撤销选中<font color="red">' + selectRow.length
							+ '</font>条数据？', {
						btn : [ '确定', '取消' ],
						shade : false
					// 不显示遮罩
					}, function(index) {
						$.ajax({
							type : 'post',
							url : url + '&ids=' + selectDataId,
							dateType : 'json',
							success : function(data) {
								layer.open({
									title : "温馨提示",
									content : data.msg
								});
								searchForm();
							}
						});
						layer.close(index);
					});
				}
			});

			/*******************************************************************
			 * 删除方法
			 */
			$removeBtn.click(function() {
				// 获取地址
				var url = $(this).attr("name");
				// 获取选中行
				var selectRow = getListSelect();
				// 获取选中行的id
				var selectDataId = $.map(selectRow, function(row) {
					return row.id;
				});
				if (selectRow.length == 0) {
					layer.open({
						title : "温馨提示",
						content : "请选择所要删除数据"
					});
				} else {
					layer.confirm('确定删除选中<font color="red">' + selectRow.length
							+ '</font>条数据？', {
						btn : [ '确定', '取消' ],
						shade : false
					// 不显示遮罩
					}, function(index) {
						$.ajax({
							type : 'post',
							url : url + '&ids=' + selectDataId,
							dateType : 'json',
							success : function(data) {
								layer.open({
									title : "温馨提示",
									content : data.msg
								});
								searchForm();
							}
						});
						layer.close(index);
					});
				}
			});

			/**
			 * 撤销
			 */
			$trashBtn.click(function() {
				// 获取地址
				var url = $(this).attr("name");
				// 获取选中行
				var selectRow = getListSelect();
				// 获取选中行的id
				var selectDataId = $.map(selectRow, function(row) {
					return row.id;
				});
				if (selectRow.length == 0) {
					layer.open({
						title : "温馨提示",
						content : "请选择所要撤销数据"
					});
				} else {
					layer.confirm('确定撤销选中<font color="red">' + selectRow.length
							+ '</font>条数据？', {
						btn : [ '确定', '取消' ],
						shade : false
					// 不显示遮罩
					}, function(index) {
						$.ajax({
							type : 'post',
							url : url + '&ids=' + selectDataId,
							dateType : 'json',
							success : function(data) {
								layer.open({
									title : "温馨提示",
									content : data.msg
								});
								searchForm();
							}
						});
						layer.close(index);
					});
				}
			});

			/**
			 * 调整评分
			 */
			$adjustBtn.click(function() {
				// 获取地址
				var url = $(this).attr("name");
				// 获取选中行
				var selectRow = getListSelect();
				// 获取选中行的id
				var selectDataId = $.map(selectRow, function(row) {
					return row.id;
				});

				if (selectRow.length != 1) {
					layer.open({
						title : "温馨提示",
						content : "请选中一项数据"
					});
				} else {
					location.href = url + '&id=' + selectDataId;
				}

			});
			/*******************************************************************
			 * 审核通过(新闻)
			 */
			$checkBtn.click(function() {
				// 获取地址
				var url = $(this).attr("name");
				// 获取选中行
				var selectRow = getListSelect();
				// 获取选中行的id
				var selectDataId = $.map(selectRow, function(row) {
					return row.id;
				});
				if (selectRow.length == 0) {
					layer.open({
						title : "温馨提示",
						content : "请选择所要审核数据"
					});
				} else {
					layer.confirm('确定审核通过选中<font color="red">'
							+ selectRow.length + '</font>条数据？', {
						btn : [ '确定', '取消' ],
						shade : false
					// 不显示遮罩
					}, function(index) {
						$.ajax({
							type : 'post',
							url : url + '&ids=' + selectDataId,
							dateType : 'json',
							success : function(data) {
								layer.open({
									title : "温馨提示",
									content : data.msg
								});
								searchForm();
							}
						});
						layer.close(index);
					});
				}
			});

			/*******************************************************************
			 * 待审(新闻)
			 */
			$pendingBtn.click(function() {
				// 获取地址
				var url = $(this).attr("name");
				// 获取选中行
				var selectRow = getListSelect();
				// 获取选中行的id
				var selectDataId = $.map(selectRow, function(row) {
					return row.id;
				});
				if (selectRow.length == 0) {
					layer.open({
						title : "温馨提示",
						content : "请选择所要待审数据"
					});
				} else {
					layer.confirm('确定待审选中<font color="red">' + selectRow.length
							+ '</font>条数据？', {
						btn : [ '确定', '取消' ],
						shade : false
					// 不显示遮罩
					}, function(index) {
						$.ajax({
							type : 'post',
							url : url + '&ids=' + selectDataId,
							dateType : 'json',
							success : function(data) {
								layer.open({
									title : "温馨提示",
									content : data.msg
								});
								searchForm();
							}
						});
						layer.close(index);
					});
				}
			});

		});

function getListSelect(tableid) {
	if (tableid) {
		return $('#' + tableid).bootstrapTable('getSelections');
	} else {
		return $('#listTable').bootstrapTable('getSelections');
	}
}

function formatCSTDate(strDate, format) {
	return formatDate(new Date(strDate), format);
}

// 格式化日期,
function formatDate(date, format) {
	var paddNum = function(num) {
		num += "";
		return num.replace(/^(\d)$/, "0$1");
	}
	// 指定格式字符
	var cfg = {
		yyyy : date.getFullYear() // 年 : 4位
		,
		yy : date.getFullYear().toString().substring(2)// 年 : 2位
		,
		M : date.getMonth() + 1 // 月 : 如果1位的时候不补0
		,
		MM : paddNum(date.getMonth() + 1) // 月 : 如果1位的时候补0
		,
		d : date.getDate() // 日 : 如果1位的时候不补0
		,
		dd : paddNum(date.getDate())// 日 : 如果1位的时候补0
		,
		hh : paddNum(date.getHours()) // 时
		,
		h : date.getHours() // 时
		,
		mm : paddNum(date.getMinutes()) // 时
		,
		m : date.getMinutes() // 分
		,
		ss : paddNum(date.getSeconds()) // 时
		,
		s : date.getSeconds()
	// 秒
	}
	format || (format = "yyyy-MM-dd hh:mm:ss");
	return format.replace(/([a-z])(\1)*/ig, function(m) {
		return cfg[m];
	});
}