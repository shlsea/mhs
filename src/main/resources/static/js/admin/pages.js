/**
 * Created by chengwb on 2016/11/8.
 */
(function(window, $, undefined){
    window.tools = window.tools || {};

    /**
     * 翻页；可以使用，待后续优化；
     * @param option
     * {
     *  selector: 翻页的容器选择器，默认值为'.page'
     *  total: 总页数
     *  currPage: 当前页数
     *  homeClass: 首页按钮样式class，默认为'first_page'
     *  lastClass: 尾页按钮样式class，默认为'last_page'
     *  nextClass: 下一页按钮样式class，默认为'page_down'
     *  prevClass: 上一页按钮样式class，默认为'page_up'
     *  click: 页码点击事件处理方法
     *  params: 点击事件处理方法的参数
     * }
     */
    window.tools.pageTurn = function(option) {
        var totalPages = option.total;
        var currPage = option.currPage;
        //var homeClass = option.homeClass || 'first_page';
        //var lastClass = option.lastClass || 'last_page';
        var nextClass = option.nextClass || 'page_down';
        var prevClass = option.prevClass || 'page_up';
        var container = option.selector || '.page';
        var params = option.params || {};
        window.pageTurnClick = window.pageTurnClick || option.click;

        if(!totalPages || !currPage || totalPages <= 0 || currPage <= 0){
            return;
        }

        if(currPage > totalPages) {
            currPage = totalPages;
        }

        var html = '';
        var home = currPage === 1 ? 0 : 1;
        var prev = currPage === 1 ? 0 : currPage - 1;
        var last = currPage === totalPages ? 0 : totalPages;
        var next = currPage === totalPages ? 0 : currPage + 1;
        var limits = option.limits || 3;//单边限制，设置为n则最多显示2n-1项
        var pageNum = 0;
        var i = 0;

        //限制1-5，默认为3
        if(limits <= 0) {
            limits = 1;
        } else if(limits > 5) {
            limits = 5;
        }

        //生成onclick字符串
        function isClick(page) {
            if(page && page !== currPage) {
                params.page = page;
                return 'onclick="pageTurnClick(' + JSON.stringify(params).replace(/"/g, '\'')  + ')"';
            } else {
                params.page = 0;
                return '';
            }
        }

        //翻页的首部
        if (currPage === 1) {
            //html += '<a class= disabled' + isClick(home) + '>首页</a>';
            html += '<li class="disabled"><a>上一页</a></li>';
        } else {
            //html += '<a class="' + homeClass + '"' + isClick(home) + '>首页</a>';
            html += '<li><a class="' + prevClass + '" ' + isClick(prev) + '>上一页</a></li>';
        }


        //翻页中间部分处理
        if(totalPages >= limits * 2 - 1) {//总页数大于等于总显示数时
            if (currPage - limits > 0) {
                html += '<li><a class="no_interact">...</a></li>';
            }

            if(totalPages - limits >= currPage) {//
                var counts = 0;
                for (i = limits - 1; i >= 0; i--) {
                    pageNum = currPage - i;
                    if (pageNum <= 0) {
                        counts++;
                        html += '<li class=" ' + (counts === currPage ? 'active' : '') + ' "><a ' + isClick(counts) + '>' + counts + '</a></li>';
                    } else {
                        html += '<li class=" ' + (pageNum + counts === currPage ? 'active' : '') + ' "><a ' + isClick(pageNum + counts) + '>' + (pageNum + counts) + '</a></li>';
                    }
                }

                for (i = 1; i < limits; i++) {
                    pageNum = (counts === 0 ? currPage + i : limits + i);
                    if (pageNum > totalPages) {
                        break;
                    } else {
                        html += '<li class=" ' + (counts === currPage ? 'active' : '') + ' "><a ' + isClick(pageNum) + '>' + pageNum + '</a></li>';
                    }
                }
                if (counts + currPage + limits <= totalPages) {
                    html += '<li><a class="no_interact">...</a></li>';
                }
            } else {
                for (i = totalPages - (limits * 2 - 2); i <= totalPages; i++) {
                    html += '<li class=" ' + (currPage === i ? 'active' : '') + ' "><a ' + isClick(i) + '>' + i + '</a></li>';
                }
            }
        } else {//总页数小于总显示数时
            for (i = 1; i <= totalPages; i++) {
                html += '<li class=" ' + (currPage === i ? 'active' : '') + ' "><a ' + isClick(i) + '>' + i + '</a></li>';
            }
        }

        //翻页尾部
        if (currPage == totalPages) {
            html += '<li class="disabled"><a' + isClick(next) + '>下一页</a></li>';
            //html += '<a class= disabled' + isClick(last) + '>末页</a>';
        } else {
            html += '<li><a class="' + nextClass + '"' + isClick(next) + '>下一页</a></li>';
            //html += '<a class="' + lastClass + '"' + isClick(last) + '>末页</a>';
        }

        $(container).html(html);
    };
})(window, jQuery);