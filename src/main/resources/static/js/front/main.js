/**
 * Created by zzx on 2017-9-6.
 */

function screen(){
    var _H = $(window).height();
    var _w = $(window).width();
    $('.index_bannerBox,.mark_box').css('height',_H);
    $(window).resize(function(){
        var _H = $(window).height();
        var _w = $(window).width();
        $('.index_bannerBox,.mark_box').css('height',_H);

    });
}
$(function(){
     screen();
     //load 页面加载
     $('#header-box').load('header.html');
     $('#footerBox').load('footer.html');
     //二三级左边菜单高度
    var sideLeft_H = $('.content_sideRight').height()
    $('.content_sideLeft_li').css('height',sideLeft_H)

    $('.content_sideLeft_li li').click(function(){
        $(this).addClass('curr').siblings().removeClass('curr');
    })

    //管理中心
    $('.conrtol_tab_ul li').click(function(){
        var index = $(this).index();
        $(this).addClass('curr').siblings().removeClass('curr');
        $('.conrtolBox .tab_code_ts').eq(index).show().siblings().hide();
    })
    //接口
    $('.control_box_qx_list li').click(function(){
        var index = $(this).index();
        $(this).addClass('curr').siblings().removeClass('curr');
        $('#control_box_qxgl .control_box_qx').eq(index).show().siblings('.control_box_qx').hide()
    })
});

//登录注册关闭按钮
function boxClose(){
    $('#loginDiv,#registerDiv,.mark_box,.find_box,.remove_box,#Create_web').hide();
}
//登录
function login(){
    $('#loginDiv,.mark_box').show();
}
//注册
function regBox(){
    $('#registerDiv,.mark_box').show();
     $('#loginDiv').hide();
}
//找回密码
function findBox(){
    $('.find_box').show();
    $('#loginDiv').hide();
}

//删除确认
function removeBox(){
    $('.remove_box,.mark_box').show();
}

//平台创建应用
function Create_web(){
    $('#Create_web,.mark_box').show();
}
