/*!
 * Regionpicker v1.0.0
 * 
 * Copyright (c) 2017 Yuds
 * 功能需求：地区联动效果插件，
 * 1、动态读取表数据；
 * 2、控制读取地址；
 * 3、可控制读取层级；
 * 4、可控制下拉是否可选（只读）。
 * Date: 2017-02-22
 */
(function ($) {
	$.fn.extend({
        "regionPicker": function (options) {
            //检测用户传进来的参数是否合法
        	
            if (!isValid(options))
                return this;
          //使用jQuery.extend 覆盖插件默认参数
            var opts = $.extend({}, defaluts, options);
            var selects=$(this).find("select");
            var value=getDefalutValue(options);
            //创建存值hidden隐藏域
            $(this).append('<input type="hidden" name="'+options.vid+'" id="'+options.vid+'" value="'+value+'"/>')
            return init(options,selects);

        }
    });
	
    //默认参数
    var defaluts = {
    	vid: 'region',
        pid: '100000',
        url:'',
        defalut:null,
        eid:'eid',
        display:true
    };
   
    //私有方法，检测参数是否合法
    function isValid(options) {
        return !options || (options && typeof options === "object") ? true : false;
    }
    
    function init(options,selects){
    	getList(options,selects);
    }
    //初始化下拉选择并为第一个选择赋值
    function getList(options,selects){
    	var value=getDefalutValue(options);
    	var v_=$("#"+options.eid).val();
    	var v_array=new Array();
    	if(v_!=null){
    		v_array=v_.split(',');
    	}
    	$.each(selects,function (i,n){ 
    		//初始化bootstrap下拉插件
			$(this).selectpicker({
	         		maxOptions: 1,
	         		noneSelectedText:'请选择',
	         		width:150,
	         		liveSearch:true,
	         		size:6,
	         		showTick: true
			 });
			//为第一个下拉选择添加选项
			if(v_.length>0){
				appendOption($(n),options.url,v_array[i],options.eid);
			}else{
				if(i==0){
					appendOption($(n),options.url,options.pid,options.eid);
				}
			}
			
			//绑定change事件
    		 $(n).change(function(){
    			var pid= $(n).selectpicker('val');
    			appendOption($(selects[i + 1]),options.url,pid,options.eid);
    			if(pid==''||pid==null){
    				if(i>0){
    					pid=$(selects[i-1]).selectpicker('val');
    				}else{
    					pid=getDefalutValue(options);
    				}
    				
    			}
    			for(var j=i+2;j<selects.length;j++){
					appendOption($(selects[j]),options.url,0,options.eid);
				}
    			$("#"+options.vid).val(pid);
    		 });
        });
    	
    }
    
    //为下拉选择添加选项
    function appendOption(dom,url,pid,eid){
    	 $.ajax({
			   type: "post",
			   url: url,
			   data: "pid="+pid,
			   dateType : 'json',
			   success: function(data){
				 var html="<option value=''>请选择</option>";
				   if(data.length>0){
					   var value=$("#"+eid).val();
					   $.each(data,function(index,item){
						   var sel="";
						   if(value!=null && value.indexOf(item.id)>-1){
							   sel="selected='selected'";
						   }
						  html+="<option "+sel+" value='"+item.id+"' >"+item.name+"</option>"; 
					   });
				   }
				   //清空之前下拉选项
				   dom.empty();
				   //添加新的选项
				   dom.append(html);
				   dom.selectpicker('render');
				   dom.selectpicker('refresh');
				 
			   }
		 });
    }
    
    //获取初始化给定的默认值
   function getDefalutValue(options){
	   var value=options.defalut;
	   	if(options.defalut==''|| options.defalut=='undefined'|| options.defalut==null){
	   		value=options.pid;
	   	}
	   	return value;
   }
})(window.jQuery);