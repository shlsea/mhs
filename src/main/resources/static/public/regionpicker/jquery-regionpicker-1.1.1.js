/*!
 * Regionpicker v1.1.1
 * 
 * Copyright (c) 2017 Yuds
 * 功能需求：地区联动效果插件，
 * 1、动态读取表数据；
 * 2、控制读取地址；
 * 3、可控制读取层级；
 * 4、可控制下拉是否可选（只读）。
 * Date: 2017-04-21
 */
(function($) {
	$.fn.extend({
		"regionPicker" : function(options) {
			// 检测用户传进来的参数是否合法

			if (!isValid(options))
				return this;
			// 使用jQuery.extend 覆盖插件默认参数
			var opts = $.extend({}, defaluts, options);
			var selects = $(this).find("select");
			var value = getDefalutRegion(options);
			// 创建存值hidden隐藏域
			$(this).append('<input type="hidden" name="' + options.vid + '" id="'+ options.vid + '" value="' + value + '"/>')
			return init(options, selects);

		}
	});

	// 默认参数
	var defaluts = {
		vid : 'region',//保存值隐藏域ID
		pid : '100000',//查询最顶级父类ID
		url : '',//查询地址
		defalut : 'ddd',//默认地区编码排序（如：100000,510000,510100）
		eid : 'eid',//默认值地区排序（如：100000,510000,510100）
		display : false,//是否显示只读
		settings:{//bootstrap下拉控制参数
			maxOptions : 1,
			width : 150,
			liveSearch : true,
			size : 6,
			showTick : true
		}
	};

	// 私有方法，检测参数是否合法
	function isValid(options) {
		return !options || (options && typeof options === "object") ? true: false;
	}

	function init(options, selects) {
		getList(options, selects);
	}
	// 初始化下拉选择并为第一个选择赋值
	function getList(options, selects) {
		var value = getDefalutRegion(options);
		var v_ = $("#" + options.vid).val();
		var v_array = getDefalutValue(options);
		
		$.each(selects, function(i, n) {
			// 初始化bootstrap下拉插件
			$(this).selectpicker(defaluts.settings);
			// 为第一个下拉选择添加选项
			if (i == 0) {
				appendOption($(n), options.url, options.pid, options);
			}else{
				appendOption($(n), options.url, v_array[i], options);
			}
			
				
			

			// 绑定change事件
			$(n).change(function() {
				var pid = $(n).selectpicker('val');
				appendOption($(selects[i + 1]), options.url, pid, options);
				if (pid == '' || pid == null) {
					if (i > 0) {
						pid = $(selects[i - 1]).selectpicker('val');
					} else {
						pid = getDefalutValue(options);
					}

				}
				for (var j = i + 2; j < selects.length; j++) {
					appendOption($(selects[j]), options.url, 0, options);
				}
				$("#" + options.vid).val(pid);
			});
		});

	}

	// 为下拉选择添加选项
	function appendOption(dom, url, pid, options) {
		$.ajax({
			type : "post",
			url : url,
			data : "pid=" + pid,
			dateType : 'json',
			success : function(data) {
				var html = "<option value=''>请选择</option>";
				if (data.length > 0) {
					var value = $("#" + options.eid).val();
					if (value == "" || value == 'undefined' || value == null) {
						value = $("#" + options.defalut).val();
					}
					
					$.each(data, function(index, item) {
						var sel = "";
						
						if (value != null && value.indexOf(item.id) > -1) {
							sel = "selected='selected'";
						}
						html += "<option " + sel + " value='" + item.id + "' >"
								+ item.name + "</option>";
					});
				}
				// 清空之前下拉选项
				dom.empty();
				// 添加新的选项
				dom.append(html);
				dom.selectpicker('render');
				var def = $("#" + options.defalut).val();
				if (options.display) {
					var value = dom.selectpicker('val');
					if (value!="" && def.indexOf(value) > -1) {
						dom.prop('disabled', true);
					}
				}

				dom.selectpicker('refresh');
				// dom.selectpicker('toggle');

			}
		});
	}

	// 获取初始化给定的默认值
	function getDefalutValue(options) {
		var value_array = new Array();
		var def_array = new Array();
		var eid_array = new Array();
		var defalutValue=$("#"+options.defalut).val();
		if(defalutValue!=''&& defalutValue!='undefined'&& defalutValue!=null){
			def_array=defalutValue.split(',');
	   	}
		
		var eidValue=$("#"+options.eid).val();
		if(eidValue!=''&& eidValue!='undefined'&& eidValue!=null){
			eid_array=eidValue.split(',');
	   	}
		if(def_array.length>0 || eid_array.length>0){
			if(def_array.length>eid_array.length){
				value_array =def_array;
			}else{
				value_array =eid_array;
			}
		}
		
		return value_array;
	}
	
	function getDefalutRegion(options){
		var value ;
		var def_array = new Array();
		var eid_array = new Array();
		var defalutValue=$("#"+options.defalut).val();
		if(defalutValue!=''&& defalutValue!='undefined'&& defalutValue!=null){
			def_array=defalutValue.split(',');
	   	}
		
		var eidValue=$("#"+options.eid).val();
		if(eidValue!=''&& eidValue!='undefined'&& eidValue!=null){
			eid_array=eidValue.split(',');
	   	}
		if(def_array.length>0 || eid_array.length>0){
			if(def_array.length>eid_array.length){
				value =def_array[def_array.length-1];
			}else{
				value =eid_array[eid_array.length-1];
			}
		}else{
			value = options.pid;
		}
		
		return value;
	}

})(window.jQuery);