﻿/**
 * jQuery EasyUI 1.3.4 Copyright (c) 2009-2013 www.jeasyui.com. All rights
 * reserved.
 * 
 * Licensed under the GPL or commercial licenses To use it on other terms please
 * contact author: info@jeasyui.com http://www.gnu.org/licenses/gpl.txt
 * http://www.jeasyui.com/license_commercial.ep
 * 
 * jQuery EasyUI validatebox Extensions 1.0 beta jQuery EasyUI validatebox 组件扩展
 * jeasyui.extensions.validatebox.js 二次开发 流云 最近更新：2013-09-07
 * 
 * 依赖项： 1、jquery.jdirk.js v1.0 beta late 2、jeasyui.extensions.js v1.0 beta late
 * 
 * Copyright (c) 2013 ChenJianwei personal All rights reserved.
 * http://www.chenjianwei.org
 */

/*
 * 功能说明：
 */
(function ($, undefined) {


    $.fn.validatebox.extensions = {};


    var rules = {
        // 只允许输入英文字母或数字
        engNum: {
            validator: function (value) {
                return /^[0-9a-zA-Z]*$/.test(value);
            },
            message: '请输入英文字母或数字'
        },
        //单选按钮验证必选
        requireRadio: {  
            validator: function(value, param){  
                return $(param[0] + ':checked').val() != undefined;
            },  
            message: '{1}'  
        },
		engName: {
            validator: function (value) {
                return /^[0-9a-zA-Z_-]*$/.test(value);
            },
            message: '请输入英文字母、数字或下划线'
        },
        // 只允许汉字、英文字母或数字
        chsEngNum: {
            validator: function (value, param) {
                return /^([\u4E00-\uFA29]|[\uE7C7-\uE7F3]|[a-zA-Z0-9])*$/.test(value);
            },
            message: '只允许汉字、英文字母或数字。'
        },

        phone: {
            validator: function (value, param) {
                var length = value.length;
                return length == 11 && /^(((13[0-9]{1})|(15[0-9]{1})|(14[0-9]{1})|(18[0-9]{1}))+\d{8})$/
                        .test(value);
            },
            message: '手机号码格式错误。'
        },

        // 只允许汉字、英文字母、数字及下划线
        code: {
            validator: function (value, param) {
                return /^[\u0391-\uFFE5\w]+$/.test(value);
            },
            message: '只允许汉字、英文字母、数字及下划线.'
        },
        // 验证是否为合法的用户名
        name: {
            validator: function (value) { return value.isUserName(); },
            message: "用户名不合法(字母开头，允许6-16字节，允许字母数字下划线)"
        },
        // 指定字符最小长度
        minLength: {
            validator: function (value, param) { return rules.length.validator(value, [param[0]]); },
            message: "最少输入 {0} 个字符."
        },
        // 指定字符最大长度
        maxLength: {
            validator: function (value, param) { return rules.length.validator(value, [0, param[0]]); },
            message: "最多输入 {0} 个字符."
        },
        // 指定字符的长度范围
        length: {
            validator: function (value, param) {
                var len = $.trim(value).length;
                var min = param[0], max = param[1];
                return (!min || len >= min) && (!max || len <= max);
            },
            message: "输入内容长度必须介于 {0} 和 {1} 个字符数之间."
        },
        // 必须包含指定的内容
        contains: {
            validator: function (value, param) { return $.string.contains(value, param[0]); },
            message: "输入的内容必须包含 {0}."
        },
        // 以指定的字符开头
        startsWith: {
            validator: function (value, param) { return $.string.startsWith(value, param[0]); },
            message: "输入的内容必须以 {0} 作为起始字符."
        },
        // 以指定的字符结束
        endsWith: {
            validator: function (value, param) { return $.string.endsWith(value, param[0]); },
            message: "输入的内容必须以 {0} 作为起始字符."
        },
        // 长日期时间(yyyy-MM-dd hh:mm:ss)格式
        longDate: {
            validator: function (value) { return $.string.isLongDate(value); },
            message: "输入的内容必须是长日期时间(yyyy-MM-dd hh:mm:ss)格式."
        },
        // 短日期(yyyy-MM-dd)格式
        shortDate: {
            validator: function (value) { return $.string.isShortDate(value); },
            message: "输入的内容必须是短日期(yyyy-MM-dd)格式."
        },
        // 长日期时间(yyyy-MM-dd hh:mm:ss)或短日期(yyyy-MM-dd)格式
        date: {
            validator: function (value) { return $.string.isDate(value); },
            message: "输入的内容必须是长日期时间(yyyy-MM-dd hh:mm:ss)或短日期(yyyy-MM-dd)格式."
        },
        // 电话号码(中国)格式
        tel: {
            validator: function (value) { return $.string.isTel(value); },
            message: "输入的内容必须是电话号码(中国)格式."
        },
        // 移动电话号码(中国)格式
        mobile: {
            validator: function (value) { return $.string.isMobile(value); },
            message: "输入的内容必须是移动电话号码(中国)格式."
        },
        // 电话号码(中国)或移动电话号码(中国)格式
        telOrMobile: {
            validator: function (value) { return $.string.isTelOrMobile(value); },
            message: "输入的内容必须是电话号码(中国)或移动电话号码(中国)格式."
        },
        // 传真号码(中国)格式
        fax: {
            validator: function (value) { return $.string.isFax(value); },
            message: "输入的内容必须是传真号码(中国)格式."
        },
        // 电子邮箱(Email)地址格式
        email: {
            validator: function (value) { return $.string.isEmail(value); },
            message: "输入的内容必须是电子邮箱(Email)地址格式."
        },
        // 邮政编码(中国)格式
        zipCode: {
            validator: function (value) { return $.string.isZipCode(value); },
            message: "输入的内容必须是邮政编码(中国)格式."
        },
        // 必须包含中文汉字
        existChinese: {
            validator: function (value) { return $.string.existChinese(value); },
            message: "输入的内容必须是包含中文汉字."
        },
        // 必须是纯中文汉字
        chinese: {
            validator: function (value) { return $.string.isChinese(value); },
            message: "输入的内容必须是纯中文汉字."
        },
        // 必须是纯英文字母
        english: {
            validator: function (value) { return $.string.isEnglish(value); },
            message: "输入的内容必须是纯英文字母."
        },
        // 必须是合法的文件名(不能包含字符 \\/:*?\"<>|)
        fileName: {
            validator: function (value) { return $.string.isFileName(value); },
            message: "输入的内容必须是合法的文件名(不能包含字符 \\/:*?\"<>|)."
        },
        // 必须是正确的 IP地址v4 格式
        ipv4: {
            validator: function (value) { return $.string.isIPv4(value); },
            message: "输入的内容必须是正确的 IP地址v4 格式."
        },
        // 必须是正确的 url 格式
        url: {
            validator: function (value) { return $.string.isUrl(value); },
            message: "输入的内容必须是正确的 url 格式."
        },
        // 必须是正确的 IP地址v4 或 url 格式
        ipv4url: {
            validator: function (value) { return $.string.isUrlOrIPv4(value); },
            message: "输入的内容必须是正确的 IP地址v4 或 url 格式."
        },
        // 必须是正确的货币金额(阿拉伯数字表示法)格式
        currency: {
            validator: function (value) { return $.string.isCurrency(value); },
            message: "输入的内容必须是正确的货币金额(阿拉伯数字表示法)格式."
        },
        // 必须是正确 QQ 号码格式
        qq: {
            validator: function (value) { return $.string.isQQ(value); },
            message: "输入的内容必须是正确 QQ 号码格式."
        },
        // 必须是正确 MSN 账户名格式
        msn: {
            validator: function (value) { return $.string.isMSN(value); },
            message: "输入的内容必须是正确 MSN 账户名格式."
        },
        unNormal: {
            validator: function (value) { return $.string.isUnNormal(value); },
            message: "输入的内容必须是不包含空格和非法字符Z."
        },
        // 必须是合法的汽车车牌号码格式
        carNo: {
            validator: function (value) { return $.string.isCarNo(value); },
            message: "输入的内容必须是合法的汽车车牌号码格式."
        },
        // 必须是合法的汽车发动机序列号格式
        carEngineNo: {
            validator: function (value) { return $.string.isCarEngineNo(value); },
            message: "输入的内容必须是合法的汽车发动机序列号格式."
        },
        // 必须是合法的身份证号码(中国)格式
        idCard: {
            validator: function (value) { return $.string.isIDCard(value); },
            message: "输入的内容必须是合法的身份证号码(中国)格式."
        },
        // 必须是合法的整数格式
        integer: {
            validator: function (value) { return $.string.isInteger(value); },
            message: "输入的内容必须是合法的整数格式."
        },
		// 验证非零的正整数
		integerNotZero: {
            validator: function (value) { return $.string.isInteger(value) && value > 0; },
            message: "输入的内容必须是非零的正整数."
        },
		// 输入的内容必须是大于等于0的整数
        integerNum: {
            validator: function (value) { return /^[0-9]\d*$/.test(value); },
            message: "输入的内容必须是大于等于0的整数."
        },
        //只能输入1-9999的整数
        orderList: {
            validator: function (value) {
                return /^[1-9][0-9]{0,3}$/.test(value);
            },
            message: '只能输入1-9999的整数。'
        },
        // 区号验证
        cityCode: {
            validator: function (value) { return /^0[1-9]{2,3}?$/.test(value); },
            message: "区号格式错误."
        },
        // 车牌号码验证
        carNum: {
            validator: function (value) { return /^[\u4e00-\u9fa5]{1}[A-Z]{1}[A-Z_0-9]{5}$/.test(value); },
            message: "车牌号码格式错误."
        },
        // 必须是合法的整数格式且值介于 {0} 与 {1} 之间
        integerRange: {
            validator: function (value, param) {
                return $.string.isInteger(value) && ((param[0] && value >= param[0]) && (param[1] && value <= param[1]));
            },
            message: "输入的内容必须是合法的整数格式且值介于 {0} 与 {1} 之间."
        },
		// 输入的内容必须是大于等于0的数字，保留两位小数
        moreThanZeroTwo: {
            validator: function (value) { return /^[0-9]+(.[0-9]{1,2})?$/.test(value) && value>0; },
            message: "输入的内容必须是大于0的数字，保留两位小数."
        },
        moreThanZeroFour: {
            validator: function (value) { return /^[0-9]+(.[0-9]{1,4})?$/.test(value) && value>0; },
            message: "输入的内容必须是大于0的数字，保留四位小数."
        },
        doubleNumTwo: {
            validator: function (value) { return /^[0-9]+(.[0-9]{1,2})?$/.test(value); },
            message: "输入的内容必须是大于等于0的数字，保留两位小数."
        },
		// 输入的内容必须是大于等于0的数字，最大保留传入param位小数(方便统计)
        doubleNum: {
            validator: function (value, param) { eval("var reg = /^[0-9]+(.[0-9]{1,"+ param[0] +"})?$/;"); return reg.test(value); },
            message: "输入的内容必须是大于等于0的数字，最大保留{0}位小数(方便统计)."
        },
		// 大于0的数字
		doubleNumNotZero: {
            validator: function (value, param) { eval("var reg = /^[0-9]+(.[0-9]{1,"+ param[0] +"})?$/;"); return reg.test(value) && value > 0; },
            message: "输入的内容必须是大于0的数字，最大保留{0}位小数(方便统计)."
        },
		// 必须是数字
		doubleZF: {
            validator: function (value, param) { eval("var reg = /^-?[0-9]+(.[0-9]{1,"+ param[0] +"})?$/;"); return reg.test(value); },
            message: "输入的内容必须是数字，最大保留{0}位小数(方便统计)."
        },
		// 必须是不为0的数字
		doubleZFNotZero: {
            validator: function (value, param) { eval("var reg = /^-?[0-9]+(.[0-9]{1,"+ param[0] +"})?$/;"); return reg.test(value) && value != 0; },
            message: "输入的内容必须是不为0的数字，最大保留{0}位小数(方便统计)."
        },
        // 必须是指定类型的数字格式
        numeric: {
            validator: function (value, param) { return $.string.isNumeric(value, param ? param[0] : undefined); },
            message: "输入的内容必须是指定类型的数字格式."
        },
        // 必须是指定类型的数字格式且介于 {0} 与 {1} 之间
        numericRange: {
            validator: function (value, param) {
                return $.string.isNumeric(value, param ? param[2] : undefined) && ((param[0] || value >= param[0]) && (param[1] || value <= param[1]));
            },
            message: "输入的内容必须是指定类型的数字格式且介于 {0} 与 {1} 之间."
        },
        
        //  输入的内容必须是大于等于0的整数
        integerNum: {
            validator: function (value) { return /^[0-9]\d*$/.test(value); },
            message: "输入的内容必须是大于等于0的整数."
        },
        //最大最小值
        minAndMax: {
            validator: function (value,param) { 
            	var input = $(this);
            	var min = Number(input.attr("min"));
            	var max = Number(input.attr("max"));
            	return $.string.isNumeric(value) && (value >= min) && (value <= max);
        	},
            message: "输入的内容必须是大于等于0的整数."
        },
        //  输入的内容必须是大于等于0的数字，保留两位小数
        moreThanZeroTwo: {
            validator: function (value) { return /^[0-9]+(.[0-9]{1,2})?$/.test(value) && value>0; },
            message: "输入的内容必须是大于0的数字，保留两位小数."
        },
        md:{
        	validator: function(value, param){
        		var startTime1 = $(param[0]).val();
        		return value>startTime1;
        	},
        	message: '结束时间要大于开始时间！' 
        },
        mdy:{
        	validator: function(value, param){
        		var startTime1 = $(param[0]).datebox('getValue');
        		var startTime2 = value;
        		return startTime2>=startTime1;
    		},
    		message: '结束时间要大于开始时间！' 
        },
        bedRentalNum:{
        	validator: function(value, param){
        		var a = parseInt($("#totalBed").val());
        		var b = parseInt($("#rentalbed").val());
        		return a>=b;
        		},
        		message: 'b大于a'
        },
        // 必须是正确的 颜色(#FFFFFF形式) 格式
        color: {
            validator: function (value) { return $.string.isColor(value); },
            message: "输入的内容必须是正确的 颜色(#FFFFFF形式) 格式."
        },
        // 必须是安全的密码字符(由字符和数字组成，至少 6 位)格式
        password: {
            validator: function (value) { return $.string.isSafePassword(value); },
            message: "输入的内容必须是安全的密码字符(由字符和数字组成，至少 6 位)格式."
        },
        // 经度验证，要求经度整数部分为0-180小数部分为0到4位！
        lon: {
        	 validator: function (value) {
                 return /^(((\d|[1-9]\d|1[1-7]\d|0)\.\d{0,4})|(\d|[1-9]\d|1[1-7]\d|0{1,3})|180\.0{0,4}|180)$/.test(value);
             },
             message: '经度格式错误。'
        },
        // 纬度验证，要求纬度整数部分为0-90小数部分为0到4位！
        lat: {
        	 validator: function (value) {
                 return /^([0-8]?\d{1}\.\d{0,4}|90\.0{0,4}|[0-8]?\d{1}|90)$/.test(value);
             },
             message: '纬度格式错误。'
        },
        // 输入的字符必须是指定的内容相同
        equals: {
        	validator: function(value,param){    
                return value == $(param[0]).val();    
            },  
            message: "输入的内容不匹配."
        },
        // 验证单选按钮
        requireRadio: {  
            validator: function(value, param){  
                var input = $(param[0]);
                input.off('.requireRadio').on('click.requireRadio',function(){
                    $(this).focus();
                });
                return $(param[0] + ':checked').val() != undefined;
            },  
            message: '{1}'  
        },
        // 判断按钮是否选择并且最多选择几项
        requireRadioAndTerm: {  
            validator: function(value, param){  
                var input = $(param[0]);
                input.off('.requireRadio').on('click.requireRadio',function(){
                    $(this).focus();
                });
                var length = $(param[0] + ':checked').length;
                var vLength = parseInt(param[2]);
                return $(param[0] + ':checked').val() != undefined && parseInt(length) <= vLength;
            },  
            message: '{1}'  
        },
        // 验证是否必须，排除传入字符
        requiredByValue: {
            validator: function (value, param) {
                var novalue = param[0];
                if (novalue != null && novalue != '') {
                    if (novalue == value) {
                        return true;
                    } else {
                        return false;
                    }
                }
                if (value == null || value == '') {
                    return false;
                }
                return true;
            },
            message: "必须输入！"
        },
        comboxValidate : {  
            validator : function(value,param,missingMessage) {  
                if($('#'+param).selectpicker('val')!='' && $('#'+param).selectpicker('val')!=null){  
                    return true;  
                }  
                return false;  
            },  
            message : "{1}"  
        },
        tableName: {
        	validator: function(value, param) {
        		return /^[A-Z][A-Za-z]{0,10}$/.test(value);
        	},
        	message: '分数表只能输入英文字母(首字母大写),且字符串长度小于12'
        }
    };
    $.extend($.fn.validatebox.defaults.rules, rules);







})(jQuery);